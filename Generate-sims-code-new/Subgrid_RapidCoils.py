import s4l_v1.document as document
import s4l_v1.model as model
from s4l_v1 import Vec3
import s4l_v1.simulation.emfdtd as fdtd
import s4l_v1.units as units
import numpy
import HardwarePackage as HP
reload(HP)
import SimulationPackage as SP
reload(SP)
# Start New Document
# document.New()


def CreateHardware(p_logic):
	# RFS Parameters
	rfs_radius = 675.0/2
	rfs_length = 1000.0
	rfs_depth = 0.0	
	# Create RFS	
	rfs_geo = [rfs_radius,rfs_length,rfs_depth]
	rfs = HP.CreateRFShield(rfs_geo)
	
	# Short Metal Loop Values
	rfc_width = 50
	rfc_length = 250
	rfc_descriptor = 'SC'
	T = []
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*3, -130 - 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*2, -130 - 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75, -130 - 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13, -130 - 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*3, 130 + 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*2, 130 + 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75, 130 + 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13, 130 + 7,-100)))
	# Create RFC
	rfc_geo = [rfc_width, rfc_length, rfc_descriptor, T]
	rfc_s = HP.CreateLoopCoil(rfc_geo)

	# Long Metal Loop Values
	rfc_width = 50
	rfc_length = 500
	rfc_descriptor = 'LC'
	T = []
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*3, -130 - 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*2, -130 - 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75, -130 - 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13, -130 - 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*3, 130 + 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*2, 130 + 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75, 130 + 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13, 130 + 7,-175)))
	# Create RFC
	rfc_geo = [rfc_width, rfc_length, rfc_descriptor, T]
	rfc_l = HP.CreateLoopCoil(rfc_geo)
	
	# MODELS - IS IT PHANTOM SIM?
	if p_logic == 'Yes':
		p_width = 320
		p_height = 200
		p_length = 800
		p_geo = [p_width, p_height, p_length]
		phantom = HP.CreateRectangularPhantom(p_geo)
		
	HP.CreateSensorBoxes(p_logic)	
	
def CreateSimulation(dielectric, p_logic, ElemNo, rfc_descriptor, n_periods, sim_freq, sim_bw, sim_refl, R, C1, C2, C3, C4, wire_descriptor):

	entities = model.AllEntities()
	
	#Sort Entities into type
	ShieldList = []; DielectricList = []; PortList = []; CoilList = []; PECList = [];
	LE1List = []; LE2List = []; LE3List = []; LE4List = []; LEList = []; WireList = []
	SensorList = []; CoreList = []; InsulationList = []; CurrentSensorList = []

	# WireList.append(entities['WireCore'])
	# WireList.append(entities['WireInsulation'])
	# PECList.append(entities['WireCore'])
	
	
	for i in range(0,len(entities)):
		tmp = entities[i]
		
		if p_logic == 'Yes':
			if ('Phantom' in tmp.Name):
				DielectricList.append(tmp)
			
		if ('RFS' in tmp.Name):
			ShieldList.append(tmp)
			
		if (rfc_descriptor+'Loop' in tmp.Name):
			CoilList.append(tmp)
			
		if (tmp.Name == rfc_descriptor+'LE01'+str(ElemNo)):
			LE1List.append(tmp)	
			# PortList.append(tmp)
		if (tmp.Name == rfc_descriptor+'LE02'+str(ElemNo)):
			LE2List.append(tmp)
			# PortList.append(tmp)
		if (tmp.Name == rfc_descriptor+'LE03'+str(ElemNo)):
			LE3List.append(tmp)
			# PortList.append(tmp)
		if (tmp.Name == rfc_descriptor+'LE04'+str(ElemNo)):
			LE4List.append(tmp)
			# PortList.append(tmp)
		if (rfc_descriptor+'LE' in tmp.Name):
			LEList.append(tmp)
		
		if (tmp.Name == 'A'+rfc_descriptor+str(ElemNo)):
			PortList.append(tmp)
		
		if ('Main' in tmp.Name):
			SensorList.append(tmp)
			
		if (wire_descriptor+'Insu' in tmp.Name):
			InsulationList.append(tmp)
			
		if (wire_descriptor+'Core' in tmp.Name):
			CoreList.append(tmp)
			
		if (wire_descriptor+' C Sensor' in tmp.Name):
			CurrentSensorList.append(tmp)
	
	# LEList = LE1List+LE2List+LE3List+LE4List
	PECList = CoreList+ShieldList+CoilList
	AutoList = SensorList+DielectricList

	sim = SP.BaseSettings(dielectric, ElemNo, rfc_descriptor, n_periods)
	SP.SetSensors(sim, SensorList, PortList, LE1List, LE2List, LE3List, LE4List, CurrentSensorList, sim_freq)
	SP.SetMaterials(sim, PECList, DielectricList, InsulationList)
	SP.SetBoundaries(sim, 200)
	SP.SetSources(sim, PortList, sim_freq, sim_bw, sim_refl)
	SP.SetLumpedElements(sim, LE1List, LE2List, LE3List, LE4List, R, C1, C2, C3, C4)
	SP.SetGridVoxels(sim, CoreList, InsulationList, CoilList, AutoList, ShieldList, PortList, LEList, CurrentSensorList, dielectric)
	return sim
	

# Create Hardware
p_logic = 'Yes'
CreateHardware(p_logic)

# Create Sims
Sims = []; n_periods = 20
sim_freq = 64e6; sim_bw = 100e6; sim_refl = 50;

## FOR LW3PHANT SHORT COIL
# C1 = [92, 88, 88, 92, 92, 88, 88, 92]
# C2 = [300, 300, 300, 300, 300, 300, 300, 300]
# C3 = [27, 27, 27, 27, 27, 27, 27, 27]
# C4 = [300, 300, 300, 300, 300, 300, 300, 300]

## FOR DUKE LONG COIL
# C1 = [30, 30, 30, 30, 30, 30, 30, 30]
# C2 = [200, 200, 200, 200, 200, 200, 200, 200]
# C3 = [10.62, 10.62, 10.62, 10.62, 10.62, 10.62, 10.62, 10.62]
# C4 = [200, 200, 200, 200, 200, 200, 200, 200]

## FOR DUKE SHORT COIL
C1 = [90, 90, 90, 90, 90, 90, 90, 90]
C2 = [500, 500, 500, 500, 500, 500, 500, 500]
C3 = [500, 500, 500, 500, 500, 500, 500, 500]
C4 = [25, 25, 25, 25, 25, 25, 25, 25]

R = 1500

wire_descriptor =  'Realistic'#'Straight'#
rfc_descriptor =  'SC' #'LC'#
dielectric = 'Duke'

# ElemNo = 1
for ElemNo in range(1,9):
	Sims.append(CreateSimulation(dielectric, p_logic, ElemNo, rfc_descriptor, n_periods, sim_freq,
	sim_bw, sim_refl, R, C1[ElemNo-1], C2[ElemNo-1], C3[ElemNo-1], C4[ElemNo-1], wire_descriptor))

	document.AllSimulations.Add(Sims[ElemNo-1])
	Sims[ElemNo-1].UpdateGrid()
	Sims[ElemNo-1].SolverSettings.Kernel = Sims[ElemNo-1].SolverSettings.Kernel.enum.AXware