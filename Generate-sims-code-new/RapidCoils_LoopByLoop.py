import s4l_v1.document as document
import s4l_v1.model as model
from s4l_v1 import Vec3
import s4l_v1.simulation.emfdtd as fdtd
import s4l_v1.units as units
import numpy
import HardwarePackage as HP
reload(HP)
import SimulationPackage as SP
reload(SP)
# Start New Document
# document.New()

def CreateHardware(p_logic):
	# RFS Parameters
	rfs_radius = 675.0/2
	rfs_length = 1000.0
	rfs_depth = 0.0	
	# Create RFS	
	rfs_geo = [rfs_radius,rfs_length,rfs_depth]
	rfs = HP.CreateRFShield(rfs_geo)
	
	# Short Metal Loop Values
	rfc_width = 50
	rfc_length = 250
	rfc_descriptor = 'SC'
	T = []
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*3, -130 - 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*2, -130 - 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75, -130 - 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13, -130 - 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*3, 130 + 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*2, 130 + 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75, 130 + 7, -100)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13, 130 + 7,-100)))
	# Create RFC
	rfc_geo = [rfc_width, rfc_length, rfc_descriptor, T]
	rfc_s = HP.CreateLoopCoil(rfc_geo)

	# Long Metal Loop Values
	rfc_width = 50
	rfc_length = 500
	rfc_descriptor = 'LC'
	T = []
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*3, -130 - 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*2, -130 - 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75, -130 - 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13, -130 - 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*3, 130 + 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75*2, 130 + 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13 - 75, 130 + 7, -175)))
	T.append(model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(250/2 - 13, 130 + 7,-175)))
	# Create RFC
	rfc_geo = [rfc_width, rfc_length, rfc_descriptor, T]
	rfc_l = HP.CreateLoopCoil(rfc_geo)
	
	# MODELS - IS IT PHANTOM SIM?
	if p_logic == 'Yes':
		p_width = 320
		p_height = 200
		p_length = 800
		p_geo = [p_width, p_height, p_length]
		phantom = HP.CreateRectangularPhantom(p_geo)
		
	HP.CreateSensorBoxes(p_logic)
	
def BigSim(ElemNo, p_logic):
	# Create Multiport Simulation
	# Setup Setttings
	n_periods = 20.
	rfc_descriptor = 'SC'
	sim_freq = 64e6
	sim_bw = 100e6
	sim_refl = 50
	
	Resistance = 1500
	C2 = 300e-12; C4 = 300e-12 
	if ElemNo == 1 or ElemNo == 4 or ElemNo == 5 or ElemNo == 8:
		C1 = 92e-12; C3 = 20.78e-12
	if ElemNo == 2 or ElemNo == 3 or ElemNo == 6 or ElemNo == 7:
		C1 = 88e-12; C3 = 20.97e-12
	
	sim_data = [ElemNo, p_logic, n_periods, rfc_descriptor, sim_freq, sim_bw, sim_refl, 
	Resistance, C1, C2, C3, C4]
	
	sim = SP.CreateBigSim(sim_data)
	
	return sim	
	
def HuygSim(ElemNo, p_logic, BigS):
	# Create Huygens Simulation
	# Setup Settings
	n_periods = 40.
	rfc_descriptor = 'SC'
	sim_freq = 64e6
	sim_bw = 100e6
	wire_descriptor = 'Realistic'
	
	sim_data = [ElemNo, p_logic, n_periods, rfc_descriptor, sim_freq, sim_bw, wire_descriptor, BigS]
	
	sim = SP.CreateHuygensSim(sim_data)
	
	return sim

	

# Create Hardware
p_logic = 'No'
CreateHardware(p_logic)

BigS = []
HuygS = []

for ElemNo in range(1,9):
	# Create Simulation
	BigS.append(BigSim(ElemNo, p_logic))
	
	# Create Huygens Simulation
	HuygS.append(HuygSim(ElemNo, p_logic, BigS))
	
	
	
 #########################################################
### Create Wire (STRAIGHT WIRE)
# wire = model.CreateSolidCylinder(Vec3(0,0,0), Vec3(0,0,-1650), 0.5, True)
# wire.Name = 'WireCore'

# housing = model.CreateSolidCylinder(Vec3(0,0,0), Vec3(0,0,-1651), 0.6, True);
# housing.Name = 'WireInsulation'

# box = model.CreateWireBlock(Vec3(-160,-100,200), Vec3(160,100,-1675), True)
# box.Name = 'Box'

# sBox = box.Clone()
# sBox.Name = 'sBox'


## Create Wire (DUKE WIRE)
# wire = model.CreateSolidCylinder(Vec3(10,30,-90), Vec3(10,30,-330.5), 0.5, True)
# wire.Name = 'WireCore'

# housing = model.CreateSolidCylinder(Vec3(10,30,-95.5), Vec3(10,30,-330.6), 0.6, True);
# housing.Name = 'WireInsulation'


## Rest of Wires for DUKE sim
# wire2 = model.CreateSolidCylinder(Vec3(10,30,-330), Vec3(80.5,30,-330), 0.5, True)
# wire2.Name = 'WireCore2'

# housing2 = model.CreateSolidCylinder(Vec3(9.4,30,-330), Vec3(80.6,30,-330), 0.6, True);
# housing2.Name = 'WireInsulation2'

# wire3 = model.CreateSolidCylinder(Vec3(80,30,-330), Vec3(80,30,-490.5), 0.5, True)
# wire3.Name = 'WireCore3'

# housing3 = model.CreateSolidCylinder(Vec3(80,30,-329.4), Vec3(80,30,-490.6), 0.6, True);
# housing3.Name = 'WireInsulation3'

# wire4 = model.CreateSolidCylinder(Vec3(80,30,-490), Vec3(80,100.5,-490), 0.5, True)
# wire4.Name = 'WireCore4'

# housing4 = model.CreateSolidCylinder(Vec3(80,29.4,-490), Vec3(80,101,-490), 0.6, True);
# housing4.Name = 'WireInsulation4'

# wire5 = model.CreateSolidCylinder(Vec3(80,100,-490), Vec3(80,100,-1190.5), 0.5, True)
# wire5.Name = 'WireCore5'

# housing5 = model.CreateSolidCylinder(Vec3(80,100,-489.4), Vec3(80,100,-1190.6), 0.6, True);
# housing5.Name = 'WireInsulation5'

# Wire = model.Unite([wire, wire2, wire3, wire4, wire5])
# Wire.Name = 'WireCore'
# Housing = model.Unite([housing, housing2, housing3, housing4, housing5])
# Housing.Name = 'WireInsulation'



# box = model.CreateWireBlock(Vec3(-130,-40,+150), Vec3(150,100,-1250), True)
# box.Name = 'Box'

# sBox = box.Clone()
# sBox.Name = 'sBox'

# Relocate Wire+Huygens

# T1 = model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(-40,20,50))
# wire.ApplyTransform(T1);
# housing.ApplyTransform(T1);

# box.ApplyTransform(T1);

