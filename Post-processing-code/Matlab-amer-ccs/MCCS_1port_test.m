% MCCS (S-p and fields) for 1-port circuit
% 2020-07-28

%% Load and initialize
load('F.mat');  %your field(s)
load('Zp.mat'); %your Z-matrix from s4l or could just load S
Z0 = 50;        %ref impedance
freq = 298e6;   %frequency
p = 1;          %number of (driven) ports

%R = 1;         %resistivity - won't need it cos no lumped element ports

S = z2s(Zp);    %transforming Z to S if imported Z-matrix;

% If you have S-matrix, just define S=S_imported_from_s4l;

%% MCCS - matching circuit co-simulation

Nsamples = 100;
% C = linspace(0.1,10,Nsamples)*1e-12;
% Cba = linspace(0.1,10,Nsamples)*1e-12;
Cma = 100e-9;
Cse = 100e-9;
Csh = linspace(0.1,10,Nsamples)*1e-12; %shunt capacitance connected in 
                                       %parallel to port (variable Csh)

bd = 1; %input square root power (so 1 sqrt(Watt))
Sccs = S;
Smccs = [];
ad = [];
    
    for jj = 1:numel(Cma) %Cma
        
        for kk = 1:numel(Cse) %Cse
            
            for mm = 1:numel(Csh) %Csh
                
                Zma(jj) =  1./(1i*2*pi*freq*Cma(jj)); %Z1
                Zse(kk) =  1./(1i*2*pi*freq*Cse(kk)); %Z2
                Zsh(mm) =  1./(1i*2*pi*freq*Csh(mm)); %Z3
                
                Z1 = Zma(jj);
                Z2 = Zse(kk);
                Z3 = Zsh(mm);
                
                Z = [Z1+Z3 Z3; Z3 Z2+Z3];
                M = z2s(Z,Z0);
                Mdd = M(1:p,1:p);
                Mdp = M(1:p,p+1:end);
                Mpd = M(p+1:end,1:p);
                Mpp = M(p+1:end,p+1:end);
                
%                 Sigma = zeros(l);
%                     for nn = 1:l
%                         Zl = R + 1./(1i*2*pi*freq*C(ii));
%                         Sigma(nn,nn) = (Zl - Z0)/(Zl + Z0);
%                     end
%                     
%                 Sccs(ii) = Spp + Spl * Sigma * (eye(l)-Sll*Sigma)^-1 * Slp;
    
                Smccs(jj,kk,mm) = Mdd + Mdp * Sccs * (eye(p)-Mpp*Sccs)^-1 * Mpd;
                ap(jj,kk,mm) = Sccs^-1 * (Sccs^-1 - Mpp)^-1 * Mpd * bd; %the weighting necessary for field
                
                clear Z1 Z2 Z3 Z M Mdd Mdp Mpd Mpp
            end
            
        end
        
    end
    

% You want to focus on 1 value of ap (could be resonant or any other)
% depending on its indeces:
 ap_test = squeeze(ap(1,1,1)); 
    
% now combine using co-sim for fields
        [Nx,Ny,Nz,~] = size(F); %F is the field variable, could be J,B,E
        
        Fk = reshape(F,[Nx*Ny*Nz*3 p]);
        
        Ftot = reshape(Fk*ap_test,[Nx Ny Nz 3]); 
        
%% then can plot the field(s) and plot S parameters Smccs vs C
% I suppose you want to compare Ftot to F
        