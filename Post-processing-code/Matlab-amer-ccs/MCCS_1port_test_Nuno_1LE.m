% MCCS (S-p and fields) for 1-port circuit
% 2020-07-28

%% Load and initialize
path_to_results = 'G:\FakePort Simulations\Duke\Extracted Results\';

% HERE WE CHOOSE WHAT SIMULATION TO GET THE RESULTS FROM - THIS NAME WAS
% PRE-CHOSEN WHEN EXPORTING FROM S4L

simulation_name = '\Resonance Finder FP Channel 2 Duke Lines 3_2020_08_08_';

ncoils = 1;
% Import Grid
load([path_to_results 'Grid' simulation_name 'Axes'])
sz = [length(XAxis) length(YAxis) length(ZAxis)]; % STORES MATRIX SIZES (HANDY FOR OPERATIONS)
fsz = sz(1)*sz(2)*sz(3);

% IF NAMED CORRECTLY THEN THIS SHOULD GRAB E MATRICES
% for ii = 1:ncoils
%     load([path_to_results 'E Field' simulation_name 'EFields_C' num2str(ii)])
% end
% E1p = permute(reshape(E_1, [sz(1) sz(3) sz(2) 3 ncoils]), [1 3 2 4]); % ISOLATE B1+ AND REORDER IT INTO XYZ

% load('F.mat');  %your field(s)
% load('Zp.mat'); %your Z-matrix from s4l or could just load S
Z0 = 50;        %ref impedance
freq = 64e6;   %frequency
p = 1;          %number of (driven) ports

%R = 1;         %resistivity - won't need it cos no lumped element ports

% S = z2s(Zp);    %transforming Z to S if imported Z-matrix;
S = z2s(74 -299*1i);
% S = z2s(28.1 + 10.3*1i);
% If you have S-matrix, just define S=S_imported_from_s4l;
% F = E1p;

%% MCCS - matching circuit co-simulation

Nsamples = 1000;
% C = linspace(0.1,10,Nsamples)*1e-12;
% Cba = linspace(0.1,10,Nsamples)*1e-12;
% Cma = linspace(10,1000,Nsamples)*1e-12;
% Cse = linspace(10,1000,Nsamples)*1e-12;
Csh = [linspace(100,1500,Nsamples)*1e-9]; %shunt capacitance connected in 
                                       %parallel to port (variable Csh)

bd = 1; %input square root power (so 1 sqrt(Watt))
Sccs = S;
Smccs = [];
ad = [];
    
%     for jj = 1:numel(Cma) %Cma
        
%         for kk = 1:numel(Cse) %Cse
            
            for mm = 1:numel(Csh) %Csh
                
%                 Zma(jj) =  1./(1i*2*pi*freq*Cma(jj)); %Z1
%                 Zse(kk) =  1./(1i*2*pi*freq*Cse(kk)); %Z2
%                 Zsh(mm) =  -1i*(1./(2*pi*freq*Csh(mm))); %Z3
                Zsh(mm) =  (1i*2*pi*freq*Csh(mm)); %Z3
                
%                 Z1 = Zma(jj);
%                 Z2 = Zse(kk);
                Z3 = Zsh(mm);
                
%                 Z = [Z1+Z3 Z3; Z3 Z2+Z3];
                Z(:,:,mm) = [Z3 Z3; Z3 Z3];
                
                M = z2s(Z(:,:,mm),Z0);
                Mdd = M(1:p,1:p);
                Mdp = M(1:p,p+1:end);
                Mpd = M(p+1:end,1:p);
                Mpp = M(p+1:end,p+1:end);
                
%                 Sigma = zeros(l);
%                     for nn = 1:l
%                         Zl = R + 1./(1i*2*pi*freq*C(ii));
%                         Sigma(nn,nn) = (Zl - Z0)/(Zl + Z0);
%                     end
%                     
%                 Sccs(ii) = Spp + Spl * Sigma * (eye(l)-Sll*Sigma)^-1 * Slp;
    
                Smccs(mm) = Mdd + Mdp * Sccs * (eye(p)-Mpp*Sccs)^-1 * Mpd;
%                 Smccs(mm) = z2s(s2z(S)+Z(mm)); %Series

%                 Smccs(mm) = z2s(1/((1/(s2z(S))+(1/Z(mm))))); %Parallel
                ap(mm) = Sccs^-1 * (Sccs^-1 - Mpp)^-1 * Mpd * bd; %the weighting necessary for field
%                 ap(mm) = Sccs.^-1 * (Sccs.^-1 - M).^-1 * M * bd; %the weighting necessary for field
%                 ap(mm) = (1-(abs(Smccs(mm))));
%                 ap(mm) = M/(1-Sccs*M);
%                 clear Z1 Z2 Z3 Z M Mdd Mdp Mpd Mpp
            end
            
%         end
        
%     end
    

% You want to focus on 1 value of ap (could be resonant or any other)
% depending on its indeces:
 ap_test = squeeze(ap(1,1,1)); 
    
% now combine using co-sim for fields
%         [Nx,Ny,Nz,~] = size(F); %F is the field variable, could be J,B,E
        
%         Fk = reshape(F,[Nx*Ny*Nz*3 p]);
        
%         Ftot = reshape(Fk*ap_test,[Nx Ny Nz 3]); 
        
%% then can plot the field(s) and plot S parameters Smccs vs C
% I suppose you want to compare Ftot to F
figurent;smithplot(64e6*ones(1,1000),Smccs, 'GridType','Y', 'LineWidth', 2);
%%
figurent;plot(imag(Z), 20*log10(abs(squeeze(Smccs(:)))), 'LineWidth',4); grid on;
xlabel('X_{LE} (\Omega)')
ylabel('\Gamma (dB)')
%%
figurent;plot(squeeze(imag(Z(1,1,:))), abs(ap(:)), 'LineWidth',4); grid on;
xlabel('X_{LE} (\Omega)')
ylabel('|Scaling Factor|')