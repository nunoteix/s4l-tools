
% Simple Code for MCCS

% Test wrt CCS by applying the following conditions on MCCS:
        % 0   impedances on Match and Series caps (cap=INF) - short cct
        % INF impedances on Shunt cap (cap = 0)             - open cct
%% Initialize

load('Zp_marie_6pRectCoil_7T.mat'); 
Z0 = 50;        %reference impedance of 50 Ohm
freq = 298e6;   %7T
p = 1;          %number of driven ports
l = 5;          %number of lumped elements (LE) ports - simulated as lossy capacitors

R = 1;          %1 Ohm = small resistance added to the impedance of the LEs

S = z2s(Zp_marie);
Spp = S(1:p,1:p);
Spl = S(1:p,p+1:end);
Slp = S(p+1:end,1:p);
Sll = S(p+1:end,p+1:end);

%% MCCS - single value of all capacitances

% Cma = 5.6e-12;
% Cse = 100e-9;
% Csh = 5.2e-12;
Cma = 100e-9;
Cse = 100e-9;
Csh = 1e-18;

Zma =  1./(1i*2*pi*freq*Cma); %Z1
Zse =  1./(1i*2*pi*freq*Cse); %Z2
Zsh =  1./(1i*2*pi*freq*Csh); %Z3

Z1 = Zma;
Z2 = Zse;
Z3 = Zsh;

Z = [Z1+Z3 Z3; Z3 Z2+Z3];
M = z2s(Z,Z0);
Mdd = M(1:p,1:p);
Mdp = M(1:p,p+1:end);
Mpd = M(p+1:end,1:p);
Mpp = M(p+1:end,p+1:end);

% Smccs = Mdd + Mdp * Sccs * (eye(p)-Mpp*Sccs)^-1 * Mpd;
% S11 = 20*log10(abs(Smccs));
% Zmccs = gamma2z(Smccs,Z0);

%% MCCS - sweep through C (break-up capacitor values - lumped elements network)

Nsamples = 100;
Cba = linspace(0.1,10,Nsamples)*1e-12;
Sccs = [];
Smccs = [];

for ii = 1:Nsamples
    
    Sigma = zeros(l);
    for jj = 1:l
        Zl = R + 1./(1i*2*pi*freq*Cba(ii));
        Sigma(jj,jj) = (Zl - Z0)/(Zl + Z0);
    end

    Sccs(ii) = Spp + Spl * Sigma * (eye(l)-Sll*Sigma)^-1 * Slp;
    
    Smccs(ii) = Mdd + Mdp * Sccs(ii) * (eye(p)-Mpp*Sccs(ii))^-1 * Mpd;
    
end

S11 = 20*log10(abs(Smccs));
Zmccs = s2z(Smccs,Z0);

%% Original CCS

[~,Sccs_orig,Coptimal] = CCS_neat(1, Cba, R, Nsamples, Zp_marie, freq);

Sccs_orig = 20*log10(Sccs_orig);
%%
figure(2),
plot(Cba*1e12, Sccs_orig, '-.g'); hold on;
plot(Cba*1e12, S11, '-r');
xlabel('Lumped C, pF'), ylabel('S11, dB'), grid on;
%title('Cmatch~=0 open cct & X=~0 short cct in MCCS vs CCS');
legend('CCS','Modified CCS','location','northeast');

%%
error = 100*(S11-Sccs_orig)./(Sccs_orig);
figure(3),
plot(Cba*1e12, error); grid on;
xlabel('Lumped C, pF'), ylabel('Error(%)');
title('error=100*(MCCS-CCS)/CCS');

%% Add multidimensionality to S - sweep through all 4 variables: Cba, Cma, Cse, Csh 
%  try 20 samples per variable at the first instance

load('Zp_marie_6pRectCoil_7T.mat');
Z0 = 50;
freq = 298e6;
p = 1;
l = 5;

R = 1;

S = z2s(Zp_marie);
Spp = S(1:p,1:p);
Spl = S(1:p,p+1:end);
Slp = S(p+1:end,1:p);
Sll = S(p+1:end,p+1:end);

%% MCCS - multidimensional

% Optimum values:
% Cma = 5.6e-12;
% Cse = 100e-9;
% Csh = 5.2e-12;
Nsamples = 20;
% Cba = linspace(0.1,10,Nsamples)*1e-12;
% Cma = 100e-9;
% Cse = 100e-9;
% Csh = 1e-18;
C = linspace(0.1,10,Nsamples)*1e-12;

Sccs = [];
Smccs = [];

for ii = 1:Nsamples %Cba
    
    for jj = 1:Nsamples %Cma
        
        for kk = 1:Nsamples %Cse
            
            for mm = 1:Nsamples %Csh
                
                Zma(jj) =  1./(1i*2*pi*freq*C(jj)); %Z1
                Zse(kk) =  1./(1i*2*pi*freq*C(kk)); %Z2
                Zsh(mm) =  1./(1i*2*pi*freq*C(mm)); %Z3
                
                Z1 = Zma(jj);
                Z2 = Zse(kk);
                Z3 = Zsh(mm);
                
                Z = [Z1+Z3 Z3; Z3 Z2+Z3];
                M = z2s(Z,Z0);
                Mdd = M(1:p,1:p);
                Mdp = M(1:p,p+1:end);
                Mpd = M(p+1:end,1:p);
                Mpp = M(p+1:end,p+1:end);
                
                Sigma = zeros(l);
                    for nn = 1:l
                        Zl = R + 1./(1i*2*pi*freq*C(ii));
                        Sigma(nn,nn) = (Zl - Z0)/(Zl + Z0);
                    end
                    
                Sccs(ii) = Spp + Spl * Sigma * (eye(l)-Sll*Sigma)^-1 * Slp;
    
                Smccs(ii,jj,kk,mm) = Mdd + Mdp * Sccs(ii) * (eye(p)-Mpp*Sccs(ii))^-1 * Mpd;
                
                clear Z1 Z2 Z3 Z M Mdd Mdp Mpd Mpp
            end
            
        end
        
    end
    
end

%%
figure(104),
plot(C*1e12, mag2db(abs(Smccs(:,11,11,10))), '-r');
xlabel('Lumped C, pF'), ylabel('S11, dB'), grid on;
legend('MCCS','location','northeast');

%%
figure(105),
plot(C*1e12, mag2db(abs(Smccs(10,:,11,10))), '-r');
xlabel('Lumped C, pF'), ylabel('S11, dB'), grid on;
legend('MCCS','location','northeast');

%%
figure(106),
plot(C*1e12, mag2db(abs(squeeze(Smccs(10,11,:,10)))), '-r');
xlabel('Lumped C, pF'), ylabel('S11, dB'), grid on;
legend('MCCS','location','northeast');

%%
figure(107),
plot(C*1e12, mag2db(abs(squeeze(Smccs(10,11,11,:)))), '-r');
xlabel('Lumped C, pF'), ylabel('S11, dB'), grid on;
legend('MCCS','location','northeast');


%% Add multidimensionality to S - sweep through all 4 variables: Cba, Cma, Cse, Csh 
%  try 50 samples per variable now

load('Zp_marie_6pRectCoil_7T.mat');
Z0 = 50;
freq = 298e6;
p = 1;
l = 5;

R = 1;

S = z2s(Zp_marie);
Spp = S(1:p,1:p);
Spl = S(1:p,p+1:end);
Slp = S(p+1:end,1:p);
Sll = S(p+1:end,p+1:end);

%% MCCS - multidimensional

% Optimum values:
% Cma = 5.6e-12;
% Cse = 100e-9;
% Csh = 5.2e-12;
Nsamples = 50;
% Cba = linspace(0.1,10,Nsamples)*1e-12;
% Cma = 100e-9;
% Cse = 100e-9;
% Csh = 1e-18;
C = linspace(0.1,10,Nsamples)*1e-12;

Sccs = [];
Smccs = [];

for ii = 1:Nsamples %Cba
    
    for jj = 1:Nsamples %Cma
        
        for kk = 1:Nsamples %Cse
            
            for mm = 1:Nsamples %Csh
                
                Zma(jj) =  1./(1i*2*pi*freq*C(jj)); %Z1
                Zse(kk) =  1./(1i*2*pi*freq*C(kk)); %Z2
                Zsh(mm) =  1./(1i*2*pi*freq*C(mm)); %Z3
                
                Z1 = Zma(jj);
                Z2 = Zse(kk);
                Z3 = Zsh(mm);
                
                Z = [Z1+Z3 Z3; Z3 Z2+Z3];
                M = z2s(Z,Z0);
                Mdd = M(1:p,1:p);
                Mdp = M(1:p,p+1:end);
                Mpd = M(p+1:end,1:p);
                Mpp = M(p+1:end,p+1:end);
                
                Sigma = zeros(l);
                    for nn = 1:l
                        Zl = R + 1./(1i*2*pi*freq*C(ii));
                        Sigma(nn,nn) = (Zl - Z0)/(Zl + Z0);
                    end
                    
                Sccs(ii) = Spp + Spl * Sigma * (eye(l)-Sll*Sigma)^-1 * Slp;
    
                Smccs(ii,jj,kk,mm) = Mdd + Mdp * Sccs(ii) * (eye(p)-Mpp*Sccs(ii))^-1 * Mpd;
                
                clear Z1 Z2 Z3 Z M Mdd Mdp Mpd Mpp
            end
            
        end
        
    end
    
end

%%
figure(204),
plot(C*1e12, mag2db(abs(Smccs(:,28,28,26))), '-r');
xlabel('Lumped C, pF'), ylabel('S11, dB'), grid on;
legend('MCCS','location','northeast');

%%
figure(205),
plot(C*1e12, mag2db(abs(Smccs(23,:,28,26))), '-r');
xlabel('Lumped C, pF'), ylabel('S11, dB'), grid on;
legend('MCCS','location','northeast');

%%
figure(206),
plot(C*1e12, mag2db(abs(squeeze(Smccs(23,28,:,26)))), '-r');
xlabel('Lumped C, pF'), ylabel('S11, dB'), grid on;
legend('MCCS','location','northeast');

%%
figure(207),
plot(C*1e12, mag2db(abs(squeeze(Smccs(23,28,28,:)))), '-r');
xlabel('Lumped C, pF'), ylabel('S11, dB'), grid on;
legend('MCCS','location','northeast');

%% Finding GLOBAL min  of Smccs and its locations 

[minval, minidx] = min(Smccs(:));
[idxCba,idxCma,idxCse,idxCsh] = ind2sub(size(Smccs),minidx);
% plot Smccs vs C for all 4 cases below
%%
figure(304),
plot(C*1e12, mag2db(abs(Smccs(:,idxCma,idxCse,idxCsh))), '-r');
xlabel('LE Break-up C, pF'), ylabel('S11, dB'), grid on;
title('Resonance - global MIN of 4D Smccs');

%%
figure(305),
plot(C*1e12, mag2db(abs(Smccs(idxCba,:,idxCse,idxCsh))), '-r');
xlabel('MN Matching C, pF'), ylabel('S11, dB'), grid on;
title('Resonance - global MIN of 4D Smccs');

%%
figure(306),
plot(C*1e12, mag2db(abs(squeeze(Smccs(idxCba,idxCma,:,idxCsh)))), '-r');
xlabel('MN Series C, pF'), ylabel('S11, dB'), grid on;
title('Resonance - global MIN of 4D Smccs');

%%
figure(307),
plot(C*1e12, mag2db(abs(squeeze(Smccs(idxCba,idxCma,idxCse,:)))), '-r');
xlabel('MN Shunt C, pF'), ylabel('S11, dB'), grid on;
title('Resonance - global MIN of 4D Smccs');
