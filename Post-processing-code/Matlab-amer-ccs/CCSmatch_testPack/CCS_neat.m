function [Sp,Sphys_arr,Coptimal] = CCS_neat(driven_port,c,R,Nsamples,ZP,freq)
% -------------------------------------------------------------------------
% Co-Simulation routine - SJM 2018, AA 2019

% driven_port ---> an array of numbers signifying which ports are driven
% c           ---> capacitance range of caps placed on lumped elements
% R           ---> small Ohmic loss at capacitances of lumped elements
% Nsamples    ---> number of samples in the capacitance range
% ZP          ---> port impedance matrix at the coil side
% freq        ---> frequency at which the coil is operated

% Note 28-05-2020: This is a simplified version that only evaluates
% physical S matrix and optimal C at the tuned condition upon circuit 
% co-simulation, and not the total fields with their weightings. 

% To calculate total fields given per-port fields, uncomment the last bit 
% of the code below and redefine this function by adding the per-port input
% fields Esol,Bsol,Jsol and total fields Etot,Btot,Jtot in the output:
% function [Sp,Sphys_arr,Coptimal,Etot,Btot,Jtot] = CCS_all(driven_port,c,R,Nsamples,ZP,freq,Esol,Bsol,Jsol) 
% -------------------------------------------------------------------------

%%% make plot of S-params for single port
Nports = size(ZP,1);

portidx = driven_port;
lumpidx = setdiff(1:Nports,portidx);

Sp = z2s(ZP,50); % S-paramaters from per-port simulation

% Z-matrix for lumped elements, all with capacitance c
R_ohmic = R; %small ohmic loss 
Zlumped = @(c) (eye(Nports-1).*(R_ohmic-(1i/(c*2*pi*freq)))); % Z-matrix  

% equivalent S-matrix is refered to as Sigma in Arian's paper
Sigma = @(c) z2s(Zlumped(c),50);

% Physical S-matrix
Sphys = @(c)( Sp(portidx,portidx) + Sp(portidx,lumpidx)*Sigma(c)*inv(eye(Nports-1)-Sp(lumpidx,lumpidx)*Sigma(c))*Sp(lumpidx,portidx));

%c = linspace(0.1,10,500)*1e-12; % capacitances 
Sphys_arr = [];
for ii=1:Nsamples
    Sphys_arr(ii) = abs(Sphys(c(ii)));
end

% % figfp(100),
% figure,
% plot(c*1e12,mag2db(Sphys_arr))
% xlabel('Capacitance, pF')
% ylabel('S11, dB')
% grid on

[~,ix] = min(Sphys_arr);
Coptimal = c(ix);
fprintf(1,'Optimal capacitance is %1.3f pF\n',Coptimal*1e12);

% % now combine using co-sim for fields
%         [Nx,Ny,Nz,~] = size(Jsol);
%         
%         a_lumped = Sigma(Coptimal)*inv((eye(Nports-1)-Sp(lumpidx,lumpidx)*Sigma(Coptimal)))*(Sp(lumpidx,portidx))*1;
%         a_k = ones([Nports 1]);
%         a_k(lumpidx) = a_lumped;
%         
%         Ek = reshape(Esol,[Nx*Ny*Nz*3 Nports]);
%         Jk = reshape(Jsol,[Nx*Ny*Nz*3 Nports]);
%         Bk = reshape(Bsol,[Nx*Ny*Nz*3 Nports]);
%                  
%         Etot = reshape(Ek*a_k,[Nx Ny Nz 3]); 
%         Jtot = reshape(Jk*a_k,[Nx Ny Nz 3]);
%         Btot = reshape(Bk*a_k,[Nx Ny Nz 3]);
%         
end