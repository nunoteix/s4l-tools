% MCCS (S-p and fields) for 1-port circuit
% 2020-07-28

%% Load and initialize
path_to_results = 'G:\FakePort Simulations\Duke\Extracted Results\';

% HERE WE CHOOSE WHAT SIMULATION TO GET THE RESULTS FROM - THIS NAME WAS
% PRE-CHOSEN WHEN EXPORTING FROM S4L

simulation_name = '\Resonance Finder FP Channel 2 Duke Lines 3_2020_08_08_';

ncoils = 1;
% Import Grid
load([path_to_results 'Grid' simulation_name 'Axes'])
sz = [length(XAxis) length(YAxis) length(ZAxis)]; % STORES MATRIX SIZES (HANDY FOR OPERATIONS)
fsz = sz(1)*sz(2)*sz(3);

% IF NAMED CORRECTLY THEN THIS SHOULD GRAB E MATRICES
% for ii = 1:ncoils
%     load([path_to_results 'E Field' simulation_name 'EFields_C' num2str(ii)])
% end
% E1p = permute(reshape(E_1, [sz(1) sz(3) sz(2) 3 ncoils]), [1 3 2 4]); % ISOLATE B1+ AND REORDER IT INTO XYZ

% load('F.mat');  %your field(s)
% load('Zp.mat'); %your Z-matrix from s4l or could just load S
Z0 = 50;        %ref impedance
freq = 64e6;   %frequency
p = 1;          %number of (driven) ports

%R = 1;         %resistivity - won't need it cos no lumped element ports

% S = z2s(Zp);    %transforming Z to S if imported Z-matrix;
% S = z2s(74 -299*1i);
% ZL = 28.1 + 10.3*1i;
ZL = 74 -299*1i;
S = z2s(ZL,Z0);
% If you have S-matrix, just define S=S_imported_from_s4l;
% F = E1p;

%% MCCS - matching circuit co-simulation


bd = 1; %input square root power (so 1 sqrt(Watt))
Sccs = S;
Smccs = [];
ap = [];

quadrant = [];

if (real(ZL) >= Z0) && (imag(ZL) > 0)
    quadrant = 1;
elseif (real(ZL) < Z0) && (imag(ZL) > 0)
    quadrant = 2;
elseif (real(ZL) < Z0) && (imag(ZL) <= 0)
    quadrant = 3;
elseif (real(ZL) >= Z0) && (imag(ZL) <= 0)
    quadrant = 4;
end

switch quadrant
    case 1
        unit = 1e-12;
        Nsamples = 500;
        XLE = [linspace(100,1000,Nsamples)*unit];                                     
        RLE = [linspace(1,200,Nsamples)];

        for jj = 1:numel(RLE)
            for mm = 1:numel(XLE) %Csh
                ZLE(mm) =  -1i*(1./(2*pi*freq*XLE(mm))); %Z3
                Smccs(jj,mm) = z2s(1/((1/(s2z(S)+ZLE(mm)))+(1/RLE(jj)))+ZLE(mm)); %Series C, Parallel R
                Z = [RLE(jj)+ZLE(mm) ZLE(mm); ZLE(mm) RLE(jj)+ZLE(mm)];
                M = z2s(Z,Z0);
                Mdd = M(1:p,1:p);
                Mdp = M(1:p,p+1:end);
                Mpd = M(p+1:end,1:p);
                Mpp = M(p+1:end,p+1:end);
            end
        end
        network = 'RCR T-Network';
    case 2
        unit = 1e-12;
        Nsamples = 500;
        XLE = [linspace(1,50,Nsamples)*unit];                                     
        RLE = [linspace(0.1,50,Nsamples)];
        
        for jj = 1:numel(RLE)
            for mm = 1:numel(XLE) %Csh
                ZLE(mm) =  -1i*(1./(2*pi*freq*XLE(mm))); %Z3
                Smccs(jj,mm) = z2s((1/((1/(s2z(S)+RLE(jj)))+(1/(ZLE(mm)))))+RLE(jj)); %Parallel C, Series R
               
                Z = [RLE(jj)+ZLE(mm) RLE(jj); RLE(jj) RLE(jj)+ZLE(mm)];
                M = z2s(Z,Z0);
                Mdd = M(1:p,1:p);
                Mdp = M(1:p,p+1:end);
                Mpd = M(p+1:end,1:p);
                Mpp = M(p+1:end,p+1:end);
                ap(jj,mm) = Sccs^-1 * (Sccs^-1 - Mpp)^-1 * Mpd * bd;
            end
        end
        network = 'CRC T-Network';
    case 3
        unit = 1e-9;
        Nsamples = 500;
        XLE = [linspace(10,500,Nsamples)*unit];                                     
        RLE = [linspace(1,50,Nsamples)];
        
        for jj = 1:numel(RLE)
            for mm = 1:numel(XLE) %Csh
                ZLE(mm) =  1i*(2*pi*freq*XLE(mm)); %Z3
                Smccs(jj,mm) = z2s((1/((1/(s2z(S)+RLE(jj)))+(1/(ZLE(mm)))))+RLE(jj));
                Z = [RLE(jj)+ZLE(mm) ZLE(mm); ZLE(mm) RLE(jj)+ZLE(mm)];
                M = z2s(Z,Z0);
                Mdd = M(1:p,1:p);
                Mdp = M(1:p,p+1:end);
                Mpd = M(p+1:end,1:p);
                Mpp = M(p+1:end,p+1:end);
                ap(jj,mm) = Sccs^-1 * (Sccs^-1 - Mpp)^-1 * Mpd * bd;
            end
        end
        network = 'RLR T-Network';
    case 4
        unit = 1e-9;
        Nsamples = 500;
        XLE = [linspace(0.1,800,Nsamples)*unit];                                     
        RLE = 53;%[linspace(1,200,Nsamples)];
        
        for jj = 1:numel(RLE)
            for mm = 1:numel(XLE) %Csh
                ZLE(mm) =  1i*(2*pi*freq*XLE(mm)); %Z3
                Smccs(jj,mm) = z2s(1/((1/(s2z(S)+ZLE(mm)))+(1/RLE(jj)))+ZLE(mm)); %Series L, Parallel R
                Z = [RLE(jj)+ZLE(mm) RLE(jj); RLE(jj) RLE(jj)+ZLE(mm)];
                M = z2s(Z,Z0);
                Mdd = M(1:p,1:p);
                Mdp = M(1:p,p+1:end);
                Mpd = M(p+1:end,1:p);
                Mpp = M(p+1:end,p+1:end);
                ap(jj,mm) = Sccs^-1 * (Sccs^-1 - Mpp)^-1 * Mpd * bd;
                test_Smccs(jj,mm) = Mdd + Mdp * Sccs * (eye(p)-Mpp*Sccs)^-1 * Mpd;
            end
        end
        
        network = 'LRL T-Network';
end
%         end
        
%     end
    

% You want to focus on 1 value of ap (could be resonant or any other)
% depending on its indeces:
%  ap_test = squeeze(ap(1,1,1)); 
    
% now combine using co-sim for fields
%         [Nx,Ny,Nz,~] = size(F); %F is the field variable, could be J,B,E
        
%         Fk = reshape(F,[Nx*Ny*Nz*3 p]);
        
%         Ftot = reshape(Fk*ap_test,[Nx Ny Nz 3]); 
        

%%
[resonance, idx] = min(20*log10(abs(squeeze(Smccs(:)))));
[idx_R, idx_C] = ind2sub([500 500], idx);
figurent;
subplot(121)
plot(XLE/unit,...
    20*log10(abs(squeeze(Smccs(idx_R,:)))), 'LineWidth',4); grid on;
if unit == 1e-9
    xlabel('LE_{L} (nH)')
elseif unit == 1e-12
    xlabel('LE_{C} (pF)')
end
ylabel('\Gamma (dB)')
title('Optimal R')

subplot(122)
plot(real(RLE),...
    20*log10(abs(squeeze(Smccs(:,idx_C)))), 'LineWidth',4); grid on;
xlabel('R (\Omega)')
ylabel('\Gamma (dB)')
title('Optimal X_{LE}')

suptitle(network)

%% Smiths Chart
figurent;smithplot(64e6*ones(1,500),squeeze(Smccs(idx_R,:)), 'GridType','Y', 'LineWidth', 2);
%%
figurent;plot(abs(ap(:)), 'LineWidth',4); grid on;
xlabel('X_{LE} (\Omega)')
ylabel('|Scaling Factor|')

%%
figurent;plot(imag(ZLE), 20*log10(abs(squeeze(Smccs(:)))), 'LineWidth',4); grid on;
xlabel('X_{LE} (\Omega)')
ylabel('\Gamma (dB)')