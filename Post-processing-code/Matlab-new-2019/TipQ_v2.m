function [v_all, w1, avgB1] = TipQ_v2(nModes, nChannels, bestIdx, J, maskMLS, sRB1r, sRB1wiredr, lambda, text)
    [~,~,v]=svd(J(bestIdx,:)); % Single Value Decomposition from Surface Current J (wire)
%         sens_pos(kk,:) = plot_pos; % Actual position of sensor vector
    v_dmss = v(:,nModes+1:nChannels); % Creates Dark Mode Subspace (dmss)

    v_all(:,:) = v;
        
    %scaling_factor = 1e-6/abs(mean(sRB1r(:))); %%Step1
    scaling_factor = (1e-6/mean(abs(sum(sRB1r,2)))); %%Step2
    
    a_W = 1e10;
    for jj = 1:20
        [witer,~] =MLS_modeshim_max_amp_original(sRB1r.*scaling_factor*1e6,v_dmss, maskMLS,1.5*ones(nChannels,1),[],lambda,20,0.1, text);
        if sum(abs(witer).^2) < a_W
            w1 = witer.*scaling_factor;
            a_W = sum(abs(witer).^2);
        end
    end    
    [avgB1,~] = applyShim(sRB1wiredr,w1,nChannels);
end