%% Quantities for Full System

% JUST LOAD THE FORPLOTTERV4 MATRICES
npoints = 40;
Q_tip = VOPm;
% Q_tip_normed = Q_tip./b1_normVal_perfKnowledge(1);
Q_tip_normed = Q_tip./b1_normVal_fullK(11,1);
maxEV_fullQ(1:npoints) = max(eig(Q_tip_normed));
maxEV_fullQ_noNorm(1:npoints) = max(eig(Q_tip));
for ii = 1:ncoils
    powJ_i(ii) = sum(abs(Jinterp(800:816,ii)*1000).^2)/length(Jinterp(800:816));
end
powJ = mean(powJ_i);
% powN = powJ*(10^-3);
%% Loop Measurements
% repeat_noise = 100;
% for jj = 1:repeat_noise
%     if floor(jj/20) == jj/20
%         disp(['Running Random Noise ' num2str(jj) ' of ' num2str(repeat_noise)])
%     end
%     Jinterp_calc = awgn(Jinterp*1000, 25, powJ);
    
   Jinterp_calc = Jinterp*1000;

    %%
    figurent();
    subplot(121)
    plot(abs(Jinterp_calc))
    title('Absolute')
    subplot(122)
    plot(angle(Jinterp_calc))
    title('Phase')
    %%

    % [~,~,v_full]=svd(Jinterp_calc);
    % figurent;
    % plot(abs(Jinterp_calc*v_full));

    %% best positions for reduced system
    for kk = 1:length(points_1s)

        [~,posidx_1(1)] = min(abs(linterp_shift-points_1s(kk,1)));
        [~,~,v]=svd(Jinterp_calc(posidx_1,:));

        v_dmss_1s1m = v(:,2:nChannels);
        Q_dmss_1s1m = v_dmss_1s1m'*(Q_tip)*v_dmss_1s1m;
        maxEV_1s1m(kk) = max(eig(Q_dmss_1s1m));

    end

    for kk = 1:length(points_2s)

        [~,posidx_2(1)] = min(abs(linterp_shift-points_2s(kk,1)));
        [~,posidx_2(2)] = min(abs(linterp_shift-points_2s(kk,2)));
        [~,~,v]=svd(Jinterp_calc(posidx_2,:));

        v_dmss_2s1m = v(:,2:nChannels);
        Q_dmss_2s1m = v_dmss_2s1m'*(Q_tip)*v_dmss_2s1m;
        maxEV_2s1m(kk) = max(eig(Q_dmss_2s1m));

        v_dmss_2s2m = v(:,3:nChannels);
        Q_dmss_2s2m = v_dmss_2s2m'*(Q_tip)*v_dmss_2s2m;
        maxEV_2s2m(kk) = max(eig(Q_dmss_2s2m));

    end

    for kk = 1:length(points_3s)

        [~,posidx_3(1)] = min(abs(linterp_shift-points_3s(kk,1)));
        [~,posidx_3(2)] = min(abs(linterp_shift-points_3s(kk,2)));
        [~,posidx_3(3)] = min(abs(linterp_shift-points_3s(kk,3)));%Sets points in relation to phantom-air border
        [~,~,v]=svd(Jinterp_calc(posidx_3,:));

        v_dmss_3s1m = v(:,2:nChannels);
        Q_dmss_3s1m = v_dmss_3s1m'*(Q_tip)*v_dmss_3s1m;
        maxEV_3s1m(kk) = max(eig(Q_dmss_3s1m));

        v_dmss_3s2m = v(:,3:nChannels);
        Q_dmss_3s2m = v_dmss_3s2m'*(Q_tip)*v_dmss_3s2m;
        maxEV_3s2m(kk) = max(eig(Q_dmss_3s2m));

%         v_dmss_3s3m = v(:,4:nChannels);
%         Q_dmss_3s3m = v_dmss_3s3m'*(Q_tip)*v_dmss_3s3m;
%         maxEV_3s3m(kk) = max(eig(Q_dmss_3s3m));

    end

    %% Actual optimal points
    wcSAR_full(1:length(points_1s)) = abs(wcSAR_perfKnowledge(1));
    deepestSens_wcSAR_1s1m(:) = abs(maxEV_1s1m);

    for ii = 2:length(points_1s)
        logic_idx{ii} = double(find(points_2s(:,2) == points_1s(ii)));
        [all_wcSAR_2s1m{ii}] = abs(maxEV_2s1m(logic_idx{ii}));
        [all_wcSAR_2s2m{ii}] = abs(maxEV_2s2m(logic_idx{ii}));

        [min_wcSAR_2s1m(ii), temp_idx_2s1m] = min(all_wcSAR_2s1m{ii});
        [min_wcSAR_2s2m(ii), temp_idx_2s2m] = min(all_wcSAR_2s2m{ii});
        
        if length(logic_idx{ii}) == 1 
            bestPos_idx_2s1m(ii) = logic_idx{ii};
            bestPos_idx_2s2m(ii) = logic_idx{ii};
        else
            bestPos_idx_2s1m(ii) = logic_idx{ii}(temp_idx_2s1m);
            bestPos_idx_2s2m(ii) = logic_idx{ii}(temp_idx_2s2m);
        end
        
        allPos_idx_2s1m{ii} = logic_idx{ii};
        allPos_idx_2s2m{ii} = logic_idx{ii};       
        
        
        if ii > 2
            logic_idx_3{ii} = double(find(points_3s(:,3) == points_1s(ii)));
            [all_wcSAR_3s1m{ii}] = abs(maxEV_3s1m(logic_idx_3{ii}));
            [all_wcSAR_3s2m{ii}] = abs(maxEV_3s2m(logic_idx_3{ii}));
    %         [all_wcSAR_3s3m{ii}] = abs(maxEV_3s3m(logic_idx{ii}));

            [min_wcSAR_3s1m(ii),temp_idx_3s1m] = min(all_wcSAR_3s1m{ii});
            [min_wcSAR_3s2m(ii),temp_idx_3s2m] = min(all_wcSAR_3s2m{ii});
    %         min_wcSAR_3s3m(ii,jj) = min(all_wcSAR_3s3m{ii});
            if length(logic_idx_3{ii}) == 1 
                bestPos_idx_3s1m(ii) = logic_idx_3{ii};
                bestPos_idx_3s2m(ii) = logic_idx_3{ii};
            else
                bestPos_idx_3s1m(ii) = logic_idx_3{ii}(temp_idx_3s1m);
                bestPos_idx_3s2m(ii) = logic_idx_3{ii}(temp_idx_3s2m);
            end

            allPos_idx_3s1m{ii} = logic_idx_3{ii};
            allPos_idx_3s2m{ii} = logic_idx_3{ii};       
        
        end
    end
% end

%% PLOTS
%% 2s1m
figurent(100)
pp = 0;

for ii = 2:2:npoints
    pp = pp+1; 
    subplot(5,4,pp)
    scatter(all_wcSAR_2s1m{ii}, points_2s(allPos_idx_2s1m{ii}), 'LineWidth', 2);hold on;
    scatter(min_wcSAR_2s1m(ii), points_2s(bestPos_idx_2s1m(ii)),'x','r', 'LineWidth',2); 
    axis([0 80 -100 800])
    grid on;
    
%     set(gca,'FontSize',18)

%     legend('Coil Region','Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
    title(['Deepest Sensor @' num2str(points_1s(ii))])%'wcSAR - Straight Wire - Duke Long Coil')
    ylabel('Other Sensor Position')
%     xlabel('Worst Case SAR (W/Kg/W)')

end
set(gcf, 'Position',  [0, 0, 1000, 1000])
suptitle('Worst Case SAR - 2s1m, All Combinations - w/Noise')
%% 2s2m
figurent(101)
pp = 0;

for ii = 2:2:npoints
    pp = pp+1; 
    subplot(5,4,pp)
    scatter(all_wcSAR_2s2m{ii}, points_2s(allPos_idx_2s2m{ii}), 'LineWidth', 2);hold on;
    scatter(min_wcSAR_2s2m(ii), points_2s(bestPos_idx_2s2m(ii)),'x','r', 'LineWidth',2); 
    axis([0 20 -100 800])
    grid on;
    
%     set(gca,'FontSize',18)

%     legend('Coil Region','Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
    title(['Deepest Sensor @' num2str(points_1s(ii))])%'wcSAR - Straight Wire - Duke Long Coil')
    ylabel('Other Sensor Position')
%     xlabel('Worst Case SAR (W/Kg/W)')

end
set(gcf, 'Position',  [0, 0, 1000, 1000])
suptitle('Worst Case SAR - 2s2m, All Combinations - w/Noise')
%% 3s1m
figurent(102)
colormap(inferno())
pp = 0;

for ii = 3:2:npoints
    pp = pp+1; 
    subplot(5,4,pp)
    scatter(points_3s(allPos_idx_3s1m{ii},1), points_3s(allPos_idx_3s1m{ii},2),30,all_wcSAR_3s1m{ii}',...
        'filled','LineWidth', 2);hold on;
    scatter(points_3s(bestPos_idx_3s1m(ii),1), points_3s(bestPos_idx_3s1m(ii),2),50,min_wcSAR_3s1m(ii)...
        ,'x','r', 'LineWidth',2);
   
    axis([-100 800 -100 800])
    caxis([0 60])
    grid on;
    pbaspect([1.5 1 1])
    
%     set(gca,'FontSize',18)

%     legend('Coil Region','Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
    title(['Deepest Sensor @' num2str(points_1s(ii))])%'wcSAR - Straight Wire - Duke Long Coil')
    ylabel('Other Sens. Pos.')
%     xlabel('Worst Case SAR (W/Kg/W)')
            
end
set(gcf, 'Position',  [0, 0, 1000, 1000])
colorbar
suptitle('Worst Case SAR - 3s1m, All Combinations - w/Noise')
%% 3s2m
figurent(103)
colormap(inferno())
pp = 0;

for ii = 3:2:npoints
    pp = pp+1; 
    subplot(5,4,pp)
    scatter(points_3s(allPos_idx_3s2m{ii},1), points_3s(allPos_idx_3s2m{ii},2),30,all_wcSAR_3s2m{ii}',...
        'filled','LineWidth', 2);hold on;
    scatter(points_3s(bestPos_idx_3s2m(ii),1), points_3s(bestPos_idx_3s2m(ii),2),50,min_wcSAR_3s2m(ii)...
        ,'x','r', 'LineWidth',2); 
    axis([-100 800 -100 800])
    caxis([0 20])
    grid on;
    
%     set(gca,'FontSize',18)

%     legend('Coil Region','Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
    title(['Deepest Sensor @' num2str(points_1s(ii))])%'wcSAR - Straight Wire - Duke Long Coil')
    ylabel('Other Sensor Position')
%     xlabel('Worst Case SAR (W/Kg/W)')
end
set(gcf, 'Position',  [0, 0, 1000, 1000])
colorbar
suptitle('Worst Case SAR - 3s2m, All Combinations - w/Noise')
