si=size(rho3D_trunc);
cubes_1g = zeros([si,6]);
tic;
for ii = 1:si(1)
    for jj = 1:si(2)
        for kk = 1:si(3)
            if rho3D_trunc(ii,jj,kk) > 0
                cubes_1g(ii,jj,kk,:) = genMassCube(rho3D_trunc,ii,jj,kk,0.001,0.001,0.001,1);
            end
        end
    end
    progress = 100*(ii/si(1));
    fprintf('%.2f %% Completed\n', progress);
end
toc;

%%
tic;
avgQ_100mg = avgQFromCubes(Q3D_trunc,rho3D_trunc,cubes_100mg);
toc;