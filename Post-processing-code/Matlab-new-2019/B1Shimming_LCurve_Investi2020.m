%% DO THE SAME FOR THE RAPID COIL
npoints = 30;
ncoils = 8;

path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';

% HERE WE CHOOSE WHAT SIMULATION TO GET THE RESULTS FROM - THIS NAME WAS
% PRE-CHOSEN WHEN EXPORTING FROM S4L
simulation_name = '\RapidCoils_Duke_Guidewire_HighRes_2020_01_06_'; %Duke, ShortCoil, High Res
% simulation_name = '\LongRapidCoils_Duke_Guidewire_HighRes_2020_02_05_'; %Duke, Long Coil, High Res
% Import Current
J_struct = load([path_to_results 'EM Current' simulation_name 'EMCurrent']);
J = J_struct.J;
Axes = J_struct.Axes;

% Get B1+ to Acceptable Levels
load([path_to_results 'Grid' simulation_name 'Axes'])
sz = [length(XAxis) length(YAxis) length(ZAxis)]; % STORES MATRIX SIZES (HANDY FOR OPERATIONS)
fsz = sz(1)*sz(2)*sz(3);
% IF NAMED CORRECTLY THEN THIS SHOULD GRAB B1 MATRICES
for ii = 1:ncoils
    load([path_to_results 'B1 Field' simulation_name 'B1Fields_C' num2str(ii)])
end
B1 = cat(3, B1_1, B1_2, B1_3, B1_4, B1_5, B1_6, B1_7, B1_8); % BUILD THE 8 CHANNEL MATRIX
clear B1_1 B1_2 B1_3 B1_4 B1_5 B1_6 B1_7 B1_8 % FREEING SPACE
B1p = permute(reshape(squeeze(B1(:,1,:)), [sz(1) sz(3) sz(2) 8]), [1 3 2 4]); % ISOLATE B1+ AND REORDER IT INTO XYZ
[~,xx] = min(abs(XAxis - Axes(1,end)*1e-3));
[~,yy] = min(abs(YAxis - Axes(2,end)*1e-3));
[~,zz] = min(abs(ZAxis - Axes(3,end)*1e-3));

%% find WORST CASE (ALL MODES) AVERAGE B1+
path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';
% simulation_name = '\BodyCoil_Duke_Guidewire_2020_04_07_';
simulation_name = '\RapidCoils_Duke_Guidewire_HighRes_2020_01_06_'; %Duke, ShortCoil, High Res
load([path_to_results 'Density' simulation_name 'Density'])
rho = reshape(permute(reshape(rho, [sz(1) sz(3) sz(2)]), [1 3 2]), [fsz 1]); %REORDER INTO XYZ

maskR = rho; %Load Mask Properties first, if is in cell form run: rho = cellfun(@(x) double(x), rho);
maskR((maskR > 1048) & (maskR < 1050.5)) = 1; %Density of Heart Lumen (blood)
maskR((maskR > 1079) & (maskR < 1081)) = 1; %Density of Heart Muscle (plot rho and check precis value)
maskR(maskR ~= 1) = 0; % Make everything else 0
maskR = reshape(maskR, sz); %Reshape to 3D

%%
% Check manually what small box encompasses
% the Heart, as blood has same density, need to make everything else
% outside this box 0, hence the following lines:

%%%% REAL WIRE DUKE SHORT COIL, HIGH RES
maskR(1:16,:,:) = 0;
maskR(:,1:24,:) = 0;
maskR(:,:,1:1052) = 0;
maskR(150:end,:,:) = 0;
maskR(:,121:end,:) = 0;
maskR(:,:,1165:end) = 0;

% Voxelization makes everything patchy. Dilate and Erode to fill patches
se = strel('sphere',3); %Patches are small so small Dilate radius will do
dilmaskR = imdilate(maskR,se); %Dilate
filledmaskR = imerode(dilmaskR,se); %Erode, Heart Region should now be properly masked
filledmask = reshape(filledmaskR, [fsz 1]);
maskMLS = filledmask(filledmask~=0);
%%
figurent();
imagesc(filledmaskR(:,:,1119));
%%
B1pr = reshape(B1p, [fsz ncoils]);%./sqrt(ncoils);

if ncoils > 1
    for ii = 1:ncoils
        phase_quad(ii) = (1/sqrt(ncoils)).*exp(-1i*angle(B1p(xx,yy,zz-1,ii)));
        B1pr_quad(:,ii) = B1pr(:,ii)*phase_quad(ii);
    end
end

for ii = 1:ncoils
% %%%% HEART SECTION    pp
    if ncoils > 1
        maskedB1_quad(:,ii) = B1pr_quad(:,ii).*filledmask;%.*w(ii);
        maskedB1(:,ii) = B1pr(:,ii).*filledmask;%.*w(ii);
    else
        maskedB1(:,ii) = B1pr(:,ii).*filledmask;%.*w(ii);
    end
end

maskedB1_quad_reshaped = reshape(maskedB1_quad, [sz ncoils]);
maskedB1_reshaped = reshape(maskedB1, [sz ncoils]);
for ii = 1:ncoils
    lowresB1_reshaped(:,:,:,ii) = Gen3D(squeeze(maskedB1_reshaped(:,:,:,ii)), XAxis, YAxis, ZAxis, 30, 30, 150);
    lowresB1_quad_reshaped(:,:,:,ii) = Gen3D(squeeze(maskedB1_quad_reshaped(:,:,:,ii)), XAxis, YAxis, ZAxis, 30, 30, 150);
end

lowresB1_quad = reshape(lowresB1_quad_reshaped, [30*30*150 ncoils]);
lowresB1 = reshape(lowresB1_reshaped, [30*30*150 ncoils]);

nonzero_maskedB1_quad = lowresB1_quad(lowresB1_quad~=0);
nonzero_maskedB1 = lowresB1(lowresB1~=0);

B1_toShim_quad = reshape(nonzero_maskedB1_quad,[],ncoils);
B1_toShim = reshape(nonzero_maskedB1,[],ncoils);

maskMLS = ones(length(B1_toShim(:,1)),1);

%%
[L, Jinterp, pedge_ind, points_1s, points_2s, points_3s] = interpJ(npoints, ncoils, 2000, Axes, J, 756, 16);

Q_tip = VOPm;

[u,s,v] = svd(Jinterp);

scaling_factor = (1e-6/mean(abs(sum(B1_toShim,2)))); %IF QUAD NEEDS DIVIDED BY sq8
%%
lambda = 0:0.1:1;
w_fullK = zeros(8,length(lambda));
err = zeros(1,length(lambda));
pow = zeros(1,length(lambda));
niters = 20;
witer = zeros(ncoils,length(lambda),niters);
mo = 0;


for ii = 1:length(lambda)
    
    pow(ii) = 1e10;
    
    for jj = 1:niters
        [witer(:,ii,jj),~] = MLS_modeshim_max_amp(B1_toShim.*1e6,v(:,mo+1:8),scaling_factor, maskMLS,100*ones(ncoils,1),[],lambda(ii),20,0.05,0.001,'disabled');
        
        if sum(abs(witer(:,ii,jj)).^2) < pow(ii)
            w_fullK(:,ii) = witer(:,ii,jj);
            pow(ii) = sum(abs(witer(:,ii,jj)).^2);
        end
        
%         [avgB1_fullK(ii),~,err(ii)] = applyShim(B1_toShim, w_fullK(:,ii).*permute(phase_quad, [2 1]), ncoils); %quad
        [avgB1_fullK(ii),~,err(ii)] = applyShim(B1_toShim, w_fullK(:,ii), ncoils); %non-quad
        b1_normVal_fullK(ii) = (avgB1_fullK(ii).^2)./sum(abs(w_fullK(:,ii)).^2);
        if mo == 0
            wcSAR_fullK(ii) = max(eig(Q_tip));
        else
            wcSAR_fullK(ii) = max(eig(v(:,mo+1:8)'*Q_tip*v(:,mo+1:8)));
        end
        
    end
    
end

%%
figurent()
scatter(err, pow,'LineWidth', 2);
ylabel('Shimming Power (W)')
xlabel('Error')
% axis([0.035 0.065 100 400])
%%

% jj = 1;
pow_all = zeros(length(lambda),niters);
sar_ac = zeros(length(lambda),niters);
for ii = 1:length(lambda)
%     subplot(3,4,ii)
    for jj = 1:niters
        pow_all(ii,jj) = sum(abs(witer(:,ii,jj)).^2);
        sar_ac(ii,jj) = abs(witer(:,ii,jj)'*Q_tip*witer(:,ii,jj));
    end
end
%%
figurent()
Q_red = v(:,mo+1:ncoils)'*Q_tip*v(:,mo+1:ncoils);
for ii = 1:length(lambda)
    scatter(ones(1,niters)*lambda(ii),sar_ac(ii,:), 'rx', 'LineWidth',0.5);hold on;
    scatter(ones(1,niters)*lambda(ii),max(eig(Q_red)).*pow_all(ii,:), 'bx', 'LineWidth',0.5)
%     axis([0 1 0 4000])
    drawnow
    pause(1)
end
set(gca, 'FontSize',14)
ylabel('W/kg/\muT^2')
xlabel('\lambda')
legend('Actual SAR', 'Worst Case SAR')

%%
for kk = 1:length(points_1s)
    
    [~,posidx_1(1)] = min(abs(L-points_1s(kk,1)));
    [~,~,v]=svd(Jinterp(posidx_1,:));
    
    v_dmss_1s1m = v(:,2:ncoils);
    Q_dmss_1s1m = v_dmss_1s1m'*(Q_tip)*v_dmss_1s1m;
    maxEV_1s1m(kk) = max(eig(Q_dmss_1s1m));
    
end

for kk = 1:length(points_2s)
    
    [~,posidx_2(1)] = min(abs(L-points_2s(kk,1)));
    [~,posidx_2(2)] = min(abs(L-points_2s(kk,2)));
    [~,~,v]=svd(Jinterp(posidx_2,:));
    
    v_dmss_2s1m = v(:,2:ncoils);
    Q_dmss_2s1m = v_dmss_2s1m'*(Q_tip)*v_dmss_2s1m;
    maxEV_2s1m(kk) = max(eig(Q_dmss_2s1m));
    
    v_dmss_2s2m = v(:,3:ncoils);
    Q_dmss_2s2m = v_dmss_2s2m'*(Q_tip)*v_dmss_2s2m;
    maxEV_2s2m(kk) = max(eig(Q_dmss_2s2m));
    
end

for kk = 1:length(points_3s)
    
    [~,posidx_3(1)] = min(abs(L-points_3s(kk,1)));
    [~,posidx_3(2)] = min(abs(L-points_3s(kk,2)));
    [~,posidx_3(3)] = min(abs(L-points_3s(kk,3)));%Sets points in relation to phantom-air border
    [~,~,v]=svd(Jinterp(posidx_3,:));
    
    v_dmss_3s1m = v(:,2:ncoils);
    Q_dmss_3s1m = v_dmss_3s1m'*(Q_tip)*v_dmss_3s1m;
    maxEV_3s1m(kk) = max(eig(Q_dmss_3s1m));
    
    v_dmss_3s2m = v(:,3:ncoils);
    Q_dmss_3s2m = v_dmss_3s2m'*(Q_tip)*v_dmss_3s2m;
    maxEV_3s2m(kk) = max(eig(Q_dmss_3s2m));
    
    %     v_dmss_3s3m = v(:,4:ncoils);
    %     Q_dmss_3s3m = v_dmss_3s3m'*(Q_tip)*v_dmss_3s3m;
    %     maxEV_3s3m(kk) = max(eig(Q_dmss_3s3m));
    
    
end
%% Actual optimal points
wcSAR_full(1:length(points_1s)) = abs(wcSAR_fullK(1))*1e-12;
deepestSens_wcSAR_1s1m = abs(maxEV_1s1m)*1e-12;

for ii = 2:length(points_1s)
    logic_idx = find(points_2s(:,2) == points_1s(ii));
    [deepestSens_wcSAR_2s1m(ii), temp_idx_2s1m] = min(abs(maxEV_2s1m(logic_idx))*1e-12);
    [deepestSens_wcSAR_2s2m(ii), temp_idx_2s2m] = min(abs(maxEV_2s2m(logic_idx))*1e-12);
    
    bestPos_idx_2s1m(ii) = logic_idx(temp_idx_2s1m);
    bestPos_idx_2s2m(ii) = logic_idx(temp_idx_2s2m);
    
    bestPos_2s1m(ii,:) = points_2s(bestPos_idx_2s1m(ii),:);
    bestPos_2s2m(ii,:) = points_2s(bestPos_idx_2s2m(ii),:);
    
    [~,bestIdx_2s1m_pt1] = min(abs(L-bestPos_2s1m(ii,1)));
    [~,bestIdx_2s1m_pt2] = min(abs(L-bestPos_2s1m(ii,2)));
    bestIdx_2s1m(ii,:) = [bestIdx_2s1m_pt1, bestIdx_2s1m_pt2];
    
    [~,bestIdx_2s2m_pt1] = min(abs(L-bestPos_2s2m(ii,1)));
    [~,bestIdx_2s2m_pt2] = min(abs(L-bestPos_2s2m(ii,2)));
    bestIdx_2s2m(ii,:) = [bestIdx_2s2m_pt1, bestIdx_2s2m_pt2];
    
    [deepestSens_wcSAR_2s(ii), bestMode_2s(ii)] = min([deepestSens_wcSAR_2s1m(ii), deepestSens_wcSAR_2s2m(ii)]);
end
%%

for ii = 3:length(points_1s)
    logic_idx = find(points_3s(:,3) == points_1s(ii));
    [deepestSens_wcSAR_3s1m(ii), temp_idx_3s1m] = min(abs(maxEV_3s1m(logic_idx))*1e-12);
    [deepestSens_wcSAR_3s2m(ii), temp_idx_3s2m] = min(abs(maxEV_3s2m(logic_idx))*1e-12);
%     [deepestSens_wcSAR_3s3m(ii), temp_idx_3s3m] = min(abs(maxEV_3s3m(logic_idx))*1e-12);
    
    bestPos_idx_3s1m(ii) = logic_idx(temp_idx_3s1m);
    bestPos_idx_3s2m(ii) = logic_idx(temp_idx_3s2m);
%     bestPos_idx_3s3m(ii) = logic_idx(temp_idx_3s3m);
    
    bestPos_3s1m(ii,:) = points_3s(bestPos_idx_3s1m(ii),:);
    bestPos_3s2m(ii,:) = points_3s(bestPos_idx_3s2m(ii),:);
%     bestPos_3s3m(ii,:) = points_3s(bestPos_idx_3s3m(ii),:);
    
    [~,bestIdx_3s1m_pt1] = min(abs(L-bestPos_3s1m(ii,1)));
    [~,bestIdx_3s1m_pt2] = min(abs(L-bestPos_3s1m(ii,2)));
    [~,bestIdx_3s1m_pt3] = min(abs(L-bestPos_3s1m(ii,3)));
    bestIdx_3s1m(ii,:) = [bestIdx_3s1m_pt1, bestIdx_3s1m_pt2, bestIdx_3s1m_pt3];
    
    [~,bestIdx_3s2m_pt1] = min(abs(L-bestPos_3s2m(ii,1)));
    [~,bestIdx_3s2m_pt2] = min(abs(L-bestPos_3s2m(ii,2)));
    [~,bestIdx_3s2m_pt3] = min(abs(L-bestPos_3s2m(ii,3)));
    bestIdx_3s2m(ii,:) = [bestIdx_3s2m_pt1, bestIdx_3s2m_pt2, bestIdx_3s2m_pt3];
    
%     [~,bestIdx_3s3m_pt1] = min(abs(L-bestPos_3s3m(ii,1)));
%     [~,bestIdx_3s3m_pt2] = min(abs(L-bestPos_3s3m(ii,2)));
%     [~,bestIdx_3s3m_pt3] = min(abs(L-bestPos_3s3m(ii,3)));
%     bestIdx_3s3m(ii,:) = [bestIdx_3s3m_pt1, bestIdx_3s3m_pt2, bestIdx_3s3m_pt3];
    
    [deepestSens_wcSAR_3s(ii), bestMode_3s(ii)] = min([deepestSens_wcSAR_3s1m(ii), deepestSens_wcSAR_3s2m(ii)]);%, deepestSens_wcSAR_3s3m(ii)]);
end

%% B1+ Shim
text = 'disabled';
lambda = 0.3; % 0:0.1:1; %[100 50 0];%
% for ii = 1:length(lambda)
    for kk = 1:length(points_1s)
        disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_1s))])
        
        [~,bestIdx_1s1m(kk)] = min(abs(L-points_1s(kk)));
        
        [modes_1s1m(:,:,kk,ii), w1s1m(:,kk,ii), avgB1_1s1m(kk,ii), err_1s1m(kk,ii)] = TipQ_v3(1, ncoils,...
            bestIdx_1s1m(kk), Jinterp, maskMLS, B1_toShim,Q_tip,0.1,scaling_factor, lambda, text);
    end
    %%
    for kk = 2:length(points_1s)
        disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_1s))])
        
        [modes_2s1m(:,:,kk,ii), w2s1m(:,kk,ii), avgB1_2s1m(kk,ii), err_2s1m(kk,ii)] = TipQ_v3(1, ncoils,...
            bestIdx_2s1m(kk,:), Jinterp, maskMLS, B1_toShim, Q_tip,8,scaling_factor, lambda(ii), text);
        
        [modes_2s2m(:,:,kk,ii), w2s2m(:,kk,ii), avgB1_2s2m(kk,ii), err_2s2m(kk,ii)] = TipQ_v3(2, ncoils,...
            bestIdx_2s2m(kk,:), Jinterp, maskMLS, B1_toShim, Q_tip,8,scaling_factor, lambda(ii), text);
    end
    %%
    for kk = 3:length(points_1s)
        disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_1s))])
        
        [modes_3s1m(:,:,kk,ii), w3s1m(:,kk,ii), avgB1_3s1m(kk,ii), err_3s1m(kk,ii)] = TipQ_v3(1, ncoils,...
            bestIdx_3s1m(kk,:), Jinterp, maskMLS, B1_toShim, Q_tip,8,scaling_factor, lambda(ii), text);
        
        [modes_3s2m(:,:,kk,ii), w3s2m(:,kk,ii), avgB1_3s2m(kk,ii), err_3s2m(kk,ii)] = TipQ_v3(2, ncoils,...
            bestIdx_3s2m(kk,:), Jinterp, maskMLS, B1_toShim, Q_tip,8,scaling_factor, lambda(ii), text);
        
        %     [modes_3s3m(:,:,kk), w3s3m(:,kk), avgB1_3s3m(kk)] = TipQ_v2(3, ncoils, bestIdx_3s3m(kk,:), Jinterp, maskMLS, B1_toShim, nonzero_maskedB1wired_reshaped, lambda(3), text);
    end
% end
%%
 b1weights_1s1m = (avgB1_1s1m.^2)./sum(abs(w1s1m).^2,1);
 b1weights_2s1m = (avgB1_2s1m.^2)./sum(abs(w2s1m).^2,1);
 b1weights_2s2m = (avgB1_2s2m.^2)./sum(abs(w2s2m).^2,1);
 b1weights_3s1m = (avgB1_3s1m.^2)./sum(abs(w3s1m).^2,1);
 b1weights_3s2m = (avgB1_3s2m.^2)./sum(abs(w3s2m).^2,1);
 b1weights_3s3m = (avgB1_3s3m.^2)./sum(abs(w3s3m).^2,1);
 
% [maxEV_redQ_2s1m, maxEV_redQ_2s1m_noNorm, shimmedB1_2s1m, modes_2s1m, b1weights_2s1m, w2s1m, avgB1_2s1m] = twoSensorTipQ(1,nChannels, points_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_2s2m, maxEV_redQ_2s2m_noNorm, shimmedB1_2s, modes_2s, b1weights_2s, w2, avgB1_2s] = twoSensorTipQ(2, nChannels, points_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_3s3m, maxEV_redQ_3s3m_noNorm, shimmedB1_3s, modes_3s, b1weights_3s, w3, avgB1_3s] = threeSensorTipQ(3, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_3s2m, maxEV_redQ_3s2m_noNorm, shimmedB1_3s2m, modes_3s2m, b1weights_3s2m, w3s2m, avgB1_3s2m] = threeSensorTipQ(2, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_3s1m, maxEV_redQ_3s1m_noNorm, shimmedB1_3s1m, modes_3s1m, b1weights_3s1m, w3s1m, avgB1_3s1m] = threeSensorTipQ(1, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);
% 