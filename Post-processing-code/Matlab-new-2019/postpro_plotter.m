% maskedB1_reshaped = reshape(maskedB1, [sz 8]);
 
% for ii = 1:nChannels
%     lowres_B1(:,:,:,ii) = Gen3D(squeeze(maskedB1_reshaped(:,:,:,1)), xaxis, yaxis, zaxis, 30, 30, 150);
% end


%%Needs:
%- Jinterp, linterp_shift
%- nonzero_maskedB1reshaped
%- maskMLS
%- nChannels
%- Q_tip
%- points_w, points_pos, points_3_pos

%% Full Knowledge
[~,~,v] = svd(Jinterp);
for ii = 1:4
    a_W = 1e10;
    for jj = 1:10
        
        [witer,~] = MLS_modeshim_max_amp(nonzero_maskedB1reshaped.*scaling_factor*1e6,v(:,ii:8), maskMLS,1.5*ones(nChannels,1),[],0.8,20,0.08,'enabled');
        
        if sum(abs(witer).^2) < a_W
            wout(:,ii) = witer.*scaling_factor;
            a_W = sum(abs(witer).^2);
        end
    end
    
    
    [avgB1_perfKnowledge(ii),~] = applyShim(nonzero_maskedB1wired_reshaped, wout(:,ii), nChannels);
    b1_normVal_perfKnowledge(ii) = (avgB1_perfKnowledge(ii).^2)./sum(abs(wout(:,ii)).^2);
    if ii == 1
        wcSAR_perfKnowledge(ii) = max(eig(Q_tip));
    else
        wcSAR_perfKnowledge(ii) = max(eig(v(:,ii:8)'*Q_tip*v(:,ii:8)));
    end
end



%% Full Knowledge Plot
figure;
subplot(1,3,1)
bar(0:3, wcSAR_perfKnowledge, 'r')
title('wcSAR')
xlabel('Modes Extracted')
ylabel('W/Kg/W')

subplot(132)
bar(0:3, b1_normVal_perfKnowledge*1e12, 'b')
title('Capacity to generate B1+ After Shim')
xlabel('Modes Extracted')
ylabel('\muT^2/W')

subplot(133)
bar(0:3, wcSAR_perfKnowledge./(b1_normVal_perfKnowledge*1e12), 'g')
title('Normalised wcSAR')
xlabel('Modes Extracted')
ylabel('W/Kg/\muT^2')

%% Limited Knowledge
for kk = 1:length(points_w)
    [~,posidx_1(1)] = min(abs(linterp_shift-points_w(kk,1)));
    [~,~,v]=svd(J(posidx,:));
    
    v_dmss = v(:,2:nChannels);
    Q_dmss = v_dmss'*(Q_tip)*v_dmss;
    
    maxEV_1s1m(kk) = max(eig(Q_dmss_noNorm));
end

for kk = 1:length(points_pos)
    [~,posidx_2(1)] = min(abs(linterp_shift-points_pos(kk,1)));
    [~,posidx_2(2)] = min(abs(linterp_shift-points_pos(kk,2)));
    
    v_dmss_2s1m = v(:,2:nChannels);
    Q_dmss_2s1m = v_dmss_2s1m'*(Q_tip)*v_dmss_2s1m;
    maxEV_2s1m(kk) = max(eig(Q_dmss_2s1m));
    
    v_dmss_2s2m = v(:,3:nChannels);
    Q_dmss_2s2m = v_dmss_2s2m'*(Q_tip)*v_dmss_2s2m;
    
    
end

for kk = 1:length(points_3_pos)
    [~,posidx_3(1)] = min(abs(linterp_shift-points_3_pos(kk,1)));
    [~,posidx_3(2)] = min(abs(linterp_shift-points_3_pos(kk,2)));
    [~,posidx_3(3)] = min(abs(linterp_shift-points_3_pos(kk,3)));%Sets points in relation to phantom-air border
end

if nSensors == 2 || nSensors == 3
    [~,posidx(2)] = min(abs(linterp_shift-points_w(kk,2)));
end
if nSensors == 3
    [~,posidx(3)] = min(abs(linterp_shift-points_w(kk,3)));%Sets points in relation to phantom-air border
end

%         plot_pos = (posidx-pedge_ind).*st; % Calculates actual metric distance of the points for plotting
[~,~,v]=svd(J(posidx,:)); % Single Value Decomposition from Surface Current J (wire)
%         sens_pos(kk,:) = plot_pos; % Actual position of sensor vector
v_dmss = v(:,nModes+1:nChannels);
%%
figure;plot(linterp_shift, abs(Jinterp*v)*1000, 'LineWidth', 2)
% axis([-600 800 0 14])
ylabel('Modes applied to Current (mA/W)')
xlabel('Distance along Wire (mm)')
legend('Mode 1', 'Mode 2', 'Mode 3', 'Mode 4', 'Mode 5', 'Mode 6', 'Mode 7', 'Mode 8')

%% Generate quantities to plot
wcSAR_full = abs(maxEV_fullQ_noNorm)*1e-12;
deepestSens_wcSAR_1s1m = abs(maxEV_redQ_1s1m_noNorm)*1e-12;

for ii = 2:length(points_w)
    logic_idx = find(points_pos(:,2) == points_w(ii));
    [deepestSens_wcSAR_2s1m(ii), temp_idx_2s1m] = min(abs(maxEV_redQ_2s1m_noNorm(logic_idx))*1e-12);
    [deepestSens_wcSAR_2s2m(ii), temp_idx_2s2m] = min(abs(maxEV_redQ_2s2m_noNorm(logic_idx))*1e-12);
    
    bestPos_idx_2s1m(ii) = logic_idx(temp_idx_2s1m);
    bestPos_idx_2s2m(ii) = logic_idx(temp_idx_2s1m);
    
    [deepestSens_wcSAR_2s(ii), bestMode_2s(ii)] = min([deepestSens_wcSAR_2s1m(ii), deepestSens_wcSAR_2s2m(ii)]);
end


for ii = 3:length(points_w)
    logic_idx = find(points_3_pos(:,3) == points_w(ii));
    [deepestSens_wcSAR_3s1m(ii), temp_idx_3s1m] = min(abs(maxEV_redQ_3s1m_noNorm(logic_idx))*1e-12);
    [deepestSens_wcSAR_3s2m(ii), temp_idx_3s2m] = min(abs(maxEV_redQ_3s2m_noNorm(logic_idx))*1e-12);
    [deepestSens_wcSAR_3s3m(ii), temp_idx_3s3m] = min(abs(maxEV_redQ_3s3m_noNorm(logic_idx))*1e-12);
    
    bestPos_idx_3s1m(ii) = logic_idx(temp_idx_3s1m);
    bestPos_idx_3s2m(ii) = logic_idx(temp_idx_3s2m);
    bestPos_idx_3s3m(ii) = logic_idx(temp_idx_3s3m);
    
    [deepestSens_wcSAR_3s(ii), bestMode_3s(ii)] = min([deepestSens_wcSAR_3s1m(ii), deepestSens_wcSAR_3s2m(ii), deepestSens_wcSAR_3s3m(ii)]);
end

actualShim_SAR_fullSys(1:npoints) = abs(wout(:,1)'*Q_tip*wout(:,1));%./b1_normVal;
% actualShim_SAR_fullSys(1:npoints) = abs(w'*Q_tip*w);%./b1_normVal;
for ii = 1:npoints
    actualShim_SAR_1s1m(ii) = abs(w1s1m(:,ii)'*Q_tip*w1s1m(:,ii));%./b1weights_1s1m(ii);
    
    if ii >= 2
        actualShim_SAR_2s1m(ii) = abs(w2s1m(:,bestPos_idx_2s1m(ii))'*Q_tip*w2s1m(:,bestPos_idx_2s1m(ii)));%./b1weights_2s1m(bestPos_idx_2s1m(ii));
        actualShim_SAR_2s2m(ii) = abs(w2s2m(:,bestPos_idx_2s2m(ii))'*Q_tip*w2s2m(:,bestPos_idx_2s2m(ii)));%./b1weights_2s2m(bestPos_idx_2s2m(ii));
    end
    
    if ii >= 3
        actualShim_SAR_3s1m(ii) = abs(w3s1m(:,bestPos_idx_3s1m(ii))'*Q_tip*w3s1m(:,bestPos_idx_3s1m(ii)));%./b1weights_3s1m(bestPos_idx_3s1m(ii));
        actualShim_SAR_3s2m(ii) = abs(w3s2m(:,bestPos_idx_3s2m(ii))'*Q_tip*w3s2m(:,bestPos_idx_3s2m(ii)));%./b1weights_3s2m(bestPos_idx_3s2m(ii));
        actualShim_SAR_3s3m(ii) = abs(w3s3m(:,bestPos_idx_3s3m(ii))'*Q_tip*w3s3m(:,bestPos_idx_3s3m(ii)));%./b1weights_3s3m(bestPos_idx_3s3m(ii));
    end
end


%% Do the Normalised Plot with all cases
figure;
plot(points_w, wcSAR_full/b1_normVal_perfKnowledge(1), 'LineWidth', 3); hold on
scatter(points_w,deepestSens_wcSAR_1s1m./b1weights_1s1m,'o','filled', 'LineWidth',2)
scatter(points_w(2:end),deepestSens_wcSAR_2s1m(2:end)./b1weights_2s1m(bestPos_idx_2s1m(2:npoints)), 'o','filled', 'LineWidth',2)
scatter(points_w(2:end),deepestSens_wcSAR_2s2m(2:end)./b1weights_2s2m(bestPos_idx_2s2m(2:npoints)), 'o','filled', 'LineWidth',2)
scatter(points_w(3:end),deepestSens_wcSAR_3s1m(3:end)./b1weights_3s1m(bestPos_idx_3s1m(3:npoints)), 'o','filled', 'LineWidth',2)
scatter(points_w(3:end),deepestSens_wcSAR_3s2m(3:end)./b1weights_3s2m(bestPos_idx_3s2m(3:npoints)), 'o','filled', 'LineWidth',2)
scatter(points_w(3:end),deepestSens_wcSAR_3s3m(3:end)./b1weights_3s3m(bestPos_idx_3s3m(3:npoints)), 'o','filled', 'LineWidth',2)
grid on

set(gca,'FontSize',16)
legend('Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('Normalised Worst Case SAR')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')

%% Do the non-normalised Plot with all cases

figure;
plot(points_w, wcSAR_full*1e12, 'LineWidth', 3); hold on
scatter(points_w,deepestSens_wcSAR_1s1m*1e12,'o','filled', 'LineWidth',2)
scatter(points_w(2:end),deepestSens_wcSAR_2s1m(2:end)*1e12, 'o','filled', 'LineWidth',2)
scatter(points_w(2:end),deepestSens_wcSAR_2s2m(2:end)*1e12, 'o','filled', 'LineWidth',2)
scatter(points_w(3:end),deepestSens_wcSAR_3s1m(3:end)*1e12, 'o','filled', 'LineWidth',2)
scatter(points_w(3:end),deepestSens_wcSAR_3s2m(3:end)*1e12, 'o','filled', 'LineWidth',2)
scatter(points_w(3:end),deepestSens_wcSAR_3s3m(3:end)*1e12, 'o','filled', 'LineWidth',2)
grid on

set(gca,'FontSize',16)
legend('Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('Worst Case SAR')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/W')
xlabel('Deepest Sensor Position')

%% Do the B1+/W plots
figure;
b1_normVal_plot(1:length(points_w)) = b1_normVal_perfKnowledge(1);
plot(points_w, b1_normVal_plot*1e12, 'LineWidth', 3); hold on
scatter(points_w,b1weights_1s1m*1e12,'o','filled', 'LineWidth',2)
scatter(points_w(2:end),b1weights_2s1m(bestPos_idx_2s1m(2:npoints))*1e12, 'o','filled', 'LineWidth',2)
scatter(points_w(2:end),b1weights_2s2m(bestPos_idx_2s2m(2:npoints))*1e12, 'o','filled', 'LineWidth',2)
scatter(points_w(3:end),b1weights_3s1m(bestPos_idx_3s1m(3:npoints))*1e12, 'o','filled', 'LineWidth',2)
scatter(points_w(3:end),b1weights_3s2m(bestPos_idx_3s2m(3:npoints))*1e12, 'o','filled', 'LineWidth',2)
scatter(points_w(3:end),b1weights_3s3m(bestPos_idx_3s3m(3:npoints))*1e12, 'o','filled', 'LineWidth',2)
grid on

set(gca,'FontSize',16)
legend('Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('Capacity to generate B1+ After Shim')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('\muT^2/W')
xlabel('Deepest Sensor Position')

%% Do the best case modes plot
figure
plot(points_w, wcSAR_full, 'LineWidth', 3); hold on
scatter(points_w,deepestSens_wcSAR_1s1m,'o','filled', 'LineWidth',2)
scatter(points_w(bestMode_2s == 1),deepestSens_wcSAR_2s(bestMode_2s == 1), 'd','filled', 'LineWidth',2)
scatter(points_w(bestMode_2s == 2),deepestSens_wcSAR_2s(bestMode_2s == 2), 'd','filled', 'LineWidth',2)
scatter(points_w(bestMode_3s == 1),deepestSens_wcSAR_3s(bestMode_3s == 1), 'sq','filled', 'LineWidth',2)
scatter(points_w(bestMode_3s == 2),deepestSens_wcSAR_3s(bestMode_3s == 2), 'sq','filled', 'LineWidth',2)
scatter(points_w(bestMode_3s == 3),deepestSens_wcSAR_3s(bestMode_3s == 3), 'sq','filled', 'LineWidth',2)
grid on

set(gca,'FontSize',16)

legend('Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('Normalised Worst Case SAR')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')

%% DO THE ACTUAL SHIM PLOT
   
figure;
plot(points_w, actualShim_SAR_fullSys, 'LineWidth', 3); hold on
scatter(points_w,actualShim_SAR_1s1m,'o','filled', 'LineWidth',2)
scatter(points_w(2:end),actualShim_SAR_2s1m(2:end), 'o','filled', 'LineWidth',2)
scatter(points_w(2:end),actualShim_SAR_2s2m(2:end), 'o','filled', 'LineWidth',2)
scatter(points_w(3:end),actualShim_SAR_3s1m(3:end), 'o','filled', 'LineWidth',2)
scatter(points_w(3:end),actualShim_SAR_3s2m(3:end), 'o','filled', 'LineWidth',2)
scatter(points_w(3:end),actualShim_SAR_3s3m(3:end), 'o','filled', 'LineWidth',2)
grid on

set(gca,'FontSize',16)

legend('Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('SAR After Shimming')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position (mm)')
