testQ = Q_reshaped(24:41, 24:41, 288:290,:,:);
testQ = permute(testQ, [4 5 1 2 3]);
testQ = testQ(:,:,:);
testQ = permute(testQ, [3 1 2]);

a_max = 0;
ii_max = 0;
for ii = 1:1600
    a = max(eig(squeeze(testQ(ii,:,:))));
    if a > a_max
        a_max = a;
        ii_max = ii;
    end
end

%%

for ii = 1:984592
    max_eig(ii) = max(eig(squeeze(Q(ii,:,:))));
end
max_eig_reshaped = reshape(max_eig, sz);

figure;distributionGenerator(abs(squeeze(max_eig_reshaped(:,31,260:end))).*scaling_factor, xaxis, zaxis(260:end), 1, 0, 90, 'nearest');

%%
a_max = 0;
ii_max = 0;
for ii = 1:984592
    a(ii) = max(eig(squeeze(Q(ii,:,:))));
    if a(ii) > a_max
        a_max = a(ii);
        ii_max = ii;
    end
end
a = reshape(a, sz);

%%
figure;
jetblack = jet(10000);
jetblack(1,:) = [0,0,0];
colormap(jetblack)
imagesc(abs(squeeze(a(:,31,260:end))))
% testA = distributionGenerator(abs(squeeze(a(:,31,260:end))).*scaling_factor, xaxis, zaxis(260:end), 1, 0, 90, 'nearest');
