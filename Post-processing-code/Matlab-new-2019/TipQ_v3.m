function [v_all, w1, witer, avgB1, err] = TipQ_v3(nModes, nChannels, bestIdx, J, maskMLS, sRB1r,amp_con,s_f, lambda, text)
    [~,~,v]=svd(J(bestIdx,:)); % Single Value Decomposition from Surface Current J (wire)
%         sens_pos(kk,:) = plot_pos; % Actual position of sensor vector
    v_dmss = v(:,nModes+1:nChannels); % Creates Dark Mode Subspace (dmss)

    v_all(:,:) = v;
        
%     scaling_factor = 1e-6/abs(mean(sRB1r(:)));
    a_W = 1e10;
    for jj = 1:50
        [witer(:,jj),~] =MLS_modeshim_max_amp(sRB1r*1e6,v_dmss,s_f, maskMLS,amp_con*ones(nChannels,1),[],lambda,20,0.1,0.001, text);
        
        if sum(abs(witer(:,jj)).^2) < a_W
            w1 = witer(:,jj);
            a_W = sum(abs(witer(:,jj)).^2);
        end
%         if abs(witer(:,jj)'*Q_tip*witer(:,jj)) < a_W
%             w1 = witer(:,jj);
%         	a_W = abs(witer(:,jj)'*Q_tip*witer(:,jj));
%         end
    end    
    [avgB1,~,err] = applyShim(sRB1r,w1,nChannels);
end