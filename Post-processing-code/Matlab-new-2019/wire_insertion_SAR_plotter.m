insert_l = [314, 550,641,743,790,847,875,903,928, 953, 1003];
peak_SAR1g = [0.04, 0.29, 0.47, 1.16, 1.77,2.29,2.78, 3.37,3.36, 3.1, 3.2];

figurent;

plot(insert_l(5:end),peak_SAR1g(5:end),'-.o', 'LineWidth', 2); hold on; %Organs
plot(insert_l(1:5), peak_SAR1g(1:5),'-.o', 'LineWidth', 2); hold on; %Artery+Heart

scatter(875, 1.94, 'LineWidth',2); %Fat point

plot(insert_l(4),peak_SAR1g(4),'o', 'LineWidth', 2,'MarkerSize', 12); hold on; %Original Config

plot([875, 875], [1.94, 2.78], '-.', 'Color',[0.9290 0.6940 0.1250]);
grid on; set(gca, 'FontSize', 14); xlabel('Insertion Depth (mm)'); ylabel('Peak 1g SAR'); title('Body Coil - Guidewire Insertion Depth')
legend({'Other Organs','Arteries & Heart', 'Fat', 'Fully Inserted Configuration'}, 'Location', 'northwest')