max_val =  60; 
    %deepestSens_wcSAR_3s3m(3:end)./b1weights_3s3m(3:end);
    %deepestSens_wcSAR_1s1m./b1weights_1s1m; %
    %wcSAR_full/b1_normVal_perfKnowledge(1);%deepestSens_wcSAR_1s1m./b1weights_1s1m;
    %
    %deepestSens_wcSAR_3s1m(3:end)./b1weights_3s1m(bestPos_idx_3s1m(3:npoints));...
    %...
    %...
    %deepestSens_wcSAR_2s1m(2:end)./b1weights_2s1m(bestPos_idx_2s1m(2:npoints)); % 

figurent;
coil_reg = 400; %200;%435;% 

pth = [coil_reg 0; 800 0; 800 max(max_val);...
    coil_reg max(max_val)]; %SHORT COIL


l_coil_reg = 200;%435; %400; %

pth1 = [l_coil_reg 0; coil_reg 0; coil_reg max(max_val);...
    l_coil_reg max(max_val)]; %LONG COIL

faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'red', 'FaceAlpha',.18, 'EdgeColor','none');


hold on

h(1) = plot(points_1s(2:end),1e12.*deepestSens_wcSAR_2s2m_sc(2:end).*b1weights_2s2m_sc(2:end), '-.o', 'LineWidth',2);
set(h(1), 'MarkerFaceColor', get(h(1), 'color'));
% h(2) = plot(points_w(3:end),deepestSens_wcSAR_3s2m_sc(3:end)./b1weights_3s2m_sc(3:end), '-.o', 'LineWidth',2);
% set(h(2), 'MarkerFaceColor', get(h(2), 'color'));

patch('Faces', faces, 'Vertices',pth1,'FaceColor', 'blue', 'FaceAlpha',.18, 'EdgeColor','none');
h(3) = plot(points_1s(2:end),1e12.*deepestSens_wcSAR_2s2m_lc(2:end).*b1weights_2s2m_lc(2:end), '-.o', 'LineWidth',2);
set(h(3), 'MarkerFaceColor', get(h(3), 'color'));
% h(4) = plot(points_w(3:end),deepestSens_wcSAR_3s2m_lc(3:end)./b1weights_3s2m_lc(3:end), '-.o', 'LineWidth',2);
% set(h(4), 'MarkerFaceColor', get(h(4), 'color'));

grid on

set(gca,'FontSize',14)
legend({'Short Coil Region','Short Coil 2s2m','Long Coil Region','Long Coil 2s2m'},'Location','northeast','AutoUpdate','off');
title('Worst Case SAR per \muT^2, Shimmed B_1^+')%'Limited Knowledge of J - Short Coil Case')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')
axis([-100 points_1s(end) 0 max(max_val)])

uistack(h(1),'top')
uistack(h(2),'top')
% uistack(h(3),'top')
set(gcf, 'Position',  [100, 100, 700, 450])