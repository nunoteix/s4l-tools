ll = 1;
b1weights_fullK_all = squeeze(sum(abs(witer(:,opt_l(1),:,1).^2),1));
b1weights_1s1m_all = squeeze(sum(abs(w1s1m_all).^2,1));
b1weights_2s1m_all = squeeze(sum(abs(w2s1m_all).^2,1));
b1weights_2s2m_all = squeeze(sum(abs(w2s2m_all).^2,1));
b1weights_3s1m_all = squeeze(sum(abs(w3s1m_all).^2,1));
b1weights_3s2m_all = squeeze(sum(abs(w3s2m_all).^2,1));
b1weights_3s3m_all = squeeze(sum(abs(w3s3m_all).^2,1));

nwcSAR_fullK = b1weights_fullK_all*wcSAR_full.*1e12;

for ii = 1:length(points_1s)
    nwcSAR_1s1m(:,ii) = b1weights_1s1m_all(:,ii).*deepestSens_wcSAR_1s1m(ii).*1e12;
    nwcSAR_2s1m(:,ii) = b1weights_2s1m_all(:,ii).*deepestSens_wcSAR_2s1m(ii).*1e12;
    nwcSAR_2s2m(:,ii) = b1weights_2s2m_all(:,ii).*deepestSens_wcSAR_2s2m(ii).*1e12;
    nwcSAR_3s1m(:,ii) = b1weights_3s1m_all(:,ii).*deepestSens_wcSAR_3s1m(ii).*1e12;
    nwcSAR_3s2m(:,ii) = b1weights_3s2m_all(:,ii).*deepestSens_wcSAR_3s2m(ii).*1e12;
    nwcSAR_3s3m(:,ii) = b1weights_3s3m_all(:,ii).*deepestSens_wcSAR_3s3m(ii).*1e12;
end

max_val = 260;%max(nwcSAR_fullK);  %1e12.*deepestSens_wcSAR_1s1m.*max(b1weights_1s1m_all);
% 

figurent;
coil_reg = 400; %435; %200;%

pth = [coil_reg 0; 800 0; 800 max(max_val);...
    coil_reg max(max_val)]; %SHORT COIL

faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'red', 'FaceAlpha',.18, 'EdgeColor','none');
% a1 = axes();
hold on

boxplot(nwcSAR_1s1m, 'Positions', round(points_1s), 'Labels', round(points_1s), 'Symbol', '',...
    'Colors', [0.8500, 0.3250, 0.0980], 'LabelOrientation', 'inline')
boxplot(nwcSAR_2s1m(:,2:end), 'Positions', round(points_1s(2:end)), 'Labels', round(points_1s(2:end)), 'Symbol', '',...
    'Colors', [0.9290, 0.6940, 0.1250], 'LabelOrientation', 'inline')
boxplot(nwcSAR_3s1m(:,3:end), 'Positions', round(points_1s(3:end)), 'Labels', round(points_1s(3:end)), 'Symbol', '',...
    'Colors', [0.4940, 0.1840, 0.5560], 'LabelOrientation', 'inline')
boxplot(nwcSAR_2s2m(:,2:end), 'Positions', round(points_1s(2:end)), 'Labels', round(points_1s(2:end)), 'Symbol', '',...
    'Colors', [0.4660, 0.6740, 0.1880], 'LabelOrientation', 'inline')
boxplot(nwcSAR_3s2m(:,3:end), 'Positions', round(points_1s(3:end)), 'Labels', round(points_1s(3:end)), 'Symbol', '',...
    'Colors', [0.3010, 0.7450, 0.9330], 'LabelOrientation', 'inline')
% boxplot(nwcSAR_3s3m(:,3:end), 'Positions', round(points_1s(3:end)), 'Labels', round(points_1s(3:end)), 'Symbol', '',...
%     'Colors', [0.6350, 0.0780, 0.1840], 'LabelOrientation', 'inline')
boxplot(nwcSAR_fullK, 'Positions', round(points_1s), 'Labels', round(points_1s), 'Symbol', '',...
    'Colors', [0, 0.4470, 0.7410], 'LabelOrientation', 'inline'); 
grid on

set(gca,'FontSize',14)
legend({'Coil Region','Full System', '1s1m', '2s1m', '3s1m','2s2m','3s2m','3s3m'},'Location','northeast','AutoUpdate','off');
title('SAR_{nwc}')%'Limited Knowledge of J - Short Coil Case')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')
axis([-100 800 0 max(max_val)])

% uistack(h(1),'top')
% uistack(h(2),'top')
% uistack(h(3),'top')
set(gcf, 'Position',  [100, 100, 700, 450])

% % For PHANTOM
% a2 = axes();
% a2.Position = [0.17 0.7 0.5 0.15]; % xlocation, ylocation, xsize, ysize
% h(1) = plot(points_w(2:end),deepestSens_wcSAR_2s2m(2:end)./b1weights_2s2m(2:end), '-.o','Color',...
%     [0.4940 0.1840 0.5560], 'LineWidth',2,'MarkerFaceColor', [0.4940 0.1840 0.5560]);
%     hold on;
% h(2) = plot(points_w(3:end),deepestSens_wcSAR_3s2m(3:end)./b1weights_3s2m(3:end), '-.o','Color',...
%     [0.3010 0.7450 0.9330], 'LineWidth',2,'MarkerFaceColor',[0.3010 0.7450 0.9330]);
% h(3) = plot(points_w(3:end),deepestSens_wcSAR_3s3m(3:end)./b1weights_3s3m(3:end), '-.o','Color',...
%     [0.6350 0.0780 0.1840], 'LineWidth',1);
% % axis off
% set(gca,'FontSize',14)
% axis([-100 points_w(end) 0 7])
% grid on
%% DO THE ACTUAL SHIM PLOT
for jj = 1:niters
    actualShim_SAR_fullK_all(jj,1:npoints) = abs(witer(:,opt_l(1),jj,1)'*Q_tip*witer(:,opt_l(1),jj,1));%./b1_normVal;
end
% actualShim_SAR_fullSys(1:npoints) = abs(w'*Q_tip*w);%./b1_normVal;
for jj = 1:50
    for ii = 1:npoints
        actualShim_SAR_1s1m_all(jj,ii) = abs(w1s1m_all(:,jj,ii)'*Q_tip*w1s1m_all(:,jj,ii));%./b1weights_1s1m(ii);
        
        if ii >= 2
            actualShim_SAR_2s1m_all(jj,ii) = abs(w2s1m_all(:,jj,ii)'*Q_tip*w2s1m_all(:,jj,ii));%./b1weights_2s1m(bestPos_idx_2s1m(ii));
            actualShim_SAR_2s2m_all(jj,ii) = abs(w2s2m_all(:,jj,ii)'*Q_tip*w2s2m_all(:,jj,ii));%./b1weights_2s2m(bestPos_idx_2s2m(ii));
        end
        
        if ii >= 3
            actualShim_SAR_3s1m_all(jj,ii) = abs(w3s1m_all(:,jj,ii)'*Q_tip*w3s1m_all(:,jj,ii));%./b1weights_3s1m(bestPos_idx_3s1m(ii));
            actualShim_SAR_3s2m_all(jj,ii) = abs(w3s2m_all(:,jj,ii)'*Q_tip*w3s2m_all(:,jj,ii));%./b1weights_3s2m(bestPos_idx_3s2m(ii));
            actualShim_SAR_3s3m_all(jj,ii) = abs(w3s3m_all(:,jj,ii)'*Q_tip*w3s3m_all(:,jj,ii));%./b1weights_3s3m(bestPos_idx_3s3m(ii));
        end
    end
end
figurent;

max_val = max(actualShim_SAR_1s1m_all);%max(actualShim_SAR_fullK_all);%actualShim_SAR_3s3m(3:end;
        % actualShim_SAR_2s2m(2:end);%

pth = [coil_reg 0; 800 0; 800 max(max_val); coil_reg max(max_val)];
faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'red', 'FaceAlpha',.18,'EdgeColor','none');

hold on

boxplot(actualShim_SAR_1s1m_all, 'Positions', round(points_1s), 'Labels', round(points_1s), 'Symbol', '',...
    'Colors', [0.8500, 0.3250, 0.0980], 'LabelOrientation', 'inline')
boxplot(actualShim_SAR_2s1m_all(:,2:end), 'Positions', round(points_1s(2:end)), 'Labels', round(points_1s(2:end)), 'Symbol', '',...
    'Colors', [0.9290, 0.6940, 0.1250], 'LabelOrientation', 'inline')
boxplot(actualShim_SAR_3s1m_all(:,3:end), 'Positions', round(points_1s(3:end)), 'Labels', round(points_1s(3:end)), 'Symbol', '',...
    'Colors', [0.4940, 0.1840, 0.5560], 'LabelOrientation', 'inline')
boxplot(actualShim_SAR_2s2m_all(:,2:end), 'Positions', round(points_1s(2:end)), 'Labels', round(points_1s(2:end)), 'Symbol', '',...
    'Colors', [0.4660, 0.6740, 0.1880], 'LabelOrientation', 'inline')
boxplot(actualShim_SAR_3s2m_all(:,3:end), 'Positions', round(points_1s(3:end)), 'Labels', round(points_1s(3:end)), 'Symbol', '',...
'Colors', [0.3010, 0.7450, 0.9330], 'LabelOrientation', 'inline')
% boxplot(actualShim_SAR_3s3m_all(:,3:end), 'Positions', round(points_1s(3:end)), 'Labels', round(points_1s(3:end)), 'Symbol', '',...
%     'Colors', [0.6350, 0.0780, 0.1840], 'LabelOrientation', 'inline')
boxplot(actualShim_SAR_fullK_all, 'Positions', round(points_1s), 'Labels', round(points_1s), 'Symbol', '',...
    'Colors', [0, 0.4470, 0.7410], 'LabelOrientation', 'inline'); 
grid on

set(gca,'FontSize',14)

legend('Coil Region','Full System', '1s1m', '2s1m', '3s1m','2s2m','3s2m','3s3m');
title('Expected SAR per \muT^2, Shimmed B_1^+')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position (mm)')

axis([-100 points_1s(end) 0 max(max_val)])

set(gcf, 'Position',  [100, 100, 700, 450])


%% Do the B1+/W plots without ^2
figurent;
b1_normVal_plot(1:length(points_1s)) = b1_normVal_fullK(1);

max_val = max(ones(1,npoints)./sqrt(b1weights_fullK)); %b1weights_1s1m*1e12; % 
min_val = min(1./sqrt(b1weights_3s3m(2:npoints)));

pth = [coil_reg 0; 800 0; 800 max(max_val); coil_reg max(max_val)];
faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'red', 'FaceAlpha',.18, 'EdgeColor','none');

hold on

plot(points_1s, ones(1,npoints)./sqrt(b1weights_fullK), 'LineWidth', 3); hold on
plot(points_1s, 1./sqrt(b1weights_1s1m),'-.o', 'LineWidth',2)
plot(points_1s(2:end), 1./sqrt(b1weights_2s1m(2:npoints)), '-.o', 'LineWidth',2)
plot(points_1s(2:end), 1./sqrt(b1weights_2s2m(2:npoints)), '-.o', 'LineWidth',2)
plot(points_1s(3:end), 1./sqrt(b1weights_3s1m(3:npoints)), '-.o', 'LineWidth',2)
plot(points_1s(3:end), 1./sqrt(b1weights_3s2m(3:npoints)), '-.o', 'LineWidth',2)
plot(points_1s(3:end), 1./sqrt(b1weights_3s3m(3:npoints)), '-.o', 'LineWidth',2)
grid on

set(gca,'FontSize',16)
% legend('Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('Capacity to generate homogeneous B_1^+')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('\muT/\surd{W}')
xlabel('Deepest Sensor Position')

axis([-100 points_1s(end) min(min_val) max(max_val)])

set(gcf, 'Position',  [100, 100, 700, 450])

%% Do the best case modes plot
figurent
plot(points_w, wcSAR_full, 'LineWidth', 3); hold on
scatter(points_w,deepestSens_wcSAR_1s1m,'o','filled', 'LineWidth',2)
scatter(points_w(bestMode_2s == 1),deepestSens_wcSAR_2s(bestMode_2s == 1), 'd','filled', 'LineWidth',2)
scatter(points_w(bestMode_2s == 2),deepestSens_wcSAR_2s(bestMode_2s == 2), 'd','filled', 'LineWidth',2)
scatter(points_w(bestMode_3s == 1),deepestSens_wcSAR_3s(bestMode_3s == 1), 'sq','filled', 'LineWidth',2)
scatter(points_w(bestMode_3s == 2),deepestSens_wcSAR_3s(bestMode_3s == 2), 'sq','filled', 'LineWidth',2)
scatter(points_w(bestMode_3s == 3),deepestSens_wcSAR_3s(bestMode_3s == 3), 'sq','filled', 'LineWidth',2)
grid on

set(gca,'FontSize',16)

legend('Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('Normalised Worst Case SAR')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')
