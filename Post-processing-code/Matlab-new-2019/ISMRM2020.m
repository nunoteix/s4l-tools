ncoils = 8;

% path_to_results = 'G:\Huygens Simulations\Phantom\Extracted Results\'; %phantom
path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';

% simulation_name = '\RapidCoils_Phantom_Guidewire_2019_06_19_'; %Phantom
simulation_name = '\RapidCoils_Duke_Guidewire_2019_06_23_'; %Duke, Short Coil
% simulation_name = '\LongRapidCoils_Duke_Guidewire_2019_06_24_'; %Duke, Long Coil
% 
% load([path_to_results 'Density' simulation_name 'Density'])
% load([path_to_results 'EM Current' simulation_name 'EMCurrent'])
load([path_to_results 'Grid' simulation_name 'Axes'])
% 
% xaxis =(XAxis(1:end-1)+XAxis(2:end))'/2;
% yaxis =(YAxis(1:end-1)+YAxis(2:end))'/2;
% zaxis =(ZAxis(1:end-1)+ZAxis(2:end))'/2;
% sz = [length(XAxis)-1 length(YAxis)-1 length(ZAxis)-1];
% fsz = length(xaxis)*length(yaxis)*length(zaxis);
% 
% for ii = 1:ncoils
%     load([path_to_results 'B1 Field' simulation_name 'B1Fields_C' num2str(ii)])
% end
% B1 = cat(3, B1_1, B1_2, B1_3, B1_4, B1_5, B1_6, B1_7, B1_8);
% clear B1_1 B1_2 B1_3 B1_4 B1_5 B1_6 B1_7 B1_8
% B1p = squeeze(B1(:,1,:));
% B1p_s = sum(reshape(B1p, [sz 8]),4);
% 
% for  ii = 1:ncoils
%     load([path_to_results 'E Field' simulation_name 'EFields_C' num2str(ii)])
% end
% E = cat(3, E_1, E_2, E_3, E_4, E_5, E_6, E_7, E_8);
% clear E_1 E_2 E_3 E_4 E_5 E_6 E_7 E_8
% Eabs = squeeze(sqrt(E(:,1,:).^2 + E(:,2,:).^2 + E(:,3,:).^2));

for ii = 1:ncoils
    load([path_to_results 'Point Q Matrices' simulation_name 'PointQMatrices_pt' num2str(ii-1)])
end
Q_thick = cat(3, QMatrix_pt0,QMatrix_pt1,QMatrix_pt2,QMatrix_pt3,QMatrix_pt4,QMatrix_pt5,QMatrix_pt6,QMatrix_pt7);
clear QMatrix_pt0 QMatrix_pt1 QMatrix_pt2 QMatrix_pt3 QMatrix_pt4 QMatrix_pt5 QMatrix_pt6 QMatrix_pt7

%%
ncoils = 8;

% path_to_results = 'G:\Huygens Simulations\Phantom\Extracted Results\'; %phantom
path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';

% simulation_name = '\RapidCoils_Phantom_Guidewire_Thin_2019_10_23_'; %Phantom Thin Wire
simulation_name = '\RapidCoils_Duke_Guidewire_Thin_2019_10_23_'; %Duke Thin Wire, Short Coil
% simulation_name = '\LongRapidCoils_Duke_Guidewire_Thin_2019_10_23_'; %Duke Thin Wire, Long Coil
% 
% load([path_to_results 'Density' simulation_name 'Density'])
% load([path_to_results 'EM Current' simulation_name 'EMCurrent'])
load([path_to_results 'Grid' simulation_name 'Axes'])

% xaxis =(XAxis(1:end-1)+XAxis(2:end))'/2;
% yaxis =(YAxis(1:end-1)+YAxis(2:end))'/2;
% zaxis =(ZAxis(1:end-1)+ZAxis(2:end))'/2;
% sz = [length(XAxis)-1 length(YAxis)-1 length(ZAxis)-1];
% fsz = length(xaxis)*length(yaxis)*length(zaxis);

% for ii = 1:ncoils
%     load([path_to_results 'B1 Field' simulation_name 'B1Fields_C' num2str(ii)])
% end
% B1 = cat(3, B1_1, B1_2, B1_3, B1_4, B1_5, B1_6, B1_7, B1_8);
% clear B1_1 B1_2 B1_3 B1_4 B1_5 B1_6 B1_7 B1_8
% B1p = squeeze(B1(:,1,:));
% B1p_s = sum(reshape(B1p, [sz 8]),4);
% 
% for  ii = 1:ncoils
%     load([path_to_results 'E Field' simulation_name 'EFields_C' num2str(ii)])
% end
% E = cat(3, E_1, E_2, E_3, E_4, E_5, E_6, E_7, E_8);
% clear E_1 E_2 E_3 E_4 E_5 E_6 E_7 E_8
% Eabs = squeeze(sqrt(E(:,1,:).^2 + E(:,2,:).^2 + E(:,3,:).^2));

for ii = 1:ncoils
    load([path_to_results 'Point Q Matrices' simulation_name 'PointQMatrices_pt' num2str(ii-1)])
end
Q_thin = cat(3, QMatrix_pt0,QMatrix_pt1,QMatrix_pt2,QMatrix_pt3,QMatrix_pt4,QMatrix_pt5,QMatrix_pt6,QMatrix_pt7);
clear QMatrix_pt0 QMatrix_pt1 QMatrix_pt2 QMatrix_pt3 QMatrix_pt4 QMatrix_pt5 QMatrix_pt6 QMatrix_pt7


%%

xaxis_thick =(XAxis_thick(1:end-1)+XAxis_thick(2:end))'/2;
yaxis_thick =(YAxis_thick(1:end-1)+YAxis_thick(2:end))'/2;
zaxis_thick =(ZAxis_thick(1:end-1)+ZAxis_thick(2:end))'/2;
sz_thick = [length(XAxis_thick)-1 length(YAxis_thick)-1 length(ZAxis_thick)-1];
fsz_thick = length(xaxis_thick)*length(yaxis_thick)*length(zaxis_thick);


xaxis_thin =(XAxis_thin(1:end-1)+XAxis_thin(2:end))'/2;
yaxis_thin =(YAxis_thin(1:end-1)+YAxis_thin(2:end))'/2;
zaxis_thin =(ZAxis_thin(1:end-1)+ZAxis_thin(2:end))'/2;
sz_thin = [length(XAxis_thin)-1 length(YAxis_thin)-1 length(ZAxis_thin)-1];
fsz_thin = length(xaxis_thin)*length(yaxis_thin)*length(zaxis_thin);


for ii = 1:length(Q_thin)
    maxEV_allQ_thin(ii) = abs(max(eig(squeeze(Q_thin(ii,:,:)))));
end

for ii = 1:length(Q_thick)
    maxEV_allQ_thick(ii) = abs(max(eig(squeeze(Q_thick(ii,:,:)))));
end

%%
wcSAR_thin = reshape(maxEV_allQ_thin, sz_thin);
wcSAR_thick = reshape(maxEV_allQ_thick, sz_thick);

wcSAR_thin_lg = Gen3D(wcSAR_thin, xaxis_thin, yaxis_thin, zaxis_thin, 175, 175, 600);
wcSAR_thick_lg = Gen3D(wcSAR_thick, xaxis_thick, yaxis_thick, zaxis_thick, 175, 175, 600);

diff_wcSAR = wcSAR_thin_lg - wcSAR_thick_lg;

wcSAR_thin_lg_mip = squeeze(max(wcSAR_thin_lg, [], 2));
wcSAR_thick_lg_mip = squeeze(max(wcSAR_thick_lg, [], 2));
diff_wcSAR_mip = squeeze(max(abs(diff_wcSAR), [], 3 ));
%%
figurent;colormap(inferno())
imagesc(flipud(wcSAR_thick_lg_mip));
colorbar

figurent;colormap(inferno())
imagesc(log10(flipud(wcSAR_thin_lg_mip)));

figurent;colormap(inferno())
imagesc(flipud(diff_wcSAR_mip));

figurent;colormap(inferno())
imagesc(flipud(wcSAR_thin_lg_mip));

figurent;colormap(inferno())
imagesc(flipud(wcSAR_thick_lg_mip));


figurent;colormap(inferno())
imagesc(flipud(abs(diff_wcSAR_mip)));

figurent; colormap(inferno())
%%
ii = 548;
    
imagesc(imrotate(flipud(squeeze(diff_wcSAR(:,:,ii))),90));
caxis([-0.05 0.05])
colorbar

%% BREAK HERE TO TRANSITION TO PHANTOM SCRIPTS
% Grid
load('G:\Wire Voxelization\Phantom\Extracted Results\Grid\PlaneWave_Phantom_Thin_Axes.mat')
xaxis =(XAxis(1:end-1)+XAxis(2:end))'/2;
yaxis =(YAxis(1:end-1)+YAxis(2:end))'/2;
zaxis =(ZAxis(1:end-1)+ZAxis(2:end))'/2;
sz = [length(XAxis)-1 length(YAxis)-1 length(ZAxis)-1];
fsz = length(xaxis)*length(yaxis)*length(zaxis);

%% Current Distributions
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Thin_EMCurrent.mat')


ltrue(1) = 0;
for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end
% lshifted = ltrue - ltrue(19);
for ii = 1
    Jinterp_ini(ii,:) = interp1(J(ii,:), 1:36/2000:36);
end
linterp = interp1(ltrue,1:36/2000:36);

% points_w = linspace(-100,570,npoints)'; %For Phantom
% pedge = ltrue(21); % For Phantom

points_w = linspace(-100,790,npoints)'; %For Duke
pedge = ltrue(16); % For Duke

linterp_shift = linterp-pedge;
[~, pedge_ind] = min(abs(linterp-pedge));
st = linterp(2)-linterp(1);
kk = 1;
mm = 1;
for ii = 1:length(points_w)
    for jj = ii+1:length(points_w)
        points_pos(kk,1:2) = [points_w(ii) points_w(jj)];
        for ll = jj+1:length(points_w)
                points_3_pos(mm,1:3) = [points_w(ii) points_w(jj) points_w(ll)];
                mm = mm+1;
        end
        kk = kk + 1;
    end
end


Jinterp = permute(Jinterp_ini, [2 1]);

figurent;plot(ltrue,abs(J*1000), 'LineWidth', 2); hold on
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_ThinError_EMCurrent.mat')
plot(ltrue,abs(J*1000),'-.', 'LineWidth', 2); hold on
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Thick_EMCurrent.mat')
plot(ltrue,abs(J*1000),'-.', 'LineWidth', 2); hold on
% load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_ThickTipIn_EMCurrent.mat')
% plot(abs(J*1000)); hold on
% load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_ThickTipEdge_EMCurrent.mat')
% plot(abs(J*1000));

xlabel('Distance along Wire (mm)')
ylabel('Induced Current (mA)')
grid on;
set(gca, 'FontSize', 14)
legend('0mm Diameter', '0mm Diameter', '1mm Diameter',  'Location', 'northwest')%'ThickTipin', 'ThinTipEdge',;

%% SAR Distri
res = 0.0005;
load('G:\Wire Voxelization\Phantom\Extracted Results\SAR\PlaneWave_Phantom_Thin_SAR_C1.mat')
SAR_Thin = Gen3D(reshape(SAR_1, sz), xaxis, yaxis, zaxis, res, res, res);
SAR_Thin_mip = squeeze(max(SAR_Thin, [], 3));
load('G:\Wire Voxelization\Phantom\Extracted Results\SAR\PlaneWave_Phantom_ThinError_SAR_C1.mat')
SAR_ThinError = Gen3D(reshape(SAR_1, sz), xaxis, yaxis, zaxis, res, res, res);
SAR_ThinError_mip = squeeze(max(SAR_ThinError, [], 3));
load('G:\Wire Voxelization\Phantom\Extracted Results\SAR\PlaneWave_Phantom_Thick_SAR_C1.mat')
SAR_Thick = Gen3D(reshape(SAR_1, sz), xaxis, yaxis, zaxis, res, res, res);
SAR_Thick_mip = squeeze(max(SAR_Thick, [], 3));
load('G:\Wire Voxelization\Phantom\Extracted Results\SAR\PlaneWave_Phantom_ThickTipIn_SAR_C1.mat')
SAR_ThickTipIn = Gen3D(reshape(SAR_1, sz), xaxis, yaxis, zaxis, res, res, res);
SAR_ThickTipIn_mip = squeeze(max(SAR_ThickTipIn, [], 3));
load('G:\Wire Voxelization\Phantom\Extracted Results\SAR\PlaneWave_Phantom_ThickTipEdge_SAR_C1.mat')
SAR_ThickTipEdge = Gen3D(reshape(SAR_1, sz), xaxis, yaxis, zaxis, res, res, res);
SAR_ThickTipEdge_mip = squeeze(max(SAR_ThickTipEdge, [], 3));

%%

% figure(5)
colormap(inferno())
ii = 2251;
    
imagesc(xaxis, yaxis, imrotate(flipud(squeeze(test(:,:,ii))),90));
caxis([0 100])
colorbar
%%
test = 100*((SAR_Thin - SAR_ThickTipIn)./SAR_Thin);
test_mip = max(test, [], 3);
figurent;
imagesc(xaxis, yaxis, imrotate(flipud(test_mip),90));
caxis([0 100])

%%
figurent;
imagesc(xaxis, yaxis, imrotate(flipud(log10(SAR_Thick_mip)),90));
caxis([-5 2])
colorbar

%% THIN vs. THICK STRAIGHT WIRE FULLY INSULATED.
figurent;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Straightwire_ThinInsu_EMCurrent.mat')
plot(Axes(3,:), abs(J*1000), 'LineWidth' , 2); hold on
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Straightwire_ThickInsu_EMCurrent.mat')
plot(Axes(3,:), abs(J*1000), 'LineWidth' , 2); hold on;
set(gca, 'FontSize', 14)

xlabel('Distance along the wire')
ylabel('Current (mA)')

legend('Thin - Fully Insulated', 'Thick - Fully Insulated', 'Location', 'northwest')

%% THIN vs. THICK STRAIGHT WIRE UNCAPPED PHANTOM.
figurent;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Straightwire_Thin_EMCurrent.mat')
plot(Axes(3,:), abs(J*1000), 'LineWidth' , 2); hold on;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Straightwire_ThickVol_EMCurrent.mat')
plot(Axes(3,:), abs(J*1000), 'LineWidth' , 2); hold on;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Straightwire_ThickTip_EMCurrent.mat')
plot(Axes(3,:), abs(J*1000), 'LineWidth' , 2); hold on;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Straightwire_Thick_EMCurrent.mat')
plot(Axes(3,:), abs(J*1000), 'LineWidth' , 2); hold on;
set(gca, 'FontSize', 14)

xlabel('Distance along the wire')
ylabel('Current (mA)')

legend('Case 1', 'Case 2', 'Case 3', 'Location', 'northwest')
%% STRAIGHT VS. ANGLED
figurent;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Straightwire_Thick_EMCurrent.mat')
ax = Axes(3,:);
plot(ax, abs(J*1000), 'LineWidth' , 2); hold on;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom 1_Straightwire_ThickAngled_EMCurrent.mat')
plot(ax, abs(J*1000), 'LineWidth' , 2); hold on;
set(gca, 'FontSize', 14)

xlabel('Distance along the wire')
ylabel('Current (mA)')
legend('Case 2', 'Case 5', 'Location', 'northwest')


figurent;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Straightwire_Thin_EMCurrent.mat')
ax = Axes(3,:);
plot(ax, abs(J*1000), 'LineWidth' , 2); hold on;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom 1_Straightwire_ThinAngled_EMCurrent.mat')
plot(ax, abs(J*1000), 'LineWidth' , 2); hold on;
set(gca, 'FontSize', 14)

xlabel('Distance along the wire')
ylabel('Current (mA)')
legend('Case 1', 'Case 4', 'Location', 'northwest')

%% THIN vs. THICK STRAIGHT WIRE UNCAPPED DUKE.
figurent;
load('G:\Huygens Simulations\Duke\Extracted Results\EM Current\RapidCoils_Duke_Guidewire_Thin_2019_10_23_EMCurrent_Error.mat')
plot(ltrue-pedge, abs(J(2,:)*1000), 'LineWidth' , 2); hold on;
load('G:\Huygens Simulations\Duke\Extracted Results\EM Current\RapidCoils_Duke_Guidewire_2019_06_23_EMCurrent.mat')
plot(ltrue-pedge, abs(J(2,:)*1000), 'LineWidth' , 2); hold on;
set(gca, 'FontSize', 14)

xlabel('Distance along the wire')
ylabel('Current (mA)')

legend('100% Thin - Uncapped', '100% Thick - Uncapped', ' Thin + Thick Tip - Uncapped', 'Location', 'northwest')

%% THIN INSULATION vs. THICK INSULATION FOR THICK WIRE UNCAPPED DUKE.
figurent;

load('G:\Huygens Simulations\Duke\Extracted Results\EM Current\RapidCoils_Duke_Guidewire_2019_06_23_EMCurrent.mat')

npoints = 40; ltrue(1) = 0;
for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end
linterp = interp1(ltrue,1:36/2000:36); points_w = linspace(-100,790,npoints)'; pedge = ltrue(16); % For Duke

for ii = 1:8
    subplot(2,4,ii)
    load('G:\Huygens Simulations\Duke\Extracted Results\EM Current\RapidCoils_Duke_Guidewire_2019_06_23_EMCurrent.mat')
    plot(ltrue-pedge, abs(J(ii,:)*1000), 'LineWidth' , 2); hold on;
    load('G:\Huygens Simulations\Duke\Extracted Results\EM Current\RapidCoils_Duke_Guidewire_ThickInsu_2019_06_23_EMCurrent.mat')
    plot(ltrue-pedge, abs(J(ii,:)*1000), 'LineWidth' , 2); hold on;
    set(gca, 'FontSize', 14)

    xlabel('Distance along the wire')
    ylabel('Current (mA)')
    axis([-600 800 0 8])
    legend('Thin Insulation + Vox Errors', 'Thick Insulation', 'Location', 'northwest')
end

%% THIN INSULATION vs. THICK INSULATION FOR THICK WIRE UNCAPPED Phantom.
figurent;

load('G:\Huygens Simulations\Phantom\Extracted Results\EM Current\RapidCoils_Phantom_Guidewire_2019_06_19_EMCurrent.mat')

npoints = 40; ltrue(1) = 0;
for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end
linterp = interp1(ltrue,1:36/2000:36); points_w = linspace(-100,790,npoints)'; pedge = ltrue(16); % For Duke

for ii = 1:8
    subplot(2,4,ii)
    load('G:\Huygens Simulations\Phantom\Extracted Results\EM Current\RapidCoils_Phantom_Guidewire_2019_06_19_EMCurrent.mat')
    plot(ltrue-pedge, abs(J(ii,:)*1000), 'LineWidth' , 2); hold on;
    load('G:\Huygens Simulations\Phantom\Extracted Results\EM Current\RapidCoils_Phantom_Guidewire_ThickInsu_2019_06_19_EMCurrent.mat')
    plot(ltrue-pedge, abs(J(ii,:)*1000), 'LineWidth' , 2); hold on;
    set(gca, 'FontSize', 14)

    xlabel('Distance along the wire')
    ylabel('Current (mA)')
    axis([-600 800 0 11])
    legend('Thin Insulation', 'Thick Insulation', 'Location', 'northwest')
end

%% VOXELIZATION CONVERGENCE BARE WIRE
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_CoreFine0.5_EMCurrent.mat')
ltrue(1) = 0;
for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end

figurent;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_ThinCoreFine2_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_CoreFine2_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mb(1) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_CoreFine1_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mb(2) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_CoreFine0.5_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mb(3) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_CoreFine0.4_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mb(4) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_CoreFine0.3_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mb(5) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_CoreFine0.2_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mb(6) = max(abs(J*1000));


set(gca, 'FontSize', 14)


xlabel('Distance along the wire')
ylabel('Current (mA)')

legend('Inf. Thin Wire', '2mm Grid', '1mm Grid', '0.5mm Grid', '0.4mm Grid', '0.3mm Grid', '0.2mm Grid', 'Location', 'northwest')
set(gcf, 'Position',  [0, 0, 1050, 310])
title('Bare Wire - Grid Resolution')

%%

figurent;
scatter([0.5 0.4 0.3 0.2], mb(3:end), 100, 'filled');
hold on
scatter([2 1], mb(1:2), 100, 'filled');
legend('No Vox Errors', 'Vox Errors');

grid on
set(gca, 'FontSize', 14)
xlabel('Grid Resolution (mm)')
ylabel('Peak Current (mA)')
title('Bare Wire - Grid Resolution')

%% VOXELIZATION CONVERGENCE CAPPED WIRE
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_CoreFine0.5_EMCurrent.mat')
ltrue(1) = 0;
for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end

figurent;
% subplot(121)
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_InsuFine2_EMCurrent.mat')
plot(ltrue, abs(J*1000),'-.', 'LineWidth' , 2); hold on;
mg(1) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_InsuFine1_EMCurrent.mat')
plot(ltrue, abs(J*1000),'-.', 'LineWidth' , 2); hold on;
mg(2) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_InsuFine0.6_EMCurrent.mat')
plot(ltrue, abs(J*1000),'-.', 'LineWidth' , 2); hold on;
mg(3) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_InsuFine0.5_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mg(4) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_InsuFine0.4_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mg(5) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_InsuFine0.3_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mg(6) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_InsuFine0.2_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mg(7) = max(abs(J*1000));

set(gca, 'FontSize', 14)


xlabel('Distance along the wire (mm)')
ylabel('Induced Current (mA)')
grid on
legend('2mm', '1mm', '0.6mm', '0.5mm', '0.4mm', '0.3mm', '0.2mm', 'Location', 'northwest')
set(gcf, 'Position',  [0, 0, 690, 380])
title('Grid Maximum Step Effect')
%%
% figurent;
subplot(122)
scatter([0.5 0.4 0.3 0.2], mg(4:end), 100, 'filled');
hold on
scatter([2 1 0.6], mg(1:3), 100, 'filled');
legend({'No Vox Errors', 'Vox Errors'}, 'Location', 'northwest');

grid on
set(gca, 'FontSize', 14)
xlabel('Grid Maximum Step (mm)')
ylabel('Peak Current (mA)')
% suptitle('')
suptitle('Grid Maximum Step Effect')


%% VOXELIZATION CONVERGENCE CAPPED WIRE AND DIFFERENT INSULATION THICKNESSES
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_InsuFine0.2_EMCurrent.mat')
ltrue(1) = 0;
for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end

figurent;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_3RInsuFine0.2_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mi(1) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_2.25RInsuFine0.2_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mi(2) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_1.75RInsuFine0.2_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mi(3) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_InsuFine0.2_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mi(4) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_1RInsuFine0.2_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mi(5) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_0.75RInsuFine0.11_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mi(6) = max(abs(J*1000));
% load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_0.75RInsuFine0.2_EMCurrent.mat')
% plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
% mi(7) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_CoreFine0.2_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mb(7) = max(abs(J*1000));


set(gca, 'FontSize', 14)


xlabel('Distance along the wire')
ylabel('Current (mA)')

legend('2.5mm Insu Thickness', '1.75mm Insu Thickness','1.25mm Insu Thickness','0.75mm Insu Thickness','0.5mm Insu Thickness','0.25mm Insu Thickness','0mm Insu Thickness','0.25mm Insu Thickness + Vox Err', 'Location', 'northwest')
set(gcf, 'Position',  [0, 0, 1050, 310])
title('Uncapped Wire - Insulation Thickness')

%% ____

figurent;
scatter([2.5 1.75 1.25 0.75 0.5 0.25], mi(1:6), 100, 'filled');
hold on;
scatter(0.25, mi(7), 100, 'filled');
legend('No Vox Errors', 'Vox Errors');
grid on
set(gca, 'FontSize', 14)
xlabel('Insulation Thickness (mm)')
ylabel('Peak Current (mA)')
title('Uncapped Wire - Insulation Thickness')

%% NEW FEB 2020
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_4mmSensors_EMCurrent.mat')
ltrue(1) = 0;
for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end

figurent;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_4mmSensors_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_3mmSensors_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mb(1) = max(abs(J*1000));
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_2mmSensors_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mb(2) = max(abs(J*1000));


set(gca, 'FontSize', 14)


xlabel('Distance along the wire')
ylabel('Current (mA)')

legend('4mm radius', '3mm radius', '2mm radius')% '0.5mm Grid', '0.4mm Grid', '0.3mm Grid', '0.2mm Grid', 'Location', 'northwest')
set(gcf, 'Position',  [0, 0, 1050, 310])
title('Uncapped Wire - Current Sensor Radius')
%%%% NEW FEB 2020
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_4mmSensors_EMCurrent.mat')
ltrue(1) = 0;
for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end
%%
figurent;
% subplot(121)
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_1.25RInsu_ThickCore_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_1.75RInsu_ThickCore_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_1.25RInsu_ThinCore_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_1.75RInsu_ThinCore_EMCurrent.mat')
plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
mb(1) = max(abs(J*1000));

set(gca, 'FontSize', 18)
grid on

xlabel('Distance along the wire')
ylabel('Current (mA)')

legend({'1mm Wire Core (1.25mm Insulation)', '1mm Wire Core (1.75mm Insulation)'...
    'Core as "Line" (1.25mm Insulation)', 'Core as "Line" (1.75mm Insulation)'}, 'Location', 'northwest')% '0.5mm Grid', '0.4mm Grid', '0.3mm Grid', '0.2mm Grid', 'Location', 'northwest')
set(gcf, 'Position',  [0, 0, 1050, 410])
title('Wire Model')
% 
% subplot(122)
% load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_InsuFine0.2_EMCurrent.mat')
% plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
% load('G:\Wire Voxelization\Phantom\Extracted Results\EM Current\PlaneWave_Phantom_Testwire_1.75InsuFine0.2_EMCurrent.mat')
% plot(ltrue, abs(J*1000), 'LineWidth' , 2); hold on;
% mb(1) = max(abs(J*1000));
% 
% 
% set(gca, 'FontSize', 14)
% 
% 
% xlabel('Distance along the wire')
% ylabel('Current (mA)')
% 
% legend('1.25mm Insu. Thickness - Thick Core', '1.75mm Insu. Thickness - Thick Core')% '0.5mm Grid', '0.4mm Grid', '0.3mm Grid', '0.2mm Grid', 'Location', 'northwest')
% set(gcf, 'Position',  [0, 0, 1050, 310])
% title('Uncapped Wire - Current Sensor Radius')
