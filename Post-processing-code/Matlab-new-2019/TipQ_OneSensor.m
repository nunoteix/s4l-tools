function [maxEV_redQ_1s1m, maxEV_redQ_1s1m_noNorm, v_all, b1weights_1s, w1, avgB1] = TipQ_OneSensor(nModes, nChannels, nSensors, points_w, linterp_shift, J, maskMLS, sRB1r, sRB1wiredr, Q_tip, text)
%     nSensors = 1;
%     nModes = nChannels - nSensors;
    dbstop if error
    disp(['For ' num2str(nChannels) ' channels and ' num2str(nSensors)...
        ' sensor: Calculating maximum Eigenvalue of the Q-Matrix reduced to '...
        num2str(nChannels-nModes) ' modes.'])
    
    for kk = 1:length(points_w)
        if strcmp(text, 'enabled')
            disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_w))])
        end
        
        [~,posidx(1)] = min(abs(linterp_shift-points_w(kk,1)));
        if nSensors == 2 || nSensors == 3
            [~,posidx(2)] = min(abs(linterp_shift-points_w(kk,2)));
        end
        if nSensors == 3
            [~,posidx(3)] = min(abs(linterp_shift-points_w(kk,3)));%Sets points in relation to phantom-air border
        end
        
%         plot_pos = (posidx-pedge_ind).*st; % Calculates actual metric distance of the points for plotting
        [~,~,v]=svd(J(posidx,:)); % Single Value Decomposition from Surface Current J (wire)
%         sens_pos(kk,:) = plot_pos; % Actual position of sensor vector
        v_dmss = v(:,nModes+1:nChannels); % Creates Dark Mode Subspace (dmss)

        v_all(:,:,kk) = v;
        
%         moded_B1(:,:,kk) = sRB1r*v;
%         moded_wiredB1(:,:,kk) = sRB1wiredr*v;
        
        scaling_factor = 1e-6/abs(mean(sRB1r(:)));

        [w1(:,kk),~] =MLS_modeshim_max_amp(sRB1r.*scaling_factor*1e6,v_dmss, maskMLS,1.5*ones(nChannels,1),[],0.9,20,0.08, 'enabled');
        w1(:,kk) = w1(:,kk)*scaling_factor;
        [avgB1(kk),holder] = applyShim(sRB1wiredr,w1(:,kk),nChannels);

%         pp_absShimmed(:,nSensors,kk) = holder; % Save shimmed B1+ for all sens_pos

        % SECTION FOR EIG VALUES @TIP
        Q_tip_normed = Q_tip./((avgB1(kk)^2)/sum(abs(w1(:,kk)))); % Normalise the "WCSAR" to that AvgB1+
        
        Q_dmss = v_dmss'*(Q_tip_normed)*v_dmss; % Reduce Q-matrix to the DMSS
        Q_dmss_noNorm = v_dmss'*(Q_tip)*v_dmss;
        
        maxEV_redQ_1s1m(kk) = max(eig(Q_dmss)); %Calculate the "WCSAR" for the DMSS
        maxEV_redQ_1s1m_noNorm(kk) = max(eig(Q_dmss_noNorm)); %Calculate the "WCSAR" for the DMSS
    end
    
    b1weights_1s = ((avgB1.^2)./sum(abs(w1)));    
end