%%Needs:
%- Jinterp, linterp_shift
%- nonzero_maskedB1reshaped
%- maskMLS
%- nChannels
%- Q_tip
%- points_w, points_pos, points_3_pos
npoints = length(points_w);

% for ii = 1:length(Q)
%     maxEV_allQ(ii) = abs(max(eig(squeeze(Q(ii,:,:)))));
% endnea
% [~,idx_maxQ] = max(maxEV_allQ);
% [xtip, ytip, ztip] = ind2sub(sz, idx_maxQ);
% Qr = reshape(Q, [sz 8 8]);
% Q_tip = squeeze(Qr(xtip,ytip,ztip,:,:));

Q_tip = VOPm;


[~,~,v] = svd(Jinterp);
%% Full Knowledge
scaling_factor = 1e-6/abs(mean(nonzero_maskedB1reshaped(:))); %%DOUBT ABOUT HOW THIS IS DONE 09/10/2019
lambda = [120 100 100 15];
for ii = 1:4
    a_W = 1e10;
    for jj = 1:50
        
%         if ii == 1
%             [witer,~] = MLS_modeshim_max_amp(nonzero_maskedB1reshaped.*scaling_factor*1e6,eye(8), maskMLS,1.5*ones(nChannels,1),[],0.8,20,0.05,'enabled');
%         else
            [witer,~] = MLS_modeshim_max_amp(nonzero_maskedB1reshaped.*scaling_factor*1e6,v(:,ii:8), maskMLS,1.5*ones(nChannels,1),[],lambda(ii),20,0.1,'enabled');
%         end
        if sum(abs(witer).^2) < a_W
            wout(:,ii) = witer.*scaling_factor;
            a_W = sum(abs(witer).^2);
        end
    end
    
    
%     [avgB1_perfKnowledge(ii),~] = applyShim(nonzero_maskedB1wired_reshaped, wout(:,ii), nChannels);
    [avgB1_perfKnowledge(ii),~] = applyShim(nonzero_maskedB1reshaped, wout(:,ii), nChannels);
    b1_normVal_perfKnowledge(ii) = (avgB1_perfKnowledge(ii).^2)./sum(abs(wout(:,ii)).^2);
    if ii == 1
        wcSAR_perfKnowledge(ii) = max(eig(Q_tip));
    else
        wcSAR_perfKnowledge(ii) = max(eig(v(:,ii:8)'*Q_tip*v(:,ii:8)));
    end
end

%% Full Knowledge Plot
figurent;
subplot(1,3,1)
set(gcf, 'Position',  [0, 0, 1050, 310])


bar(0:3, wcSAR_perfKnowledge, 'r')
title('wcSAR')
xlabel('Modes Extracted')
ylabel('W/Kg/W')
set(gca,'FontSize',12)

subplot(132)
bar(0:3, (b1_normVal_perfKnowledge./avgB1_perfKnowledge(1))*1e6, 'b')
title('Generating homogeneous B1+')
xlabel('Modes Extracted')
ylabel('\muT/W')
set(gca,'FontSize',12)

subplot(133)
bar(0:3, wcSAR_perfKnowledge./(b1_normVal_perfKnowledge*1e12), 'g')
title('wcSAR per \muT^2, Shimmed B_1^+')
xlabel('Modes Extracted')
ylabel('W/Kg/\muT^2')
set(gca,'FontSize',12)





%% Plot Curent distri by mode

figurent;plot(linterp_shift, abs(Jinterp*v)*1000, 'LineWidth', 2); grid on
% axis([-600 800 0 20])
axis([-800 600 0 20])
ylabel('Modes applied to Current (mA/W)')
xlabel('Distance along Wire (mm)')
legend({'Mode 1', 'Mode 2', 'Mode 3', 'Mode 4', 'Mode 5', 'Mode 6', 'Mode 7', 'Mode 8'}, 'Location', 'northwest')
set(gca,'FontSize',14)
set(gcf, 'Position',  [0, 0, 1050, 350])


%% Do the Normalised Plot with all cases


b1weights_1s1m = (avgB1_1s1m.^2)./sum(abs(w1s1m).^2,1);
b1weights_2s1m = (avgB1_2s1m.^2)./sum(abs(w2s1m).^2,1);
b1weights_2s2m = (avgB1_2s2m.^2)./sum(abs(w2s2m).^2,1);
b1weights_3s1m = (avgB1_3s1m.^2)./sum(abs(w3s1m).^2,1);
b1weights_3s2m = (avgB1_3s2m.^2)./sum(abs(w3s2m).^2,1);
b1weights_3s3m = (avgB1_3s3m.^2)./sum(abs(w3s3m).^2,1);



max_val =  wcSAR_full/b1_normVal_perfKnowledge(1); 
    %deepestSens_wcSAR_3s3m(3:end)./b1weights_3s3m(3:end);
    %deepestSens_wcSAR_1s1m./b1weights_1s1m; %
    %wcSAR_full/b1_normVal_perfKnowledge(1);%deepestSens_wcSAR_1s1m./b1weights_1s1m;
    %
    %deepestSens_wcSAR_3s1m(3:end)./b1weights_3s1m(bestPos_idx_3s1m(3:npoints));...
    %...
    %...
    %deepestSens_wcSAR_2s1m(2:end)./b1weights_2s1m(bestPos_idx_2s1m(2:npoints)); % 

figurent;
coil_reg = 400; %200;%435; %

pth = [coil_reg 0; 800 0; 800 max(max_val);...
    coil_reg max(max_val)]; %SHORT COIL
% 
% pth = [200 0; 800 0; 800 max(max_val);...
%     200 max(max_val)]; %LONG COIL

faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'red', 'FaceAlpha',.18, 'EdgeColor','none');
% a1 = axes();
hold on

plot(points_w, wcSAR_full/b1_normVal_perfKnowledge(1), 'LineWidth', 3); 
plot(points_w,deepestSens_wcSAR_1s1m./b1weights_1s1m,'-.o', 'LineWidth',1)
plot(points_w(2:end),deepestSens_wcSAR_2s1m(2:end)./b1weights_2s1m(2:end), '-.o', 'LineWidth',1)
h(1) = plot(points_w(2:end),deepestSens_wcSAR_2s2m(2:end)./b1weights_2s2m(2:end), '-.o', 'LineWidth',2,...
    'MarkerFaceColor', [0.4940 0.1840 0.5560]);
plot(points_w(3:end),deepestSens_wcSAR_3s1m(3:end)./b1weights_3s1m(3:end), '-.o', 'LineWidth',1)
h(2) = plot(points_w(3:end),deepestSens_wcSAR_3s2m(3:end)./b1weights_3s2m(3:end), '-.o', 'LineWidth',2,...
    'MarkerFaceColor',[0.3010 0.7450 0.9330]);
% h(3) = plot(points_w(3:end),deepestSens_wcSAR_3s3m(3:end)./b1weights_3s3m(3:end), '-.o', 'LineWidth',1);

grid on

set(gca,'FontSize',14)
legend({'Coil Region','Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m'},'Location','northeast','AutoUpdate','off');
title('Worst Case SAR per \muT^2, Shimmed B_1^+')%'Limited Knowledge of J - Short Coil Case')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')
axis([-100 points_w(end) 0 max(max_val)])

uistack(h(1),'top')
uistack(h(2),'top')
% uistack(h(3),'top')
set(gcf, 'Position',  [100, 100, 700, 450])

% % For PHANTOM
% a2 = axes();
% a2.Position = [0.17 0.7 0.5 0.15]; % xlocation, ylocation, xsize, ysize
% h(1) = plot(points_w(2:end),deepestSens_wcSAR_2s2m(2:end)./b1weights_2s2m(2:end), '-.o','Color',...
%     [0.4940 0.1840 0.5560], 'LineWidth',2,'MarkerFaceColor', [0.4940 0.1840 0.5560]);
%     hold on;
% h(2) = plot(points_w(3:end),deepestSens_wcSAR_3s2m(3:end)./b1weights_3s2m(3:end), '-.o','Color',...
%     [0.3010 0.7450 0.9330], 'LineWidth',2,'MarkerFaceColor',[0.3010 0.7450 0.9330]);
% h(3) = plot(points_w(3:end),deepestSens_wcSAR_3s3m(3:end)./b1weights_3s3m(3:end), '-.o','Color',...
%     [0.6350 0.0780 0.1840], 'LineWidth',1);
% % axis off
% set(gca,'FontSize',14)
% axis([-100 points_w(end) 0 7])
% grid on

%% Do the non-normalised Plot with all cases

max_val = wcSAR_full*1e12;

figurent;

pth = [coil_reg 0; 800 0; 800 max(max_val); coil_reg max(max_val)];
faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'red', 'FaceAlpha',.18, 'EdgeColor','none');

hold on

plot(points_w, wcSAR_full*1e12, 'LineWidth', 3); hold on
plot(points_w,deepestSens_wcSAR_1s1m*1e12,'-.o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s1m(2:end)*1e12, '-.o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s2m(2:end)*1e12, '-.o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s1m(3:end)*1e12, '-.o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s2m(3:end)*1e12, '-.o', 'LineWidth',2)
% plot(points_w(3:end),deepestSens_wcSAR_3s3m(3:end)*1e12, '-.o', 'LineWidth',2)
grid on

set(gca,'FontSize',14)
legend('Coil Region', 'Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('Worst Case SAR')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/W')
xlabel('Deepest Sensor Position')

axis([-100 points_w(end) 0 max(max_val)])

set(gcf, 'Position',  [100, 100, 700, 450])

%% Do the B1+/W plots without ^2
figurent;
b1_normVal_plot(1:length(points_w)) = b1_normVal_perfKnowledge(1);

max_val = (b1_normVal_plot./avgB1_perfKnowledge(1))*1e6; %b1weights_1s1m*1e12; % 
min_val = (b1weights_2s2m(3:npoints)./avgB1_2s2m(3:npoints))*1e6;

pth = [coil_reg 0; 800 0; 800 max(max_val); coil_reg max(max_val)];
faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'red', 'FaceAlpha',.18, 'EdgeColor','none');

hold on

plot(points_w, (b1_normVal_plot./avgB1_perfKnowledge(1))*1e6, 'LineWidth', 3); hold on
plot(points_w,(b1weights_1s1m./avgB1_1s1m)*1e6,'-.o', 'LineWidth',2)
plot(points_w(2:end),(b1weights_2s1m(2:npoints)./avgB1_2s1m(2:npoints))*1e6, '-.o', 'LineWidth',2)
plot(points_w(2:end),(b1weights_2s2m(2:npoints)./avgB1_2s2m(2:npoints))*1e6, '-.o', 'LineWidth',2)
plot(points_w(3:end),(b1weights_3s1m(3:npoints)./avgB1_3s1m(3:npoints))*1e6, '-.o', 'LineWidth',2)
plot(points_w(3:end),(b1weights_3s2m(3:npoints)./avgB1_3s2m(3:npoints))*1e6, '-.o', 'LineWidth',2)
% plot(points_w(3:end),(b1weights_3s3m(3:npoints)./avgB1_3s3m(3:npoints))*1e6, '-.o', 'LineWidth',2)
grid on

set(gca,'FontSize',16)
% legend('Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('Capacity to generate homogeneous B_1^+')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('\muT/W')
xlabel('Deepest Sensor Position')

axis([-100 points_w(end) min(min_val) max(max_val)])

set(gcf, 'Position',  [100, 100, 700, 450])
%% DO THE ACTUAL SHIM PLOT

actualShim_SAR_fullSys(1:npoints) = abs(wout(:,1)'*Q_tip*wout(:,1));%./b1_normVal;
% actualShim_SAR_fullSys(1:npoints) = abs(w'*Q_tip*w);%./b1_normVal;
for ii = 1:npoints
    actualShim_SAR_1s1m(ii) = abs(w1s1m(:,ii)'*Q_tip*w1s1m(:,ii));%./b1weights_1s1m(ii);
    
    if ii >= 2
        actualShim_SAR_2s1m(ii) = abs(w2s1m(:,ii)'*Q_tip*w2s1m(:,ii));%./b1weights_2s1m(bestPos_idx_2s1m(ii));
        actualShim_SAR_2s2m(ii) = abs(w2s2m(:,ii)'*Q_tip*w2s2m(:,ii));%./b1weights_2s2m(bestPos_idx_2s2m(ii));
    end
    
    if ii >= 3
        actualShim_SAR_3s1m(ii) = abs(w3s1m(:,ii)'*Q_tip*w3s1m(:,ii));%./b1weights_3s1m(bestPos_idx_3s1m(ii));
        actualShim_SAR_3s2m(ii) = abs(w3s2m(:,ii)'*Q_tip*w3s2m(:,ii));%./b1weights_3s2m(bestPos_idx_3s2m(ii));
        actualShim_SAR_3s3m(ii) = abs(w3s3m(:,ii)'*Q_tip*w3s3m(:,ii));%./b1weights_3s3m(bestPos_idx_3s3m(ii));
    end
end

figurent;

max_val = actualShim_SAR_1s1m;%actualShim_SAR_fullSys;%actualShim_SAR_3s3m(3:end;
        % actualShim_SAR_2s2m(2:end);%

pth = [coil_reg 0; 800 0; 800 max(max_val); coil_reg max(max_val)];
faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'red', 'FaceAlpha',.18,'EdgeColor','none');

hold on

plot(points_w, actualShim_SAR_fullSys, 'LineWidth', 3); hold on
% plot(points_w, actualShim_SAR_quad, 'LineWidth', 3);
plot(points_w,actualShim_SAR_1s1m,'-.o', 'LineWidth',2)
plot(points_w(2:end),actualShim_SAR_2s1m(2:end), '-.o', 'LineWidth',2)
plot(points_w(2:end),actualShim_SAR_2s2m(2:end), '-.o', 'LineWidth',2)
plot(points_w(3:end),actualShim_SAR_3s1m(3:end), '-.o', 'LineWidth',2)
plot(points_w(3:end),actualShim_SAR_3s2m(3:end), '-.o', 'LineWidth',2)
% plot(points_w(3:end),actualShim_SAR_3s3m(3:end), '-.o', 'LineWidth',2)
grid on

set(gca,'FontSize',14)

legend('Coil Region','Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('Expected SAR per \muT^2, Shimmed B_1^+')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position (mm)')

axis([-100 points_w(end) 0 max(max_val)])

set(gcf, 'Position',  [100, 100, 700, 450])


%% Do the best case modes plot
figurent
plot(points_w, wcSAR_full, 'LineWidth', 3); hold on
scatter(points_w,deepestSens_wcSAR_1s1m,'o','filled', 'LineWidth',2)
scatter(points_w(bestMode_2s == 1),deepestSens_wcSAR_2s(bestMode_2s == 1), 'd','filled', 'LineWidth',2)
scatter(points_w(bestMode_2s == 2),deepestSens_wcSAR_2s(bestMode_2s == 2), 'd','filled', 'LineWidth',2)
scatter(points_w(bestMode_3s == 1),deepestSens_wcSAR_3s(bestMode_3s == 1), 'sq','filled', 'LineWidth',2)
scatter(points_w(bestMode_3s == 2),deepestSens_wcSAR_3s(bestMode_3s == 2), 'sq','filled', 'LineWidth',2)
scatter(points_w(bestMode_3s == 3),deepestSens_wcSAR_3s(bestMode_3s == 3), 'sq','filled', 'LineWidth',2)
grid on

set(gca,'FontSize',16)

legend('Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('Normalised Worst Case SAR')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')
