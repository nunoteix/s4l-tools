npoints = 40;
ncoils = 1;
ref_val = max(abs(Ew_abs_mipY(:)));
%% LOAD DATA

% path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';
path_to_results = 'G:\FakePort Simulations\Duke\Extracted Results\';

% HERE WE CHOOSE WHAT SIMULATION TO GET THE RESULTS FROM - THIS NAME WAS
% PRE-CHOSEN WHEN EXPORTING FROM S4L

% simulation_name = '\BodyCoil_Duke_Guidewire_2020_04_07_';
% simulation_name = '\BodyCoil_Duke_Guidewire_Capacitor_2020_07_28_';
% simulation_name = '\BodyCoil_Duke_Guidewire_Inductor_2020_07_28_';
simulation_name = '\Resonance Finder FP Channel 2 Duke Lines 3_2020_08_08_';
% simulation_name = '\Series Matched FP Channel 1 Duke_2020_08_08_';
% simulation_name = '\Series Matched Corrected FP Channel 1 Duke_2020_08_08_';

% Import Current
J_struct = load([path_to_results 'EM Current' simulation_name 'EMCurrent']);
J = J_struct.J;
Axes = J_struct.Axes;

% Import Grid
load([path_to_results 'Grid' simulation_name 'Axes'])
sz = [length(XAxis) length(YAxis) length(ZAxis)]; % STORES MATRIX SIZES (HANDY FOR OPERATIONS)
fsz = sz(1)*sz(2)*sz(3);

%% IMPORT E-FIELD
% IF NAMED CORRECTLY THEN THIS SHOULD GRAB E MATRICES
for ii = 1:ncoils
    load([path_to_results 'E Field' simulation_name 'EFields_C' num2str(ii)])
end
E1p = permute(reshape(E_1, [sz(1) sz(3) sz(2) 3 ncoils]), [1 3 2 4]); % ISOLATE B1+ AND REORDER IT INTO XYZ

% Eabs = abs(sqrt(squeeze(E_1(:,1))*conj(E_1(:,1))+E_1(:,2)*conj(E_1(:,2))+E_1(:,3)*conj(E_1(:,3))));
[~,xx] = min(abs(XAxis - Axes(1,end)*1e-3));
[~,yy] = min(abs(YAxis - Axes(2,end)*1e-3));
[~,zz] = min(abs(ZAxis - Axes(3,end)*1e-3));

%%
[L, Jinterp, pedge_ind, points_1s, points_2s, points_3s] = interpJ(npoints, ncoils, 2000, Axes, J, 735, 16); %Duke

ltrue(1) = 0;

for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end

LE_loc = (ltrue(12)+ltrue(13))/2;
LE_L = LE_loc - ltrue(16);

[~, LE_L_ind] = min(abs(L-LE_L));

%%
figurent();plot(L,abs(Jinterp)*1000, 'LineWidth', 2)
hold on;
scatter(LE_L, abs(Jinterp(LE_L_ind)*1000), 'r')

axis([-800 800 0 200])
ylabel('{\bf{J}} (mA/\surdW)')
xlabel('Distance along Wire (mm)')
legend('\bf{J, Wire + Not Matched}', 'Lumped Element Location', 'Location', 'northwest')
% legend('\bf{J, Wire + Capacitor (Complex Z)}', 'Lumped Element Location', 'Location', 'northwest')
% legend('\bf{J, Full Wire}', 'Lumped Element Location', 'Location', 'northwest')
% legend({'\bf{J, Wire + Fake Port}', 'Port Location'}, 'Location', 'northwest')
set(gca,'FontSize',12)
set(gcf, 'Position',  [0, 0, 1050, 350])

%% E-field plotters
E = E1p;%./2.6728e+04;
Ew = E;%.*(exp(1i*2*pi*rand));
% Ew = real(E1p) + 1i*rand*imag(E1p);%.*(exp(1i*rand));
E1p_abs = sqrt((E(:,:,:,1).*conj(E(:,:,:,1)))+(E(:,:,:,2).*conj(E(:,:,:,2)))+(E(:,:,:,3).*conj(E(:,:,:,3))));
Ew_abs = sqrt((Ew(:,:,:,1).*conj(Ew(:,:,:,1)))+(Ew(:,:,:,2).*conj(Ew(:,:,:,2)))+(Ew(:,:,:,3).*conj(Ew(:,:,:,3))));

Ew_abs_mipY = squeeze(max(Ew_abs, [], 2));
figurent; imagesc(20*log10(abs(Ew_abs_mipY)./ref_val))
caxis([-50 0])
title('E-Field (dB) Not Matched')
% set(gca,'FontSize',12)
% set(gcf, 'Position',  [0, 0, 1050, 350])
% figurent; imagesc(abs(Ew_abs_mipY));
%%
Ediff_abs_mipY = squeeze(max(E1p_abs-Ew_abs, [], 2));
figurent; imagesc(abs(Ediff_abs_mipY))

%% What about J?
figurent();plot(L,abs(Jinterp)*1000, 'LineWidth', 2)
figurent();plot(L,abs(Jinterp.*(exp(1i*2*pi*rand)))*1000, 'LineWidth', 2)