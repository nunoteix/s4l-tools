
x = categorical({'~= 0.001g', '0.1g', '1g', '10g'});
x = reordercats(x,{'~= 0.001g', '0.1g', '1g', '10g'});
figurent()%;bar(x,wcSAR);
plot(bars,wcSAR);
% text(1:length(wcSAR),abs(wcSAR),num2str(abs(wcSAR)'),'vert','bottom','horiz','center');
% box off
title('Worst Case SAR - Different Mass Averaging')
set(gca, 'FontSize', 14)
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')