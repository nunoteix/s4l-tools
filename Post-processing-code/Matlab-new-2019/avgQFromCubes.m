function avgQ = avgQFromCubes(Q3D,rho3D,cubes)
%% V3
si = size(Q3D);
% Q = reshape(Q3D, [si(1)*si(2)*si(3) 8 8]);
rho = reshape(rho3D, [si(1)*si(2)*si(3) 1]);
cubes_list = reshape(cubes, [si(1)*si(2)*si(3) 6]);

avgQ = zeros(si(1)*si(2)*si(3), si(4), si(5), 'single');
% avgQ = complex(avgQ);
% avgQ = zeros(10000000, si(4), si(5), 'single');
% avgQ = cell(si(1)*si(2)*si(3), si(4), si(5);
% [~,idx] = min(abs(divisors(length(rho))-100));
% div_loop = divisors(length(rho));
golden_no = 50000;%div_loop(idx);
reset = 0;
new_val = zeros(golden_no,si(4),si(5));
for ii = 1:length(rho)

    if rho(ii)>0
        cube_Q = Q3D(cubes_list(ii,1):cubes_list(ii,2),...
            cubes_list(ii,3):cubes_list(ii,4),...
            cubes_list(ii,5):cubes_list(ii,6),:,:);

        cube_rho = reshape(rho3D(cubes_list(ii,1):cubes_list(ii,2),...
            cubes_list(ii,3):cubes_list(ii,4),...
            cubes_list(ii,5):cubes_list(ii,6)),[],1);

        cube_nz = length(cube_rho(cube_rho~=0));

        st = size(cube_Q);
        cube_Q = reshape(cube_Q, [st(1)*st(2)*st(3),st(4), st(5)]);

        new_val(ii-(golden_no*reset),:,:) = sum(cube_Q,1)./cube_nz;
    end
    
    if ii-(golden_no*reset) == golden_no
            avgQ((golden_no*reset)+1:ii,:,:) = new_val;
            reset = reset + 1;
            new_val = zeros(golden_no,si(4),si(5));
            
            progress = 100*((golden_no*reset)/length(rho));
            fprintf('%.2f %% Completed\n', progress);
            
    elseif ii == length(rho)
            avgQ((golden_no*reset)+1:end,:,:) = new_val(1:ii-(golden_no*reset),:,:);
            disp('100% Completed');
    end

end

end
% %% V2
% si = size(Q3D);
% % Q = reshape(Q3D, [si(1)*si(2)*si(3) 8 8]);
% rho = reshape(rho3D, [si(1)*si(2)*si(3) 1]);
% cubes_list = reshape(cubes, [si(1)*si(2)*si(3) 6]);
% 
% avgQ = zeros(si(1)*si(2)*si(3), si(4), si(5), 'single');
% % avgQ = complex(avgQ);
% for ii = 1:length(rho)
%     
%     if rho(ii)>0
%         tic;
% %         x_min = cubes_list(ii,1);
% %         x_max = cubes_list(ii,2); 
% %         y_min = cubes_list(ii,3);
% %         y_max = cubes_list(ii,4);
% %         z_min = cubes_list(ii,5);
% %         z_max = cubes_list(ii,6);
%         cube_Q = Q3D(cubes_list(ii,1):cubes_list(ii,2),...
%             cubes_list(ii,3):cubes_list(ii,4),...
%             cubes_list(ii,5):cubes_list(ii,6),:,:);
%         toc;
% %         tic;            
%         for ll = 1:si(4)
% %             tic
%             for cc = ll:si(5)
%                 cube_Q_entry = cube_Q(:,:,:,ll,cc);
%                 cube_Q_entry = cube_Q_entry(:);
%                 cube_Q_nz = length(cube_Q_entry(cube_Q_entry~=0));
%                 tic;
%                 avgQ(ii,ll,cc) = sum(cube_Q_entry)/cube_Q_nz;
%                 toc;
%                 avgQ(ii,cc,ll) = avgQ(ii,ll,cc)';
%             end
% %             toc;
%         end
% %         toc;disp('\n last toc')
%     end
%     progress = 100*(ii/length(rho));
%     if floor(progress) == progress
%         fprintf('%.2f %% Completed\n', progress);
%     end
%     toc;
% end
% 
% end
%% V1
% si = size(Q3D);
% avgQ = zeros(si);
% for ii = 1:si(1)
% %     tic;
%     for jj = 1:si(2)
%         for kk = 1:si(3)
%             tic;
%             if rho3D(ii,jj,kk)>0
%                 x_min = cubes(ii,jj,kk,1);
%                 x_max = cubes(ii,jj,kk,2); 
%                 y_min = cubes(ii,jj,kk,3);
%                 y_max = cubes(ii,jj,kk,4);
%                 z_min = cubes(ii,jj,kk,5);
%                 z_max = cubes(ii,jj,kk,6);
%                     
%                 for ll = 1:si(4)
%                     for mm = ll:si(5)
%                     cube_Q_entry = Q3D(x_min:x_max,y_min:y_max,z_min:z_max,ll,mm);
%                     cube_Q_entry = cube_Q_entry(:);
%                     cube_Q_nz = length(cube_Q_entry(cube_Q_entry~=0));
% 
%                     avgQ(ii,jj,kk,ll,mm) = sum(cube_Q_entry)/cube_Q_nz;
%                     avgQ(ii,jj,kk,mm,ll) = avgQ(ii,jj,kk,ll,mm)';
%                     end
%                 end
%             end
%             toc;
%         end
%     end
%     progress = 100*(ii/si(1));
%     fprintf('%.2f %% Completed\n', progress);
% %     toc;
% end
%              