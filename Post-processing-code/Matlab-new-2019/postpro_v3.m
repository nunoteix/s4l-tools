%% Quantities for Full System
npoints = 40;

for ii = 1:nChannels
    powJ_i(ii) = sum(abs(Jinterp(:,ii)*1000).^2)/length(Jinterp);
end
powJ = mean(powJ_i);
powN = powJ*(10^-3);
add_noise = 'N';
if add_noise == 'Y'
    Jinterp_calc = awgn(Jinterp*1000, 45, 'measured');
%     Jinterp_calc_real = awgn(real(Jinterp*1000), 30, 'measured');
%     Jinterp_calc_imag = awgn(imag(Jinterp*1000), 30, 'measured');

%     Jinterp_calc_abs = abs(Jinterp*1000) + (0.4*rand(length(Jinterp),8));
%     Jinterp_calc_phase = angle(Jinterp*1000) + (0.4*rand(length(Jinterp),8));

%     Jinterp_calc_real = real(Jinterp*1000) + (sqrt(powN/2)*randn(length(Jinterp),8));
%     Jinterp_calc_imag = imag(Jinterp*1000) + (sqrt(powN/2)*randn(length(Jinterp),8));
    
%     Jinterp_calc = Jinterp_calc_real + 1i*Jinterp_calc_imag;
%     Jinterp_calc = Jinterp_calc_abs.*exp(1i*Jinterp_calc_phase);
else
    Jinterp_calc = Jinterp*1000;
end
%%
figurent();
subplot(211)
plot(linterp_shift,abs(Jinterp_calc))
title('Absolute')
axis([-600 800 0 7.5])
% axis([-800 600 0 12])
ylabel('Current per Channel (mA/W)')
xlabel('Distance along Wire (mm)')
legend({'Channel 1', 'Channel 2', 'Channel 3', 'Channel 4', 'Channel 5', 'Channel 6', 'Channel 7', 'Channel 8'}, 'Location', 'northwest')
set(gca,'FontSize',14)
set(gcf, 'Position',  [0, 0, 1050, 600])
subplot(212)
plot(linterp_shift, angle(Jinterp_calc))
title('Phase')
axis([-600 800 -3.5 3.5])
% axis([-800 600 -3.5 3.5])
ylabel('Current per Channel (rad)')
xlabel('Distance along Wire (mm)')
set(gca,'FontSize',14)
set(gcf, 'Position',  [0, 0, 1050, 600])
%%
Q_tip = VOPm;
Q_tip_normed = Q_tip./b1_normVal_perfKnowledge(1);
maxEV_fullQ(1:npoints) = max(eig(Q_tip_normed));
maxEV_fullQ_noNorm(1:npoints) = max(eig(Q_tip));

% [~,~,v_full]=svd(Jinterp_calc);
% figurent;
% plot(abs(Jinterp_calc*v_full));

%% best positions for reduced system
for kk = 1:length(points_w)
    
    [~,posidx_1(1)] = min(abs(linterp_shift-points_w(kk,1)));
    [~,~,v]=svd(Jinterp_calc(posidx_1,:));
    
    v_dmss_1s1m = v(:,2:nChannels);
    Q_dmss_1s1m = v_dmss_1s1m'*(Q_tip)*v_dmss_1s1m;
    maxEV_1s1m(kk) = max(eig(Q_dmss_1s1m));
    
end

for kk = 1:length(points_pos)
    
    [~,posidx_2(1)] = min(abs(linterp_shift-points_pos(kk,1)));
    [~,posidx_2(2)] = min(abs(linterp_shift-points_pos(kk,2)));
    [~,~,v]=svd(Jinterp_calc(posidx_2,:));
    
    v_dmss_2s1m = v(:,2:nChannels);
    Q_dmss_2s1m = v_dmss_2s1m'*(Q_tip)*v_dmss_2s1m;
    maxEV_2s1m(kk) = max(eig(Q_dmss_2s1m));
    
    v_dmss_2s2m = v(:,3:nChannels);
    Q_dmss_2s2m = v_dmss_2s2m'*(Q_tip)*v_dmss_2s2m;
    maxEV_2s2m(kk) = max(eig(Q_dmss_2s2m));
    
end

for kk = 1:length(points_3_pos)
    
    [~,posidx_3(1)] = min(abs(linterp_shift-points_3_pos(kk,1)));
    [~,posidx_3(2)] = min(abs(linterp_shift-points_3_pos(kk,2)));
    [~,posidx_3(3)] = min(abs(linterp_shift-points_3_pos(kk,3)));%Sets points in relation to phantom-air border
    [~,~,v]=svd(Jinterp_calc(posidx_3,:));
    
    v_dmss_3s1m = v(:,2:nChannels);
    Q_dmss_3s1m = v_dmss_3s1m'*(Q_tip)*v_dmss_3s1m;
    maxEV_3s1m(kk) = max(eig(Q_dmss_3s1m));
    
    v_dmss_3s2m = v(:,3:nChannels);
    Q_dmss_3s2m = v_dmss_3s2m'*(Q_tip)*v_dmss_3s2m;
    maxEV_3s2m(kk) = max(eig(Q_dmss_3s2m));
    
    v_dmss_3s3m = v(:,4:nChannels);
    Q_dmss_3s3m = v_dmss_3s3m'*(Q_tip)*v_dmss_3s3m;
    maxEV_3s3m(kk) = max(eig(Q_dmss_3s3m));

end

%% Actual optimal points
wcSAR_full(1:length(points_w)) = abs(wcSAR_perfKnowledge(1));
deepestSens_wcSAR_1s1m = abs(maxEV_1s1m);

for ii = 2:length(points_w)
    logic_idx{ii} = double(find(points_pos(:,2) == points_w(ii)));
    [all_wcSAR_2s1m{ii}] = abs(maxEV_2s1m(logic_idx{ii}));
    [all_wcSAR_2s2m{ii}] = abs(maxEV_2s2m(logic_idx{ii}));
    
    deepestSens_wcSAR_2s1m(ii) = mean(all_wcSAR_2s1m{ii});
    deepestSens_wcSAR_2s2m(ii) = mean(all_wcSAR_2s2m{ii});
    
    errup_2s1m(ii) = abs(mean(all_wcSAR_2s1m{ii}) - max(all_wcSAR_2s1m{ii}));
    errup_2s2m(ii) = abs(mean(all_wcSAR_2s2m{ii}) - max(all_wcSAR_2s2m{ii}));
    
    errdn_2s1m(ii) = abs(mean(all_wcSAR_2s1m{ii}) - min(all_wcSAR_2s1m{ii}));
    errdn_2s2m(ii) = abs(mean(all_wcSAR_2s2m{ii}) - min(all_wcSAR_2s2m{ii}));
    
    [min_wcSAR_2s1m(ii), temp_idx_2s1m] = min(all_wcSAR_2s1m{ii});
    [min_wcSAR_2s2m(ii), temp_idx_2s2m] = min(all_wcSAR_2s2m{ii});

    bestPos_idx_2s1m{ii} = logic_idx{ii}(temp_idx_2s1m);
    bestPos_idx_2s2m{ii} = logic_idx{ii}(temp_idx_2s2m);
    
    bestPos_2s1m(ii,:) = points_pos(bestPos_idx_2s1m{ii},:);
    bestPos_2s2m(ii,:) = points_pos(bestPos_idx_2s2m{ii},:);
    
    allPos_idx_2s1m{ii} = logic_idx{ii};
    allPos_idx_2s2m{ii} = logic_idx{ii};
    
    allPos_2s1m{ii} = points_pos(allPos_idx_2s1m{ii},:);
    allPos_2s2m{ii} = points_pos(allPos_idx_2s2m{ii},:);
    
	if ii > 2
        logic_idx{ii} = double(find(points_3_pos(:,3) == points_w(ii)));
        [all_wcSAR_3s1m{ii}] = abs(maxEV_3s1m(logic_idx{ii}));
        [all_wcSAR_3s2m{ii}] = abs(maxEV_3s2m(logic_idx{ii}));
%         [all_wcSAR_3s3m{ii}] = abs(maxEV_3s3m(logic_idx{ii}));


        deepestSens_wcSAR_3s1m(ii) = mean(all_wcSAR_3s1m{ii});
        deepestSens_wcSAR_3s2m(ii) = mean(all_wcSAR_3s2m{ii});
%         deepestSens_wcSAR_3s3m(ii) = mean(all_wcSAR_3s3m{ii});

        errup_3s1m(ii) = abs(mean(all_wcSAR_3s1m{ii}) - max(all_wcSAR_3s1m{ii}));
        errup_3s2m(ii) = abs(mean(all_wcSAR_3s2m{ii}) - max(all_wcSAR_3s2m{ii}));
%         errup_3s3m(ii) = abs(mean(all_wcSAR_3s3m{ii}) - max(all_wcSAR_3s3m{ii}));

        errdn_3s1m(ii) = abs(mean(all_wcSAR_3s1m{ii}) - min(all_wcSAR_3s1m{ii}));
        errdn_3s2m(ii) = abs(mean(all_wcSAR_3s2m{ii}) - min(all_wcSAR_3s2m{ii}));
%         errdn_3s3m(ii) = abs(mean(all_wcSAR_3s3m{ii}) - min(all_wcSAR_3s3m{ii}));

        [min_wcSAR_3s1m(ii), temp_idx_3s1m] = min(all_wcSAR_3s1m{ii});
        [min_wcSAR_3s2m(ii), temp_idx_3s2m] = min(all_wcSAR_3s2m{ii});
%         [min_wcSAR_3s3m(ii), temp_idx_3s3m(ii)] = min(all_wcSAR_3s3m{ii});

        bestPos_idx_3s1m{ii} = logic_idx{ii}(temp_idx_3s1m);
        bestPos_idx_3s2m{ii} = logic_idx{ii}(temp_idx_3s2m);

        bestPos_3s1m(ii,:) = points_3_pos(bestPos_idx_3s1m{ii},:);
        bestPos_3s2m(ii,:) = points_3_pos(bestPos_idx_3s2m{ii},:);
        
        allPos_idx_3s1m{ii} = logic_idx{ii};
        allPos_idx_3s2m{ii} = logic_idx{ii};
%         allPos_idx_3s3m{ii} = logic_idx{ii};

        allPos_3s1m{ii} = points_3_pos(allPos_idx_3s1m{ii},:);
        allPos_3s2m{ii} = points_3_pos(allPos_idx_3s2m{ii},:);
%         allPos_3s3m{ii} = points_3_pos(allPos_idx_3s3m{ii},:);
    end
end

%% Only 2 Sens Out
    figurent(11);

% if add_noise == 'Y'
    hold on
%     errorbar(points_w(2:end), deepestSens_wcSAR_2s2m(2:end), errdn_2s2m(2:end), errup_2s2m(2:end),...
%         '-.o','Color', 'r','LineWidth',2);
    plot(points_w(2:end), min_wcSAR_2s2m(2:end), '-.o','Color', 'r','LineWidth', 2);

%     errorbar(points_w(3:end), deepestSens_wcSAR_3s2m(3:end), errdn_3s2m(3:end), errup_3s2m(3:end),... 
%         '-.o','Color', 'b','LineWidth',2);
    plot(points_w(3:end), min_wcSAR_3s2m(3:end), '-.o','Color', 'b','LineWidth', 2);
% else
    hold on;
%     errorbar(points_w(2:end), deepestSens_wcSAR_2s2m(2:end), errdn_2s2m(2:end), errup_2s2m(2:end),...
%         '-.o','Color', 'm','LineWidth',2);
    plot(points_w(2:end), mean_wcSAR_2s2m(2:end), '-.o','Color', 'm','LineWidth', 2);

%     errorbar(points_w(3:end), deepestSens_wcSAR_3s2m(3:end), errdn_3s2m(3:end), errup_3s2m(3:end),... 
%         '-.o','Color', 'c','LineWidth',2);
    plot(points_w(3:end), mean_wcSAR_3s2m(3:end),'-.o', 'Color', 'c','LineWidth', 2);
% end
    %%
    legend('2s2m','3s2m','Mean of 100 Noises 2s2m','Mean of 100 Noises 3s2m')

    grid on;
    
    set(gca,'FontSize',14)
    title('Worst Case SAR')%'wcSAR - Straight Wire - Duke Long Coil')
    ylabel('W/Kg/W')
    xlabel('Deepest Sensor Position')
    set(gcf, 'Position',  [100, 100, 700, 450])
    axis([-100 points_w(end) 0 60])
%% PLOT WITH ERROR BARS
    figurent;
    plot(points_w, deepestSens_wcSAR_1s1m, '-.o','LineWidth',2); hold on;
    errorbar(points_w(2:end), deepestSens_wcSAR_2s1m(2:end), errdn_2s1m(2:end), errup_2s1m(2:end),...
        '-.o','Color', [0.8500 0.3250 0.0980],'LineWidth',2);
    plot(points_w(2:end), deepestSens_wcSAR_2s1m(2:end)-errdn_2s1m(2:end), 'Color', [0.8500 0.3250 0.0980],'LineWidth', 4);
    errorbar(points_w(2:end), deepestSens_wcSAR_2s2m(2:end), errdn_2s2m(2:end), errup_2s2m(2:end),...
        '-.o','Color', [0.9290 0.6940 0.1250],'LineWidth',2);
    plot(points_w(2:end), deepestSens_wcSAR_2s2m(2:end)-errdn_2s2m(2:end), 'Color', [0.9290 0.6940 0.1250],'LineWidth', 4);
    errorbar(points_w(3:end), deepestSens_wcSAR_3s1m(3:end), errdn_3s1m(3:end), errup_3s1m(3:end),...
        '-.o','Color', [0.4940 0.1840 0.5560],'LineWidth',2);
    plot(points_w(3:end), deepestSens_wcSAR_3s1m(3:end)-errdn_3s1m(3:end), 'Color', [0.4940 0.1840 0.5560],'LineWidth', 4)
    errorbar(points_w(3:end), deepestSens_wcSAR_3s2m(3:end), errdn_3s2m(3:end), errup_3s2m(3:end),... 
        '-.o','Color', [0.4660 0.6740 0.1880],'LineWidth',2);
    plot(points_w(3:end), deepestSens_wcSAR_3s2m(3:end)-errdn_3s2m(3:end), 'Color', [0.4660 0.6740 0.1880],'LineWidth', 4);

%     errorbar(points_w(3:end), deepestSens_wcSAR_3s3m(3:end), err_3s3m(3:end), 'LineWidth',2); hold on;
    legend('1s1m','2s1m','2s1m min', '2s2m','2s2m min', '3s1m','3s1m min', '3s2m','3s2m min', '3s3m')
    
grid on;
set(gca,'FontSize',14)
title('Worst Case SAR - All Sensor Positions w/o Noise')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/W')
xlabel('Deepest Sensor Position')
set(gcf, 'Position',  [100, 100, 700, 450])
axis([-100 points_w(end) 0 80])

%% INDIVIDUAL PLOTS OF ALL VALUES
figurent;
plot(points_w, deepestSens_wcSAR_1s1m, 'o','Color', [0.8500 0.3250 0.0980],'LineWidth',0.5); hold on;
for ii = 2:npoints
    plot(ones(1,length(all_wcSAR_2s1m{ii}))*points_w(ii), all_wcSAR_2s1m{ii},'o','Color', [0.9290 0.6940 0.1250],...
        'LineWidth',0.5); hold on
end
for ii = 2:npoints
    plot(ones(1,length(all_wcSAR_2s2m{ii}))*points_w(ii), all_wcSAR_2s2m{ii},'o','Color', [0.4940 0.1840 0.5560],...
        'LineWidth',0.5); hold on
end
for ii = 3:npoints
    plot(ones(1,length(all_wcSAR_3s1m{ii}))*points_w(ii), all_wcSAR_3s1m{ii},'o','Color', [0.4660 0.6740 0.1880],...
        'LineWidth',0.5); hold on
end
for ii = 3:npoints
    plot(ones(1,length(all_wcSAR_3s2m{ii}))*points_w(ii), all_wcSAR_3s2m{ii},'o','Color', [0.3010 0.7450 0.9330],...
        'LineWidth',0.5); hold on
end
grid on;
set(gca,'FontSize',14)
title('Worst Case SAR - All Sensor Positions')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/W')
xlabel('Deepest Sensor Position')
set(gcf, 'Position',  [100, 100, 700, 450])
axis([-100 points_w(end) 0 80])

%% PLOTS TO CHECK POSITION


figurent(156)
jj = 0;
for ii = 2:2:40
    jj = jj+1;
    subplot(5,4,jj)
    plot(all_wcSAR_2s1m{ii}', allPos_2s1m{ii}(:,1), 'o', 'LineWidth', 2); hold on
    plot(min_wcSAR_2s1m(ii), bestPos_2s1m(ii,1),'-x','Color', 'r', 'LineWidth', 3, 'MarkerSize', 10);
    grid on
    axis([0 80 -100 800])
    title(['Deepest Sensor @' num2str(points_w(ii))])
    hold off
end
suptitle('Worst Case SAR - 2s1m, All Combination - w/ Noise')
%% SCATTER w/ COLOR
figurent(116)
jj = 0 ;
% inf_fitted = inferno(ceil(maxEV_fullQ_noNorm(1)*10));
colormap(inferno())
for ii = 3:2:40
    jj = jj+1;
    subplot(4,5,jj+1)
    ph1 = plot( bestPos_3s2m(ii,1), bestPos_3s2m(ii,2), '-x','Color', 'r', 'LineWidth', 3, 'MarkerSize', 10);hold on
    
    scatter( allPos_3s2m{ii}(:,1), allPos_3s2m{ii}(:,2), 30, all_wcSAR_3s2m{ii}, 'o','filled','LineWidth', 1); 
    grid on
%     axis([-100.1 max(allPos_3s2m{ii}(:,1)) -100.1 max(allPos_3s2m{ii}(:,2))])
    axis([ -100 800 -100 800])% min(all_wcSAR_3s2m{ii})-0.01 max(all_wcSAR_3s2m{ii})+0.01])
    %     xlim([ 0 25])
    title(['Deepest Sensor @' num2str(points_w(ii))])
    hold off
    lgd = legend(['S1@' num2str(round(bestPos_3s2m(ii,1))) ' & S2@' num2str(round(bestPos_3s2m(ii,2)))],...
        'Location', 'best', 'Orientation', 'vertical');
    lgd.FontSize = 8;
%         
    caxis([0 20])
%     colorbar
    uistack(ph1,'top')
end
colorbar
suptitle('Worst Case SAR - 3s2m, All Combination - Noise')
hold off














%% SCRAPS
figurent(112);

jj = 0;
for ii = 3:2:40
    jj = jj+1;
    subplot(4,5,jj+1)
    plot3( bestPos_3s2m(ii,1), bestPos_3s2m(ii,2), min_wcSAR_3s2m(ii),'-x','Color', 'r', 'LineWidth', 3, 'MarkerSize', 10);hold on
    plot3( allPos_3s2m{ii}(:,1), allPos_3s2m{ii}(:,2), all_wcSAR_3s2m{ii}', 'o', 'Color', 'b','LineWidth', 1,'MarkerSize', 2); 
    grid on
%     axis([0 max(all_wcSAR_3s2m{ii}) -100.1 max(allPos_3s2m{ii}(:,1)) -100.1 max(allPos_3s2m{ii}(:,2))])
    axis([ -100 800 -100 800 min(all_wcSAR_3s2m{ii})-0.01 max(all_wcSAR_3s2m{ii})+0.01])
    %     xlim([ 0 25])
    title(['Deepest Sensor @' num2str(points_w(ii))])
    hold off
    lgd = legend(['S1@' num2str(round(bestPos_3s2m(ii,1))) ' & S2@' num2str(round(bestPos_3s2m(ii,2)))],...
        'Location', 'best', 'Orientation', 'vertical');
    lgd.FontSize = 8;
end
suptitle('3s2m - No Noise')
hold off

figurent(107);

% jj = 0;
for ii = 3:40
%     jj = jj+1;
    subplot(5,8,ii)
    plot(all_wcSAR_3s1m{ii}', allPos_3s1m{ii}(:,1), 'o', 'LineWidth', 2); hold on
    plot(min_wcSAR_3s1m(ii), bestPos_3s1m(ii,1),'-x','Color', 'r', 'LineWidth', 3, 'MarkerSize', 10);
    grid on
    axis([0 80 -100 800])
    title(['Deepest Sensor @' num2str(points_w(ii))])
    hold off
end
suptitle('3s1m - No Noise')


hold off
% axis([0 60 -100 800])
figurent();
scatter3(all_wcSAR_3s1m{ii}', allPos_3s1m{ii}(:,1), allPos_3s1m{ii}(:,2));

% hold off
figurent(102);
for ii = 2:40
    plot(all_wcSAR_2s2m{ii}', allPos_2s2m{ii}(:,1), '-o', 'LineWidth', 2); hold on
end
plot(min_wcSAR_2s2m(2:end), bestPos_2s2m(2:end,1),'-x','Color', 'r', 'LineWidth', 3, 'MarkerSize', 10);

grid on
hold off
axis([0 60 -100 800])

figurent(101);
for ii = 3:40
    plot(all_wcSAR_2s1m{ii}', allPos_2s1m{ii}(:,1), '-o', 'LineWidth', 2); hold on
end
plot(min_wcSAR_2s1m, bestPos_2s1m,'-x','Color', 'r', 'LineWidth', 3, 'MarkerSize', 10);
grid on
hold off