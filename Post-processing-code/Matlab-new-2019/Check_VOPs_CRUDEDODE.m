%LOAD RHO AND Q

ncoils = 8;

path_to_results = 'G:\Huygens Simulations\Phantom\Extracted Results\'; %phantom
% path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';

simulation_name = '\RapidCoils_Phantom_Guidewire_2019_06_19_'; %Phantom
% simulation_name = '\RapidCoils_Phantom_Guidewire_Thin_2019_10_23_'; %Phantom Thin Wire
% simulation_name = '\RapidCoils_Duke_Guidewire_2019_06_23_'; %Duke, Short Coil
% simulation_name = '\RapidCoils_Duke_Guidewire_ThickInsu_2019_06_23_'; %Duke, Short Coil
% simulation_name = '\LongRapidCoils_Duke_Guidewire_2019_06_24_'; %Duke, Long Coil

load([path_to_results 'Density' simulation_name 'Density'])
load([path_to_results 'Grid' simulation_name 'Axes'])

xaxis =(XAxis(1:end-1)+XAxis(2:end))'/2;
yaxis =(YAxis(1:end-1)+YAxis(2:end))'/2;
zaxis =(ZAxis(1:end-1)+ZAxis(2:end))'/2;
sz = [length(XAxis)-1 length(YAxis)-1 length(ZAxis)-1];
fsz = length(xaxis)*length(yaxis)*length(zaxis);
szr = [length(xaxis) length(yaxis) length(zaxis(250:end))];

for ii = 1:ncoils
    load([path_to_results 'Point Q Matrices' simulation_name 'PointQMatrices_pt' num2str(ii-1)])
end
Q = cat(3, QMatrix_pt0,QMatrix_pt1,QMatrix_pt2,QMatrix_pt3,QMatrix_pt4,QMatrix_pt5,QMatrix_pt6,QMatrix_pt7);
clear QMatrix_pt0 QMatrix_pt1 QMatrix_pt2 QMatrix_pt3 QMatrix_pt4 QMatrix_pt5 QMatrix_pt6 QMatrix_pt7


%% PREP VOP

temprho = rho;
temprho(rho ~= 0) = 1;
rhoR = reshape(temprho, [sz 1]);

rhoR = rhoR(:,:,250:end); %% UNCOMMENT IF DUKE

Q_reshaped = reshape(Q, [sz 8 8]);

Q_reduced = Q_reshaped(:,:,250:end,:,:); %%DUKE

% Q_reduced = Q_reshaped; %% PHANTOM
% Q_replaced = Q_reduced; %% PHANTOM

[VOPm, vop_map_ind] = VOP_gen_2(Q_reduced, rhoR, 0.10); %BUILD VOPS


%% BUILD D and maxEV

% A1 = repmat(VOPm(1,:,:), [172*213*484 1 1]); %% DUKE
A1 = repmat(VOPm(1,:,:), [sz(1)*sz(2)*sz(3) 1 1]); %% PHANTOM


% Q_reshaped = reshape(Q_replaced, [172*213*484 8 8]); %% DUKE
Q_reshaped = reshape(Q_replaced, [sz(1)*sz(2)*sz(3) 8 8]); %% PHANTOM

VOP_compare = bsxfun(@minus, Q_reshaped, A1);

for ii = 1:length(Q_reshaped)
    VOP_location(ii) = 1/(abs(max(eig(VOP_compare(ii)))));
end

% VOP_location_r = reshape(VOP_location, [172 213 484]); %%DUKE
VOP_location_r = reshape(VOP_location, [sz]); %%PHANTOM

VOP_location_MIP = squeeze(max(VOP_location_r, [], 2));

for ii = 1:length(Q_reshaped)
    maxEV_allQ(ii) = abs(max(eig(squeeze(Q_reshaped(ii,:,:)))));
end

maxEV_MIP_y = squeeze(max(reshape(maxEV_allQ, [172 213 484]), [], 1)); %% DUKE
% maxEV_MIP_y = squeeze(max(reshape(maxEV_allQ, [sz]), [], 1)); %% PHANTOM

maxEV_MIP_x = squeeze(max(reshape(maxEV_allQ, [172 213 484]), [], 2)); %% DUKE
% maxEV_MIP_x = squeeze(max(reshape(maxEV_allQ, [sz]), [], 2)); %% PHANTOM

% figurent;distributionGenerator(maxEV_MIP, xaxis, zaxis(250:end), 1, 0,5); %% DUKE
% figurent;distributionGenerator(maxEV_MIP, xaxis, zaxis, 1, 0,5); %% PHANTOM


%% OTHER RANDOM FIGURE TO CHECK VALUES
imagesc(VOP_location_MIP);


figurent;distributionGenerator(VOP_location_MIP, xaxis, zaxis(250:end), 1, 0.1, 0.4);


figure;for ii=1:10,subplot(2,5,ii);imagesc(squeeze(sum(vop_map_ind==ii,2)));end

%% BUILD DISTANCE VECTOR FROM ALL R TO TIP
% axes = zeros([szr 3]); %% DUKE
% axes = zeros([sz 3]); %% PHANTOM

zaxis_r = zaxis(250:end); %% DUKE
% zaxis_r = zaxis; %% PHANTOM

for ii = 1:length(xaxis)
    for jj = 1:length(yaxis)
        for kk = 1:length(zaxis_r)
            axes(ii,jj,kk,:) = [xaxis(ii) yaxis(jj) zaxis_r(kk)];
        end
    end
end

% t_axes = reshape(axes, [szr(1)*szr(2)*szr(3) 3]); %% DUKE
t_axes = reshape(axes, [sz(1)*sz(2)*sz(3) 3]); %% PHANTOM

[~,idx_max] = max(maxEV_allQ(:));
max_pos = t_axes(idx_max,:);
max_pos_mx = repmat(max_pos, length(t_axes),1);
minus_dist = bsxfun(@minus, t_axes, max_pos_mx);
dist_from_max = minus_dist.^2;
dist_from_max_final = sqrt(sum(dist_from_max, 2));


%% PLOT FOR SIMILIARITY BETWEEN VOP AND Q
data_entries = (1./VOP_location(:));
figurent;
scatter(dist_from_max_final, data_entries,50,'x', 'o')

% axis([7e-4 1 min(data_entries)-0.02 max(data_entries)]); %% DUKE
axis([1.5e-4 1 min(data_entries)-0.25 max(data_entries)]); %% PHANTOM
set(gca, 'XScale', 'log')
grid on
hold on
refline(0,min(data_entries));
set(gca, 'FontSize', 14);
xlabel('Distance from wcSAR Location (m)')
ylabel('Maximum Eigenvalue of D')
title('Wire Tip Dominance over VOP Compression')
legend({'D(r)', 'D(wcSAR Location)'}, 'Location', 'southeast')

%% PLOT LINEAR GRID FIGURE FOR wcSAR
% YZ PLANE
zaxis_r = zaxis(250:end);
a = distributionGenerator(maxEV_MIP_y, yaxis, zaxis_r, 0, 0,1);
figurent;
% distributionGenerator(flipud(maxEV_MIP), flipud(xaxis), flipud(zaxis(250:end)), 1, 0,5);
imagesc(zaxis_r, yaxis , log(rot90((imrotate(a,-90)),2)))
colormap(inferno())
colorbar

caxis([-3.5 1]) %DUKE
% caxis([-5 3]) %PHANTOM

% axis([-0.05 0.05 min(zaxis_r) -0.4]) %DUKE

% axis([-0.72 max(zaxis) -0.025 0.025 ]) %PHANTOM
axis([-0.62 max(zaxis_r) min(yaxis) max(yaxis)]) %DUKE
% title('Worst Case SAR (log_{10})')

set(gca, 'FontSize', 14)
xlabel('z-axis (m)')
ylabel('y-axis (m)')

%% PLOT LINEAR GRID FIGURE FOR wcSAR 
% XZ PLANE
zaxis_r = zaxis(250:end);
a = distributionGenerator(maxEV_MIP_x, xaxis, zaxis_r, 0, 0,1);
figurent;
% distributionGenerator(flipud(maxEV_MIP), flipud(xaxis), flipud(zaxis(250:end)), 1, 0,5);
imagesc(xaxis, zaxis_r, log(fliplr(a)))
colormap(inferno())
colorbar

caxis([-3.5 1]) %DUKE
% caxis([-5 3]) %PHANTOM

% axis([-0.05 0.05 min(zaxis_r) -0.4]) %DUKE
% axis([-0.05 0.05 0.72 max(zaxis_r)]) %PHANTOM

axis([min(xaxis) max(xaxis) -0.62 max(zaxis)]) %DUKE
% axis([min(xaxis) max(xaxis) -0.72 max(zaxis)]) %PHANTOM
% title('Worst Case SAR (log_{10})')

set(gca, 'FontSize', 14, 'YDir', 'normal')
xlabel('x-axis (m)')
ylabel('z-axis (m)')
