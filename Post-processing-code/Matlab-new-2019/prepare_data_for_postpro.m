ncoils = 8;

% path_to_results = 'G:\Huygens Simulations\Phantom\Extracted Results\'; %phantom
path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';

% simulation_name = '\RapidCoils_Phantom_Guidewire_2019_06_19_'; %Phantom
% simulation_name = '\RapidCoils_Phantom_Guidewire_Thin_2019_10_23_'; %Phantom Thin Wire
% simulation_name = '\RapidCoils_Phantom_Guidewire_ThickInsu_2019_06_19_'; %Phantom Thick Insulation
% simulation_name = '\RapidCoils_Duke_Guidewire_2019_06_23_'; %Duke, Short Coil
% simulation_name = '\RapidCoils_Duke_Guidewire_ThickInsu_2019_06_23_'; %Duke, Short Coil
% simulation_name = '\LongRapidCoils_Duke_Guidewire_2019_06_24_'; %Duke, Long Coil
simulation_name = '\LongRapidCoils_Duke_Guidewire_ThickInsu_2019_06_24_'; %Phantom Thick Insulation


load([path_to_results 'Density' simulation_name 'Density'])
load([path_to_results 'EM Current' simulation_name 'EMCurrent'])
load([path_to_results 'Grid' simulation_name 'Axes'])

xaxis =(XAxis(1:end-1)+XAxis(2:end))'/2;
yaxis =(YAxis(1:end-1)+YAxis(2:end))'/2;
zaxis =(ZAxis(1:end-1)+ZAxis(2:end))'/2;
sz = [length(XAxis)-1 length(YAxis)-1 length(ZAxis)-1];
fsz = length(xaxis)*length(yaxis)*length(zaxis);

for ii = 1:ncoils
    load([path_to_results 'B1 Field' simulation_name 'B1Fields_C' num2str(ii)])
end
B1 = cat(3, B1_1, B1_2, B1_3, B1_4, B1_5, B1_6, B1_7, B1_8);
clear B1_1 B1_2 B1_3 B1_4 B1_5 B1_6 B1_7 B1_8
B1p = squeeze(B1(:,1,:));
B1p_s = sum(reshape(B1p, [sz 8]),4);

for  ii = 1:ncoils
    load([path_to_results 'E Field' simulation_name 'EFields_C' num2str(ii)])
end
E = cat(3, E_1, E_2, E_3, E_4, E_5, E_6, E_7, E_8);
clear E_1 E_2 E_3 E_4 E_5 E_6 E_7 E_8
Eabs = squeeze(sqrt(E(:,1,:).^2 + E(:,2,:).^2 + E(:,3,:).^2));

% for ii = 1:ncoils
%     load([path_to_results 'Point Q Matrices' simulation_name 'PointQMatrices_pt' num2str(ii-1)])
% end
% Q = cat(3, QMatrix_pt0,QMatrix_pt1,QMatrix_pt2,QMatrix_pt3,QMatrix_pt4,QMatrix_pt5,QMatrix_pt6,QMatrix_pt7);
% clear QMatrix_pt0 QMatrix_pt1 QMatrix_pt2 QMatrix_pt3 QMatrix_pt4 QMatrix_pt5 QMatrix_pt6 QMatrix_pt7



%% Section Attempting to Create Heart Mask


% xtip = 31;
% ytip = 31;
% ztip = 288; % THIS IS FOR PHANTOM THICK


xtip = 44;
ytip = 116;
ztip = 584; %THIS IS FOR DUKE

%% FOR PHANTOM
dielectric = 'Duke'% 'Phantom' %

if strcmp(dielectric, 'Phantom')
    maskR = reshape(rho, [sz 1]);
    Eabs_s = sum(reshape(Eabs, [sz 8]),4);
    temprho = rho;
    temprho(rho~=0) = 1;
    for xx = 1:sz(1)
        for yy = 1:sz(2)
            for zz = 1:sz(3)
                if (xaxis(xx)-xaxis(xtip))^2+(yaxis(yy)-yaxis(ytip))^2+(zaxis(zz)-zaxis(ztip))^2 <= 0.05^2
                    maskR(xx,yy,zz) = 1;
                else
                    maskR(xx,yy,zz) = 0;
                end
            end
        end
    end

    filledmaskR = maskR.*reshape(temprho, [sz 1]);
    wiredmaskR = filledmaskR;
    
%     filledmaskR(15:49, 13:52, 1:289) = 0; % FOR V2RCOIL Thsi was atest
%     removing even more area around wire. Jo's suggestion 

    filledmaskR(23:42, 23:42, 1:289) = 0; % FOR V2RCOIL these were the original values
%     a = abs(squeeze(B1p_s(:,31,:)).*squeeze(filledmaskR(:,31,:)));
%     figure;
%     newE = distributionGenerator(a(:,270:end), xaxis, zaxis(270:end), 1, 0, 1e-6, 'nearest');
end


if strcmp(dielectric, 'Duke')%% FOR DUKE
    maskR = rho; %Load Mask Properties first, if is in cell form run: rho = cellfun(@(x) double(x), rho);
    maskR(maskR == 1049.75) = 1; %Density of Heart Lumen (blood)
    maskR(maskR == 1.080800048828125e+03) = 1; %Density of Heart Muscle (plot rho and check precis value)
    maskR(maskR ~= 1) = 0; % Make everything else 0
    maskR = reshape(maskR, sz); %Reshape to 3D

    % maskR(14:123,2:106,318:345); % Check in S4L what small box encompasses
    % the Heart, as blood as same density you need to make everything else
    % outside this box 0, hence the following lines:

    % %%%% RCOIL
    % maskR(1:14,:,:) = 0;
    % maskR(:,1:2,:) = 0;
    % maskR(:,:,1:318) = 0;
    % maskR(123:end,:,:) = 0;
    % maskR(:,106:end,:) = 0;
    % maskR(:,:,345:end) = 0;

    %%%% V2RCOIL
    % maskR(1,:,:) = 0;
    % % maskR(:,1:7,:) = 0;
    % maskR(:,:,1:318) = 0;
    % maskR(101:end,:,:) = 0;
    % maskR(:,106:end,:) = 0;
    % maskR(:,:,343:end) = 0;

    %%%% FRCOIL
    % maskR(1:12,:,:) = 0;
    % maskR(:,1:2,:) = 0;
    % maskR(:,:,1:322) = 0;
    % maskR(130:end,:,:) = 0;
    % maskR(:,106:end,:) = 0;
    % maskR(:,:,349:end) = 0;

    %%%% REAL WIRE V2OIL
    maskR(1:6,:,:) = 0;
    maskR(:,1:32,:) = 0;
    maskR(:,:,1:555) = 0;
    maskR(129:end,:,:) = 0;
    maskR(:,184:end,:) = 0;
    maskR(:,:,646:end) = 0;

    % Voxelization makes everything patchy. Dilate and Erode to fill patches
    se = strel('sphere',2); %Patches are small so small Dilate radius will do
    dilmaskR = imdilate(maskR,se); %Dilate
    filledmaskR = imerode(dilmaskR,se); %Erode, Heart Region should now be properly masked

    wiredmaskR = filledmaskR;
    % filledmaskR(50:81, 25:47, 1:333) = 0; % FOR RCOIL
    % filledmaskR(34:64, 17:45, 1:333) = 0; % FOR V2RCOIL
    % filledmaskR(46:77, 20:50, 1:340) = 0; % FOR FRCOIL
    filledmaskR(44:65, 102:120, 584:end) = 0; % FOR REAL WIRE
end
%%
% for ii = 15
%     distributionGenerator(abs(squeeze(B1p_s(ii,:,500:end).*filledmaskR(ii,:,500:end))), yaxis, zaxis(500:end), 1, 0, 1e-6, 'nearest');
% end
    %% BOTH

filledmask = reshape(filledmaskR, [fsz 1]);
wiredmask = reshape(wiredmaskR, [fsz 1]);

maskMLS = filledmask(filledmask~=0);

sMask = wiredmask(wiredmask ~= 0);

%%
% figure;
for ii = 1:8
% %%%% HEART SECTION    
    maskedB1(:,ii) = (B1p(:,ii)+(1e-8*filledmask)).*filledmask;%.*w(ii);
    maskedB1wired(:,ii) = (B1p(:,ii)+(1e-8*wiredmask)).*wiredmask;
end

maskedB1_reshaped = reshape(maskedB1, [sz 8]);
for ii = 1:8
    lowresB1_reshaped(:,:,:,ii) = Gen3D(squeeze(maskedB1_reshaped(:,:,:,ii)), xaxis, yaxis, zaxis, 30, 30, 150);
end
lowresB1 = reshape(lowresB1_reshaped, [30*30*150 8]);

% nonzero_maskedB1 = maskedB1(maskedB1~=0);
nonzero_maskedB1 = lowresB1(lowresB1~=0);
% sRB1_x = RB1_x(RB1_x~=0);
nonzero_maskedB1 = nonzero_maskedB1(:) - 1e-8;%repmat(maskMLS(:),8,1);
% wired_pos = find(maskedB1wired(:,1));
nonzero_maskedB1wired = maskedB1wired(maskedB1wired~=0);
nonzero_maskedB1wired = nonzero_maskedB1wired - 1e-8; %repmat(sMask(:),8,1);

% heartRB1 = reshape(RB1,[sz 8]);
% heartRB1wired = reshape(RB1wired, [sz 8]);


%% GET Q-TIP
% Q_reshaped = reshape(Q, [sz 8 8]);
% Q_tip = squeeze(Q_reshaped(xtip,ytip,ztip,:,:));
Q_tip = VOPm;

%%
handle = 'FinalRapidCoil_Duke_15T_RealWire_StrippedTip';
save(['G:/ISMRM2019/mats for Optimised script/' handle '_tipQ'],'Q_tip','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_Masks'],'filledmask', 'wiredmask','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_maskMLS'],'maskMLS','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_J'],'J','Jaxis','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_heartB1'],'sRB1','sRB1wired','wired_pos','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_Density'],'rho','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle 'p_Axis'],'fsz','sz','xaxis','yaxis','zaxis','-v7.3')
