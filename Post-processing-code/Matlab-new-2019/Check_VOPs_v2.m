%LOAD RHO AND Q

ncoils = 8;
cutoff_z = 400; %800; % 

% path_to_results = 'G:\Huygens Simulations\Phantom\Extracted Results\'; %phantom
path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';

simulation_name = '\RapidCoils_Duke_Guidewire_HighRes_2020_01_06_'; %Duke Short Coil High Res
% simulation_name = '\LongRapidCoils_Duke_Guidewire_HighRes_2020_02_05_'; %Duke Long Coil High Res
% simulation_name = '\RapidCoils_Duke_Guidewire_ThickInsu_2020_02_10_'; %Duke Short Coil Thick Insu

% simulation_name = '\RapidCoils_Phantom_Guidewire_2020_02_12_'; %Phantom Short Coil

load([path_to_results 'Density' simulation_name 'Density'])
load([path_to_results 'Grid' simulation_name 'Axes'])

xaxis = XAxis;
yaxis = YAxis;
zaxis = ZAxis;
sz = [length(XAxis) length(YAxis) length(ZAxis)];
fsz = length(xaxis)*length(yaxis)*length(zaxis);
szr = [length(xaxis) length(yaxis) length(zaxis(cutoff_z:end))];

Q = zeros(fsz,8,8, 'single');
for ii = 1:ncoils
    for jj = 1:ncoils
        Q_entry = load([path_to_results 'Interpolated Q Matrices' simulation_name 'InterpolatedQMatrices_pt' num2str(ii) num2str(jj)]);
        Q_entry = struct2cell(Q_entry);
        Q(:,ii,jj) = Q_entry{1};
        fprintf(['Building Q-Matrix: \nCurrently on Line ' num2str(ii) ', Column ' num2str(jj) '\n'])
    end
end


%% PREP VOP

temprho = rho;
temprho(rho ~= 0) = 1;
rhoR = permute(reshape(temprho, [sz(1) sz(3) sz(2)]), [1 3 2]);

Q_reshaped = permute(reshape(Q, [sz(1) sz(3) sz(2) 8 8]), [1 3 2 4 5]);

rhoR = rhoR(:,:,cutoff_z:end); %% UNCOMMENT IF DUKE
Q_reshaped = Q_reshaped(:,:,cutoff_z:end,:,:); %% UNCOMMENT IF DUKE

%% Check if Q-matrix is as it should be before calculating VOPs
fszr = sz(1)*sz(2)*length(cutoff_z:sz(3)); % IF DUKE, CHANGE START VALUE
% fszr = sz(1)*sz(2)*sz(3);

% Q_line = reshape(Q_reshaped, [fszr 8 8]);
Q_line = reshape(avgQ_1g_r, [fszr 8 8]);
for ii = 1:fszr
    Q_max_ev(ii) = max(eig(squeeze(Q_line(ii,:,:))));
end

Q_max_ev = reshape(Q_max_ev, [sz(1) sz(2) length(cutoff_z:sz(3))]);

figurent;
imagesc(abs(squeeze(max(Q_max_ev,[],2))));
%% If Q-matrices look right then call VOP_gen_2 to calculate VOPs

%  [VOPm, vop_map_ind] = VOP_gen_2(Q_reshaped, rhoR, 0.1); %BUILD VOPS (Q, Mask of points to check (rho>0 = 1), Error %)
 [VOPm, vop_map_ind] = VOP_gen_2(avgQ_1g_r(:,:,400:end,:,:), rhoR(:,:,400:end), 0.05); 

%% BUILD Q-MATRICES VISUAL REP
maxEV_MIP_y = squeeze(max(Q_max_ev, [], 1)); %% DUKE
% maxEV_MIP_y = squeeze(max(reshape(maxEV_allQ, [sz]), [], 1)); %% PHANTOM

maxEV_MIP_x = squeeze(max(Q_max_ev, [], 2)); %% DUKE
% maxEV_MIP_x = squeeze(max(reshape(maxEV_allQ, [sz]), [], 2)); %% PHANTOM

% figurent;distributionGenerator(maxEV_MIP, xaxis, zaxis(250:end), 1, 0,5); %% DUKE
% figurent;distributionGenerator(maxEV_MIP, xaxis, zaxis, 1, 0,5); %% PHANTOM

%% YZ PLANE
zaxis_r = ZAxis(cutoff_z:end);
a = distributionGenerator(maxEV_MIP_y, yaxis, zaxis_r, 0, 0,1);
figurent;
% distributionGenerator(flipud(maxEV_MIP), flipud(xaxis), flipud(zaxis(250:end)), 1, 0,5);
imagesc(zaxis_r, yaxis , log(rot90((imrotate(a,-90)),2)))
colormap(inferno())
colorbar

caxis([-3.5 1]) %DUKE
% caxis([-5 3]) %PHANTOM

% axis([-0.05 0.05 min(zaxis_r) -0.4]) %DUKE

% axis([-0.72 max(zaxis) -0.025 0.025 ]) %PHANTOM
axis([-0.62 max(zaxis_r) min(yaxis) max(yaxis)]) %DUKE
% title('Worst Case SAR (log_{10})')

set(gca, 'FontSize', 18)
xlabel('z-axis (m)')
ylabel('y-axis (m)')

set(gcf, 'Position',  [100, 100, 800, 300])

%% XZ PLANE
zaxis_r = zaxis(cutoff_z:end);
a = distributionGenerator(maxEV_MIP_x, xaxis, zaxis_r, 0, 0,1);
figurent;
% distributionGenerator(flipud(maxEV_MIP), flipud(xaxis), flipud(zaxis(250:end)), 1, 0,5);
imagesc(xaxis, zaxis_r, log(fliplr(a)))
colormap(inferno())
colorbar

caxis([-3.5 1]) %DUKE
% caxis([-5 3]) %PHANTOM

% axis([-0.05 0.05 min(zaxis_r) -0.4]) %DUKE
% axis([-0.05 0.05 0.72 max(zaxis_r)]) %PHANTOM

axis([min(xaxis) max(xaxis) -0.62 max(zaxis_r)]) %DUKE
% axis([min(xaxis) max(xaxis) -0.72 max(zaxis)]) %PHANTOM
% title('Worst Case SAR (log_{10})')

set(gca, 'FontSize', 18, 'YDir', 'normal')
xlabel('x-axis (m)')
ylabel('z-axis (m)')

set(gcf, 'Position',  [100, 100, 395, 700])

