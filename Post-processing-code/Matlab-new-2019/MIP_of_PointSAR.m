for ii = 1:length(Q)
    maxEV_allQ(ii) = abs(max(eig(squeeze(Q(ii,:,:)))));
end

maxEV_allQ_reshape = reshape(maxEV_allQ, sz);
%%
mip_maxEV = max(reshape(maxEV_allQ, sz),[],2);
%%

figurent; distributionGenerator(maxEV_allQ_mip, xaxis, zaxis, 1); colormap(inferno)

title('MIP of Maximum Eigenvalue of Q-Matrix - Non-uniform grid')
xlabel('z (mm)')
ylabel('x (mm)')
colormap(inferno)
colorbar
caxis([0 1])
set(gcf,'color','w');
%%
figure;
distributionGenerator(log10(squeeze(mip_maxEV(:,120:end))'), zaxis(120:end), xaxis, 1);
title('Log10 of MIP of Maximum Eigenvalue of Q-Matrix - Uniform grid ')
xlabel('z (mm)')
ylabel('x (mm)')
colormap(jet)
colorbar
caxis([-5 2])
set(gcf,'color','w');

%%
[~,idx_maxQ] = max(maxEV_allQ);

[xtip, ytip, ztip] = ind2sub(sz, idx_maxQ);
