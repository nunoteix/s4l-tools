%%
npoints = 40;
% %%%%%% IF NEW CURERNT METHOD
ltrue(1) = 0;
for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end
% lshifted = ltrue - ltrue(19);
for ii = 1:8
    Jinterp_ini(ii,:) = interp1(J(ii,:), 1:36/2000:36);
end
linterp = interp1(ltrue,1:36/2000:36);

% points_w = linspace(-100,570,npoints)'; %For Phantom
% pedge = ltrue(21); % For Phantom

points_w = linspace(-100,790,npoints)'; %For Duke
pedge = ltrue(16); % For Duke

linterp_shift = linterp-pedge;
[~, pedge_ind] = min(abs(linterp-pedge));
st = linterp(2)-linterp(1);
kk = 1;
mm = 1;
for ii = 1:length(points_w)
    for jj = ii+1:length(points_w)
        points_pos(kk,1:2) = [points_w(ii) points_w(jj)];
        for ll = jj+1:length(points_w)
                points_3_pos(mm,1:3) = [points_w(ii) points_w(jj) points_w(ll)];
                mm = mm+1;
        end
        kk = kk + 1;
    end
end
Jinterp = permute(Jinterp_ini, [2 1]);


% FOR ALL
jetblack = jet(4096);
jetblack(1,:) = [0,0,0];

%% find WORST CASE (ALL MODES) AVERAGE B1+
nChannels = 8;

% MLS SHIM
nonzero_maskedB1reshaped = reshape(nonzero_maskedB1,[],nChannels);
nonzero_maskedB1wired_reshaped = reshape(nonzero_maskedB1wired,[],nChannels);

maskMLS = ones(length(nonzero_maskedB1reshaped(:,1)),1);

[u,s,v] = svd(Jinterp);
scaling_factor = 1e-6/abs(mean(nonzero_maskedB1reshaped(:)));

[w,z] = MLS_modeshim_max_amp(nonzero_maskedB1reshaped.*scaling_factor*1e6,v, maskMLS,1.5*ones(nChannels,1),[],0.9,20,0.08,'enabled');
w = w.*scaling_factor;
[avgB1_full,~] = applyShim(nonzero_maskedB1wired_reshaped, w, nChannels);
b1_normVal = (avgB1_full.^2)./sum(abs(w).^2);

% shimmed_B1 = zeros(length(wiredmask),1);
% shimmed_B1(wired_pos(:,1)~=0) = shimmedB1_holder;
% shimmed_B1 = reshape(shimmed_B1, [sz 1]); 
%%
Q_tip_normed = Q_tip./b1_normVal;
maxEV_fullQ(1:npoints) = max(eig(Q_tip_normed));
maxEV_fullQ_noNorm(1:npoints) = max(eig(Q_tip));

% avgB1 = zeros(15,2);

%% SVD + MODING B1 + MLS + EIG Q
% [maxEV_redQ_1s1m, maxEV_redQ_1s1m_noNorm, shimmedB1_1s, modes_1s, b1weights_1s, w1, avgB1_1s] = oneSensorTipQv2(nChannels, points_w, pedge_ind, st, J, maskMLS, sRB1r, sRB1wiredr, Q_tip);

% [maxEV_redQ_2s2m, maxEV_redQ_2s2m_noNorm, shimmedB1_2s, modes_2s, b1weights_2s, w2, avgB1_2s] = twoSensorTipQv2(nChannels, points_pos, pedge_ind, st, J, maskMLS, sRB1r, sRB1wiredr, Q_tip);

for kk = 1:length(points_w)
    [modes_1s1m(:,:,kk), w1s1m(:,kk), avgB1_1s1m(kk)] = TipQ(kk, 1, nChannels, 1, points_w, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, 'enabled');
end

for kk = 1:length(points_pos)
    [modes_2s1m(:,:,kk), w2s1m(:,kk), avgB1_2s1m(kk)] = TipQ(kk, 1, nChannels, 2, points_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, 'enabled');

    [modes_2s2m(:,:,kk), w2s2m(:,kk), avgB1_2s2m(kk)] = TipQ(kk, 2, nChannels, 2, points_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, 'enabled');
end

for kk = 1:length(points_3_pos)
    [modes_3s1m(:,:,kk), w3s1m(:,kk), avgB1_3s1m(kk)] = TipQ(kk, 1, nChannels, 3, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, 'enabled');

    [modes_3s2m(:,:,kk), w3s2m(:,kk), avgB1_3s2m(kk)] = TipQ(kk, 2, nChannels, 3, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, 'enabled');

    [modes_3s3m(:,:,kk), w3s3m(:,kk), avgB1_3s3m(kk)] = TipQ(kk, 3, nChannels, 3, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, 'enabled');
end
 b1weights_1s1m = (avgB1_1s1m.^2)./sum(abs(w1s1m).^2,1);
 b1weights_2s1m = (avgB1_2s1m.^2)./sum(abs(w2s1m).^2,1);
 b1weights_2s2m = (avgB1_2s2m.^2)./sum(abs(w2s2m).^2,1);
 b1weights_3s1m = (avgB1_3s1m.^2)./sum(abs(w3s1m).^2,1);
 b1weights_3s2m = (avgB1_3s2m.^2)./sum(abs(w3s2m).^2,1);
 b1weights_3s3m = (avgB1_3s3m.^2)./sum(abs(w3s3m).^2,1);
 
% [maxEV_redQ_2s1m, maxEV_redQ_2s1m_noNorm, shimmedB1_2s1m, modes_2s1m, b1weights_2s1m, w2s1m, avgB1_2s1m] = twoSensorTipQ(1,nChannels, points_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_2s2m, maxEV_redQ_2s2m_noNorm, shimmedB1_2s, modes_2s, b1weights_2s, w2, avgB1_2s] = twoSensorTipQ(2, nChannels, points_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_3s3m, maxEV_redQ_3s3m_noNorm, shimmedB1_3s, modes_3s, b1weights_3s, w3, avgB1_3s] = threeSensorTipQ(3, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_3s2m, maxEV_redQ_3s2m_noNorm, shimmedB1_3s2m, modes_3s2m, b1weights_3s2m, w3s2m, avgB1_3s2m] = threeSensorTipQ(2, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_3s1m, maxEV_redQ_3s1m_noNorm, shimmedB1_3s1m, modes_3s1m, b1weights_3s1m, w3s1m, avgB1_3s1m] = threeSensorTipQ(1, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);
% 
%% NORMED


wcSAR_full = abs(maxEV_fullQ)*1e-12;
deepestSens_wcSAR_1s1m = abs(maxEV_redQ_1s1m)*1e-12;
for ii = 2:length(points_w)
    logic_idx = find(points_pos(:,2) == points_w(ii));
    deepestSens_wcSAR_2s1m(ii) = abs(min(maxEV_redQ_2s1m(logic_idx)))*1e-12;
    deepestSens_wcSAR_2s2m(ii) = abs(min(maxEV_redQ_2s2m(logic_idx)))*1e-12;
end

for ii = 3:length(points_w)
    logic_idx = find(points_3_pos(:,3) == points_w(ii));
    deepestSens_wcSAR_3s1m(ii) = abs(min(maxEV_redQ_3s1m(logic_idx)))*1e-12;
    deepestSens_wcSAR_3s2m(ii) = abs(min(maxEV_redQ_3s2m(logic_idx)))*1e-12;
    deepestSens_wcSAR_3s3m(ii) = abs(min(maxEV_redQ_3s3m(logic_idx)))*1e-12;
end

figure;
plot(points_w, wcSAR_full, 'o', 'LineWidth', 2); hold on
plot(points_w,deepestSens_wcSAR_1s1m,'o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s1m(2:end), 'o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s2m(2:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s1m(3:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s2m(3:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s3m(3:end), 'o', 'LineWidth',2)
grid on

legend('Full', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')


%% NO NORM


wcSAR_full_noNorm = abs(maxEV_fullQ_noNorm);
deepestSens_wcSAR_1s1m_noNorm = abs(maxEV_redQ_1s1m_noNorm);
for ii = 2:length(points_w)
    logic_idx = find(points_pos(:,2) == points_w(ii));
    deepestSens_wcSAR_2s1m_noNorm(ii) = abs(min(maxEV_redQ_2s1m_noNorm(logic_idx)));
    deepestSens_wcSAR_2s2m_noNorm(ii) = abs(min(maxEV_redQ_2s2m_noNorm(logic_idx)));
end

for ii = 3:length(points_w)
    logic_idx = find(points_3_pos(:,3) == points_w(ii));
    deepestSens_wcSAR_3s1m(ii) = abs(min(maxEV_redQ_3s1m(logic_idx)))*1e-12;
    deepestSens_wcSAR_3s2m(ii) = abs(min(maxEV_redQ_3s2m(logic_idx)))*1e-12;
    deepestSens_wcSAR_3s3m(ii) = abs(min(maxEV_redQ_3s3m(logic_idx)))*1e-12;
end

figure;
plot(points_w, wcSAR_full, 'o', 'LineWidth', 2); hold on
plot(points_w,deepestSens_wcSAR_1s1m,'o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s1m(2:end), 'o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s2m(2:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s1m(3:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s2m(3:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s3m(3:end), 'o', 'LineWidth',2)
grid on

legend('Full', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('wcSAR - Straight Wire - Phantom')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')

%%

[u_fullWire,s_fullWire,v_fullWire]=svd(Jinterp(:,:));


%% MAKE MAPS
cmap = cmapTipQ(maxEV_redQ_2s2m, maxEV_redQ_1s1m, maxEV_fullQ, points_w);

b1wMap = b1weightsMap(b1weights_2s, b1weights_1s, b1_normVal, points_w);

noNormMap = cmapTipQ_noNorm(maxEV_redQ_2s2m_noNorm, maxEV_redQ_1s1m_noNorm, maxEV_fullQ_noNorm, points_w);

%% THREE SENSORS
cmap = cmapTipQ(maxEV_redQ_2s2m, maxEV_redQ_1s1m, maxEV_fullQ, points_w);

b1wMap = b1weightsMap(b1weights_2s, b1weights_1s, b1_normVal, points_w);

noNormMap = cmapTipQ_noNorm(maxEV_redQ_2s2m_noNorm, maxEV_redQ_1s1m_noNorm, maxEV_fullQ_noNorm, points_w);