function cube_coord = genMassCube(rho3D, xx, yy, zz, x_step, y_step, z_step, grams)

si=size(rho3D);
% range = floor((nthroot((grams*1e-3)/(rho3D(xx,yy,zz)*(x_step*y_step*z_step)),3)-1)/2)-2;
% disp(range)
% if range < 0
%     range = 0;
% end
range = 0;
% 
cube_mass = 0;   
old_cube_mass = 0;
old_cube_coord = zeros(1,6);

while(cube_mass < grams*1e-3)
    
    range = range+1;
    
    if xx-range < 1
        x_min = 1;
    else
        x_min = xx-range;
    end
    
    if xx+range > si(1)
        x_max = si(1);
    else
        x_max = xx+range;
    end
        
    if yy-range < 1
        y_min = 1;
    else
        y_min = yy-range;
    end
    
    if yy+range > si(2)
        y_max = si(2);
    else
        y_max = yy+range;
    end
    
    if zz-range < 1
        z_min = 1;
    else
        z_min = zz-range;
    end
    
    if zz+range > si(3)
        z_max = si(3);
    else
        z_max = zz+range;
    end        
    
    cube_rho = rho3D(x_min:x_max,y_min:y_max,z_min:z_max);
    
    cube_rho_list = cube_rho(:);
    cube_rho_list_nz = cube_rho_list(cube_rho_list > 0);
    
    cube_mass = sum(cube_rho_list_nz)*(x_step*y_step*z_step);
    cube_coord = [x_min, x_max, y_min, y_max, z_min, z_max];
    
    if cube_mass > grams*1e-3
       diff_new = abs((grams*1e-3)-cube_mass);
       diff_old = abs((grams*1e-3)-old_cube_mass);
       if diff_old < diff_new
           cube_coord = old_cube_coord;
       end
    else
        old_cube_mass = cube_mass;
        old_cube_coord = cube_coord;
    end
end


end