%% FIGURE 2 - SAR 1G HotSPOTS
%LOAD RHO AND Q

ncoils = 8;
cutoff_z = 400; % 800; % % Duke vs Phantom

path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';
% path_to_results = 'G:\Huygens Simulations\Phantom\Extracted Results\';

% HERE WE CHOOSE WHAT SIMULATION TO GET THE RESULTS FROM - THIS NAME WAS
% PRE-CHOSEN WHEN EXPORTING FROM S4L
simulation_name = '\RapidCoils_Duke_Guidewire_HighRes_2020_01_06_'; %Duke, ShortCoil, High Res
% simulation_name = '\LongRapidCoils_Duke_Guidewire_HighRes_2020_02_05_'; %Duke, Long Coil, High Res
% simulation_name = '\RapidCoils_Phantom_Guidewire_2020_02_12_'; %Phantom, Short Coil, High Res

load('G:\Mass Average Data\AvgQ1g_RapidCoils_Duke_Guidewire_2020_03_22.mat'); %Duke, ShortCoil, High Res
% load('G:\Mass Average Data\AvgQ1g_LongRapidCoils_Duke_Guidewire_2020_03_25.mat'); %Duke, Long Coil, High Res
% load('G:\Mass Average Data\AvgQ1g_RapidCoils_Phantom_Guidewire_2020_03_24'); Short Coil, High Res

load([path_to_results 'Density' simulation_name 'Density'])
load([path_to_results 'Grid' simulation_name 'Axes'])

xaxis = XAxis;
yaxis = YAxis;
zaxis = ZAxis;
sz = [length(XAxis) length(YAxis) length(ZAxis)];
fsz = length(xaxis)*length(yaxis)*length(zaxis);
szr = [length(xaxis) length(yaxis) length(zaxis(cutoff_z:end))];


% PREP VOP

temprho = rho;
temprho(rho ~= 0) = 1;
rhoR = permute(reshape(temprho, [sz(1) sz(3) sz(2)]), [1 3 2]);


rhoR = rhoR(:,:,cutoff_z:end); %% UNCOMMENT IF DUKE

% Check if Q-matrix is as it should be before calculating VOPs
fszr = sz(1)*sz(2)*length(cutoff_z:sz(3)); % IF DUKE, CHANGE START VALUE

Q_line = reshape(avgQ_1g, [fszr 8 8]);
for ii = 1:fszr
    Q_max_ev(ii) = max(eig(squeeze(Q_line(ii,:,:))));
end

Q_max_ev = reshape(Q_max_ev, [sz(1) sz(2) length(cutoff_z:sz(3))]);

% figurent;
% imagesc(abs(squeeze(max(Q_max_ev,[],2))));

% BUILD Q-MATRICES VISUAL REP
maxEV_MIP_y = squeeze(max(Q_max_ev, [], 1)); %% DUKE


maxEV_MIP_x = squeeze(max(Q_max_ev, [], 2)); %% DUKE

%% YZ PLANE
zaxis_r = ZAxis(cutoff_z:end);
a = distributionGenerator(maxEV_MIP_y, yaxis, zaxis_r, 0, 0,1);
figurent;
imagesc(yaxis.*1000 ,zaxis_r.*1000,  log(fliplr(a)))
colormap(inferno())
colorbar


% caxis([-3.5 1]) %DUKE
axis([min(yaxis) max(yaxis) -0.7 max(zaxis_r) ]*1000) %DUKE

caxis([-5 1]) %PHANTOM
% axis([ -0.025 0.025 -0.7 max(zaxis)  ].*1000) %PHANTOM

% title('Worst Case SAR (log_{10})')

set(gca, 'FontSize', 14, 'YDir', 'normal')
xlabel('y-axis (mm)')
ylabel('z-axis (mm)')

% set(gcf, 'Position',  [100, 100, 800, 300])
set(gcf, 'Position',  [100, 100, 300, 700])


%% XZ PLANE
zaxis_r = zaxis(cutoff_z:end);
a = distributionGenerator(maxEV_MIP_x, xaxis, zaxis_r, 0, 0,1);
figurent;
imagesc(xaxis*1000, zaxis_r*1000, log(fliplr(a)))
colormap(inferno())
colorbar


% axis([-0.05 0.05 min(zaxis_r) -0.4]) %DUKE
% caxis([-3.5 1]) %DUKE

caxis([-5 1]) %PHANTOM
% axis([-0.05 0.05 0.72 max(zaxis_r)]) %PHANTOM

axis([min(xaxis) max(xaxis) -0.7 max(zaxis_r)]*1000) %DUKE
% axis([min(xaxis) max(xaxis) -0.72 max(zaxis)]) %PHANTOM
% title('Worst Case SAR (log_{10})')

set(gca, 'FontSize', 14, 'YDir', 'normal')
xlabel('x-axis (mm)')
ylabel('z-axis (mm)')

set(gcf, 'Position',  [100, 100, 300, 700])


%% Current Decomposed Into Modes - Figure 3
% Load For Plotter for all cases

npoints = length(points_1s);
[~,sv,v] = svd(Jinterp*1000);
sv=nonzeros(sv);

figurent;plot(L, abs(Jinterp*v)*1000, 'LineWidth', 2); grid on
hold on;
plot(L, abs(Jinterp*phase_quad)*1000,'k-.', 'LineWidth', 1.5 );

% axis([-600 800 0 20])
axis([-800 800 0 20])
ylabel('Mode Decomposition of {\bf{J}} (mA/\surdW)')
xlabel('Distance along Wire (mm)')
legend({['\bf{J*V_{1}}, s.v. =  ' num2str(round(sv(1),1))],...
    ['\bf{J*V_{2}}, s.v. = ' num2str(round(sv(2),1))],...
    ['\bf{J*V_{3}}, s.v. =  ' num2str(round(sv(3),1))],...
    ['\bf{J*V_{4}}, s.v. =  ' num2str(round(sv(4),1))],...
    ['\bf{J*V_{5}}, s.v. =  ' num2str(round(sv(5),1))],...
    ['\bf{J*V_{6}}, s.v. =  ' num2str(round(sv(6),1))],...
    ['\bf{J*V_{7}}, s.v. =  ' num2str(round(sv(7),1))],...
    ['\bf{J*V_{8}}, s.v. =  ' num2str(round(sv(8),1))],...
    '\bf{J*w_{quad}}'}, 'Location', 'northwest')
set(gca,'FontSize',12)
set(gcf, 'Position',  [0, 0, 1050, 350])
%% Full Knowledge Plot (Figure 4)
% Needs the ForPlotterv9 data load in
% Run 3 times, one for each Scenario

figurent;
subplot(1,4,1)
set(gcf, 'Position',  [0, 0, 750, 125])
% set(gcf, 'Position',  [0, 0, 1050, 310])

bar(0:3, wcSAR_fullK(opt_l(1),:), 'r')
% title('SAR_{wc}')
% xlabel('Modes Extracted ({\itK})')
ylabel('W/Kg/W')
set(gca,'FontSize',10)
set(gca,'xtick',[])
axis([-1 4 0 4])

subplot(142)
for ii = 1:4
    plot_val(ii) = sqrt(sum(abs(w_fullK(:,opt_l(ii),ii).^2),1));
end
bar(0:3, 1./plot_val, 'b')
% title('B_1^+ (Shimmed)')
% xlabel('Modes Extracted ({\itK})')
ylabel('\muT/\surdW')
set(gca,'FontSize',10)
set(gca,'xtick',[])
axis([-1 4 0 0.3])

subplot(143)
for ii = 1:4
    plot_val(ii) = wcSAR_fullK(opt_l(ii),ii).*sum(abs(w_fullK(:,opt_l(ii),ii).^2),1);
end
bar(0:3, plot_val, 'g')
% title('SAR_{swc}')
% xlabel('Modes Extracted ({\itK})')
ylabel('W/Kg/\muT^2')
set(gca,'FontSize',10)
set(gca,'xtick',[])
axis([-1 4 0 abs(plot_val(1))+10])

subplot(144)
for ii = 1:4
    plot_val(ii) = w_fullK(:,opt_l(ii),ii)'*VOPm*w_fullK(:,opt_l(ii),ii);
end
bar(0:3, plot_val(1:4), 'w'	); hold on
% bar(1, plot_val(2), 'EdgeColor', [0.8500 0.3250 0.0980]	)
% bar(2, plot_val(3), 'EdgeColor', [0.9290 0.6940 0.1250]	)
% bar(3, plot_val(4), 'EdgeColor', [0.4940 0.1840 0.5560]	)
% title('SAR_{ex}')
% xlabel('Modes Extracted ({\itK})')
set(gca,'xtick',[])
axis([-1 4 0 max(abs(plot_val(:)))+0.5])
ylabel('W/Kg/\muT^2')
set(gca,'FontSize',10)

%% Phantom Data Figure 5: Curve Inflection + XsYm + Fixed vs. Full
% Load data for Phantom Fixed and Run (Figure4_2)
% Then, Load data for Phantom Best and Run (Figure4_1)

b1weights_fullK = sum(abs(w_fullK(:,opt_l(1),1).^2),1);
b1weights_1s1m = sum(abs(w1s1m).^2,1);
b1weights_2s1m = sum(abs(w2s1m).^2,1);
b1weights_2s2m = sum(abs(w2s2m).^2,1);
b1weights_3s1m = sum(abs(w3s1m).^2,1);
b1weights_3s2m = sum(abs(w3s2m).^2,1);
b1weights_3s3m = sum(abs(w3s3m).^2,1);

max_val = 3.5;
coil_reg = 435;
% max_val = wcSAR_full*1e12;

figurent;

pth = [coil_reg 0; 800 0; 800 max(max_val); coil_reg max(max_val)];
faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'black', 'FaceAlpha',.1, 'EdgeColor','none');

hold on

plot(points_1s, wcSAR_full*1e12, 'LineWidth', 3); hold on
plot(points_1s,deepestSens_wcSAR_1s1m*1e12,'-.o', 'LineWidth',2)
plot(points_1s(2:end),deepestSens_wcSAR_2s1m(2:end)*1e12, '-.o', 'LineWidth',2)
plot(points_1s(3:end),deepestSens_wcSAR_3s1m(3:end)*1e12, '-.o', 'LineWidth',2)
plot(points_1s(2:end),deepestSens_wcSAR_2s2m(2:end)*1e12, '-.o', 'LineWidth',2)
plot(points_1s(3:end),deepestSens_wcSAR_3s2m(3:end)*1e12, '-.o', 'LineWidth',2)
% plot(points_1s(3:end),deepestSens_wcSAR_3s3m(3:end)*1e12, '-.o', 'LineWidth',2)
grid on

set(gca,'FontSize',14)
legend('Coil Region', 'Full System', 'S=1, K=1', 'S=2, K=1', 'S=3, K=1','S=2, K=2','S=3, K=2','S=3, K=3');
% title('SAR_{wc} - 2nd & 3rd Sensing Positions Fixed'); % FIXED POINTS
title('SAR_{wc} - Best 2nd & 3rd Sensing Positions'); % BEST POINTS
ylabel('W/Kg/W')
xlabel('Deepest Sensor Position (mm)')

axis([-100 points_1s(end) 0 max(max_val)])

set(gcf, 'Position',  [100, 100, 700, 450])

%% FIGURE 6 - Phantom vs. Duke (Short Coil)
% Do the Normalised Plot with all cases

ll = 1;
b1weights_fullK = sum(abs(w_fullK(:,opt_l(1),1).^2),1);
b1weights_1s1m = sum(abs(w1s1m).^2,1);
b1weights_2s1m = sum(abs(w2s1m).^2,1);
b1weights_2s2m = sum(abs(w2s2m).^2,1);
b1weights_3s1m = sum(abs(w3s1m).^2,1);
b1weights_3s2m = sum(abs(w3s2m).^2,1);
b1weights_3s3m = sum(abs(w3s3m).^2,1);%./((avgB1_3s3m*1e6).^2);

b1weights_fullK_all = squeeze(sum(abs(witer(:,opt_l(1),:,1).^2),1));
nwcSAR_fullK = b1weights_fullK_all*wcSAR_full.*1e12;

max_val = 60; %240;%

figurent;
coil_reg = 400; %200;%435; %

pth = [coil_reg 0; 800 0; 800 max(max_val);...
    coil_reg max(max_val)]; %SHORT COIL

faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'black', 'FaceAlpha',.1, 'EdgeColor','none');
% a1 = axes();
hold on

plot(points_1s, 1e12.*wcSAR_full.*b1weights_fullK, 'LineWidth', 2); 
plot(points_1s,1e12.*deepestSens_wcSAR_1s1m.*b1weights_1s1m,'-.o', 'LineWidth',2);
plot(points_1s(2:end),1e12.*deepestSens_wcSAR_2s2m(2:end).*b1weights_2s2m(2:end), '-.o', 'LineWidth',2);
%test
plot(points_1s(3:end),1e12.*deepestSens_wcSAR_3s3m(3:end).*b1weights_3s3m(3:end), '-.o', 'LineWidth',2);
grid on

step = (points_1s(2)-points_1s(1))/2;
grid on
hold on
for pp = 1:length(points_1s)
    pth = [points_1s(pp)-step min(1e12.*deepestSens_wcSAR_1s1m(pp).*b1weights_1s1m_all(:,pp));...
        points_1s(pp)+step min(1e12.*deepestSens_wcSAR_1s1m(pp).*b1weights_1s1m_all(:,pp));...
        points_1s(pp)+step max(1e12.*deepestSens_wcSAR_1s1m(pp).*b1weights_1s1m_all(:,pp));...
        points_1s(pp)-step max(1e12.*deepestSens_wcSAR_1s1m(pp).*b1weights_1s1m_all(:,pp))];
    faces = [1 2 3 4];
    patch('Faces', faces, 'Vertices',pth,'FaceColor', [0.8500, 0.3250, 0.0980], 'FaceAlpha',.5,'EdgeColor','none');
end

grid on
for pp = 1:length(points_1s)
    pth = [points_1s(pp)-step min(1e12.*deepestSens_wcSAR_2s2m(pp).*b1weights_2s2m_all(:,pp));...
        points_1s(pp)+step min(1e12.*deepestSens_wcSAR_2s2m(pp).*b1weights_2s2m_all(:,pp));...
        points_1s(pp)+step max(1e12.*deepestSens_wcSAR_2s2m(pp).*b1weights_2s2m_all(:,pp));...
        points_1s(pp)-step max(1e12.*deepestSens_wcSAR_2s2m(pp).*b1weights_2s2m_all(:,pp))];
    faces = [1 2 3 4];
    patch('Faces', faces, 'Vertices',pth,'FaceColor', [0.9290, 0.6940, 0.1250], 'FaceAlpha',.5,'EdgeColor','none');
end

pth = [-100 min(1e12.*wcSAR_full(1).*b1weights_fullK_all);...
    points_1s(end) min(1e12.*wcSAR_full(1).*b1weights_fullK_all);...
    points_1s(end) max(1e12.*wcSAR_full(1).*b1weights_fullK_all);...
    -100 max(1e12.*wcSAR_full(1).*b1weights_fullK_all)];
faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', [0, 0.4470, 0.7410], 'FaceAlpha',.18,'EdgeColor','none');

set(gca,'FontSize',14)
legend({'Coil Region', 'Full System', 'K=1', 'K=2'},'Location','northeast','AutoUpdate','off');
title('Duke-S, SAR_{nwc}') % CHANGE DUKE PHANTOM
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position (mm)')
% axis([-100 points_1s(end) 0 max(max_val)])

axis([-100 800 0 max(max_val)])

set(gcf, 'Position',  [100, 100, 700, 450])

%% FIGURE 6 PART II
% Do the non-normalised Plot with all cases

max_val = 3.5;

figurent;

pth = [coil_reg 0; 800 0; 800 max(max_val); coil_reg max(max_val)];
faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'black', 'FaceAlpha',.1, 'EdgeColor','none');

hold on

plot(points_1s, wcSAR_full*1e12, 'LineWidth', 2); hold on
plot(points_1s,deepestSens_wcSAR_1s1m*1e12,'-.o', 'LineWidth',2)
plot(points_1s(2:end),deepestSens_wcSAR_2s2m(2:end)*1e12, '-.o', 'LineWidth',2)

grid on

set(gca,'FontSize',14)
legend('Coil Region', 'Full System', 'K=1', 'K=2');
title('Phantom-S, SAR_{wc}') % CHANGE DUKE PHANTOM
ylabel('W/Kg/W')
xlabel('Deepest Sensor Position (mm)')

% axis([-100 points_1s(end) 0 max(max_val)])
axis([-100 800 0 max(max_val)])

set(gcf, 'Position',  [100, 100, 700, 450])

%% FIGURE 7
% DO THE ACTUAL SHIM PLOT
% LOAD FORPLOTTER
% LOAD 1G AVG
Q_tip = VOPm;

ll = 1;
b1weights_fullK_all = squeeze(sum(abs(witer(:,opt_l(1),:,1).^2),1));
b1weights_1s1m_all = squeeze(sum(abs(w1s1m_all).^2,1));
b1weights_2s1m_all = squeeze(sum(abs(w2s1m_all).^2,1));
b1weights_2s2m_all = squeeze(sum(abs(w2s2m_all).^2,1));
b1weights_3s1m_all = squeeze(sum(abs(w3s1m_all).^2,1));
b1weights_3s2m_all = squeeze(sum(abs(w3s2m_all).^2,1));
b1weights_3s3m_all = squeeze(sum(abs(w3s3m_all).^2,1));

nwcSAR_fullK = b1weights_fullK_all*wcSAR_full.*1e12;

for ii = 1:length(points_1s)
    nwcSAR_1s1m(:,ii) = b1weights_1s1m_all(:,ii).*deepestSens_wcSAR_1s1m(ii).*1e12;
    nwcSAR_2s1m(:,ii) = b1weights_2s1m_all(:,ii).*deepestSens_wcSAR_2s1m(ii).*1e12;
    nwcSAR_2s2m(:,ii) = b1weights_2s2m_all(:,ii).*deepestSens_wcSAR_2s2m(ii).*1e12;
    nwcSAR_3s1m(:,ii) = b1weights_3s1m_all(:,ii).*deepestSens_wcSAR_3s1m(ii).*1e12;
    nwcSAR_3s2m(:,ii) = b1weights_3s2m_all(:,ii).*deepestSens_wcSAR_3s2m(ii).*1e12;
    nwcSAR_3s3m(:,ii) = b1weights_3s3m_all(:,ii).*deepestSens_wcSAR_3s3m(ii).*1e12;
end

max_val = 10;
% 

figurent;
coil_reg = 435; %400; %200;%

actualShim_SAR_fullSys(1:npoints) = abs(w_fullK(:,opt_l(1),1)'*Q_tip*w_fullK(:,opt_l(1),1));%./b1_normVal;
% actualShim_SAR_fullSys(1:npoints) = abs(w'*Q_tip*w);%./b1_normVal;
for ii = 1:npoints
    actualShim_SAR_1s1m(ii) = abs(w1s1m(:,ii)'*Q_tip*w1s1m(:,ii));%./b1weights_1s1m(ii);
    
    if ii >= 2
        actualShim_SAR_2s1m(ii) = abs(w2s1m(:,ii)'*Q_tip*w2s1m(:,ii));%./b1weights_2s1m(bestPos_idx_2s1m(ii));
        actualShim_SAR_2s2m(ii) = abs(w2s2m(:,ii)'*Q_tip*w2s2m(:,ii));%./b1weights_2s2m(bestPos_idx_2s2m(ii));
    end
    
    if ii >= 3
        actualShim_SAR_3s1m(ii) = abs(w3s1m(:,ii)'*Q_tip*w3s1m(:,ii));%./b1weights_3s1m(bestPos_idx_3s1m(ii));
        actualShim_SAR_3s2m(ii) = abs(w3s2m(:,ii)'*Q_tip*w3s2m(:,ii));%./b1weights_3s2m(bestPos_idx_3s2m(ii));
        actualShim_SAR_3s3m(ii) = abs(w3s3m(:,ii)'*Q_tip*w3s3m(:,ii));%./b1weights_3s3m(bestPos_idx_3s3m(ii));
    end
end

for jj = 1:niters
    actualShim_SAR_fullK_all(jj,1:npoints) = abs(witer(:,opt_l(1),jj,1)'*Q_tip*witer(:,opt_l(1),jj,1));%./b1_normVal;
end
% actualShim_SAR_fullSys(1:npoints) = abs(w'*Q_tip*w);%./b1_normVal;
for jj = 1:50
    for ii = 1:npoints
        actualShim_SAR_1s1m_all(jj,ii) = abs(w1s1m_all(:,jj,ii)'*Q_tip*w1s1m_all(:,jj,ii));%./b1weights_1s1m(ii);
        
        if ii >= 2
            actualShim_SAR_2s1m_all(jj,ii) = abs(w2s1m_all(:,jj,ii)'*Q_tip*w2s1m_all(:,jj,ii));%./b1weights_2s1m(bestPos_idx_2s1m(ii));
            actualShim_SAR_2s2m_all(jj,ii) = abs(w2s2m_all(:,jj,ii)'*Q_tip*w2s2m_all(:,jj,ii));%./b1weights_2s2m(bestPos_idx_2s2m(ii));
        end
        
        if ii >= 3
            actualShim_SAR_3s1m_all(jj,ii) = abs(w3s1m_all(:,jj,ii)'*Q_tip*w3s1m_all(:,jj,ii));%./b1weights_3s1m(bestPos_idx_3s1m(ii));
            actualShim_SAR_3s2m_all(jj,ii) = abs(w3s2m_all(:,jj,ii)'*Q_tip*w3s2m_all(:,jj,ii));%./b1weights_3s2m(bestPos_idx_3s2m(ii));
            actualShim_SAR_3s3m_all(jj,ii) = abs(w3s3m_all(:,jj,ii)'*Q_tip*w3s3m_all(:,jj,ii));%./b1weights_3s3m(bestPos_idx_3s3m(ii));
        end
    end
end

% max_val = max(actualShim_SAR_1s1m_all);%max(actualShim_SAR_fullK_all);%actualShim_SAR_3s3m(3:end;
        % actualShim_SAR_2s2m(2:end);%

pth = [coil_reg 0; 800 0; 800 max(max_val); coil_reg max(max_val)];
faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'black', 'FaceAlpha',.1,'EdgeColor','none');

hold on

plot(points_1s, actualShim_SAR_fullSys, 'LineWidth', 2); hold on
plot(points_1s,actualShim_SAR_1s1m,'-.o', 'LineWidth',2)
plot(points_1s(2:end),actualShim_SAR_2s2m(2:end), '-.o', 'LineWidth',2)

step = (points_1s(2)-points_1s(1))/2;
grid on
for pp = 1:length(points_1s)
    pth = [points_1s(pp)-step min(actualShim_SAR_1s1m_all(:,pp));...
        points_1s(pp)+step min(actualShim_SAR_1s1m_all(:,pp));...
        points_1s(pp)+step max(actualShim_SAR_1s1m_all(:,pp));...
        points_1s(pp)-step max(actualShim_SAR_1s1m_all(:,pp))];
    faces = [1 2 3 4];
    patch('Faces', faces, 'Vertices',pth,'FaceColor', [0.8500, 0.3250, 0.0980], 'FaceAlpha',.5,'EdgeColor','none');
end

grid on
for pp = 1:length(points_1s)
    pth = [points_1s(pp)-step min(actualShim_SAR_2s2m_all(:,pp));...
        points_1s(pp)+step min(actualShim_SAR_2s2m_all(:,pp));...
        points_1s(pp)+step max(actualShim_SAR_2s2m_all(:,pp));...
        points_1s(pp)-step max(actualShim_SAR_2s2m_all(:,pp))];
    faces = [1 2 3 4];
    patch('Faces', faces, 'Vertices',pth,'FaceColor', [0.9290, 0.6940, 0.1250], 'FaceAlpha',.5,'EdgeColor','none');
end

pth = [-100-step min(actualShim_SAR_fullK_all(:,1));...
    points_1s(end)+step min(actualShim_SAR_fullK_all(:,1)); points_1s(end)+step max(actualShim_SAR_fullK_all(:,1));...
    -100-step max(actualShim_SAR_fullK_all(:,1))];
faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', [0, 0.4470, 0.7410], 'FaceAlpha',.1,'EdgeColor','none');

set(gca,'FontSize',14)

legend('Coil Region','Full System', 'K=1', 'K=2');
title('Phantom, SAR_{ex}')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position (mm)')

axis([-100 800 0 max(max_val)])

set(gcf, 'Position',  [100, 100, 700, 450])

%% FIGURE 8

% Re-do FIgure 5 Part I for the Long Coil Data.
%% FIGURE 9
max_val =  60; 

figurent;
coil_reg = 400; %200;%435;% 

pth = [coil_reg 0; 800 0; 800 max(max_val);...
    coil_reg max(max_val)]; %SHORT COIL


l_coil_reg = 200;%435; %400; %

pth1 = [l_coil_reg 0; coil_reg 0; coil_reg max(max_val);...
    l_coil_reg max(max_val)]; %LONG COIL

faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'black', 'FaceAlpha',.3, 'EdgeColor','none');


hold on

h(1) = plot(points_1s(2:end),1e12.*deepestSens_wcSAR_2s2m_sc(2:end).*b1weights_2s2m_sc(2:end), '-.o', 'LineWidth',2);
set(h(1), 'MarkerFaceColor', get(h(1), 'color'));
% h(2) = plot(points_w(3:end),deepestSens_wcSAR_3s2m_sc(3:end)./b1weights_3s2m_sc(3:end), '-.o', 'LineWidth',2);
% set(h(2), 'MarkerFaceColor', get(h(2), 'color'));

patch('Faces', faces, 'Vertices',pth1,'FaceColor', 'black', 'FaceAlpha',.1, 'EdgeColor','none');
h(3) = plot(points_1s(2:end),1e12.*deepestSens_wcSAR_2s2m_lc(2:end).*b1weights_2s2m_lc(2:end), '-.o', 'LineWidth',2);
set(h(3), 'MarkerFaceColor', get(h(3), 'color'));
% h(4) = plot(points_w(3:end),deepestSens_wcSAR_3s2m_lc(3:end)./b1weights_3s2m_lc(3:end), '-.o', 'LineWidth',2);
% set(h(4), 'MarkerFaceColor', get(h(4), 'color'));

grid on

set(gca,'FontSize',14)
legend({'Short Coil Region','Duke-S, K=2','Long Coil Region','Duke-L, K=2'},'Location','northeast','AutoUpdate','off');
title('SAR_{nwc}')%'Limited Knowledge of J - Short Coil Case')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position (mm)')
axis([-100 800 0 max(max_val)])

set(gcf, 'Position',  [100, 100, 700, 450])