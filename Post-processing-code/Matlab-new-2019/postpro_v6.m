%% DO THE SAME FOR THE RAPID COIL
npoints = 40;
ncoils = 8;

path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';

% HERE WE CHOOSE WHAT SIMULATION TO GET THE RESULTS FROM - THIS NAME WAS
% PRE-CHOSEN WHEN EXPORTING FROM S4L
simulation_name = '\RapidCoils_Duke_Guidewire_HighRes_2020_01_06_'; %Duke, ShortCoil, High Res
% simulation_name = '\LongRapidCoils_Duke_Guidewire_HighRes_2020_02_05_'; %Duke, Long Coil, High Res
% Import Current
J_struct = load([path_to_results 'EM Current' simulation_name 'EMCurrent']);
J = J_struct.J;
Axes = J_struct.Axes;

% Get B1+ to Acceptable Levels
load([path_to_results 'Grid' simulation_name 'Axes'])
sz = [length(XAxis) length(YAxis) length(ZAxis)]; % STORES MATRIX SIZES (HANDY FOR OPERATIONS)
fsz = sz(1)*sz(2)*sz(3);
% IF NAMED CORRECTLY THEN THIS SHOULD GRAB B1 MATRICES
for ii = 1:ncoils
    load([path_to_results 'B1 Field' simulation_name 'B1Fields_C' num2str(ii)])
end
B1 = cat(3, B1_1, B1_2, B1_3, B1_4, B1_5, B1_6, B1_7, B1_8); % BUILD THE 8 CHANNEL MATRIX
clear B1_1 B1_2 B1_3 B1_4 B1_5 B1_6 B1_7 B1_8 % FREEING SPACE
B1p = permute(reshape(squeeze(B1(:,1,:)), [sz(1) sz(3) sz(2) 8]), [1 3 2 4]); % ISOLATE B1+ AND REORDER IT INTO XYZ
[~,xx] = min(abs(XAxis - Axes(1,end)*1e-3));
[~,yy] = min(abs(YAxis - Axes(2,end)*1e-3));
[~,zz] = min(abs(ZAxis - Axes(3,end)*1e-3));

%% find WORST CASE (ALL MODES) AVERAGE B1+
path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';
% simulation_name = '\BodyCoil_Duke_Guidewire_2020_04_07_';
simulation_name = '\RapidCoils_Duke_Guidewire_HighRes_2020_01_06_'; %Duke, ShortCoil, High Res
load([path_to_results 'Density' simulation_name 'Density'])
rho = reshape(permute(reshape(rho, [sz(1) sz(3) sz(2)]), [1 3 2]), [fsz 1]); %REORDER INTO XYZ

maskR = rho; %Load Mask Properties first, if is in cell form run: rho = cellfun(@(x) double(x), rho);
maskR((maskR > 1048) & (maskR < 1050.5)) = 1; %Density of Heart Lumen (blood)
maskR((maskR > 1079) & (maskR < 1081)) = 1; %Density of Heart Muscle (plot rho and check precis value)
maskR(maskR ~= 1) = 0; % Make everything else 0
maskR = reshape(maskR, sz); %Reshape to 3D

%%
% Check manually what small box encompasses
% the Heart, as blood has same density, need to make everything else
% outside this box 0, hence the following lines:

%%%% REAL WIRE DUKE SHORT COIL, HIGH RES
maskR(1:16,:,:) = 0;
maskR(:,1:24,:) = 0;
maskR(:,:,1:1052) = 0;
maskR(150:end,:,:) = 0;
maskR(:,121:end,:) = 0;
maskR(:,:,1165:end) = 0;

% Voxelization makes everything patchy. Dilate and Erode to fill patches
se = strel('sphere',3); %Patches are small so small Dilate radius will do
dilmaskR = imdilate(maskR,se); %Dilate
filledmaskR = imerode(dilmaskR,se); %Erode, Heart Region should now be properly masked
filledmask = reshape(filledmaskR, [fsz 1]);
maskMLS = filledmask(filledmask~=0);
%%
figurent();
imagesc(filledmaskR(:,:,1119));
%%
B1pr = reshape(B1p, [fsz ncoils]);%./sqrt(ncoils);

if ncoils > 1
    for ii = 1:ncoils
        phase_quad(ii) = angle(B1p(xx,yy,zz-1,ii));
        B1pr_quad(:,ii) = B1pr(:,ii)*exp(1i*-phase_quad(ii));
    end
end

for ii = 1:ncoils
% %%%% HEART SECTION    
    if ncoils > 1
        maskedB1_quad(:,ii) = B1pr_quad(:,ii).*filledmask;%.*w(ii);
        maskedB1(:,ii) = B1pr(:,ii).*filledmask;%.*w(ii);
    else
        maskedB1(:,ii) = B1pr(:,ii).*filledmask;%.*w(ii);
    end
end

maskedB1_quad_reshaped = reshape(maskedB1_quad, [sz ncoils]);
maskedB1_reshaped = reshape(maskedB1, [sz ncoils]);
for ii = 1:ncoils
    lowresB1_reshaped(:,:,:,ii) = Gen3D(squeeze(maskedB1_reshaped(:,:,:,ii)), XAxis, YAxis, ZAxis, 30, 30, 150);
    lowresB1_quad_reshaped(:,:,:,ii) = Gen3D(squeeze(maskedB1_quad_reshaped(:,:,:,ii)), XAxis, YAxis, ZAxis, 30, 30, 150);
end

lowresB1_quad = reshape(lowresB1_quad_reshaped, [30*30*150 ncoils]);
lowresB1 = reshape(lowresB1_reshaped, [30*30*150 ncoils]);

nonzero_maskedB1_quad = lowresB1_quad(lowresB1_quad~=0);
nonzero_maskedB1 = lowresB1(lowresB1~=0);

B1_toShim_quad = reshape(nonzero_maskedB1_quad,[],ncoils);
B1_toShim = reshape(nonzero_maskedB1,[],ncoils);

maskMLS = ones(length(B1_toShim(:,1)),1);

%%
[L, Jinterp, pedge_ind, points_1s, points_2s, points_3s] = interpJ(npoints, ncoils, 2000, Axes, J, 756, 16);
Q_tip = VOPm/ncoils;

[u,s,v] = svd(Jinterp);
% 
% lambda = [0.02 0.015 0.015]; %Long coil High res
% 
% lambda = [0.2 0.15 0.1]; %Short coil High res
% lambda = [0.3 0.25 0.1]; %Long coil High res

lambda = [0.1 0.1 0.1]; %Long coil High res
% scaling_factor = 1e-6/abs(mean(nonzero_maskedB1reshaped(:))); %%DOUBT ABOUT HOW THIS IS DONE 09/10/2019

scaling_factor = (1e-6/mean(abs(sum(B1_toShim,2))));
% w_init = scaling_factor*exp(1i*-phase_quad);
%%
disp('next')
for ii = 1:3
    a_W = 1e10; 
    for jj = 1:10
        
        % Start from Quad
%         [witer,~] = MLS_modeshim_max_amp(B1_toShim_quad.*1e6,v(:,ii:8),scaling_factor, maskMLS,6*ones(ncoils,1),[],lambda(ii),20,0.05,0.0001,'disabled');
%         for kk=1:ncoils
%         	witer(kk,:) = witer(kk).*exp(1i*-phase_quad(kk));
%         end
        % Start from 111
        [witer,~] = MLS_modeshim_max_amp(B1_toShim.*1e6,v(:,ii:8),scaling_factor, maskMLS,6*ones(ncoils,1),[],lambda(ii),20,0.05,0.001,'disabled');
%     
%         if abs(witer'*Q_tip*witer) < a_W
%             wout(:,ii) = witer;
%         	a_W = abs(witer'*Q_tip*witer);
%             disp([ii jj a_W norm(wout(:,ii))]);
%         end
%         if sum(abs(witer).^2) < a_W
%             wout(:,ii) = witer;
%             a_W = sum(abs(witer).^2);
%         end
    end
    
    [avgB1_perfKnowledge(ii),~,err(ii)] = applyShim(B1_toShim, wout(:,ii), ncoils);
    b1_normVal_perfKnowledge(ii) = (avgB1_perfKnowledge(ii).^2)./sum(abs(wout(:,ii)).^2);
    if ii == 1
        wcSAR_perfKnowledge(ii) = max(eig(Q_tip));
    else
        wcSAR_perfKnowledge(ii) = max(eig(v(:,ii:8)'*Q_tip*v(:,ii:8)));
    end
end


%% Quantities for Full System
% Q_tip = squeeze(VOPm(1,:,:);

for ii = 1:4
    if ii == 1
        wcSAR_perfKnowledge(ii) = max(eig(Q_tip));
    else
        wcSAR_perfKnowledge(ii) = max(eig(v(:,ii:8)'*Q_tip*v(:,ii:8)));
    end
end
Q_tip_normed = Q_tip./b1_normVal_perfKnowledge(1);
maxEV_fullQ(1:npoints) = max(eig(Q_tip_normed));
maxEV_fullQ_noNorm(1:npoints) = max(eig(Q_tip));

%% best positions for reduced system
for kk = 1:length(points_1s)
    
    [~,posidx_1(1)] = min(abs(L-points_1s(kk,1)));
    [~,~,v]=svd(Jinterp(posidx_1,:));
    
    v_dmss_1s1m = v(:,2:ncoils);
    Q_dmss_1s1m = v_dmss_1s1m'*(Q_tip)*v_dmss_1s1m;
    maxEV_1s1m(kk) = max(eig(Q_dmss_1s1m));
    
end

for kk = 1:length(points_2s)
    
    [~,posidx_2(1)] = min(abs(L-points_2s(kk,1)));
    [~,posidx_2(2)] = min(abs(L-points_2s(kk,2)));
    [~,~,v]=svd(Jinterp(posidx_2,:));
    
    v_dmss_2s1m = v(:,2:ncoils);
    Q_dmss_2s1m = v_dmss_2s1m'*(Q_tip)*v_dmss_2s1m;
    maxEV_2s1m(kk) = max(eig(Q_dmss_2s1m));
    
    v_dmss_2s2m = v(:,3:ncoils);
    Q_dmss_2s2m = v_dmss_2s2m'*(Q_tip)*v_dmss_2s2m;
    maxEV_2s2m(kk) = max(eig(Q_dmss_2s2m));
    
end

for kk = 1:length(points_3s)
    
    [~,posidx_3(1)] = min(abs(L-points_3s(kk,1)));
    [~,posidx_3(2)] = min(abs(L-points_3s(kk,2)));
    [~,posidx_3(3)] = min(abs(L-points_3s(kk,3)));%Sets points in relation to phantom-air border
    [~,~,v]=svd(Jinterp(posidx_3,:));
    
    v_dmss_3s1m = v(:,2:ncoils);
    Q_dmss_3s1m = v_dmss_3s1m'*(Q_tip)*v_dmss_3s1m;
    maxEV_3s1m(kk) = max(eig(Q_dmss_3s1m));
    
    v_dmss_3s2m = v(:,3:ncoils);
    Q_dmss_3s2m = v_dmss_3s2m'*(Q_tip)*v_dmss_3s2m;
    maxEV_3s2m(kk) = max(eig(Q_dmss_3s2m));
    
%     v_dmss_3s3m = v(:,4:ncoils);
%     Q_dmss_3s3m = v_dmss_3s3m'*(Q_tip)*v_dmss_3s3m;
%     maxEV_3s3m(kk) = max(eig(Q_dmss_3s3m));

end

%% Actual optimal points
wcSAR_full(1:length(points_1s)) = abs(wcSAR_perfKnowledge(1))*1e-12;
deepestSens_wcSAR_1s1m = abs(maxEV_1s1m)*1e-12;

for ii = 2:length(points_1s)
    logic_idx = find(points_2s(:,2) == points_1s(ii));
    [deepestSens_wcSAR_2s1m(ii), temp_idx_2s1m] = min(abs(maxEV_2s1m(logic_idx))*1e-12);
    [deepestSens_wcSAR_2s2m(ii), temp_idx_2s2m] = min(abs(maxEV_2s2m(logic_idx))*1e-12);
    
    bestPos_idx_2s1m(ii) = logic_idx(temp_idx_2s1m);
    bestPos_idx_2s2m(ii) = logic_idx(temp_idx_2s2m);
    
    bestPos_2s1m(ii,:) = points_2s(bestPos_idx_2s1m(ii),:);
    bestPos_2s2m(ii,:) = points_2s(bestPos_idx_2s2m(ii),:);
    
    [~,bestIdx_2s1m_pt1] = min(abs(L-bestPos_2s1m(ii,1)));
    [~,bestIdx_2s1m_pt2] = min(abs(L-bestPos_2s1m(ii,2)));
    bestIdx_2s1m(ii,:) = [bestIdx_2s1m_pt1, bestIdx_2s1m_pt2];
    
    [~,bestIdx_2s2m_pt1] = min(abs(L-bestPos_2s2m(ii,1)));
    [~,bestIdx_2s2m_pt2] = min(abs(L-bestPos_2s2m(ii,2)));
    bestIdx_2s2m(ii,:) = [bestIdx_2s2m_pt1, bestIdx_2s2m_pt2];
    
    [deepestSens_wcSAR_2s(ii), bestMode_2s(ii)] = min([deepestSens_wcSAR_2s1m(ii), deepestSens_wcSAR_2s2m(ii)]);
end
%%

for ii = 3:length(points_1s)
    logic_idx = find(points_3s(:,3) == points_1s(ii));
    [deepestSens_wcSAR_3s1m(ii), temp_idx_3s1m] = min(abs(maxEV_3s1m(logic_idx))*1e-12);
    [deepestSens_wcSAR_3s2m(ii), temp_idx_3s2m] = min(abs(maxEV_3s2m(logic_idx))*1e-12);
%     [deepestSens_wcSAR_3s3m(ii), temp_idx_3s3m] = min(abs(maxEV_3s3m(logic_idx))*1e-12);
    
    bestPos_idx_3s1m(ii) = logic_idx(temp_idx_3s1m);
    bestPos_idx_3s2m(ii) = logic_idx(temp_idx_3s2m);
%     bestPos_idx_3s3m(ii) = logic_idx(temp_idx_3s3m);
    
    bestPos_3s1m(ii,:) = points_3s(bestPos_idx_3s1m(ii),:);
    bestPos_3s2m(ii,:) = points_3s(bestPos_idx_3s2m(ii),:);
%     bestPos_3s3m(ii,:) = points_3s(bestPos_idx_3s3m(ii),:);
    
    [~,bestIdx_3s1m_pt1] = min(abs(L-bestPos_3s1m(ii,1)));
    [~,bestIdx_3s1m_pt2] = min(abs(L-bestPos_3s1m(ii,2)));
    [~,bestIdx_3s1m_pt3] = min(abs(L-bestPos_3s1m(ii,3)));
    bestIdx_3s1m(ii,:) = [bestIdx_3s1m_pt1, bestIdx_3s1m_pt2, bestIdx_3s1m_pt3];
    
    [~,bestIdx_3s2m_pt1] = min(abs(L-bestPos_3s2m(ii,1)));
    [~,bestIdx_3s2m_pt2] = min(abs(L-bestPos_3s2m(ii,2)));
    [~,bestIdx_3s2m_pt3] = min(abs(L-bestPos_3s2m(ii,3)));
    bestIdx_3s2m(ii,:) = [bestIdx_3s2m_pt1, bestIdx_3s2m_pt2, bestIdx_3s2m_pt3];
    
%     [~,bestIdx_3s3m_pt1] = min(abs(L-bestPos_3s3m(ii,1)));
%     [~,bestIdx_3s3m_pt2] = min(abs(L-bestPos_3s3m(ii,2)));
%     [~,bestIdx_3s3m_pt3] = min(abs(L-bestPos_3s3m(ii,3)));
%     bestIdx_3s3m(ii,:) = [bestIdx_3s3m_pt1, bestIdx_3s3m_pt2, bestIdx_3s3m_pt3];
    
    [deepestSens_wcSAR_3s(ii), bestMode_3s(ii)] = min([deepestSens_wcSAR_3s1m(ii), deepestSens_wcSAR_3s2m(ii)]);%, deepestSens_wcSAR_3s3m(ii)]);
end

%% B1+ Shim
text = 'disabled';
lambda = [0 0 0]; %[100 50 0];%


for kk = 1:length(points_1s)
    disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_1s))])
   
    [~,bestIdx_1s1m(kk)] = min(abs(L-points_1s(kk)));
    
    [modes_1s1m(:,:,kk), w1s1m(:,kk), avgB1_1s1m(kk)] = TipQ_v3(1, ncoils, bestIdx_1s1m(kk), Jinterp, maskMLS, B1_toShim,Q_tip,6,scaling_factor, lambda(1),  text);    
end
%%
for kk = 2:length(points_w)
    disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_w))])
    
    [modes_2s1m(:,:,kk), w2s1m(:,kk), avgB1_2s1m(kk)] = TipQ_v2(1, ncoils, bestIdx_2s1m(kk,:), Jinterp, maskMLS, B1_toShim, nonzero_maskedB1wired_reshaped, lambda(1), text);

    [modes_2s2m(:,:,kk), w2s2m(:,kk), avgB1_2s2m(kk)] = TipQ_v2(2, ncoils, bestIdx_2s2m(kk,:), Jinterp, maskMLS, B1_toShim, nonzero_maskedB1wired_reshaped, lambda(2), text);
end

for kk = 3:length(points_w)
    disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_w))])
        
    [modes_3s1m(:,:,kk), w3s1m(:,kk), avgB1_3s1m(kk)] = TipQ_v2(1, ncoils, bestIdx_3s1m(kk,:), Jinterp, maskMLS, B1_toShim, nonzero_maskedB1wired_reshaped, lambda(1), text);

    [modes_3s2m(:,:,kk), w3s2m(:,kk), avgB1_3s2m(kk)] = TipQ_v2(2, ncoils, bestIdx_3s2m(kk,:), Jinterp, maskMLS, B1_toShim, nonzero_maskedB1wired_reshaped, lambda(2), text);

    [modes_3s3m(:,:,kk), w3s3m(:,kk), avgB1_3s3m(kk)] = TipQ_v2(3, ncoils, bestIdx_3s3m(kk,:), Jinterp, maskMLS, B1_toShim, nonzero_maskedB1wired_reshaped, lambda(3), text);
end

 b1weights_1s1m = (avgB1_1s1m.^2)./sum(abs(w1s1m).^2,1);
 b1weights_2s1m = (avgB1_2s1m.^2)./sum(abs(w2s1m).^2,1);
 b1weights_2s2m = (avgB1_2s2m.^2)./sum(abs(w2s2m).^2,1);
 b1weights_3s1m = (avgB1_3s1m.^2)./sum(abs(w3s1m).^2,1);
 b1weights_3s2m = (avgB1_3s2m.^2)./sum(abs(w3s2m).^2,1);
 b1weights_3s3m = (avgB1_3s3m.^2)./sum(abs(w3s3m).^2,1);
 
% [maxEV_redQ_2s1m, maxEV_redQ_2s1m_noNorm, shimmedB1_2s1m, modes_2s1m, b1weights_2s1m, w2s1m, avgB1_2s1m] = twoSensorTipQ(1,nChannels, points_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_2s2m, maxEV_redQ_2s2m_noNorm, shimmedB1_2s, modes_2s, b1weights_2s, w2, avgB1_2s] = twoSensorTipQ(2, nChannels, points_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_3s3m, maxEV_redQ_3s3m_noNorm, shimmedB1_3s, modes_3s, b1weights_3s, w3, avgB1_3s] = threeSensorTipQ(3, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_3s2m, maxEV_redQ_3s2m_noNorm, shimmedB1_3s2m, modes_3s2m, b1weights_3s2m, w3s2m, avgB1_3s2m] = threeSensorTipQ(2, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_3s1m, maxEV_redQ_3s1m_noNorm, shimmedB1_3s1m, modes_3s1m, b1weights_3s1m, w3s1m, avgB1_3s1m] = threeSensorTipQ(1, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);
% 
%% NORMED


wcSAR_full = abs(maxEV_fullQ)*1e-12;
deepestSens_wcSAR_1s1m = abs(maxEV_redQ_1s1m)*1e-12;
for ii = 2:length(points_w)
    logic_idx = find(points_pos(:,2) == points_w(ii));
    deepestSens_wcSAR_2s1m(ii) = abs(min(maxEV_redQ_2s1m(logic_idx)))*1e-12;
    deepestSens_wcSAR_2s2m(ii) = abs(min(maxEV_redQ_2s2m(logic_idx)))*1e-12;
end

for ii = 3:length(points_w)
    logic_idx = find(points_3_pos(:,3) == points_w(ii));
    deepestSens_wcSAR_3s1m(ii) = abs(min(maxEV_redQ_3s1m(logic_idx)))*1e-12;
    deepestSens_wcSAR_3s2m(ii) = abs(min(maxEV_redQ_3s2m(logic_idx)))*1e-12;
    deepestSens_wcSAR_3s3m(ii) = abs(min(maxEV_redQ_3s3m(logic_idx)))*1e-12;
end

figure;
plot(points_w, wcSAR_full, 'o', 'LineWidth', 2); hold on
plot(points_w,deepestSens_wcSAR_1s1m,'o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s1m(2:end), 'o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s2m(2:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s1m(3:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s2m(3:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s3m(3:end), 'o', 'LineWidth',2)
grid on

legend('Full', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')


%% NO NORM


wcSAR_full_noNorm = abs(maxEV_fullQ_noNorm);
deepestSens_wcSAR_1s1m_noNorm = abs(maxEV_redQ_1s1m_noNorm);
for ii = 2:length(points_w)
    logic_idx = find(points_pos(:,2) == points_w(ii));
    deepestSens_wcSAR_2s1m_noNorm(ii) = abs(min(maxEV_redQ_2s1m_noNorm(logic_idx)));
    deepestSens_wcSAR_2s2m_noNorm(ii) = abs(min(maxEV_redQ_2s2m_noNorm(logic_idx)));
end

for ii = 3:length(points_w)
    logic_idx = find(points_3_pos(:,3) == points_w(ii));
    deepestSens_wcSAR_3s1m(ii) = abs(min(maxEV_redQ_3s1m(logic_idx)))*1e-12;
    deepestSens_wcSAR_3s2m(ii) = abs(min(maxEV_redQ_3s2m(logic_idx)))*1e-12;
    deepestSens_wcSAR_3s3m(ii) = abs(min(maxEV_redQ_3s3m(logic_idx)))*1e-12;
end

figure;
plot(points_w, wcSAR_full, 'o', 'LineWidth', 2); hold on
plot(points_w,deepestSens_wcSAR_1s1m,'o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s1m(2:end), 'o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s2m(2:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s1m(3:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s2m(3:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s3m(3:end), 'o', 'LineWidth',2)
grid on

legend('Full', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('wcSAR - Straight Wire - Phantom')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')

%%

[u_fullWire,s_fullWire,v_fullWire]=svd(Jinterp(:,:));


%% MAKE MAPS
cmap = cmapTipQ(maxEV_redQ_2s2m, maxEV_redQ_1s1m, maxEV_fullQ, points_w);

b1wMap = b1weightsMap(b1weights_2s, b1weights_1s, b1_normVal, points_w);

noNormMap = cmapTipQ_noNorm(maxEV_redQ_2s2m_noNorm, maxEV_redQ_1s1m_noNorm, maxEV_fullQ_noNorm, points_w);

%% THREE SENSORS
cmap = cmapTipQ(maxEV_redQ_2s2m, maxEV_redQ_1s1m, maxEV_fullQ, points_w);

b1wMap = b1weightsMap(b1weights_2s, b1weights_1s, b1_normVal, points_w);

noNormMap = cmapTipQ_noNorm(maxEV_redQ_2s2m_noNorm, maxEV_redQ_1s1m_noNorm, maxEV_fullQ_noNorm, points_w);