
ncoils = 8;
% ISSUE WITH LOADING QUANTITIES FROM S4L AFTER INTERPOLATION
% MATRICES IN S4L ARE (Z, Y, X); CURRENTLY (28/01/2020) INTERPOLATION SPITS
% IT AS (Y, Z, X). WHEN EXTRACTING TO MATLAB, SAVEMAT PACKAGE INVERTS TO
% PYTHON'S (1, 2, 3) TO (3, 2, 1). NATURALLY MATRICES COME IN MATLAB AS
% (X, Z, Y). ALL MATRICES SHOULD BE RESHAPED TO (SIZE(X), SIZE(Z), SIZE(Y))
% IN ORDER TO MAKE SENSE IN MATLAB. IF THIS IS DONE IN THIS FIRST LOADING
% SECTION THEN IT DOESN'T NEED TO BE DONE ANYWHERE ELSE IN THE CODE.

% HERE WE PICK THE FOLDER PATH WHERE RESULTS ARE STORED.
% path_to_results = 'G:\Huygens Simulations\Phantom\Extracted Results\'; %phantom
path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';

% HERE WE CHOOSE WHAT SIMULATION TO GET THE RESULTS FROM - THIS NAME WAS
% PRE-CHOSEN WHEN EXPORTING FROM S4L
simulation_name = '\RapidCoils_Duke_Guidewire_HighRes_2020_01_06_'; %Duke, ShortCoil, High Res
% simulation_name = '\LongRapidCoils_Duke_Guidewire_HighRes_2020_02_05_'; %Duke, ShortCoil, High Res
% simulation_name = '\RapidCoils_Duke_Guidewire_ThickInsu_2020_02_10_'; %Duke, ShortCoil, High Res
% simulation_name = '\RapidCoils_Phantom_Guidewire_2020_02_12_'; %Duke, ShortCoil, High Res

load([path_to_results 'Density' simulation_name 'Density'])% IF NAMED CORRECTLY THEN THIS SHOULD GRAB DENSITY MATRIX
load([path_to_results 'EM Current' simulation_name 'EMCurrent']) % IF NAMED CORRECTLY THEN THIS SHOULD GRAB CURRENT INFO
load([path_to_results 'Grid' simulation_name 'Axes']) % IF NAMED CORRECTLY THEN THIS SHOULD GRAB THE GRID

% xaxis =(XAxis(1:end-1)+XAxis(2:end))'/2;
% yaxis =(YAxis(1:end-1)+YAxis(2:end))'/2;
% zaxis =(ZAxis(1:end-1)+ZAxis(2:end))'/2;
sz = [length(XAxis) length(YAxis) length(ZAxis)]; % STORES MATRIX SIZES (HANDY FOR OPERATIONS)
fsz = length(XAxis)*length(YAxis)*length(ZAxis); % STORES UNMATRIXED SIZE (HANDY FOR OPERATIONS)

rho = reshape(permute(reshape(rho, [sz(1) sz(3) sz(2)]), [1 3 2]), [fsz 1]); %REORDER INTO XYZ

% IF NAMED CORRECTLY THEN THIS SHOULD GRAB B1 MATRICES
for ii = 1:ncoils
    load([path_to_results 'B1 Field' simulation_name 'B1Fields_C' num2str(ii)])
end
B1 = cat(3, B1_1, B1_2, B1_3, B1_4, B1_5, B1_6, B1_7, B1_8); % BUILD THE 8 CHANNEL MATRIX
clear B1_1 B1_2 B1_3 B1_4 B1_5 B1_6 B1_7 B1_8 % FREEING SPACE
B1p = reshape(permute(reshape(squeeze(B1(:,1,:)), [sz(1) sz(3) sz(2) 8]), [1 3 2 4]), [fsz 8]); % ISOLATE B1+ AND REORDER IT INTO XYZ
B1p_s = reshape(sum(B1p,2), sz); % SUM ALL CHANNELS (HANDY FOR TESTS)

% IF NAMED CORRECTLY THEN THIS SHOULD GRAB E-FIELD MATRICES
% for  ii = 1:ncoils
%     load([path_to_results 'E Field' simulation_name 'EFields_C' num2str(ii)])
% end
% E = cat(3, E_1, E_2, E_3, E_4, E_5, E_6, E_7, E_8); % BUILD THE 8 CHANNEL MATRIX
% clear E_1 E_2 E_3 E_4 E_5 E_6 E_7 E_8 % FREEING SPACE
% E = reshape(permute(reshape(E, [sz(1) sz(3) sz(2) 3 8]), [1 3 2 4 5]), [fsz 3 8]); % REORDER INTO XYZ
% Eabs = squeeze(sqrt(E(:,1,:).^2 + E(:,2,:).^2 + E(:,3,:).^2)); % CALCULATE ABSOLUTE E-FIELD

Q = zeros(fsz,8,8, 'single'); % SET DIMENSIONS FOR Q
%THIS WILL BUILD AN ENTIRE Q-MATRICES FROM VECTORS. EACH VECTOR
%CORRESPONDS TO A MATRIX ENTRY FOR ALL VOXELS.
for ii = 1:ncoils
    for jj = 1:ncoils
        Q_entry = load([path_to_results 'Interpolated Q Matrices' simulation_name 'InterpolatedQMatrices_pt' num2str(ii) num2str(jj)]);
        Q_entry = struct2cell(Q_entry);
        Q(:,ii,jj) = Q_entry{1};
        fprintf(['Building Q-Matrix: \nCurrently on Line ' num2str(ii) ', Column ' num2str(jj) '\n'])
    end
end
Q = reshape(permute(reshape(Q, [sz(1) sz(3) sz(2) 8 8]), [1 3 2 4 5]), [fsz 8 8]); % REORDER INTO XYZ


%% Section Attempting to Create Heart Mask

%%
test_rho = reshape(rho, sz);
figurent;
% imagesc(squeeze(max(flipud(abs(test_rho)),[],2)))
%%
ii = 85;
imagesc(squeeze(flipud(abs(B1p_s(:,ii,:)).*test_rho(:,ii,:))))
%%
imagesc(squeeze(max(flipud(filledmaskR),[],3)))
%%
% xtip = 50;
% ytip = 65;
% ztip = 1419; % THIS IS FOR PHANTOM HIGH RES

xtip = 96;
ytip = 71;
ztip = 1129; %THIS IS FOR DUKE HIGH RES, NEEDS TO BE CHECKED MANUALLY FOR EACH NEW GRID

%% FOR PHANTOM
dielectric = 'Duke'% 'Phantom' %

if strcmp(dielectric, 'Phantom')
    maskR = reshape(rho, [sz 1]);
%     Eabs_s = sum(reshape(Eabs, [sz 8]),4);
    temprho = rho;
    temprho(rho~=0) = 1;
    for xx = 1:sz(1)
        for yy = 1:sz(2)
            for zz = 1:sz(3)
                if (XAxis(xx)-XAxis(xtip))^2+(YAxis(yy)-YAxis(ytip))^2+(ZAxis(zz)-ZAxis(ztip))^2 <= 0.05^2
                    maskR(xx,yy,zz) = 1;
                else
                    maskR(xx,yy,zz) = 0;
                end
            end
        end
    end

    filledmaskR = maskR.*reshape(temprho, [sz 1]);
    wiredmaskR = filledmaskR;
    
%     filledmaskR(15:49, 13:52, 1:289) = 0; % FOR V2RCOIL Thsi was atest
%     removing even more area around wire. Jo's suggestion 

%     filledmaskR(23:42, 23:42, 1:289) = 0; % FOR V2RCOIL these were the original values
%     a = abs(squeeze(B1p_s(:,31,:)).*squeeze(filledmaskR(:,31,:)));
%     figure;
%     newE = distributionGenerator(a(:,270:end), xaxis, zaxis(270:end), 1, 0, 1e-6, 'nearest');
end


if strcmp(dielectric, 'Duke')%% FOR DUKE
    maskR = rho; %Load Mask Properties first, if is in cell form run: rho = cellfun(@(x) double(x), rho);
    maskR((maskR > 1048) & (maskR < 1050.5)) = 1; %Density of Heart Lumen (blood)
    maskR((maskR > 1079) & (maskR < 1081)) = 1; %Density of Heart Muscle (plot rho and check precis value)
    maskR(maskR ~= 1) = 0; % Make everything else 0
    maskR = reshape(maskR, sz); %Reshape to 3D

    % Check manually what small box encompasses
    % the Heart, as blood has same density, need to make everything else
    % outside this box 0, hence the following lines:

    %%%% REAL WIRE DUKE SHORT COIL, HIGH RES
    maskR(1:16,:,:) = 0;
    maskR(:,1:24,:) = 0;
    maskR(:,:,1:1052) = 0;
    maskR(150:end,:,:) = 0;
    maskR(:,121:end,:) = 0;
    maskR(:,:,1165:end) = 0;

    % Voxelization makes everything patchy. Dilate and Erode to fill patches
    se = strel('sphere',3); %Patches are small so small Dilate radius will do
    dilmaskR = imdilate(maskR,se); %Dilate
    filledmaskR = imerode(dilmaskR,se); %Erode, Heart Region should now be properly masked

    wiredmaskR = filledmaskR;
    
    % If wire region needs to be deleted, check where manually:
    % filledmaskR(50:81, 25:47, 1:333) = 0; % FOR RCOIL
    % filledmaskR(34:64, 17:45, 1:333) = 0; % FOR V2RCOIL
    % filledmaskR(46:77, 20:50, 1:340) = 0; % FOR FRCOIL
%     filledmaskR(44:65, 102:120, 584:end) = 0; % FOR REAL WIRE
end
%%
% for ii = 15
%     distributionGenerator(abs(squeeze(B1p_s(ii,:,500:end).*filledmaskR(ii,:,500:end))), yaxis, zaxis(500:end), 1, 0, 1e-6, 'nearest');
% end
%% BOTH

filledmask = reshape(filledmaskR, [fsz 1]);
wiredmask = reshape(wiredmaskR, [fsz 1]);

maskMLS = filledmask(filledmask~=0);

sMask = wiredmask(wiredmask ~= 0);

%%
% figure;
for ii = 1:8
% %%%% HEART SECTION    
    maskedB1(:,ii) = (B1p(:,ii)+(1e-8*filledmask)).*filledmask;%.*w(ii);
    maskedB1wired(:,ii) = (B1p(:,ii)+(1e-8*wiredmask)).*wiredmask;
end

maskedB1_reshaped = reshape(maskedB1, [sz 8]);
for ii = 1:8
    lowresB1_reshaped(:,:,:,ii) = Gen3D(squeeze(maskedB1_reshaped(:,:,:,ii)), XAxis, YAxis, ZAxis, 30, 30, 150);
end
lowresB1 = reshape(lowresB1_reshaped, [30*30*150 8]);

% nonzero_maskedB1 = maskedB1(maskedB1~=0);
nonzero_maskedB1 = lowresB1(lowresB1~=0);
% sRB1_x = RB1_x(RB1_x~=0);
nonzero_maskedB1 = nonzero_maskedB1(:) - 1e-8;%repmat(maskMLS(:),8,1);
% wired_pos = find(maskedB1wired(:,1));
nonzero_maskedB1wired = maskedB1wired(maskedB1wired~=0);
nonzero_maskedB1wired = nonzero_maskedB1wired - 1e-8; %repmat(sMask(:),8,1);

% heartRB1 = reshape(RB1,[sz 8]);
% heartRB1wired = reshape(RB1wired, [sz 8]);


%% GET Q-TIP
% Q_reshaped = reshape(Q, [sz 8 8]);
% Q_tip = squeeze(Q_reshaped(xtip,ytip,ztip,:,:));

Q_tip = VOPm;

%%
handle = 'FinalRapidCoil_Duke_15T_RealWire_StrippedTip';
save(['G:/ISMRM2019/mats for Optimised script/' handle '_tipQ'],'Q_tip','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_Masks'],'filledmask', 'wiredmask','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_maskMLS'],'maskMLS','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_J'],'J','Jaxis','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_heartB1'],'sRB1','sRB1wired','wired_pos','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_Density'],'rho','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle 'p_Axis'],'fsz','sz','xaxis','yaxis','zaxis','-v7.3')
