%%
npoints = 40;
ncoils = 8;
% %%%%%% IF NEW CURERNT METHOD
ltrue(1) = 0;
for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end
% lshifted = ltrue - ltrue(19);
for ii = 1:ncoils
    Jinterp_ini(ii,:) = interp1(J(ii,:), 1:36/2000:36);
end
linterp = interp1(ltrue,1:36/2000:36);

% points_w = linspace(-100,594,npoints)'; %For Phantom
% pedge = ltrue(21); % For Phantom

points_w = linspace(-100,756,npoints)'; %For Duke
pedge = ltrue(16); % For Duke

linterp_shift = linterp-pedge;
[~, pedge_ind] = min(abs(linterp-pedge));
st = linterp(2)-linterp(1);
kk = 1;
mm = 1;
for ii = 1:length(points_w)
    for jj = ii+1:length(points_w)
        points_pos(kk,1:2) = [points_w(ii) points_w(jj)];
        for ll = jj+1:length(points_w)
                points_3_pos(mm,1:3) = [points_w(ii) points_w(jj) points_w(ll)];
                mm = mm+1;
        end
        kk = kk + 1;
    end
end
Jinterp = permute(Jinterp_ini, [2 1]);


% FOR ALL
jetblack = jet(4096);
jetblack(1,:) = [0,0,0];


% Q_tip = VOPm;
% 
%% find WORST CASE (ALL MODES) AVERAGE B1+
ncoils = 8;

% MLS SHIM
nonzero_maskedB1reshaped = reshape(nonzero_maskedB1,[],ncoils);
nonzero_maskedB1wired_reshaped = reshape(nonzero_maskedB1wired,[],ncoils);


maskMLS = ones(length(nonzero_maskedB1reshaped(:,1)),1);
%%
ncoils = 8;
npoints= 40;
Q_tip = VOPm/ncoils;

[u,s,v] = svd(permute(Jinterp, [1 2]));
% lambda = [4 3 3 0.45]; %Short coil High res old
lambda = [0 0 0]; %Long coil High res
% lambda = [0.6 0.4 0.4]; %Short coil High res


% lambda = [0 0 0]; %Long coil High res
% scaling_factor = 1e-6/abs(mean(nonzero_maskedB1reshaped(:))); %%DOUBT ABOUT HOW THIS IS DONE 09/10/2019
B1_toShim = nonzero_maskedB1reshaped./sqrt(ncoils);
scaling_factor = (1e-6/mean(abs(sum(B1_toShim,2))));
%%
disp('next')
for ii = 1:3
    a_W = 1e10;
    for jj = 1:1

        [witer,~] = MLS_modeshim_max_amp(B1_toShim.*scaling_factor*1e6,v(:,ii:8), maskMLS,1*ones(ncoils,1),[],lambda(ii),20,0.05,0.0001,'disabled');

%         if sum(abs(witer).^2) < a_W
%             wout(:,ii) = witer.*scaling_factor;
%             a_W = sum(abs(witer).^2);
%         end
        if abs(witer'*Q_tip*witer) < a_W
            wout(:,ii) = witer.*scaling_factor;
            a_W = abs(witer'*Q_tip*witer);
        end

    end
    
    [avgB1_perfKnowledge(ii),~,err(ii)] = applyShim(B1_toShim, wout(:,ii), ncoils);
    b1_normVal_perfKnowledge(ii) = (avgB1_perfKnowledge(ii).^2)./sum(abs(wout(:,ii)).^2);
    if ii == 1
        wcSAR_perfKnowledge(ii) = max(eig(Q_tip));
    else
        wcSAR_perfKnowledge(ii) = max(eig(v(:,ii:8)'*Q_tip*v(:,ii:8)));
    end
end


%% Quantities for Full System
% Q_tip = squeeze(VOPm(1,:,:);

for ii = 1:4
    if ii == 1
        wcSAR_perfKnowledge(ii) = max(eig(Q_tip));
    else
        wcSAR_perfKnowledge(ii) = max(eig(v(:,ii:8)'*Q_tip*v(:,ii:8)));
    end
end
Q_tip_normed = Q_tip./b1_normVal_perfKnowledge(1);
maxEV_fullQ(1:npoints) = max(eig(Q_tip_normed));
maxEV_fullQ_noNorm(1:npoints) = max(eig(Q_tip));

%% best positions for reduced system
for kk = 1:length(points_w)
    
    [~,posidx_1(1)] = min(abs(linterp_shift-points_w(kk,1)));
    [~,~,v]=svd(Jinterp(posidx_1,:));
    
    v_dmss_1s1m = v(:,2:ncoils);
    Q_dmss_1s1m = v_dmss_1s1m'*(Q_tip)*v_dmss_1s1m;
    maxEV_1s1m(kk) = max(eig(Q_dmss_1s1m));
    
end

for kk = 1:length(points_pos)
    
    [~,posidx_2(1)] = min(abs(linterp_shift-points_pos(kk,1)));
    [~,posidx_2(2)] = min(abs(linterp_shift-points_pos(kk,2)));
    [~,~,v]=svd(Jinterp(posidx_2,:));
    
    v_dmss_2s1m = v(:,2:ncoils);
    Q_dmss_2s1m = v_dmss_2s1m'*(Q_tip)*v_dmss_2s1m;
    maxEV_2s1m(kk) = max(eig(Q_dmss_2s1m));
    
    v_dmss_2s2m = v(:,3:ncoils);
    Q_dmss_2s2m = v_dmss_2s2m'*(Q_tip)*v_dmss_2s2m;
    maxEV_2s2m(kk) = max(eig(Q_dmss_2s2m));
    
end

for kk = 1:length(points_3_pos)
    
    [~,posidx_3(1)] = min(abs(linterp_shift-points_3_pos(kk,1)));
    [~,posidx_3(2)] = min(abs(linterp_shift-points_3_pos(kk,2)));
    [~,posidx_3(3)] = min(abs(linterp_shift-points_3_pos(kk,3)));%Sets points in relation to phantom-air border
    [~,~,v]=svd(Jinterp(posidx_3,:));
    
    v_dmss_3s1m = v(:,2:ncoils);
    Q_dmss_3s1m = v_dmss_3s1m'*(Q_tip)*v_dmss_3s1m;
    maxEV_3s1m(kk) = max(eig(Q_dmss_3s1m));
    
    v_dmss_3s2m = v(:,3:ncoils);
    Q_dmss_3s2m = v_dmss_3s2m'*(Q_tip)*v_dmss_3s2m;
    maxEV_3s2m(kk) = max(eig(Q_dmss_3s2m));
    
    v_dmss_3s3m = v(:,4:ncoils);
    Q_dmss_3s3m = v_dmss_3s3m'*(Q_tip)*v_dmss_3s3m;
    maxEV_3s3m(kk) = max(eig(Q_dmss_3s3m));

end

%% Actual optimal points
wcSAR_full(1:length(points_w)) = abs(wcSAR_perfKnowledge(1))*1e-12;
deepestSens_wcSAR_1s1m = abs(maxEV_1s1m)*1e-12;

for ii = 2:length(points_w)
    logic_idx = find(points_pos(:,2) == points_w(ii));
    [deepestSens_wcSAR_2s1m(ii), temp_idx_2s1m] = min(abs(maxEV_2s1m(logic_idx))*1e-12);
    [deepestSens_wcSAR_2s2m(ii), temp_idx_2s2m] = min(abs(maxEV_2s2m(logic_idx))*1e-12);
    
    bestPos_idx_2s1m(ii) = logic_idx(temp_idx_2s1m);
    bestPos_idx_2s2m(ii) = logic_idx(temp_idx_2s2m);
    
    bestPos_2s1m(ii,:) = points_pos(bestPos_idx_2s1m(ii),:);
    bestPos_2s2m(ii,:) = points_pos(bestPos_idx_2s2m(ii),:);
    
    [~,bestIdx_2s1m_pt1] = min(abs(linterp_shift-bestPos_2s1m(ii,1)));
    [~,bestIdx_2s1m_pt2] = min(abs(linterp_shift-bestPos_2s1m(ii,2)));
    bestIdx_2s1m(ii,:) = [bestIdx_2s1m_pt1, bestIdx_2s1m_pt2];
    
    [~,bestIdx_2s2m_pt1] = min(abs(linterp_shift-bestPos_2s2m(ii,1)));
    [~,bestIdx_2s2m_pt2] = min(abs(linterp_shift-bestPos_2s2m(ii,2)));
    bestIdx_2s2m(ii,:) = [bestIdx_2s2m_pt1, bestIdx_2s2m_pt2];
    
    [deepestSens_wcSAR_2s(ii), bestMode_2s(ii)] = min([deepestSens_wcSAR_2s1m(ii), deepestSens_wcSAR_2s2m(ii)]);
end
%%

for ii = 3:length(points_w)
    logic_idx = find(points_3_pos(:,3) == points_w(ii));
    [deepestSens_wcSAR_3s1m(ii), temp_idx_3s1m] = min(abs(maxEV_3s1m(logic_idx))*1e-12);
    [deepestSens_wcSAR_3s2m(ii), temp_idx_3s2m] = min(abs(maxEV_3s2m(logic_idx))*1e-12);
    [deepestSens_wcSAR_3s3m(ii), temp_idx_3s3m] = min(abs(maxEV_3s3m(logic_idx))*1e-12);
    
    bestPos_idx_3s1m(ii) = logic_idx(temp_idx_3s1m);
    bestPos_idx_3s2m(ii) = logic_idx(temp_idx_3s2m);
    bestPos_idx_3s3m(ii) = logic_idx(temp_idx_3s3m);
    
    bestPos_3s1m(ii,:) = points_3_pos(bestPos_idx_3s1m(ii),:);
    bestPos_3s2m(ii,:) = points_3_pos(bestPos_idx_3s2m(ii),:);
    bestPos_3s3m(ii,:) = points_3_pos(bestPos_idx_3s3m(ii),:);
    
    [~,bestIdx_3s1m_pt1] = min(abs(linterp_shift-bestPos_3s1m(ii,1)));
    [~,bestIdx_3s1m_pt2] = min(abs(linterp_shift-bestPos_3s1m(ii,2)));
    [~,bestIdx_3s1m_pt3] = min(abs(linterp_shift-bestPos_3s1m(ii,3)));
    bestIdx_3s1m(ii,:) = [bestIdx_3s1m_pt1, bestIdx_3s1m_pt2, bestIdx_3s1m_pt3];
    
    [~,bestIdx_3s2m_pt1] = min(abs(linterp_shift-bestPos_3s2m(ii,1)));
    [~,bestIdx_3s2m_pt2] = min(abs(linterp_shift-bestPos_3s2m(ii,2)));
    [~,bestIdx_3s2m_pt3] = min(abs(linterp_shift-bestPos_3s2m(ii,3)));
    bestIdx_3s2m(ii,:) = [bestIdx_3s2m_pt1, bestIdx_3s2m_pt2, bestIdx_3s2m_pt3];
    
    [~,bestIdx_3s3m_pt1] = min(abs(linterp_shift-bestPos_3s3m(ii,1)));
    [~,bestIdx_3s3m_pt2] = min(abs(linterp_shift-bestPos_3s3m(ii,2)));
    [~,bestIdx_3s3m_pt3] = min(abs(linterp_shift-bestPos_3s3m(ii,3)));
    bestIdx_3s3m(ii,:) = [bestIdx_3s3m_pt1, bestIdx_3s3m_pt2, bestIdx_3s3m_pt3];
    
    [deepestSens_wcSAR_3s(ii), bestMode_3s(ii)] = min([deepestSens_wcSAR_3s1m(ii), deepestSens_wcSAR_3s2m(ii), deepestSens_wcSAR_3s3m(ii)]);
end

%% B1+ Shim
text = 'disabled';
lambda = [60 50 1]; %[100 50 0];%
for kk = 1:length(points_w)
    disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_w))])
   
    [~,bestIdx_1s1m(kk)] = min(abs(linterp_shift-points_w(kk)));
    
    [modes_1s1m(:,:,kk), w1s1m(:,kk), avgB1_1s1m(kk)] = TipQ_v2(1, ncoils, bestIdx_1s1m(kk), Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, lambda(1),  text);    
end

for kk = 2:length(points_w)
    disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_w))])
    
    [modes_2s1m(:,:,kk), w2s1m(:,kk), avgB1_2s1m(kk)] = TipQ_v2(1, ncoils, bestIdx_2s1m(kk,:), Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, lambda(1), text);

    [modes_2s2m(:,:,kk), w2s2m(:,kk), avgB1_2s2m(kk)] = TipQ_v2(2, ncoils, bestIdx_2s2m(kk,:), Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, lambda(2), text);
end

for kk = 3:length(points_w)
    disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_w))])
        
    [modes_3s1m(:,:,kk), w3s1m(:,kk), avgB1_3s1m(kk)] = TipQ_v2(1, ncoils, bestIdx_3s1m(kk,:), Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, lambda(1), text);

    [modes_3s2m(:,:,kk), w3s2m(:,kk), avgB1_3s2m(kk)] = TipQ_v2(2, ncoils, bestIdx_3s2m(kk,:), Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, lambda(2), text);

    [modes_3s3m(:,:,kk), w3s3m(:,kk), avgB1_3s3m(kk)] = TipQ_v2(3, ncoils, bestIdx_3s3m(kk,:), Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, lambda(3), text);
end

 b1weights_1s1m = (avgB1_1s1m.^2)./sum(abs(w1s1m).^2,1);
 b1weights_2s1m = (avgB1_2s1m.^2)./sum(abs(w2s1m).^2,1);
 b1weights_2s2m = (avgB1_2s2m.^2)./sum(abs(w2s2m).^2,1);
 b1weights_3s1m = (avgB1_3s1m.^2)./sum(abs(w3s1m).^2,1);
 b1weights_3s2m = (avgB1_3s2m.^2)./sum(abs(w3s2m).^2,1);
 b1weights_3s3m = (avgB1_3s3m.^2)./sum(abs(w3s3m).^2,1);
 
% [maxEV_redQ_2s1m, maxEV_redQ_2s1m_noNorm, shimmedB1_2s1m, modes_2s1m, b1weights_2s1m, w2s1m, avgB1_2s1m] = twoSensorTipQ(1,nChannels, points_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_2s2m, maxEV_redQ_2s2m_noNorm, shimmedB1_2s, modes_2s, b1weights_2s, w2, avgB1_2s] = twoSensorTipQ(2, nChannels, points_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_3s3m, maxEV_redQ_3s3m_noNorm, shimmedB1_3s, modes_3s, b1weights_3s, w3, avgB1_3s] = threeSensorTipQ(3, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_3s2m, maxEV_redQ_3s2m_noNorm, shimmedB1_3s2m, modes_3s2m, b1weights_3s2m, w3s2m, avgB1_3s2m] = threeSensorTipQ(2, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

% [maxEV_redQ_3s1m, maxEV_redQ_3s1m_noNorm, shimmedB1_3s1m, modes_3s1m, b1weights_3s1m, w3s1m, avgB1_3s1m] = threeSensorTipQ(1, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);
% 
%% NORMED


wcSAR_full = abs(maxEV_fullQ)*1e-12;
deepestSens_wcSAR_1s1m = abs(maxEV_redQ_1s1m)*1e-12;
for ii = 2:length(points_w)
    logic_idx = find(points_pos(:,2) == points_w(ii));
    deepestSens_wcSAR_2s1m(ii) = abs(min(maxEV_redQ_2s1m(logic_idx)))*1e-12;
    deepestSens_wcSAR_2s2m(ii) = abs(min(maxEV_redQ_2s2m(logic_idx)))*1e-12;
end

for ii = 3:length(points_w)
    logic_idx = find(points_3_pos(:,3) == points_w(ii));
    deepestSens_wcSAR_3s1m(ii) = abs(min(maxEV_redQ_3s1m(logic_idx)))*1e-12;
    deepestSens_wcSAR_3s2m(ii) = abs(min(maxEV_redQ_3s2m(logic_idx)))*1e-12;
    deepestSens_wcSAR_3s3m(ii) = abs(min(maxEV_redQ_3s3m(logic_idx)))*1e-12;
end

figure;
plot(points_w, wcSAR_full, 'o', 'LineWidth', 2); hold on
plot(points_w,deepestSens_wcSAR_1s1m,'o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s1m(2:end), 'o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s2m(2:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s1m(3:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s2m(3:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s3m(3:end), 'o', 'LineWidth',2)
grid on

legend('Full', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')


%% NO NORM


wcSAR_full_noNorm = abs(maxEV_fullQ_noNorm);
deepestSens_wcSAR_1s1m_noNorm = abs(maxEV_redQ_1s1m_noNorm);
for ii = 2:length(points_w)
    logic_idx = find(points_pos(:,2) == points_w(ii));
    deepestSens_wcSAR_2s1m_noNorm(ii) = abs(min(maxEV_redQ_2s1m_noNorm(logic_idx)));
    deepestSens_wcSAR_2s2m_noNorm(ii) = abs(min(maxEV_redQ_2s2m_noNorm(logic_idx)));
end

for ii = 3:length(points_w)
    logic_idx = find(points_3_pos(:,3) == points_w(ii));
    deepestSens_wcSAR_3s1m(ii) = abs(min(maxEV_redQ_3s1m(logic_idx)))*1e-12;
    deepestSens_wcSAR_3s2m(ii) = abs(min(maxEV_redQ_3s2m(logic_idx)))*1e-12;
    deepestSens_wcSAR_3s3m(ii) = abs(min(maxEV_redQ_3s3m(logic_idx)))*1e-12;
end

figure;
plot(points_w, wcSAR_full, 'o', 'LineWidth', 2); hold on
plot(points_w,deepestSens_wcSAR_1s1m,'o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s1m(2:end), 'o', 'LineWidth',2)
plot(points_w(2:end),deepestSens_wcSAR_2s2m(2:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s1m(3:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s2m(3:end), 'o', 'LineWidth',2)
plot(points_w(3:end),deepestSens_wcSAR_3s3m(3:end), 'o', 'LineWidth',2)
grid on

legend('Full', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('wcSAR - Straight Wire - Phantom')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')

%%

[u_fullWire,s_fullWire,v_fullWire]=svd(Jinterp(:,:));


%% MAKE MAPS
cmap = cmapTipQ(maxEV_redQ_2s2m, maxEV_redQ_1s1m, maxEV_fullQ, points_w);

b1wMap = b1weightsMap(b1weights_2s, b1weights_1s, b1_normVal, points_w);

noNormMap = cmapTipQ_noNorm(maxEV_redQ_2s2m_noNorm, maxEV_redQ_1s1m_noNorm, maxEV_fullQ_noNorm, points_w);

%% THREE SENSORS
cmap = cmapTipQ(maxEV_redQ_2s2m, maxEV_redQ_1s1m, maxEV_fullQ, points_w);

b1wMap = b1weightsMap(b1weights_2s, b1weights_1s, b1_normVal, points_w);

noNormMap = cmapTipQ_noNorm(maxEV_redQ_2s2m_noNorm, maxEV_redQ_1s1m_noNorm, maxEV_fullQ_noNorm, points_w);