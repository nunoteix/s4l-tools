%% DO THE SAME FOR THE RAPID COIL
npoints = 40;
ncoils = 8;

% path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';
path_to_results = 'G:\Huygens Simulations\Phantom\Extracted Results\';

% HERE WE CHOOSE WHAT SIMULATION TO GET THE RESULTS FROM - THIS NAME WAS
% PRE-CHOSEN WHEN EXPORTING FROM S4L
% simulation_name = '\RapidCoils_Duke_Guidewire_HighRes_2020_01_06_'; %Duke, ShortCoil, High Res
% simulation_name = '\LongRapidCoils_Duke_Guidewire_HighRes_2020_02_05_'; %Duke, Long Coil, High Res
simulation_name = '\RapidCoils_Phantom_Guidewire_2020_02_12_'; %Phantom, Short Coil, High Res

% Import Current
J_struct = load([path_to_results 'EM Current' simulation_name 'EMCurrent']);
J = J_struct.J;
Axes = J_struct.Axes;

% Get B1+ to Acceptable Levels
load([path_to_results 'Grid' simulation_name 'Axes'])
sz = [length(XAxis) length(YAxis) length(ZAxis)]; % STORES MATRIX SIZES (HANDY FOR OPERATIONS)
fsz = sz(1)*sz(2)*sz(3);

% IF NAMED CORRECTLY THEN THIS SHOULD GRAB B1 MATRICES
for ii = 1:ncoils
    load([path_to_results 'B1 Field' simulation_name 'B1Fields_C' num2str(ii)])
end
B1 = cat(3, B1_1, B1_2, B1_3, B1_4, B1_5, B1_6, B1_7, B1_8); % BUILD THE 8 CHANNEL MATRIX
clear B1_1 B1_2 B1_3 B1_4 B1_5 B1_6 B1_7 B1_8 % FREEING SPACE
B1p = permute(reshape(squeeze(B1(:,1,:)), [sz(1) sz(3) sz(2) 8]), [1 3 2 4]); % ISOLATE B1+ AND REORDER IT INTO XYZ
[~,xx] = min(abs(XAxis - Axes(1,end)*1e-3));
[~,yy] = min(abs(YAxis - Axes(2,end)*1e-3));
[~,zz] = min(abs(ZAxis - Axes(3,end)*1e-3));

%% find WORST CASE (ALL MODES) AVERAGE B1+
% path_to_results = 'G:\Huygens Simulations\Duke\Extracted Results\';
path_to_results = 'G:\Huygens Simulations\Phantom\Extracted Results\';

% simulation_name = '\BodyCoil_Duke_Guidewire_2020_04_07_';
% simulation_name = '\RapidCoils_Duke_Guidewire_HighRes_2020_01_06_'; %Duke, ShortCoil, High Res
simulation_name = '\RapidCoils_Phantom_Guidewire_2020_02_12_'; %Duke, ShortCoil, High Res

load([path_to_results 'Density' simulation_name 'Density'])
rho = reshape(permute(reshape(rho, [sz(1) sz(3) sz(2)]), [1 3 2]), [fsz 1]); %REORDER INTO XYZ
%%
dielectric = 'Phantom'; % 'Duke'; % 
if strcmp(dielectric, 'Phantom')
    maskR = reshape(rho, [sz 1]);
    %     Eabs_s = sum(reshape(Eabs, [sz 8]),4);
    temprho = rho;
    temprho(rho~=0) = 1;
    for XX = 1:sz(1)
        for YY = 1:sz(2)
            for ZZ = 1:sz(3)
                if (XAxis(XX)-XAxis(xx))^2+(YAxis(YY)-YAxis(yy))^2+(ZAxis(ZZ)-ZAxis(zz))^2 <= 0.05^2
                    maskR(XX,YY,ZZ) = 1;
                else
                    maskR(XX,YY,ZZ) = 0;
                end
            end
        end
    end
    
    filledmaskR = maskR.*reshape(temprho, [sz 1]);
    filledmask = reshape(filledmaskR, [fsz 1]);
    
elseif strcmp(dielectric, 'Duke')
    maskR = rho; %Load Mask Properties first, if is in cell form run: rho = cellfun(@(x) double(x), rho);
    maskR((maskR > 1048) & (maskR < 1050.5)) = 1; %Density of Heart Lumen (blood)
    maskR((maskR > 1079) & (maskR < 1081)) = 1; %Density of Heart Muscle (plot rho and check precis value)
    maskR(maskR ~= 1) = 0; % Make everything else 0
    maskR = reshape(maskR, sz); %Reshape to 3D
    
    % Check manually what small box encompasses
    % the Heart, as blood has same density, need to make everything else
    % outside this box 0, hence the following lines:
    
    %%%% REAL WIRE DUKE SHORT COIL, HIGH RES
    maskR(1:16,:,:) = 0;
    maskR(:,1:24,:) = 0;
    maskR(:,:,1:1052) = 0;
    maskR(150:end,:,:) = 0;
    maskR(:,121:end,:) = 0;
    maskR(:,:,1165:end) = 0;
    
    % Voxelization makes everything patchy. Dilate and Erode to fill patches
    se = strel('sphere',3); %Patches are small so small Dilate radius will do
    dilmaskR = imdilate(maskR,se); %Dilate
    filledmaskR = imerode(dilmaskR,se); %Erode, Heart Region should now be properly masked
    filledmask = reshape(filledmaskR, [fsz 1]);
    maskMLS = filledmask(filledmask~=0);
end
%%
figurent();
% imagesc(filledmaskR(:,:,1119)); %DUKE
imagesc(filledmaskR(:,:,1410)); %PHANTOM
%%
% vers = 'new'; % 'old'; %
zero_phase = ones(8,1)./sqrt(ncoils);
% B1_S4L = reshape(B1p, [fsz ncoils]);
B1pr = reshape(B1p, [fsz ncoils]);
% B1pr_quad = zeros(size(B1_S4L));
phase_quad = zeros(8,1);

if strcmp(dielectric, 'Phantom')
    z_offset = 1;
elseif strcmp(dielectric, 'Duke')
    z_offset = -1;
end

if ncoils > 1
    for ii = 1:ncoils
        phase_quad(ii) = zero_phase(ii).*exp(-1i*angle(B1p(xx,yy,zz+z_offset,ii))); % CHANGE IF PHANTOM
%         B1pr_quad(:,ii) = B1_S4L(:,ii)*phase_quad(ii);
%         B1pr(:,ii) = B1_S4L(:,ii)*zero_phase(ii);
    end
end

for ii = 1:ncoils
% %%%% HEART SECTION    
%     if ncoils > 1
%         maskedB1_quad(:,ii) = B1pr_quad(:,ii).*filledmask;%.*w(ii);
        maskedB1(:,ii) = B1pr(:,ii).*filledmask;%.*w(ii);
%     else
%         maskedB1(:,ii) = B1pr(:,ii).*filledmask;%.*w(ii);
%     end
end

% maskedB1_quad_reshaped = reshape(maskedB1_quad, [sz ncoils]);
maskedB1_reshaped = reshape(maskedB1, [sz ncoils]);
for ii = 1:ncoils
    lowresB1_reshaped(:,:,:,ii) = Gen3D(squeeze(maskedB1_reshaped(:,:,:,ii)), XAxis, YAxis, ZAxis, 30, 30, 150);
%     lowresB1_quad_reshaped(:,:,:,ii) = Gen3D(squeeze(maskedB1_quad_reshaped(:,:,:,ii)), XAxis, YAxis, ZAxis, 30, 30, 150);
end

% lowresB1_quad = reshape(lowresB1_quad_reshaped, [30*30*150 ncoils]);
lowresB1 = reshape(lowresB1_reshaped, [30*30*150 ncoils]);

% nonzero_maskedB1_quad = lowresB1_quad(lowresB1_quad~=0);
nonzero_maskedB1 = lowresB1(lowresB1~=0);

% B1_toShim_quad = reshape(nonzero_maskedB1_quad,[],ncoils);
nonzero_B1 = reshape(nonzero_maskedB1,[],ncoils);

maskMLS = ones(length(nonzero_B1(:,1)),1);

%%
% [L, Jinterp, pedge_ind, points_1s, points_2s, points_3s] = interpJ(npoints, ncoils, 2000, Axes, J, 735, 16); %Duke
[L, Jinterp, pedge_ind, points_1s, points_2s, points_3s] = interpJ(npoints, ncoils, 2000, Axes, J, 575, 21); %Phantom %594

[u,s,v] = svd(Jinterp);

Q_tip = VOPm;

% for ii=1:ncoils
%     B1_toShim(:,ii) = nonzero_B1(:,ii)*zero_phase(ii);
%     B1_toShim_quad(:,ii) = nonzero_B1(:,ii)*phase_quad(ii);
% end

% scaling_factor = (1e-6/mean(abs(sum(B1_toShim,2))));
% scaling_factor = (1e-6/mean(abs(sum(B1_toShim_quad,2))));
scaling_factor = (1e-6/mean(abs(nonzero_B1*phase_quad)));

%%

% lambda = [0:0.2:0.4, 0.8, 1.3, 2.5:2:10.5, 15:15:75];
lambda = [0:0.1:1,1.5, 2.5:1:6.5, 8, 15, 30];
% lambda = [0:0.1:0.3, 0.5, 1:0.25:2, 3,4, 8, 15, 20, 25];
w_fullK = zeros(8,length(lambda),4);
avgB1_fullK = zeros(length(lambda),4);
err = zeros(length(lambda),4);
pow = zeros(length(lambda),4);
niters = 50;
witer = zeros(ncoils,length(lambda),niters,4);

tol = 0.1; %Step1&2&3&4&5&6
% tol = 0.05; %Step7

for mo = 1:4
    
    for ii = 1:length(lambda)
        
        pow(ii,mo) = 1e10;
        
        for jj = 1:niters
            %             [witer(:,ii,jj),~] = MLS_modeshim_max_amp(B1_toShim.*1e6,v(:,mo+1:8),scaling_factor, maskMLS,100*ones(ncoils,1),[],lambda(ii),20,tol,0.001,'disabled');
            [witer(:,ii,jj,mo),~] = MLS_modeshim_max_amp(nonzero_B1.*1e6,v(:,mo:8),scaling_factor, maskMLS,100*ones(ncoils,1),[],lambda(ii),20,tol,0.001,'disabled');
            if sum(abs(witer(:,ii,jj,mo)).^2) < pow(ii,mo)
                w_fullK(:,ii,mo) = witer(:,ii,jj,mo);
                pow(ii,mo) = sum(abs(witer(:,ii,jj,mo)).^2);
            end
            %         [avgB1_fullK(ii),~,err(ii)] = applyShim(B1_toShim, w_fullK(:,ii), ncoils);
            [avgB1_fullK(ii,mo),~,err(ii,mo)] = applyShim(nonzero_B1, w_fullK(:,ii,mo), ncoils);
            b1_normVal_fullK(ii,mo) = (avgB1_fullK(ii,mo).^2)./sum(abs(w_fullK(:,ii,mo)).^2);
            if mo == 1
                wcSAR_fullK(ii,mo) = max(eig(Q_tip));
            else
                wcSAR_fullK(ii,mo) = max(eig(v(:,mo:8)'*Q_tip*v(:,mo:8)));
            end
            
        end
        
    end
end
%% Quantities for Full System
% Q_tip = squeeze(VOPm(1,:,:);

% for ii = 1:4
%     if ii == 1
%         wcSAR_fullK(ii) = max(eig(Q_tip));
%     else
%         wcSAR_fullK(ii) = max(eig(v(:,ii:8)'*Q_tip*v(:,ii:8)));
%     end
% end
wcSAR_perfKnowledge(1:length(points_1s)) = wcSAR_fullK(opt_l(1),1); %index of wcSAR_full should be optimal lambda pos
% Q_tip_normed = Q_tip*pow(14,1);
%%
maxEV_fullQ(1:npoints) = max(eig(Q_tip_normed));
maxEV_fullQ_noNorm(1:npoints) = max(eig(Q_tip));
%%

figurent()
hold on;
for mo = 1:4
    plot(err(:,mo), pow(:,mo),'o-','LineWidth', 2); %Step5
end
ylabel('Shimming Power (W)')
xlabel('Error')
legend('No Modes Removed', '1 Mode Removed', '2 Modes Removed', '3 Modes Removed')
set(gca, 'FontSize',14)
grid on
% axis([0 0.5 0 max(pow(:,3))])
% jj = 1;

% opt_l = [13 12 13 12]; %[14 13 13 10]; %Short Coil %
% opt_l = [12 12 12 12]; %Long Coil
opt_l = [11 11 11 10]; %Phantom
%%

pow_all = zeros(length(lambda),niters, 4);
sar_ac = zeros(length(lambda),niters, 4);
[u,s,v] = svd(Jinterp);

for mo = 1:4
    for ii = 1:length(lambda)
        %     subplot(3,4,ii)
        for jj = 1:niters
            pow_all(ii,jj,mo) = sum(abs(witer(:,ii,jj,mo)).^2);
            sar_ac(ii,jj,mo) = abs(witer(:,ii,jj,mo)'*Q_tip*witer(:,ii,jj,mo));
        end
    end
    Q_red = v(:,mo:ncoils)'*Q_tip*v(:,mo:ncoils);
    wcSAR_all(:,:,mo) = max(eig(Q_red)).*pow_all(:,:,mo);
%     disp(max(eig(Q_red)))
end



%%
figurent()
for mo = 1:4
    subplot(2,2,mo)
    
    for ii = 1:length(lambda)
        scatter(ones(1,niters)*lambda(ii),sar_ac(ii,:,mo), 'rx', 'LineWidth',0.5);hold on;
%         scatter(ones(1,niters)*lambda(ii),wcSAR_all(ii,:,mo), 'bx', 'LineWidth',0.5)
%         axis([0 6.5 0 200]) %Short Coil
%         axis([0 6.5 0 1000]) %Long Coil
        axis([0 6.5 0 40]) %Phantom
        %     drawnow
        %     pause(1)
    end
    
    set(gca, 'FontSize',14)
    ylabel('W/kg/\muT^2')
    xlabel('\lambda')
    legend('Actual SAR')
end
%%
figurent()
hold on
for mo = 1:4
    subplot(2,2,mo)
    hold on
    Q_red = v(:,mo:ncoils)'*Q_tip*v(:,mo:ncoils);
    for ii = 1:length(lambda)
%         scatter(ones(1,niters)*lambda(ii),sar_ac(ii,:,mo), 'rx', 'LineWidth',0.5);hold on;
        scatter(ones(1,niters)*lambda(ii),wcSAR_all(ii,:,mo), 'bx', 'LineWidth',0.5)
        axis([0 6.5 0 10000]) %Short Coil
%         axis([0 6.5 0 60000]) %Long Coil
%         axis([0 6.5 0 6000]) %Phantom
        %     drawnow
        %     pause(1)
    end
    
    set(gca, 'FontSize',14)
    ylabel('W/kg/\muT^2')
    xlabel('\lambda')
    legend('Worst Case SAR')
end
%%
figurent()
hold on
for mo = 1:4
    subplot(2,2,mo)
    boxplot(sar_ac(:,:,mo)',lambda, 'Symbol','');
%     boxplot(abs(wcSAR_all(:,:,mo))',lambda, 'Symbol','');
    set(gca, 'FontSize',14)
    ylabel('W/kg/\muT^2')
    xlabel('\lambda')
%     legend('Worst Case SAR')
end
%%
figurent()
hold on
for mo = 1:4
    subplot(2,2,mo)
%     boxplot(sar_ac(:,:,mo)',lambda, 'Symbol','');
    boxplot(abs(wcSAR_all(:,:,mo))',lambda, 'Symbol','');
    set(gca, 'FontSize',14)
    ylabel('W/kg/\muT^2')
    xlabel('\lambda')
%     legend('Actual SAR')
end
%%
figurent()
for_boxer = [sar_ac(opt_l(1),:,1); sar_ac(opt_l(2),:,2); sar_ac(opt_l(3),:,3); sar_ac(opt_l(4),:,4)];
boxplot(for_boxer', 0:3, 'Symbol','x', 'Labels',{'All Modes', '-1 Mode', '-2 Modes', '-3 Modes'});
ylim([0 250]);

set(gca, 'FontSize',12)
ylabel('W/kg/\muT^2')
xlabel('\lambda')
title('Actual SAR - Full Knowledge of J')
%%
figurent()
for_boxer = [abs(wcSAR_all(opt_l(1),:,1)); abs(wcSAR_all(opt_l(2),:,2)); abs(wcSAR_all(opt_l(3),:,3)); abs(wcSAR_all(opt_l(4),:,4))];
boxplot(for_boxer', 0:3, 'Symbol','x', 'Labels',{'All Modes', '-1 Mode', '-2 Modes', '-3 Modes'});
ylim([0 1700]);
set(gca, 'FontSize',12)
ylabel('W/kg/\muT^2')
xlabel('\lambda')
title('Worst Case SAR - Full Knowledge of J')
%% best positions for reduced system
for kk = 1:length(points_1s)
    
    [~,posidx_1(1)] = min(abs(L-points_1s(kk,1)));
    [~,~,v]=svd(Jinterp(posidx_1,:));
    
    v_dmss_1s1m = v(:,2:ncoils);
    Q_dmss_1s1m = v_dmss_1s1m'*(Q_tip)*v_dmss_1s1m;
    maxEV_1s1m(kk) = max(eig(Q_dmss_1s1m));
    
end

for kk = 1:length(points_2s)
    
    [~,posidx_2(1)] = min(abs(L-points_2s(kk,1)));
    [~,posidx_2(2)] = min(abs(L-points_2s(kk,2)));
    [~,~,v]=svd(Jinterp(posidx_2,:));
    
    v_dmss_2s1m = v(:,2:ncoils);
    Q_dmss_2s1m = v_dmss_2s1m'*(Q_tip)*v_dmss_2s1m;
    maxEV_2s1m(kk) = max(eig(Q_dmss_2s1m));
    
    v_dmss_2s2m = v(:,3:ncoils);
    Q_dmss_2s2m = v_dmss_2s2m'*(Q_tip)*v_dmss_2s2m;
    maxEV_2s2m(kk) = max(eig(Q_dmss_2s2m));
    
end

for kk = 1:length(points_3s)
    
    [~,posidx_3(1)] = min(abs(L-points_3s(kk,1)));
    [~,posidx_3(2)] = min(abs(L-points_3s(kk,2)));
    [~,posidx_3(3)] = min(abs(L-points_3s(kk,3)));%Sets points in relation to phantom-air border
    [~,~,v]=svd(Jinterp(posidx_3,:));
    
    v_dmss_3s1m = v(:,2:ncoils);
    Q_dmss_3s1m = v_dmss_3s1m'*(Q_tip)*v_dmss_3s1m;
    maxEV_3s1m(kk) = max(eig(Q_dmss_3s1m));
    
    v_dmss_3s2m = v(:,3:ncoils);
    Q_dmss_3s2m = v_dmss_3s2m'*(Q_tip)*v_dmss_3s2m;
    maxEV_3s2m(kk) = max(eig(Q_dmss_3s2m));
    
    v_dmss_3s3m = v(:,4:ncoils);
    Q_dmss_3s3m = v_dmss_3s3m'*(Q_tip)*v_dmss_3s3m;
    maxEV_3s3m(kk) = max(eig(Q_dmss_3s3m));

end

%% Actual optimal points
wcSAR_full(1:length(points_1s)) = abs(wcSAR_fullK(opt_l(1),1))*1e-12;
deepestSens_wcSAR_1s1m = abs(maxEV_1s1m)*1e-12;

for ii = 2:length(points_1s)
    logic_idx = find(points_2s(:,2) == points_1s(ii));
    [deepestSens_wcSAR_2s1m(ii), temp_idx_2s1m] = min(abs(maxEV_2s1m(logic_idx))*1e-12);
    [deepestSens_wcSAR_2s2m(ii), temp_idx_2s2m] = min(abs(maxEV_2s2m(logic_idx))*1e-12);
    
    bestPos_idx_2s1m(ii) = logic_idx(temp_idx_2s1m);
    bestPos_idx_2s2m(ii) = logic_idx(temp_idx_2s2m);
    
    bestPos_2s1m(ii,:) = points_2s(bestPos_idx_2s1m(ii),:);
    bestPos_2s2m(ii,:) = points_2s(bestPos_idx_2s2m(ii),:);
    
    [~,bestIdx_2s1m_pt1] = min(abs(L-bestPos_2s1m(ii,1)));
    [~,bestIdx_2s1m_pt2] = min(abs(L-bestPos_2s1m(ii,2)));
    bestIdx_2s1m(ii,:) = [bestIdx_2s1m_pt1, bestIdx_2s1m_pt2];
    
    [~,bestIdx_2s2m_pt1] = min(abs(L-bestPos_2s2m(ii,1)));
    [~,bestIdx_2s2m_pt2] = min(abs(L-bestPos_2s2m(ii,2)));
    bestIdx_2s2m(ii,:) = [bestIdx_2s2m_pt1, bestIdx_2s2m_pt2];
    
    [deepestSens_wcSAR_2s(ii), bestMode_2s(ii)] = min([deepestSens_wcSAR_2s1m(ii), deepestSens_wcSAR_2s2m(ii)]);
end
%%

for ii = 3:length(points_1s)
    logic_idx = find(points_3s(:,3) == points_1s(ii));
    [deepestSens_wcSAR_3s1m(ii), temp_idx_3s1m] = min(abs(maxEV_3s1m(logic_idx))*1e-12);
    [deepestSens_wcSAR_3s2m(ii), temp_idx_3s2m] = min(abs(maxEV_3s2m(logic_idx))*1e-12);
    [deepestSens_wcSAR_3s3m(ii), temp_idx_3s3m] = min(abs(maxEV_3s3m(logic_idx))*1e-12);
    
    bestPos_idx_3s1m(ii) = logic_idx(temp_idx_3s1m);
    bestPos_idx_3s2m(ii) = logic_idx(temp_idx_3s2m);
    bestPos_idx_3s3m(ii) = logic_idx(temp_idx_3s3m);
    
    bestPos_3s1m(ii,:) = points_3s(bestPos_idx_3s1m(ii),:);
    bestPos_3s2m(ii,:) = points_3s(bestPos_idx_3s2m(ii),:);
    bestPos_3s3m(ii,:) = points_3s(bestPos_idx_3s3m(ii),:);
    
    [~,bestIdx_3s1m_pt1] = min(abs(L-bestPos_3s1m(ii,1)));
    [~,bestIdx_3s1m_pt2] = min(abs(L-bestPos_3s1m(ii,2)));
    [~,bestIdx_3s1m_pt3] = min(abs(L-bestPos_3s1m(ii,3)));
    bestIdx_3s1m(ii,:) = [bestIdx_3s1m_pt1, bestIdx_3s1m_pt2, bestIdx_3s1m_pt3];
    
    [~,bestIdx_3s2m_pt1] = min(abs(L-bestPos_3s2m(ii,1)));
    [~,bestIdx_3s2m_pt2] = min(abs(L-bestPos_3s2m(ii,2)));
    [~,bestIdx_3s2m_pt3] = min(abs(L-bestPos_3s2m(ii,3)));
    bestIdx_3s2m(ii,:) = [bestIdx_3s2m_pt1, bestIdx_3s2m_pt2, bestIdx_3s2m_pt3];
    
    [~,bestIdx_3s3m_pt1] = min(abs(L-bestPos_3s3m(ii,1)));
    [~,bestIdx_3s3m_pt2] = min(abs(L-bestPos_3s3m(ii,2)));
    [~,bestIdx_3s3m_pt3] = min(abs(L-bestPos_3s3m(ii,3)));
    bestIdx_3s3m(ii,:) = [bestIdx_3s3m_pt1, bestIdx_3s3m_pt2, bestIdx_3s3m_pt3];
    
    [deepestSens_wcSAR_3s(ii), bestMode_3s(ii)] = min([deepestSens_wcSAR_3s1m(ii), deepestSens_wcSAR_3s2m(ii), deepestSens_wcSAR_3s3m(ii)]);
end

%% B1+ Shim

text = 'disabled';
lambda_opt = [lambda(opt_l(2)) lambda(opt_l(3)) lambda(opt_l(4))];

for kk = 1:length(points_1s)
    disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_1s))])
    
    [~,bestIdx_1s1m(kk)] = min(abs(L-points_1s(kk)));
    
    [modes_1s1m(:,:,kk), w1s1m(:,kk), w1s1m_all(:,:,kk), avgB1_1s1m(kk), err_1s1m(kk,ii)] = TipQ_v3(1, ncoils,...
        bestIdx_1s1m(kk), Jinterp, maskMLS, nonzero_B1, 100, scaling_factor, lambda_opt(1),  text);
end

for kk = 2:length(points_1s)
    disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_1s))])
    
    [modes_2s1m(:,:,kk), w2s1m(:,kk), w2s1m_all(:,:,kk), avgB1_2s1m(kk), err_2s1m(kk,ii)] = TipQ_v3(1, ncoils,...
        bestIdx_2s1m(kk,:), Jinterp, maskMLS, nonzero_B1, 100, scaling_factor, lambda_opt(1), text);
    
    [modes_2s2m(:,:,kk), w2s2m(:,kk), w2s2m_all(:,:,kk), avgB1_2s2m(kk), err_2s2m(kk,ii)] = TipQ_v3(2, ncoils,...
        bestIdx_2s2m(kk,:), Jinterp, maskMLS, nonzero_B1,100, scaling_factor, lambda_opt(2), text);
end

for kk = 3:length(points_1s)
    disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_1s))])
    
    [modes_3s1m(:,:,kk), w3s1m(:,kk), w3s1m_all(:,:,kk), avgB1_3s1m(kk), err_3s1m(kk,ii)] = TipQ_v3(1, ncoils,...
        bestIdx_3s1m(kk,:), Jinterp, maskMLS, nonzero_B1, 100, scaling_factor, lambda_opt(1), text);
    
    [modes_3s2m(:,:,kk), w3s2m(:,kk), w3s2m_all(:,:,kk), avgB1_3s2m(kk), err_3s2m(kk,ii)] = TipQ_v3(2, ncoils,...
        bestIdx_3s2m(kk,:), Jinterp, maskMLS, nonzero_B1, 100, scaling_factor, lambda_opt(2), text);
    
    [modes_3s3m(:,:,kk), w3s3m(:,kk), w3s3m_all(:,:,kk), avgB1_3s3m(kk), err_3s3m(kk,ii)] = TipQ_v3(3, ncoils,...
        bestIdx_3s3m(kk,:), Jinterp, maskMLS, nonzero_B1, 100, scaling_factor, lambda_opt(3), text);
end

%% PLOTS
%% Do the Normalised Plot with all cases
ll = 1;
b1weights_fullK = sum(abs(w_fullK(:,9).^2),1);
b1weights_1s1m = sum(abs(w1s1m).^2,1);
b1weights_2s1m = sum(abs(w2s1m).^2,1);
b1weights_2s2m = sum(abs(w2s2m).^2,1);
b1weights_3s1m = sum(abs(w3s1m).^2,1);
b1weights_3s2m = sum(abs(w3s2m).^2,1);
% b1weights_3s3m = (avgB1_3s3m.^2)./sum(abs(w3s3m).^2,1);

max_val = 1e12.*deepestSens_wcSAR_3s1m.*b1weights_3s1m;
% 1e12.*wcSAR_full.*b1weights_fullK;  %

figurent;
coil_reg = 400; %200;%435; %

pth = [coil_reg 0; 800 0; 800 max(max_val);...
    coil_reg max(max_val)]; %SHORT COIL


faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'red', 'FaceAlpha',.18, 'EdgeColor','none');
hold on

plot(points_1s, 1e12.*wcSAR_full.*b1weights_fullK, 'LineWidth', 3); 
plot(points_1s,1e12.*deepestSens_wcSAR_1s1m.*b1weights_1s1m,'-.o', 'LineWidth',1)
plot(points_1s(2:end),1e12.*deepestSens_wcSAR_2s1m(2:end).*b1weights_2s1m(2:end), '-.o', 'LineWidth',1)
h(1) = plot(points_1s(2:end),1e12.*deepestSens_wcSAR_2s2m(2:end).*b1weights_2s2m(2:end), '-.o', 'LineWidth',2,...
    'MarkerFaceColor', [0.4940 0.1840 0.5560]);
plot(points_1s(3:end),1e12.*deepestSens_wcSAR_3s1m(3:end).*b1weights_3s1m(3:end), '-.o', 'LineWidth',1)
h(2) = plot(points_1s(3:end),1e12.*deepestSens_wcSAR_3s2m(3:end).*b1weights_3s2m(3:end), '-.o', 'LineWidth',2,...
    'MarkerFaceColor',[0.3010 0.7450 0.9330]);

grid on

set(gca,'FontSize',14)
legend({'Coil Region','Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m'},'Location','northeast','AutoUpdate','off');
title('Worst Case SAR per \muT^2, Shimmed B_1^+')%'Limited Knowledge of J - Short Coil Case')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position')
axis([-100 points_1s(end) 0 max(max_val)])

uistack(h(1),'top')
uistack(h(2),'top')
set(gcf, 'Position',  [100, 100, 700, 450])

%% PLOTS 2

actualShim_SAR_fullSys(1:npoints) = abs(w_fullK(:,9)'*Q_tip*w_fullK(:,9));%./b1_normVal;
% actualShim_SAR_fullSys(1:npoints) = abs(w'*Q_tip*w);%./b1_normVal;
for ii = 1:npoints
    actualShim_SAR_1s1m(ii) = abs(w1s1m(:,ii)'*Q_tip*w1s1m(:,ii));%./b1weights_1s1m(ii);
    
    if ii >= 2
        actualShim_SAR_2s1m(ii) = abs(w2s1m(:,ii)'*Q_tip*w2s1m(:,ii));%./b1weights_2s1m(bestPos_idx_2s1m(ii));
        actualShim_SAR_2s2m(ii) = abs(w2s2m(:,ii)'*Q_tip*w2s2m(:,ii));%./b1weights_2s2m(bestPos_idx_2s2m(ii));
    end
    
    if ii >= 3
        actualShim_SAR_3s1m(ii) = abs(w3s1m(:,ii)'*Q_tip*w3s1m(:,ii));%./b1weights_3s1m(bestPos_idx_3s1m(ii));
        actualShim_SAR_3s2m(ii) = abs(w3s2m(:,ii)'*Q_tip*w3s2m(:,ii));%./b1weights_3s2m(bestPos_idx_3s2m(ii));
%         actualShim_SAR_3s3m(ii) = abs(w3s3m(:,ii)'*Q_tip*w3s3m(:,ii));%./b1weights_3s3m(bestPos_idx_3s3m(ii));
    end
end

figurent;

max_val = max(test3)+10; %actualShim_SAR_1s1m;%actualShim_SAR_fullSys;%actualShim_SAR_3s3m(3:end;
        % actualShim_SAR_2s2m(2:end);%

pth = [coil_reg 0; 800 0; 800 max(max_val); coil_reg max(max_val)];
faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'red', 'FaceAlpha',.18,'EdgeColor','none');

% ILLUSTRATIVE ADDON / DON'T KEEP
hold on
pth = [-100 min(test3); 800 min(test3); 800 max(test3); -100 max(test3)];
faces = [1 2 3 4];
patch('Faces', faces, 'Vertices',pth,'FaceColor', 'blue', 'FaceAlpha',.18,'EdgeColor','none');

plot(points_1s, actualShim_SAR_fullSys, 'LineWidth', 3); hold on
% plot(points_w, actualShim_SAR_quad, 'LineWidth', 3);
plot(points_1s,actualShim_SAR_1s1m,'-.o', 'LineWidth',2)
plot(points_1s(2:end),actualShim_SAR_2s1m(2:end), '-.o', 'LineWidth',2)
plot(points_1s(2:end),actualShim_SAR_2s2m(2:end), '-.o', 'LineWidth',2)
plot(points_1s(3:end),actualShim_SAR_3s1m(3:end), '-.o', 'LineWidth',2)
plot(points_1s(3:end),actualShim_SAR_3s2m(3:end), '-.o', 'LineWidth',2)
% plot(points_w(3:end),actualShim_SAR_3s3m(3:end), '-.o', 'LineWidth',2)
grid on

set(gca,'FontSize',14)

legend('Coil Region','Full System Spread','Full System', '1s1m', '2s1m', '2s2m','3s1m','3s2m','3s3m');
title('Expected SAR per \muT^2, Shimmed B_1^+')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/\muT^2')
xlabel('Deepest Sensor Position (mm)')

axis([-100 points_1s(end) 0 max(max_val)])

set(gcf, 'Position',  [100, 100, 700, 450])

%% B1 Checker
[~,B1_full(:,:),~] = applyShim(maskedB1_reshaped(:,:,1119), w_fullK(:,9), ncoils);
for ii = 1:length(points_1s)
    [~,B1_1s1m(:,:,ii),~] = applyShim(maskedB1_reshaped(:,:,1119,:), w1s1m(:,ii), ncoils);
    
    [~,B1_2s1m(:,:,ii),~] = applyShim(maskedB1_reshaped(:,:,1119,:), w2s1m(:,ii), ncoils);
    
    [~,B1_2s2m(:,:,ii),~] = applyShim(maskedB1_reshaped(:,:,1119,:), w2s2m(:,ii), ncoils);
    
    [~,B1_3s1m(:,:,ii),~] = applyShim(maskedB1_reshaped(:,:,1119,:), w3s1m(:,ii), ncoils);
    
    [~,B1_3s2m(:,:,ii),~] = applyShim(maskedB1_reshaped(:,:,1119,:), w3s2m(:,ii), ncoils);
end
B1_1s1m = reshape(B1_1s1m, [sz(1) sz(2) npoints]);
B1_2s1m = reshape(B1_2s1m, [sz(1) sz(2) npoints]);
B1_2s2m = reshape(B1_2s2m, [sz(1) sz(2) npoints]);
B1_3s1m = reshape(B1_3s1m, [sz(1) sz(2) npoints]);
B1_3s2m = reshape(B1_3s2m, [sz(1) sz(2) npoints]);

figurent()
%%
imagesc(abs(B1_full(:,:)));
caxis([0.5e-6 1.2e-6])
colorbar









