% tip_L is 594 for Phantom, 756 for Duke (will vary for other Guidewire
% positions
% Also, zero_idx should be 16 for Duke and 19 for Phantom (will also vary)
function[linterp_shift, Jinterp, pedge_ind, points_w, points_pos, points_3_pos] = interpJ(npoints, ncoils, interp_points, Axes, J, tip_L, zero_idx)
ltrue(1) = 0;

for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end

for ii = 1:ncoils
    Jinterp_ini(ii,:) = interp1(J(ii,:), 1:size(J,2)/interp_points:size(J,2));
end
linterp = interp1(ltrue,1:size(J,2)/interp_points:size(J,2));

points_w = linspace(-100,tip_L,npoints)';
pedge = ltrue(zero_idx); %

linterp_shift = linterp-pedge;
[~, pedge_ind] = min(abs(linterp-pedge));
st = linterp(2)-linterp(1);
kk = 1;
mm = 1;
for ii = 1:length(points_w)
    for jj = ii+1:length(points_w)
        points_pos(kk,1:2) = [points_w(ii) points_w(jj)];
        for ll = jj+1:length(points_w)
                points_3_pos(mm,1:3) = [points_w(ii) points_w(jj) points_w(ll)];
                mm = mm+1;
        end
        kk = kk + 1;
    end
end
Jinterp = permute(Jinterp_ini, [2 1]);