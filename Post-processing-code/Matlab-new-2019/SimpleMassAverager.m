function avgQ = avgQFromCubes(Q3D, cubes)
si = size(Q3D);
avgQ = zeros(si);
for ii = 1:si(1)
    for jj = 1:si(2)
        for kk = 1:si(3)
            for ll = 1:si(4)
                for mm = ll:si(5)
                x_min = cubes(ii,jj,kk,1);
                x_max = cubes(ii,jj,kk,2); 
                y_min = cubes(ii,jj,kk,3);
                y_max = cubes(ii,jj,kk,4);
                z_min = cubes(ii,jj,kk,5);
                z_max = cubes(ii,jj,kk,6);
                
                if Q3D(ii,jj,kk,ll,mm)>0
                    
                    cube_Q = Q3D(x_min:x_max,y_min:y_max,z_min:z_max);
                    cube_Q_list = cube_Q(:);
                    cube_Q_nz = length(cube_Q_list(cube_Q_list>0));
                    
                    avgQ(ii,jj,kk,ll,mm) = sum(cube_Q_list)/cube_Q_nz;
                    avgQ(ii,jj,kk,mm,ll) = sum(cube_Q_list)/cube_Q_nz;
                end
                end
            end
        end
    end
end
                
    end