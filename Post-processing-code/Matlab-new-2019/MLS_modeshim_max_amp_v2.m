
%%%         [ss,bs,err] = MLS_modeshim_max_amp(Tx_channels,modes_matrix,mtarget,smax,zinit,lambda,nits,tol)
% use fmincon
% modes_matrix (Nc x Nm) maps the Nc channels down to Nm modes
% optimization uses modes but controls amplitudes and power based on
% channels
% 2018-07-19
function [ss,bs,err] = MLS_modeshim_max_amp_v2(Tx_channels,modes_matrix,mtarget,smax,zinit,lambda,nits,tol, text)

% collapse Tx
if ndims(Tx_channels)>2
    sz=size(Tx_channels);
    Tx_channels = reshape(Tx_channels,[prod(sz(1:end-1)) sz(end)]);
    mtarget = reshape(double(mtarget),[prod(sz(1:end-1)) 1]); % should be this size
end
Nc = size(Tx_channels,2);
Nm = size(modes_matrix,2);

modes_matrix = double(modes_matrix);

Txm = Tx_channels * modes_matrix;

if isempty(zinit)
    % Set initial target phase to that of quadrature map
    %zinit = angle(sum(Tx_channels,2));
    zinit = pi*rand(size(Tx_channels,1),1);
end
z = exp(1i*zinit);
err = zeros(nits,1);
s0 = [ones(Nm,1);zeros(Nm,1)];
scmplx = @(s)(s(1:Nm).*exp(1i*s((Nm+1):end)));

scmplx_RI = @(s)(complex(real(s(1:Nm)),real(s((Nm+1):end))));
counter = 0;
ep=1;



options = optimoptions('fmincon','Display','off','Algorithm','active-set',...
    'FiniteDifferenceStepSize',1e-3);

%%% set bounds
lb = [zeros(Nm,1); -pi*ones(Nm,1)];
ub = [10*ones(Nm,1); pi*ones(Nm,1)];

%%% set our initial guess as 'quad' - i.e. equal over channels
sQuad = ones([Nc 1]);
sMode = modes_matrix' * sQuad;
s0RI= [real(sMode);imag(sMode)];
e = norm(abs(Txm*scmplx_RI(s0RI))-mtarget)./norm(mtarget);
iter1 = 1;
while abs(e)>tol %((ep-e) > tol) %%<-- could add anoher term to control change
    
    %%% initial condition - this is over modes
    sMode = modes_matrix' * z;%sQuad;
    s0 = [abs(sMode);angle(sMode)];
    s0RI= [real(sMode);imag(sMode)];
    %%% make cost function with current z
    cfun_cplx = @(s)(norm(Txm*s-mtarget.*z)+lambda*(s'*s));
    cfun_RI = @(s)double((cfun_cplx(scmplx_RI(s))));
    %cfun = @(s)double((cfun_cplx(scmplx(s))));
    
    %%% optimize using nonlinear constraint on amplitude
    exitflag = -1;
    while exitflag<0
        %[s,~,exitflag] = fmincon(cfun,s0,[],[],[],[],lb,ub,@ampcon,options);
        [s,~,exitflag] = fmincon(cfun_RI,s0RI,[],[],[],[],[],[],@ampcon,options);
        %[s,~,exitflag] = fminsearch(cfun,s0);
        % reduce initial condition for next run
        s0RI = s0RI*0.9;
    end
    %%% Update z
    bs = Txm*scmplx_RI(s);
    z = exp(1i*angle(bs));
    
%     ep = e;
    
    e = norm(abs(bs)-mtarget)./norm(mtarget);
    conv = e;
    counter = counter+1;
    
    
%     if iter1 == 1
%         fprintf('First Iteration || Error = %1.4f\t Diff = %1.6f\t iteration %d \n',e,conv,counter);
%         iter1 = 0;
%     end
    if counter>nits
        break
    end
end
% display final error
if strcmp(text, 'enabled')
    fprintf('Last Iteration || Error = %1.4f\t Diff = %1.6f\t iteration %d \n',e,conv,counter);
end
if counter == nits+1
    disp(['Maximum iteration ' num2str(counter) ' reached.'])
end
%%% return channel shims
ss = scmplx_RI(s);
ss = modes_matrix*ss(:);

    function [c,ceq] = ampcon(x)
        x = scmplx_RI(x);
        c = abs(modes_matrix*x(:))-smax(:);
        c = double(c);
        ceq=[];
    end

end

