%%
keep_box_x = 40:49;
% keep_box_y = 103:110;
keep_box_z = 334:341;

%%
Q_col = reshape(Q_reduced, [172*213*484 8 8]);
for ii = 1:length(Q_col)
    Q_eig(ii) = max(eig(squeeze(Q_col(ii,:,:))));
end

Q_col_replaced = Q_col;
Q_tip_patch = Q_reduced(keep_box_x,:,keep_box_z,:,:);

[~,idx] = find(Q_eig>1); %Short Coil
% [~,idx] = find(Q_eig>0.4); %Long Coil

replace_Q = squeeze(Q_col(idx(401)-1, :,:));

for ii = 1:length(idx)
    Q_col_replaced(idx(ii),:,:) = replace_Q;
end

Q_replaced = reshape(Q_col_replaced, [172 213 484 8 8]);
Q_replaced(keep_box_x,:,keep_box_z,:,:) = Q_tip_patch;

%%
Q_col_replaced = reshape(Q_replaced, [172*213*484 8 8]);

for ii = 1:length(Q_col_replaced)
    Q_eig_replaced(ii) = max(eig(squeeze(Q_col_replaced(ii,:,:))));
end

Q_eig_replaced = reshape(Q_eig_replaced, [172 213 484]);

Q_eig_replaced_mip = squeeze(max(Q_eig_replaced, [], 2));

figurent; imagesc(Q_eig_replaced_mip);

