%% Quantities for Full System
npoints = 40;
Q_tip = VOPm;
Q_tip_normed = Q_tip./b1_normVal_perfKnowledge(1);
maxEV_fullQ(1:npoints) = max(eig(Q_tip_normed));
maxEV_fullQ_noNorm(1:npoints) = max(eig(Q_tip));
% for ii = 1:nChannels
%     powJ_i(ii) = sum(abs(Jinterp(:,ii)*1000).^2)/length(Jinterp);
% end
% powJ = mean(powJ_i);
% powN = powJ*(10^-3);
%% Loop Measurements
repeat_noise = 100;
for jj = 1:repeat_noise
    if floor(jj/20) == jj/20
        disp(['Running Random Noise ' num2str(jj) ' of ' num2str(repeat_noise)])
    end
    Jinterp_calc = awgn(Jinterp*1000, 25, 'measured');
    %     Jinterp_calc_real = awgn(real(Jinterp*1000), 30, 'measured');
    %     Jinterp_calc_imag = awgn(imag(Jinterp*1000), 30, 'measured');

%         Jinterp_calc_abs = abs(Jinterp*1000) + (0.4*rand(length(Jinterp),8));
%         Jinterp_calc_phase = angle(Jinterp*1000) + (0.4*rand(length(Jinterp),8));

    %     Jinterp_calc_real = real(Jinterp*1000) + (sqrt(powN/2)*randn(length(Jinterp),8));
    %     Jinterp_calc_imag = imag(Jinterp*1000) + (sqrt(powN/2)*randn(length(Jinterp),8));

%         Jinterp_calc = Jinterp_calc_real + 1i*Jinterp_calc_imag;
%         Jinterp_calc = Jinterp_calc_abs.*exp(1i*Jinterp_calc_phase);

    %%
    figurent();
    subplot(121)
    plot(abs(Jinterp_calc))
    title('Absolute')
    subplot(122)
    plot(angle(Jinterp_calc))
    title('Phase')
    %%

    % [~,~,v_full]=svd(Jinterp_calc);
    % figurent;
    % plot(abs(Jinterp_calc*v_full));

    %% best positions for reduced system
    for kk = 1:length(points_w)

        [~,posidx_1(1)] = min(abs(linterp_shift-points_w(kk,1)));
        [~,~,v]=svd(Jinterp_calc(posidx_1,:));

        v_dmss_1s1m = v(:,2:nChannels);
        Q_dmss_1s1m = v_dmss_1s1m'*(Q_tip)*v_dmss_1s1m;
        maxEV_1s1m(kk) = max(eig(Q_dmss_1s1m));

    end

    for kk = 1:length(points_pos)

        [~,posidx_2(1)] = min(abs(linterp_shift-points_pos(kk,1)));
        [~,posidx_2(2)] = min(abs(linterp_shift-points_pos(kk,2)));
        [~,~,v]=svd(Jinterp_calc(posidx_2,:));

        v_dmss_2s1m = v(:,2:nChannels);
        Q_dmss_2s1m = v_dmss_2s1m'*(Q_tip)*v_dmss_2s1m;
        maxEV_2s1m(kk) = max(eig(Q_dmss_2s1m));

        v_dmss_2s2m = v(:,3:nChannels);
        Q_dmss_2s2m = v_dmss_2s2m'*(Q_tip)*v_dmss_2s2m;
        maxEV_2s2m(kk) = max(eig(Q_dmss_2s2m));

    end

    for kk = 1:length(points_3_pos)

        [~,posidx_3(1)] = min(abs(linterp_shift-points_3_pos(kk,1)));
        [~,posidx_3(2)] = min(abs(linterp_shift-points_3_pos(kk,2)));
        [~,posidx_3(3)] = min(abs(linterp_shift-points_3_pos(kk,3)));%Sets points in relation to phantom-air border
        [~,~,v]=svd(Jinterp_calc(posidx_3,:));

        v_dmss_3s1m = v(:,2:nChannels);
        Q_dmss_3s1m = v_dmss_3s1m'*(Q_tip)*v_dmss_3s1m;
        maxEV_3s1m(kk) = max(eig(Q_dmss_3s1m));

        v_dmss_3s2m = v(:,3:nChannels);
        Q_dmss_3s2m = v_dmss_3s2m'*(Q_tip)*v_dmss_3s2m;
        maxEV_3s2m(kk) = max(eig(Q_dmss_3s2m));

%         v_dmss_3s3m = v(:,4:nChannels);
%         Q_dmss_3s3m = v_dmss_3s3m'*(Q_tip)*v_dmss_3s3m;
%         maxEV_3s3m(kk) = max(eig(Q_dmss_3s3m));

    end

    %% Actual optimal points
    wcSAR_full(1:length(points_w)) = abs(wcSAR_perfKnowledge(1));
    deepestSens_wcSAR_1s1m(:,jj) = abs(maxEV_1s1m);

    for ii = 2:length(points_w)
        logic_idx{ii} = double(find(points_pos(:,2) == points_w(ii)));
        [all_wcSAR_2s1m{ii}] = abs(maxEV_2s1m(logic_idx{ii}));
        [all_wcSAR_2s2m{ii}] = abs(maxEV_2s2m(logic_idx{ii}));

        [min_wcSAR_2s1m(ii,jj), temp_idx_2s1m] = min(all_wcSAR_2s1m{ii});
        [min_wcSAR_2s2m(ii,jj), temp_idx_2s2m] = min(all_wcSAR_2s2m{ii});
        
%         if length(logic_idx{ii}) == 1 
%             bestPos_idx_2s1m(ii,jj) = logic_idx{ii};
%             bestPos_idx_2s2m(ii,jj) = logic_idx{ii};
%         else
%             bestPos_idx_2s1m(ii,jj) = logic_idx{ii}(temp_idx_2s1m);
%             bestPos_idx_2s2m(ii,jj) = logic_idx{ii}(temp_idx_2s2m);
%         end
%         
        if ii > 2
            logic_idx{ii} = double(find(points_3_pos(:,3) == points_w(ii)));
            [all_wcSAR_3s1m{ii}] = abs(maxEV_3s1m(logic_idx{ii}));
            [all_wcSAR_3s2m{ii}] = abs(maxEV_3s2m(logic_idx{ii}));
    %         [all_wcSAR_3s3m{ii}] = abs(maxEV_3s3m(logic_idx{ii}));

            min_wcSAR_3s1m(ii,jj) = min(all_wcSAR_3s1m{ii});
            min_wcSAR_3s2m(ii,jj) = min(all_wcSAR_3s2m{ii});
    %         min_wcSAR_3s3m(ii,jj) = min(all_wcSAR_3s3m{ii});
        end
    end
end

%% Noise Variance in Results
for ii = 1:40
    mean_wcSAR_1s1m(ii) = mean(deepestSens_wcSAR_1s1m(ii,:));
    mean_wcSAR_2s1m(ii) = mean(min_wcSAR_2s1m(ii,:));
    mean_wcSAR_2s2m(ii) = mean(min_wcSAR_2s2m(ii,:));
    mean_wcSAR_3s1m(ii) = mean(min_wcSAR_3s1m(ii,:));
    mean_wcSAR_3s2m(ii) = mean(min_wcSAR_3s2m(ii,:));
%     mean_wcSAR_3s3m(ii) = mean(min_wcSAR_3s3m(ii,:));
    
    
    errup_1s1m(ii) = abs(mean_wcSAR_1s1m(ii) - max(deepestSens_wcSAR_1s1m(ii,:)));
    errup_2s1m(ii) = abs(mean_wcSAR_2s1m(ii) - max(min_wcSAR_2s1m(ii,:)));
    errup_2s2m(ii) = abs(mean_wcSAR_2s2m(ii) - max(min_wcSAR_2s2m(ii,:)));
    errup_3s1m(ii) = abs(mean_wcSAR_3s1m(ii) - max(min_wcSAR_3s1m(ii,:)));
    errup_3s2m(ii) = abs(mean_wcSAR_3s2m(ii) - max(min_wcSAR_3s2m(ii,:)));
%     errup_3s3m(ii) = abs(mean_wcSAR_3s3m(ii) - max(min_wcSAR_3s3m(ii)));
    
    errdn_1s1m(ii) = abs(mean_wcSAR_1s1m(ii) - min(deepestSens_wcSAR_1s1m(ii,:)));
    errdn_2s1m(ii) = abs(mean_wcSAR_2s1m(ii) - min(min_wcSAR_2s1m(ii,:)));
    errdn_2s2m(ii) = abs(mean_wcSAR_2s2m(ii) - min(min_wcSAR_2s2m(ii,:)));
    errdn_3s1m(ii) = abs(mean_wcSAR_3s1m(ii) - min(min_wcSAR_3s1m(ii,:)));
    errdn_3s2m(ii) = abs(mean_wcSAR_3s2m(ii) - min(min_wcSAR_3s2m(ii,:)));
%     errdn_3s3m(ii) = abs(mean_wcSAR_3s3m(ii) - min(min_wcSAR_3s3m(ii)));
end
%%
figurent();
errorbar(points_w, mean_wcSAR_1s1m, errdn_1s1m, errup_1s1m,...
        '-.o','Color', 'b','LineWidth',2); hold on
errorbar(points_w(2:end), mean_wcSAR_2s1m(2:end), errdn_2s1m(2:end), errup_2s1m(2:end),...
        '-.o','Color', 'r','LineWidth',2);
errorbar(points_w(2:end), mean_wcSAR_2s2m(2:end), errdn_2s2m(2:end), errup_2s2m(2:end),...
        '-.o','Color', 'k','LineWidth',2);
errorbar(points_w(3:end), mean_wcSAR_3s1m(3:end), errdn_3s1m(3:end), errup_3s1m(3:end),...
        '-.o','Color', 'm','LineWidth',2);
errorbar(points_w(3:end), mean_wcSAR_3s2m(3:end), errdn_3s2m(3:end), errup_3s2m(3:end),...
        '-.o','Color', 'g','LineWidth',2);
    
legend('1s1m','2s1m','2s2m','3s1m','3s2m')
    
grid on;
    
set(gca,'FontSize',14)
title('Worst Case SAR - w/ 100 Random WGN @45dB SNR')%'wcSAR - Straight Wire - Duke Long Coil')
ylabel('W/Kg/W')
xlabel('Deepest Sensor Position')
set(gcf, 'Position',  [100, 100, 700, 450])
axis([-100 points_w(end) 0 80])
%%
figurent()
plot(points_w, mean_wcSAR_1s1m, '-.o','Color', 'b','LineWidth', 1); hold on
plot(points_w(2:end), mean_wcSAR_2s1m(2:end), '-.o','Color', 'r','LineWidth', 1); hold on
plot(points_w(2:end), mean_wcSAR_2s2m(2:end), '-.o','Color', 'k','LineWidth', 1); hold on
plot(points_w(3:end), mean_wcSAR_3s1m(3:end), '-.o','Color', 'm','LineWidth', 1); hold on
plot(points_w(3:end), mean_wcSAR_3s2m(3:end), '-.o','Color', 'g','LineWidth', 1); hold on

% plot(deepestSens_wcSAR_2s1m
%     errup_3s1m(ii) = abs(mean(all_wcSAR_3s1m{ii}) - max(all_wcSAR_3s1m{ii}));
%     errup_3s2m(ii) = abs(mean(all_wcSAR_3s2m{ii}) - max(all_wcSAR_3s2m{ii}));
%     errup_3s3m(ii) = abs(mean(all_wcSAR_3s3m{ii}) - max(all_wcSAR_3s3m{ii}));
    
%     errdn_3s1m(ii) = abs(mean(all_wcSAR_3s1m{ii}) - min(all_wcSAR_3s1m{ii}));
%     errdn_3s2m(ii) = abs(mean(all_wcSAR_3s2m{ii}) - min(all_wcSAR_3s2m{ii}));
%     errdn_3s3m(ii) = abs(mean(all_wcSAR_3s3m{ii}) - min(all_wcSAR_3s3m{ii}));