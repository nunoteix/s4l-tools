%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SCRIPT TO CONVERT .TXT S-PARAMETER OUTPUT FROM SIM4LIFE INTO
%.MAT COMPATIBLE WITH CIRC CO-SIM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% CHANGE INPUTS HERE
fname = 'G:\FakePort Simulations\Duke\Extracted Results\S-Params\Resonance Finder FP Channel 2 Duke_2020_08_10_SParams'; % name of the .txt file
npl = 2; % Number of total ports (ports+LE's)
nfreqs = 5001; % Number of frequencies recorded (Open .txt file and check "#Records: XXXX" )

%% RUN
fID = fopen([fname '.txt']); % opening .txt file

S = textscan(fID, '%f', 'Delimiter', '\n', 'CommentStyle', '#'); % get S data
fclose(fID);

S_mat = cell2mat(S); %convert to .mat

S_origin = reshape(S_mat, npl*npl*2+1, nfreqs)'; % organize by frequency

S_step = zeros(npl*npl,1); % S for 1 freq, used in building final matrix

freqs = squeeze(S_origin(:,1))'; % extracting frequencies
clear S
S = zeros(npl,npl,length(S_origin(:,1))); % will be the final matrix

for jj = 1:length(S_origin(:,1)) % sweeping frequencies
    for ii = 2:2:length(S_origin(1,:)) % building the complex numbers
        S_step(ii/2) = S_origin(jj,ii)+S_origin(jj,ii+1)*1i;
    end
    S(:,:,jj) = reshape(S_step, npl, npl); % building final matrix (nports, nports, frequencies)
end

clearvars -except freqs S
