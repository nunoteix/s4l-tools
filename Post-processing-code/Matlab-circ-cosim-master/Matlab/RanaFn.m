% SOMA's test cost fucntion
% range -500 -> +500

function val = RanaFn(parameters, additionalParameters)

val = 0;
for i = 1:length(parameters)-1
    val = val + (parameters(i+1)+1)*cos(sqrt(abs(parameters(i+1)-parameters(i)+1)))*sin(sqrt(abs(parameters(i+1)+parameters(i)+1)))+parameters(i)*cos(sqrt(abs(parameters(i+1)+parameters(i)+1)))*sin(sqrt(abs(parameters(i+1)-parameters(i)+1)));
end
