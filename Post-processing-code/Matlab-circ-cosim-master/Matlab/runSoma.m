% runSoma.m
% @return retVal.bestPostition - the best set of power assignments found.
% @return retVal.costValue - value of the cost function.
% @return retVal.history - history of cost value during SOMA's run.
function [retVal] = runSoma(additionalParameters);

global dim bounds;
% clc;

% SOMA init & run :

% optimizedFunctionName = 'SchwefelFn';  % bounds 0 -> +500
optimizedFunctionName = 'AckleyFn';     %bounds -20 -> +20
% optimizedFunctionName = 'RanaFn';     %bounds -500 -> +500
additionalParameters = 0;   % nothing to pass

dim = 100;

bounds = zeros(dim,2);
for i=1:length(bounds)
    bounds(i,1) = -20; %MIN
    bounds(i,2) = 20; %MAX
end

step = 0.21;
pathLength = 2.1;
prt = 0.1;
minDiv = 0;
migrations = 500;
popSize = dim*2; %15;%dim*2;

retVal = soma_all_to_one(optimizedFunctionName, bounds, step, pathLength, prt, minDiv, migrations, popSize, additionalParameters);
% retVal = soma_all_to_one_randomly(optimizedFunctionName, bounds, step, pathLength, prt, minDiv, migrations, popSize, additionalParameters);
% retVal = soma_all_to_all(optimizedFunctionName, bounds, step, pathLength, prt, minDiv, migrations, popSize, additionalParameters);
% retVal = soma_all_to_all_adaptive(optimizedFunctionName, bounds, step, pathLength, prt, minDiv, migrations, popSize, additionalParameters);

hold on;
plot(1:length(retVal.history), retVal.history,'g:');

