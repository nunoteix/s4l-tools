% SOMA - Self-Organizing Migration Algorithm
% visit http://www.ft.utb.cz/people/zelinka/soma/
% written by ibisek, MMV
% 
% STRATEGY "ALL TO ALL ADAPTIVE"
% (individuals migrate to all other individuals. Start position is the best
% one found so far.
%
% The 'additionalParameters' parameter is non-compulsory parameter; this way
% you can pass to the optimized function some more information you may need.

function [retVal]=soma_all_to_all_adaptive(optimizedFunctionName,bounds,step,pathLength,prt,minDiv,migrations,popSize, additionalParameters)

dim = length(bounds);

% create "instance" of optimized function
costFunctionHandle = str2func(optimizedFunctionName);

% generating of population within borders
individuals = zeros(popSize,dim);
costValues = zeros(popSize,1);
for i=1:popSize
    for j=1:dim
        individuals(i,j)=bounds(j,1)+(bounds(j,2)-bounds(j,1))*rand; 
    end
    costValues(i) = feval(costFunctionHandle,individuals(i,:), additionalParameters);   % find the cost value of each individual
end

indexOfLeader = 1;
% find the leader ( individual with the lowest cost value )
min = 9999999;
for i = 1:popSize
    if costValues(i)<min
        min = costValues(i);
        indexOfLeader = i;
    end
end

globalErrorHistory = zeros(migrations,1);
mig = 0;
go = true;

while(mig<migrations&&go==true) 
    
    % migrate all individuals to all others:
    for i = 1:popSize

        stopMigrations = 0;
        for indexOfTarget = 1:popSize-1
            if  indexOfTarget == i 
                if indexOfTarget ~= popSize
                    indexOfTarget = indexOfTarget + 1;  % don't migrate to itself
                else
                    stopMigrations = 1; % targetIndividual is the last one and shall migrate to itself
                    break;
                end
            end
            
            if stopMigrations
                disp(sprintf('i=%i; indexOfTarget=%i',i,indexOfTarget));
                break
            end
            
            % store the individual's start position:
            startPositionOfIndividual = individuals(i,:)';
        
            targetPosition = individuals(indexOfTarget,:)';
            tmpIndividual = individuals(i,:)';
        
            % Let's migrate!
            for t=0:step:pathLength
            
                % Generate new PRTVector for each step of this individual:
                prtVectorContainOnlyZeros = true;
                while(prtVectorContainOnlyZeros)
                    for j=1:dim
                        if rand<prt
                            PRTVector(j) = 1;
                            prtVectorContainOnlyZeros = false;
                        else
                            PRTVector(j) = 0;
                        end
                    end
                end
            
                %new position for all dimensions:
                for j=1:dim
                    tmpIndividual(j) = startPositionOfIndividual(j) + ( targetPosition(j) - startPositionOfIndividual(j) ) * t * PRTVector(j);
                end
                %tmpIndividual = startPositionOfIndividual + ( individuals(targetPosition,:)' - startPositionOfIndividual ) * t * PRTVector;
            
                %check boundaries
                for j=1:dim
                    if tmpIndividual(j)<bounds(j,1)||tmpIndividual(j)>bounds(j,2)
                        tmpIndividual(j) = bounds(j,1)+(bounds(j,2)-bounds(j,1))*rand; 
                    end
                end
            
                tmpCostValue = feval(costFunctionHandle, tmpIndividual, additionalParameters);

                if tmpCostValue<costValues(i)
                    costValues(i) = tmpCostValue;   % store better CV and position
                    individuals(i,:) = tmpIndividual';
                end
                
            end % for t
           
        end % for indexOfTargets
        
    end % for i

    % find the leader ( individual with the lowest cost value ); just for
    % logging since migrating to randomly chosen individual
    min = 9999999;
    for i = 1:popSize
        if costValues(i)<min
            min = costValues(i);
            indexOfLeader = i;
        end
    end
    
    globalErrorHistory(mig+1) = costValues(indexOfLeader);
    
    % check end conditions
    migCheck = 20;
    if mig>(migCheck+1)
        decrease = 0;
        for i=1:migCheck
            decrease = decrease + abs(globalErrorHistory(mig-i-1)-globalErrorHistory(mig-i));
        end
        if decrease < minDiv
            go = false;
        end
    end
    
    mig=mig+1;
    if round(mig/10) == mig/10
        disp(sprintf('mig = %i of %i',mig, migrations));
    end
%     disp(sprintf('mig = %i of %i',mig, migrations));
end

globalErrorHistory4Saving = zeros(mig,1);
for i=1:mig
    globalErrorHistory4Saving(i) = globalErrorHistory(i);
end

try
%     save c:/wqz/prog/matlab/soma_history.txt globalErrorHistory4Saving -ASCII -TABS
    save ~/wqz/prog/matlab/soma/out/soma_history.txt globalErrorHistory4Saving -ASCII -TABS

    temp = costValues(indexOfLeader); %/dim;
%     save c:/wqz/prog/matlab/soma_leader_cv.txt temp -ASCII -TABS
    save ~/wqz/prog/matlab/soma/out/soma_leader_cv.txt temp -ASCII -TABS

    temp = individuals(indexOfLeader,:);
%     save c:/wqz/prog/matlab/soma_leader.txt temp -ASCII -TABS
    save ~/wqz/prog/matlab/soma/out/soma_leader.txt temp -ASCII -TABS
catch
    disp('Warning! Saving paths in soma.m are set incorrectly.');
end

% return values:
retVal.bestPosition = individuals(indexOfLeader,:);
retVal.costValue = costValues(indexOfLeader); %/dim;
retVal.history = globalErrorHistory4Saving;

