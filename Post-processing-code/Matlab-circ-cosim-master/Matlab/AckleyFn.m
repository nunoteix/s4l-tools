% SOMA's test cost fucntion
% range -20 -> +20

function val = AckleyFn(parameters, additionalParameters)

val = 0;
for i = 1:length(parameters)-1
    val = val + 3*(cos(2*parameters(i))+sin(2*parameters(i+1)))+sqrt(parameters(i+1)^2+parameters(i)^2)/exp(0.2);
end
