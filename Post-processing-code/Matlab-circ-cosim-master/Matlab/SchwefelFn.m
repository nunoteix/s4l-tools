% SOMA's test cost fucntion
% Range 0 -> +500

function val = SchwefelFn(parameters, additionalParameters)

val = 0;
for i = 1:length(parameters)
    val = val + ( - parameters(i)*sin(sqrt(abs(parameters(i)))) );
end
