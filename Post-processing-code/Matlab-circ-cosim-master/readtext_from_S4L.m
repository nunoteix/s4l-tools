nports = 5;

fname = 'S_FinalRapid_PbyP_Phantom_1_5T_Ch8';

fID = fopen([fname '.txt']);

S = textscan(fID, '%f', 'Delimiter', '\n', 'CommentStyle', '#');
fclose(fID);

S_mat = cell2mat(S);
%%
S = reshape(S_mat, nports*nports*2+1, 5001)';
save(['G:\S-Params\MATLAB\' fname '.mat'], 'S');