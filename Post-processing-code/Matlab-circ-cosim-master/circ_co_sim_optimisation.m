% Circuit Co-Simulation Optimisation function - Top-level script to attempt
% to find optimal capacitor values for a given EM simulation using circuit
% simulation at a given frequency
%
%     circ_co_sim_optimisation(S,freqs,nPorts,nLumped,capValsInit,
%                                portCapInds,lambda,varargin)
%
%     S           = matrix variable of all S-parameter matrices for all
%                   ports with size (nPorts +nLumped) x (nPorts +nLumped) x 
%                   (number of frequency points)
%
%     freqs       = vector of frequencies in Hz
%     nPorts      = number of driving ports
%     nLumped     = number of lumped element ports
%     capValsInit = vector of initial set of capacitor values. This is not
%                   likely to be of length nLumped due to symmetries within
%                   simulation models - the number of different capacitor
%                   values will be smaller
%
%     portCapInds = cell of size 1 x (length(capValsInit)). Each cell entry
%                   contains indices of lumped element ports corresponding 
%                   to the different capacitor values.
%                   N.B. these indices are numbered from 1 to nLumped and 
%                   should be conisdered as such when the indices are 
%                   defined but the actual EM simulation S-paramter ports 
%                   they correspond to will have index = index + nPorts
% 
%     lambda      = tuning paramter for optimisation. As lambda is
%                   increased, optimisation favours minimisation of 
%                   off-diagonal elements of S-matrix (i.e. minimise 
%                   inter-element coupling)
%
%     Default characteristic admittance is set as z0 = 50 Ohm. Add argument
%     'z0' to modify this
%     Default selected operational frequency is set as 128MHz. Add argument
%     'selfreq' to modify this
%     Default resistance of lumped element ports is set to R = 0.3 Ohms. 
%     Add argument 'R' to modify this
%     Default inductance of lumped element ports is set to L = 0 H. 
%     Add argument 'L' to modify this
%
% Created by Arian Beqiri, King's College London, October 2014.
% Email: arian.beqiri@kcl.ac.uk

% This code is free under the terms of the MIT license. Please cite Beqiri et al. 2014, 
% DOI: 10.1002/mrm.25504 in any published work

function [lumpedVals,S_out] = circ_co_sim_optimisation(S,freqs,nPorts,nLumped,capValsInit,portCapInds,lambda,varargin)

% Default characteristic impedance
z0 = 50;

% Default calculation frequency
selFreq = 64e6;

% Default serial resistance and inductance of lumped element ports
R = 1500;
L = 0;

% Check args
for ii=1:length(varargin)
    
    % Define characteristic impedance of all ports
    if strcmpi(varargin{ii},'z0')
        z0 = varargin{ii+1};
    end
    
    % Define selected frequency to run calculation at
    if strcmpi(varargin{ii},'selFreq')
        selFreq = varargin{ii+1};
    end
    
    % Define resistance of all lumped elements
    if strcmpi(varargin{ii},'R')
        R = varargin{ii+1};
    end
    
    % Define inductance of all lumped elements
    if strcmpi(varargin{ii},'L')
        L = varargin{ii+1};
    end
end

% Separate S-Matrix into separate sub-matrices
sm.ports = S(1:nPorts,1:nPorts,:);
sm.large = S((nPorts+1):(nPorts+nLumped),(nPorts+1):(nPorts+nLumped),:);
sm.left = S((nPorts+1):(nPorts+nLumped),1:nPorts,:);
sm.right = S(1:nPorts,(nPorts+1):(nPorts+nLumped),:);

% Characteristic Admittances
y0mat = inv(diag(sqrt(z0)*ones(size(sm.large,1),1)));

%% ============= Find frequency index and squeeze matrices ================

% Find index of correct frequency
% [~,ind] = min(abs(freqs-selFreq));
ind = find(freqs == 64e6);
% Creat sub-matrices at resonant frequency
s_ind.ports = sm.ports(:,:,ind);
s_ind.large = sm.large(:,:,ind);
s_ind.left = sm.left(:,:,ind);
s_ind.right = sm.right(:,:,ind);

%% ======= Run optimisation for capacitor values ===============

% Cost function
CostFnHandle = @(Cvals) optimfun_circ(Cvals,...
    portCapInds,s_ind,y0mat,R,L,lambda,nPorts,nLumped,selFreq);

%%% fminsearch (not bounded)
options = optimset('MaxFunEvals',1e10,'TolFun',1e-12,...
        'MaxIter',200000,'TolX',1e-12);%,'Display','iter');

%     capValsOptKeep = squeeze(zeros(1,48));
% for ii = 1:50
%     [capValsOpt,f] = fminsearch(CostFnHandle,capValsInit,options);
%     if ii == 1
%         fkeep = f;
%     end
%     if f < fkeep
%         fkeep = f;
%         fkeep
%         capValsOptKeep = capValsOpt;
%     end
%     capValsInit = rand(1,nLumped).*30e-11;
% end
% capValsOpt = capValsOptKeep;
% 
% SOMA Pars
step = 0.11;
pathLength = 7;
prt = 0.1;
minDiv = 0;
migrations = 30;
popSize = 200;
% % 
% % % bounds = repmat(capValsInit',[1 2]);
% % % bounds(1:8,1) = bounds(1:8,1)*0.6; bounds(9:40:9,1) = 10e-12; bounds(41:48,1) = 80e-12; bounds(49:112,1) = 20e-12;
% % % bounds(1:8,2) = bounds(1:8,2)*1.3; bounds(9:40,2) = 50e-12; bounds(41:48,2) = 200e-12; bounds(49:112,2) = 40e-12;
% % % bounds(113:120,1) = 5e-12;
% % % bounds(113:120,2) = 25e-12;
% % 
% bounds = repmat(capValsInit',[1 2]);
% bounds(1,1) = bounds(1,1)*0.6; bounds(2:17,1) = 10e-12; bounds(18:25,1) = 80e-12;
% bounds(1,2) = bounds(1,2)*1.3; bounds(2:17,2) = 50e-12; bounds(18:25,2) = 200e-12; 
% bounds(27:34,1) = 5e-12;  bounds(26,1) = 20e-12;
% bounds(27:34,2) = 25e-12;  bounds(26,2) = 40e-12;
% % 
% % % bounds = repmat(capValsInit',[1 2]);
% % % bounds(1,1) = bounds(1,1)*0.6; bounds(2:9,1) = 10e-12; bounds(10:17,1) = 80e-12;
% % % bounds(1,2) = bounds(1,2)*1.3; bounds(2:9,2) = 50e-12; bounds(10:17,2) = 200e-12; 
% % % bounds(18,1) = 20e-12; bounds(19:26,1) = 1e-12; 
% % % bounds(18,2) = 90e-12; bounds(19:26,2) = 25e-12; 
% % 
% % bounds = repmat(capValsInit',[1 2]);
% % bounds(1,1) = bounds(1,1)*0.6; bounds(2,1) = 1e-13; bounds(3,1) = 80e-12;
% % bounds(1,2) = bounds(1,2)*1.3; bounds(2,2) = 50e-12; bounds(3,2) = 200e-12; 
% % bounds(4,1) = 20e-12; bounds(5,1) = 1e-12; 
% % bounds(4,2) = 90e-12; bounds(5,2) = 25e-12; 

% FOR HYDRA SINGLE CHANNEL 

% RAPID COIL
% bounds = repmat(capValsInit',[2 1]);
% bounds(1,1) = 10e-12;
% bounds(1,2) = 60e-12;

% bounds(2,1) = 1e-12; 
% bounds(2,2) = 250e-12;
% 
% bounds(3,1) = 1e-12; 
% bounds(3,2) = 20e-12;
% 
% bounds(4,1) = 1e-12;
% bounds(4,2) = 250e-12;

% FOR RAPID COIL
bounds = repmat(capValsInit',[2 1]);
bounds(1,1) = 1e-12; bounds(2,1) = 100e-12; bounds(3,1) = 1e-12; bounds(4,1) = 100e-12;
bounds(1,2) = 30e-12; bounds(2,2) = 200e-12; bounds(3,2) = 10e-12; bounds(4,2) = 200e-12;
% 


s_cap_opt = soma_all_to_one_sjm(CostFnHandle,bounds,step,pathLength,prt, minDiv, migrations, popSize,capValsInit); 
capValsOpt = s_cap_opt.bestPosition.';


%% ========== Generate post-optimisation lumped element array =============

% Preallocate and assign first column to resistance
lumpedVals = ones(nLumped,3)*R;

% Assign second column to capacitor values from optimisation
if length(portCapInds) > 1
    for ii=1:length(capValsOpt)
        lumpedVals(portCapInds{ii},2) = capValsOpt(ii);
    end
else
    lumpedVals(portCapInds{1},2) = capValsOpt(1);
end
% Assign third column to inductances of elements
lumpedVals(:,3) = L;

%% ============== Assess Final S-Parameter response =======================

% Output S-parameter curves over frequency range
S_out = gen_sparam_response(S,freqs,nPorts,nLumped,lumpedVals,varargin);

end