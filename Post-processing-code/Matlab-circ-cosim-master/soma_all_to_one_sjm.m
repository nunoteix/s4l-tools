% SOMA - Self-Organizing Migration Algorithm
% visit http://www.ft.utb.cz/people/zelinka/soma/
% written by ibisek, MMV
% 
% STRATEGY "ALL-TO-ONE (LEADER)", the basic version
%
% The 'additionalParameters' parameter is non-compulsory parameter; this way
% you can pass to the optimized function some more information you may need.
% sjm - hack to allow function handle

function [retVal]=soma_all_to_one_sjm(optimizedFunctionName,bounds,step,pathLength,prt,minDiv,migrations,popSize,initial_point, additionalParameters)
dim = length(bounds);

% SJM
if ischar(optimizedFunctionName)
    % create "instance" of optimized function
    costFunctionHandle = str2func(optimizedFunctionName);
else
    costFunctionHandle = optimizedFunctionName;
end
% SJM nreport is the interval to report the migration number
nreport=1;%10;

% SJM randfact is the factor to weight the random part of the initialization
randfact=0.1;

% generating of population within borders
individuals = zeros(popSize,dim);
costValues = zeros(popSize,1);

for i=1:popSize
    for j=1:dim
        individuals(i,j)=bounds(j,1)+(bounds(j,2)-bounds(j,1))*rand; 
    end
%     % SJM add random to predefined point
%     individuals(i,:) = initial_point.' + randfact*individuals(i,:);
% 
%     % SJM above can violate bounds, so now reinforce them
%     for jj=1:dim
%        %%% SJM 31-5-13: Replace if statement below with subsequent. Push to bound if outside
%        %if individuals(i,jj) > bounds(jj,2)
%        %    individuals(i,jj) = bounds(jj,1) + individuals(i,jj) - bounds(jj,2);
%        %end
%        if (individuals(i,jj) > bounds(jj,2))
%            individuals(i,jj) = bounds(jj,2);
%        else
%             if (individuals(i,jj) < bounds(jj,1))
%                 individuals(i,jj) = bounds(jj,1);
%             end
%        end
%     end
    
    %costValues(i) = feval(costFunctionHandle,individuals(i,:), additionalParameters);   % find the cost value of each individual
    costValues(i) = costFunctionHandle(individuals(i,:));   % find the cost value of each individual
end

% find the leader ( individual with the lowest cost value )
[~, indexOfLeader]=min(costValues);

globalErrorHistory = zeros(migrations,1);
mig = 0;
go = true;

tic;
leaders=zeros(migrations,1);
while(mig<migrations && go==true) 

    % migrate individuals to the leader ( except leader to himself, of course :)
    min_cost=Inf;
    new_leader=-1;
    leaderPosition = individuals(indexOfLeader,:)';
           
    for (i = 1:popSize)
    %for i=1:popSize
        skip_migration=false;
        
        % Check if this individual is not leader. If true, skip it.
        if (i==indexOfLeader)
            skip_migration=true;             
        end

        if (~skip_migration)
            % store the individual's start position
            tmpIndividual = individuals(i,:)';
            startPositionOfIndividual = tmpIndividual;
            
            % Let's migrate!
            for t=0:step:pathLength

		% Generate new PRTVector for each step of this individual:
                PRTVector=zeros(dim,1);
                prtVectorContainOnlyZeros = true;
                while(prtVectorContainOnlyZeros)
                    for j=1:dim
                        if rand<prt
                            PRTVector(j) = 1;
                            prtVectorContainOnlyZeros = false;
                        else
                            PRTVector(j) = 0;
                        end
                    end
                end

                %new position for all dimensions:
		tmpIndividual = startPositionOfIndividual + ( leaderPosition - startPositionOfIndividual) .* (t * PRTVector);

                %check boundaries
		% This alters the original soma code, which reset the 
		% value to something uniformly distributed over the whole
		% range.  Now we replace with a uniform distribution
		% between the current value and the broken bound.
                for j=1:dim
                    if tmpIndividual(j)>bounds(j,2)
                        tmpIndividual(j) = startPositionOfIndividual(j)+(bounds(j,2)-startPositionOfIndividual(j))*rand; 
                    end
                    if tmpIndividual(j)<bounds(j,1)
                        tmpIndividual(j) = bounds(j,1)+(startPositionOfIndividual(j)-bounds(j,1))*rand; 
                    end
                end

                %tmpCostValue = feval(costFunctionHandle, tmpIndividual, additionalParameters);
                tmpCostValue = costFunctionHandle(tmpIndividual);
                if tmpCostValue<costValues(i)
                    costValues(i) = tmpCostValue;   % store better CV and position
                    individuals(i,:) = tmpIndividual';
                end

            end % for t
        end % if skip_migration
    end % for i

    %calculate new minimum cost and set new leader
    [min_cost, indexOfLeader]=min(costValues);    
 
    globalErrorHistory(mig+1) = costValues(indexOfLeader);
    
    % check end conditions
    migCheck = 20;
    if mig>(migCheck+1)
        decrease = 0;
        for i=1:migCheck
            decrease = decrease + abs(globalErrorHistory(mig-i-1)-globalErrorHistory(mig-i));
        end
        if decrease < minDiv
            go = false;
        end
    end
    leaders(mig+1)=indexOfLeader;
    mig=mig+1;
    if round(mig/nreport) == mig/nreport
        tt = toc;
        t_sec = mod(tt,60);
        t_min = (tt-t_sec)/60;
        tt2=tt/mig;
        tsecav = mod(tt2,60);
        tminav=(tt2-tsecav)/60;
        disp(sprintf('mig = %i of %i (leader: %i - %1.3e)  Elapsed = %dm%1.2fs  (%dm%1.2fs per mig.)',mig, migrations, indexOfLeader, costValues(indexOfLeader),t_min,t_sec,tminav,tsecav));
    end
    	
end

globalErrorHistory4Saving = zeros(mig,1);
for i=1:mig
    globalErrorHistory4Saving(i) = globalErrorHistory(i);
end

%try
%
%	save soma_history.txt globalErrorHistory4Saving -ASCII -TABS
%	save soma_leader_ind.txt leaders -ASCII -TABS
%
%catch
%    disp('Warning! Saving paths in soma.m are set incorrectly.');
%end

% return values:
retVal.bestPosition = individuals(indexOfLeader,:);
retVal.costValue = costValues(indexOfLeader); %/dim;
retVal.history = globalErrorHistory4Saving;



