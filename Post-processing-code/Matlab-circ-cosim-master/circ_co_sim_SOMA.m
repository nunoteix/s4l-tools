% %%% Circuit Co-Simulation Code%%%%
% Separate S-Matrix into separate sub-matrices

% clear all; close all
clear all
load('C:\Users\nt16\Documents\Hydra Simulations\S_8Elements.mat') % Change File to Load
S_origin = S;
clear S
npl=sqrt((length(S_origin(1,:))-1)/2);
S_step = zeros(npl*npl,1);
S = zeros(npl,npl,length(S_origin(:,1)));
for jj = 1:length(S_origin(:,1))
    for ii = 2:2:length(S_origin(1,:))
        S_step(ii/2) = S_origin(jj,ii)+S_origin(jj,ii+1)*1i;
    end
    S(:,:,jj) = reshape(S_step, npl, npl);
end
clearvars -except S_origin S

% Put frequencies in Hz from MHz
freqs = squeeze(S_origin(:,1))';    %Get freqs from S4L

% Creat sub-matrices
s_ports = S(1:8,1:8,:);
s_large = S(9:56,9:56,:);
s_left = S(9:56,1:8,:);
s_right = S(1:8,9:56,:);

%% Define Capacitor Values and create S_cap matrix

%%%%%%%%%%%%%%%%%%%% Initial Capacitor Values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

c11 = 30;   c21 = 30;   cm1 = 10;         ring_cap = 14;
c12 = 30;   c22 = 30;   cm2 = 10;       decoup_cap = 112;
c13 = 30;   c23 = 30;   cm3 = 10;
c14 = 30;   c24 = 30;   cm4 = 10;       RC = 33;
c15 = 30;   c25 = 30;   cm5 = 10;
c16 = 30;   c26 = 30;   cm6 = 10;
c17 = 30;   c27 = 30;   cm7 = 10;
c18 = 30;   c28 = 30;   cm8 = 10;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%% Alternate Capacitor Values %%%%%%%%%%%%%%%%%%%%%%%%%%%

% c11 = 30;   c21 = 26;   cm1 = 7;         ring_cap = 14;
% c12 = 62;   c22 = 40;   cm2 = 100;       decoup_cap = 112;
% c13 = 30;   c23 = 26;   cm3 = 30;
% c14 = 20;   c24 = 20;   cm4 = 7;        RC = 33;
% c15 = 20;   c25 = 20;   cm5 = 7;
% c16 = 30;   c26 = 26;   cm6 = 30;
% c17 = 62;   c27 = 40;   cm7 = 100;
% c18 = 30;   c28 = 26;   cm8 = 7;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

s_cap_vec = zeros(48,1);



s_cap_vec(1:16) = [c21 c22 c23 c24 c25 c26 c27 c28 c21 c22 c23 c24 c25...
                    c26 c27 c28];
s_cap_vec(17:32) = [c11 c12 c13 c14 c15 c16 c17 c18 c11 c12 c13 c14 c15...
                    c16 c17 c18];
s_cap_vec(33:40) = decoup_cap;
s_cap_vec(41:48) = ring_cap;

% s_cap_vec(57:end) = RC*ones(64,1);
% s_cap_vec(17:24) = [cm1 cm2 cm3 cm4 cm5 cm6 cm7 cm8];

s_cap_vec = s_cap_vec*1e-12;
                
% Characteristic Impedances
z0mat = diag(sqrt(50)*ones(size(s_large,1),1));

% Characteristic Admittances
ymat = inv(z0mat);
              
%% Optimisation

% Added resistance to make coil more lossy
r = 0.3;

% Tuning parameter
lambda = 20;

% Indices for matrix reduction 
% ind_red = [1 9:16 17:24 25:2:55];
ind_red = [41 33:40 1:8 17:24];

% Reduce matrix sizes to remove duplicate cap values
s_cap_red  = s_cap_vec(ind_red);

ind = find(freqs == 128e6);

% Squeeze sub-matrices to value at frequency
spopt = squeeze(s_ports(:,:,ind)); sropt = squeeze(s_right(:,:,ind));
slaropt = squeeze(s_large(:,:,ind)); slopt = squeeze(s_left(:,:,ind));

options = optimset('Display','iter','MaxFunEvals',1e20,'TolFun',1e-12,'MaxIter',inf,'TolX',1e-12);

% SOMA Pars
step = 0.11;
pathLength = 7;
prt = 0.1;
minDiv = 0;
migrations =100;
popSize = 200;

bounds = repmat(s_cap_red,[1 2]);
bounds(1,1) = bounds(1,1)*0.6; bounds(2:9,1) = 80e-12; bounds(10:25,1) = 10e-12;
bounds(1,2) = bounds(1,2)*1.3; bounds(2:9,2) = 200e-12; bounds(10:25,2) = 50e-12; 

% bounds([11 15],2) = 200e-12; bounds(19,2) = 1000e-12;

% bounds(:,1) = 15*1e-12*ones(26,1); bounds(3:10,1) = ones(8,1)*1e-13;
% bounds(:,2) = 40*1e-12*ones(26,1); bounds(3:10,2) = ones(8,1)*1e-10;
% bounds(1,1) = 5e-12; bounds(1,2) = 20e-12;
% bounds(2,1) = 100e-12; bounds(2,2) = 140e-12;

% s_cap_opt = soma_all_to_one_sjm(@(s_cap_red) optimfuncirc_constr_128(s_cap_red,spopt,slaropt,slopt,sropt,ymat,r,lambda),bounds,step,pathLength,prt, minDiv, migrations, popSize,s_cap_red); s_cap_vectmp = s_cap_opt.bestPosition.';