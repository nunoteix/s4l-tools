%% Circuit domain operation to be minimised
function [s_capop] = optimfuncirc_constr_128(s_cap_red,s_ports,s_large,s_left,s_right,ymat,r,lambda)
  
    capvec = r + 1./(1i*2*pi*128000000*s_cap_red);

    
%     capvec_full(1:8) = capvec(1);
%     capvec_full(9:16) = capvec(2:9);
%     
%     capvec_full(17:20) = capvec(10:13);
%     capvec_full(21:24) = capvec(13:-1:10);
%     
%     capvec_full([25:2:31 41:2:47]) = capvec(14:21);
%     capvec_full([26:2:32 42:2:48]) = capvec(14:21);
%     
%     capvec_full([49:2:55 33:2:39]) = capvec(21:-1:14);
%     capvec_full([50:2:56 34:2:40]) = capvec(21:-1:14);
%     
    
    
    capvec_full(41:48) = capvec(1);
    capvec_full(33:40) = capvec(2:9);
%     capvec_full(17:24) = capvec(10:17);
    capvec_full(1:16) = capvec(10:25);
    capvec_full(17:32) = capvec(10:25);
%     capvec_full(57:120) = (r + 1./(1i*2*pi*128000000*(33e-12)))*ones(64,1);

    % Impedance matrix (z-parameters) at each frequency
    z_cap = diag(capvec_full);
    
    % S-parameters from z-parameters and characteristic admittances
    s_cap = (inv(ymat*z_cap*ymat + eye(48)))*(ymat*z_cap*ymat - eye(48));
    
    % Output norm of cost function
    s_capoptmp = ((s_ports + s_right*s_cap*(inv(eye(48) - s_large*s_cap))*s_left));

    % Penalty function (1.54 best for max/abs)
%     s_capop = 0*max(abs(s_capoptmp(~logical(eye(8)))))+max(abs(diag(s_capoptmp)));
    
%     s_capop = 0*max(abs(s_capoptmp(~logical(eye(8))))) +...
%               abs(sum(1./(log(abs(0.25*ones(8,1)-abs(diag(s_capoptmp)))))));

     s_capop = lambda*max(abs(s_capoptmp(~logical(eye(8))))) + norm(diag(s_capoptmp));

%     dg = diag(s_capoptmp);
% 
%     s_capop = lambda*max(abs(s_capoptmp(~logical(eye(8))))) + sqrt(sum((abs(dg([1 3:6 8]))).^2) + 4*sum(abs(dg([2 7])).^2));
end