	#####################################
### script to retrieve Rho, Sigma and Volumes ###

import s4l_v1.document as document
from scipy.io import savemat 
import numpy

# simulation = document.AllSimulations["Huygens Channel "+str(ii)] ##### <--- Rapid HUYGENS
# simulation = document.AllSimulations["Channel "+str(ii)]		   ##### <--- Rapid Small Sim
# simulation = document.AllSimulations["Huygens Element "+str(ii)] ##### <--- Hydra HUYGENS
simulation = document.AllSimulations["Element #"+str(ii)]		   ##### <--- Hydra Small Sim

simulation_extractor = simulation.Results()
########## # Adding a new EmSensorExtractor
# em_sensor_extractor = simulation_extractor["sBox"] ############## <--- Select for HUYGENS

# inputs = [simulation_extractor.Outputs["A"+str(ii)]] 			   ##### <--- Rapid Small Sim
inputs = [simulation_extractor.Outputs["A00E"+str(ii-1)+"  (E"+str(ii-1)+")"]] 	   ##### <--- Hydra Small Sim
	
em_sim_extractor = analysis.extractors.EmPortSimulationExtractor(inputs=inputs)
em_sim_extractor.Name = "Port"
em_sim_extractor.UpdateAttributes()
document.AllAlgorithms.Add(em_sim_extractor)
em_sensor_extractor = em_sim_extractor["SensBox"]



em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
em_sensor_extractor.Normalization.Normalize = True
########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
document.AllAlgorithms.Add(em_sensor_extractor)
	
# Adding a new SurfaceViewer for SAR  and El. Loss
getsar = em_sensor_extractor.Outputs["SAR(x,y,z,f0)"]
getld = em_sensor_extractor.Outputs["El. Loss Density(x,y,z,f0)"]
getE = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]

getsar.Update()
getld.Update()
getE.Update()
	
sar = getsar.Data
ld = getld.Data
EData= getE.Data
	
EField= EData.Field(0)
	
rho = ld.Field(0)/sar.Field(0)
rho = [rho[x] if numpy.isnan(rho[x]) == False else 0 for x in range(len(rho))]
rho = numpy.asarray(rho)
	
Ex = EField[:,0]
Ey = EField[:,1]
Ez = EField[:,2]
	
Eabs = (numpy.abs(numpy.sqrt(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj()))).flatten()
sig = 2*(ld.Field(0)).flatten()/(numpy.power(Eabs,2))
	
grid = EData.Grid
x = grid.XAxis
y = grid.YAxis
z = grid.ZAxis

volmesh = numpy.meshgrid(numpy.diff(z),numpy.diff(y),numpy.diff(x), indexing='ij')
vol = volmesh[0].flatten()*volmesh[1].flatten()*volmesh[2].flatten()
vol = numpy.asarray(vol)

path = r'G:\\Mask Properties\\Hydra_ThinnmerFinalPhantom_NoWire_portByPort_RHO'
mdict = {'rho':rho}
savemat(path,mdict)

# path = r'G:\\Mask Properties\\RapidCoil_GoodHBox_RPhant20cm_portByPort_SIGMA'
# mdict = {'sig':sig}
# savemat(path,mdict)	

# path = r'G:\\Mask Properties\\RapidCoil_GoodHBox_RPhant20cm_portByPort_VOLUMES'
# mdict = {'vol':vol}
# savemat(path,mdict)	

##
Eabs2 = numpy.zeros(list(sig.shape)+[8], 'complex')
for ii in range(1,9):

	simulation = document.AllSimulations["Huygens Channel "+str(ii)]
	simulation_extractor = simulation.Results()

	########## # Adding a new EmSensorExtractor
	em_sensor_extractor = simulation_extractor["SensBox"]
	em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
	em_sensor_extractor.Normalization.Normalize = True
	########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)

	getE = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]
	getE.Update()
	EData= getE.Data
	EField= EData.Field(0)
	Ex = EField[:,0]
	Ey = EField[:,1]
	Ez = EField[:,2]

	Eabs2[:,ii-1] = (numpy.abs(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj())).flatten()
	
print('E-field vector has length: '+str(Eabs2.shape))
Eabs2 = Eabs2.reshape(x.shape[0]-1, y.shape[0]-1, z.shape[0]-1, 8)
sigRS= sig.reshape(x.shape[0]-1, y.shape[0]-1, z.shape[0]-1)
rhoRS = rho.reshape(x.shape[0]-1, y.shape[0]-1, z.shape[0]-1)
print('E-field in grid has shape: '+str(Eabs2.shape))
Q = (sigRS[20,55,50]/(2*rhoRS[20,55,50]))*Eabs2[20,55,50,:]



