# -*- coding: utf-8 -*-
import s4l_v1 as s4l
import numpy

sim = s4l.simulation.GetSimulations()[1]
mult_results = sim.Results()
results = mult_results.GetSensor(mult_results.keys()[0])
sensor = results.GetSensor('Overall Field')
fieldclass = sensor.GetData(sensor.keys()[0])
field = fieldclass.Field(0)
new_shape = (fieldclass.Grid.Dimensions[0]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[2]-1)
field = field.reshape(list(new_shape)+[3])

import scipy.io

path = r'D:\ZMT Laptop\ZMT Documents\exported fields'
mdict = {'Electric Field':field}
name = '\Efield.mat'
scipy.io.savemat(path+name,mdict)

#############################################
for sim in mult_results.keys():
	results = mult_results.GetSensor(sim)
	sensor = results.GetSensor('Overall Field')
	fieldclass = sensor.GetData('EM E(x,y,z,f0)')
	field = fieldclass.Field(0)
	new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
	field = field.reshape(list(new_shape)+[3])
	
	path = r'D:\\ZMT Laptop\\ZMT Documents\\exported fields\\'
	mdict = {'Electric Field':field}
	ext = '.mat'
	scipy.io.savemat(path+sim+ext,mdict)
	
#############################################
import s4l_v1 as s4l
import numpy

sim = s4l.simulation.GetSimulations()[3]
for ii in range(8):
	results = sim.Results(ii)
	i = results.GetSensor(results.keys()[ii]).GetData('EM I(f)').GetComponent(0)
	v = results.GetSensor(results.keys()[ii]).GetData('EM U(f)').GetComponent(0)
	a = .5*(v[2500]/numpy.sqrt(50)+numpy.sqrt(50)*i[2500])
	print(a)
	sensor = results.GetSensor('Overall Field')
	fieldclass = sensor.GetData('B1(x,y,z,f0)')
	field = fieldclass.Field(0)
	new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
	field = field.reshape(list(new_shape)+[2])
	if ii == 0:
		B1all = numpy.zeros(list(field.shape[0:3])+[8],complex)
		sar = sensor.GetData('SAR(x,y,z,f0)')
		mask = [1 if x>0 else 0 for x in sar.Field(0)]
		mask = numpy.array(mask).reshape(list(new_shape))
	B1all[:,:,:,ii] = field[:,:,:,1]/a*mask
import scipy.io
path = r'D:\\ZMT Laptop\\ZMT Documents\\exported fields\\'
mdict = {'B1all':B1all, 'x_axis':fieldclass.Grid.XAxis, 'y_axis':fieldclass.Grid.YAxis, 'z_axis':fieldclass.Grid.ZAxis, 'Mask':mask}
ext = '.mat'
scipy.io.savemat(path+'B18chDipoleShield'+ext,mdict)