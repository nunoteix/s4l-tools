import s4l_v1 as s4l
import sys, os
from scipy.io import savemat

dir_name  = 'TransferFunctionPark10cm0.47SmMAT'
sims = s4l.simulation.GetSimulations()
data = {} # create structure to store data
path = 'D:\TransferFunction\\'+ dir_name #The directory we will save our data in
for sim in sims: 
  result = sim.Results()
  if numpy.size(result.keys()):
	results = sim.Results() # get simulation results 
	# extract E
	overall_field_sensor = results['Overall Field']
	E = overall_field_sensor['EM E(x,y,z,f0)']
	Edata = E.Data
	grid_data = Edata.Grid
	E.Update()
	
	H = overall_field_sensor['EM H(x,y,z,f0)']
	Hdata = H.Data
	H.Update()
	
	B = overall_field_sensor['B(x,y,z,f0)']
	Bdata = B.Data
	B.Update()
	
	J = overall_field_sensor['J(x,y,z,f0)']
	Bdata = J.Data
	J.Update()
	
	data['E_field'] = E.Data.Field(0)
	data['B_field'] = B.Data.Field(0)
	data['J_field'] = J.Data.Field(0)
	data['x_axis'] = grid_data.XAxis
	data['y_axis'] = grid_data.YAxis
	data['z_axis'] = grid_data.ZAxis
	


	if not os.path.exists(path):
		os.makedirs(path)
	
	sim_name=sim.Name
	splitted_name=sim_name.split(' ')
	filename = 'Efield_'+splitted_name[2]

	print 'saving data to: ' + path +'\\'+filename
	savemat(path+'\\'+filename,data,oned_as='column') # save data to MATLAB
	s4l.SaveDocument() # save the .smash file
	print 'Finished...'	
  else: 
	sim_name=sim.Name
	print sim_name+' had no results available yet'
	