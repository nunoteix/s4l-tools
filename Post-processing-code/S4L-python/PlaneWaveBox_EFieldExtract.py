### script to retrieve EM E(x,y,z,f0) grid and field ###

import s4l_v1.document as document
import s4l_v1.analysis as analysis
import s4l_v1.model as model
from scipy.io import savemat 
import numpy

npwb = 43
axis = numpy.zeros((npwb,), dtype=numpy.double)
for ii in range(1,npwb+1):
	# if ii < 10:
	simulation = document.AllSimulations["800Park Uncapped "+str(ii)] ##### 
	# else:
		# simulation = document.AllSimulations["800SWE 2 Boxes T"+str(ii)] ##### 
		
	simulation_extractor = simulation.Results()

	######## # Adding a new EmSensorExtractor
	em_sensor_extractor = simulation_extractor["Field Sensor"] ##############	
	# em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
	# em_sensor_extractor.Normalization.Normalize = True
	# em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)

	######## # Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
	E = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]
	E.Update()
	E_data = E.Data
	E_data_grid = E_data.Grid
	xaxis = E_data_grid.XAxis
	yaxis = E_data_grid.YAxis
	zaxis = E_data_grid.ZAxis
	E_data_field = E_data.Field(0)

	# if ii < 10:
	axisv = model.AllEntities()["PWE "+str(ii)].Transform.Translation[2]
	# else:
		# axisv = model.AllEntities()["PWE T"+str(ii)].Transform.Translation[2]
	
	axis[ii-1] = axisv
	
	path = r'G:\\Transfer Functions\\Park Paper Replication\\800mm Standing Wave Excitation Uncapped Dielectric\\E_noNorm_channel'+str(ii)
	mdict = {'Snapshot0':E_data_field, 'Axis0':xaxis, 'Axis1':yaxis, 'Axis2':zaxis, 'AxisPlot': axis}
	savemat(path,mdict)