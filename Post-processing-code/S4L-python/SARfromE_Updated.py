import s4l_v1.document as document
import s4l_v1.model as model
import s4l_v1.analysis as analysis
from s4l_v1 import Vec3
import s4l_v1.simulation.emfdtd as fdtd
import s4l_v1.units as units
import numpy
import numpy.matlib
from scipy.io import savemat 
from scipy.io import loadmat 


# Import E-Field
Eall = 0
for ii in xrange(0,8):
	mat = loadmat(r'G:\E for S4L Q-script\E_Hydra_headDuke_64p_50dB_betterSkin_noNorm\E_1uT_channel'+str(ii+1))
	Eall = Eall + mat.get(mat.keys()[5])
	
# Adding a new LocalSarEvaluator
inputs = [em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]]
local_sar_evaluator = analysis.em_evaluators.LocalSarEvaluator(inputs = inputs)
local_sar_evaluator.UpdateAttributes()
document.AllAlgorithms.Add(local_sar_evaluator)

# Adding a new AverageSarFieldEvaluator
inputs = [local_sar_evaluator.Outputs["SAR(x,y,z,f0)"]]
average_sar_field_evaluator = analysis.em_evaluators.AverageSarFieldEvaluator(inputs=inputs)
average_sar_field_evaluator.UncertaintyRegionsEnabled = False
average_sar_field_evaluator.ComputeAveragingCubesOutput = False
average_sar_field_evaluator.UpdateAttributes()
document.AllAlgorithms.Add(average_sar_field_evaluator)

# Adding a new SliceFieldViewer
inputs = [average_sar_field_evaluator.Outputs["IEEE/IEC62704-1 Avg.SAR(x,y,z,f0)"]]
slice_field_viewer = analysis.viewers.SliceFieldViewer(inputs=inputs)
slice_field_viewer.Data.Mode = slice_field_viewer.Data.Mode.enum.QuantityRealModulus
slice_field_viewer.Slice.Index = 50
slice_field_viewer.UpdateAttributes()
document.AllAlgorithms.Add(slice_field_viewer)