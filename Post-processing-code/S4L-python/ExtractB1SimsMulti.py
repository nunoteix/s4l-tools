# -*- coding: utf-8 -*-
import s4l_v1 as s4l
from scipy.io import savemat  
import s4l_v1.simulation.emfdtd as fdtd
import numpy
import sys, os
from pylab import *

sims = s4l.simulation.GetSimulations()

ii = 0 # Select which simulation no. you want (starting from 0 on top of the list)
sim = sims[ii]
results = sim.Results
nports = 24 #Select amount of ports you want to extract
for jj in range(0,nports):
		
	result = results(jj)
	# overall_field_sensor = result['Overall Field'] # Extract overall field sensor
	# overall_field_sensor = result['SensBox Box']
	# overall_field_sensor = result['SensBox']
	overall_field_sensor = result['Sens Box']	
	input_power_sensor = result['Input Power']
	
	Psensor = input_power_sensor['EM Input Power(f)']
	Psensor.Update()
	P = Psensor.Data.GetComponent(0)
	
	# Selection of result no. is based on this specific S4L Output!
	
	
	
	B1 = overall_field_sensor['B1(x,y,z,f0)'] # Extract B1 Field
	B1data = B1.Data # Contains grid and field (and other stuff)
	B1grid = B1data.Grid # Select grid data
	xaxis = B1grid.XAxis
	yaxis = B1grid.YAxis
	zaxis = B1grid.ZAxis
	B1.Update() #?
	
	B1field_unshaped = B1data.Field(0) # Select B1 field from all data
	B1field_normalized = (B1field_unshaped/sqrt(P))*sqrt(4/3)#*2#*sqrt(2)
	
	
	# Misschien alleen reeele deel van vermogen meenemen
	
	path = r'D:\\Data\\DUKE - 4th Part\\B1_24xSmallFracD_onDUKE\\' # Path your exporting to
	mdict = {'Snapshot0':B1field_normalized, 'Axis0':xaxis, 'Axis1':yaxis, 'Axis2':zaxis}
	ext = '.mat'
	savemat(path+sim.Name+str(jj+1)+ext,mdict)
	
	
	