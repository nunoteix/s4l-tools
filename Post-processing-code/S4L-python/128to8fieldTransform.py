import s4l_v1 as s4l
from scipy.io import savemat 
from scipy.io import loadmat 
import s4l_v1.simulation.emfdtd as fdtd
import numpy as np
import sys, os
from pylab import *
import gc

# Load in the weights matrix that transforms the 128 fields into 8 fields
weight_mat = loadmat(r'G:\\128 Ports to 8 Conversion Weights\\weights_final_parallel_headDuke_64p_50dB')
weights = weight_mat.get(weight_mat.keys()[1])

sims = s4l.simulation.GetSimulations()

ii = 0 # Select which simulation no. you want (starting from 0 on top of the list)
sim = sims[ii]
multresults = sim.Results()
order = multresults.keys()
nports = 64 #Select amount of ports you want to extract

results = multresults.GetSimulation(order[ii])
overall_field_sensor = results['SensBox']
B1 = overall_field_sensor['B1(x,y,z,f0)'] # Extract B1 Field
B1data = B1.Data # Contains grid and field (and other info)
B1.Update() #?
B1field = B1data.Field(0)
B1grid = B1data.Grid # Select grid data
xaxis = B1grid.XAxis
yaxis = B1grid.YAxis
zaxis = B1grid.ZAxis

savedShape = B1field.shape
del results
del overall_field_sensor
del B1
del B1data
del B1field
del B1grid

#Get the power values @128MHz for each one of the 128 excitations
P = np.zeros((nports,1))
for kk in range(0,nports):
	results = multresults.GetSimulation(order[kk])
	input = results['Input Power']
	Psensor = input['EM Input Power(f)']	
	Psensor.Update()
	P_vec = Psensor.Data.GetComponent(0)
	P[kk] = max(P_vec)
	del results
	del input
	del Psensor
	del P_vec

#Calculate the 8 ports equivalent of the 128 sim by applying the loaded weigths

def genB1temp(ii,jj,weights,P):
	results = multresults.GetSimulation(order[jj])
		
	overall_field_sensor = results['SensBox']
	
	B1 = overall_field_sensor['B1(x,y,z,f0)']
		
	B1.Update() #?
	B1field = B1.Data.Field(0)
	
	B1temp = (B1field/sqrt(P[jj]))*weights[jj,ii]

	return B1temp

def sumB(ii,jj,weights,P,B1c):
	B1temp = genB1temp(ii,jj,weights,P)
	B1 = B1c + B1temp
	B1 = copy(B1)
	return B1
	
	
for ii in xrange(0,1):

	B1c = np.zeros(list(savedShape),'complex')
	for jj in xrange(0,nports):
		B1 = sumB(ii,jj,weights,P,B1c)
		B1c = copy(B1)
		del B1
		gc.collect()
		gc.collect()
	
	path1 = r'G:\\B1\\B1_Hydra_headDuke_64p_50dB\\' # Path your exporting to
	mdict = {'Snapshot0':B1c, 'Axis0':xaxis, 'Axis1':yaxis, 'Axis2':zaxis}
	ext = '.mat'
	savemat(path1+'B1p_channel'+str(ii+1)+ext,mdict,True)
	
