# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import s4l_v1 as s4l
import scipy.io
import numpy
import scipy.linalg
from numpy import linalg as LA
import numpy.matlib

	
def Average(W,cube,E):
	xl = cube.Lower[0]
	xu = cube.Upper[0]
	yl = cube.Lower[1]
	yu = cube.Upper[1]
	zl = cube.Lower[2]
	zu = cube.Upper[2]
	return sum(sum(sum(W*E[zl:zu+1,yl:yu+1,xl:xu+1],0),0),0)/cube.Volume#.01
	
def getWeights(cube):
	W = zeros((3,2))
	LL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[0])
	LU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[7])
	for ii in range(3):
		W[ii,0] = (LU-cube.Point0)[ii]/(LU-LL)[ii]
	UL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[0])
	UU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[7])
	for ii in range(3):
		W[ii,1] = (cube.Point1-UL)[ii]/(UU-UL)[ii]

	xw =[W[0,0]]+list(numpy.ones(cube.Upper[0]-cube.Lower[0]-1))+[W[0,1]]
	yw =[W[1,0]]+list(numpy.ones(cube.Upper[1]-cube.Lower[1]-1))+[W[1,1]]	
	zw =[W[2,0]]+list(numpy.ones(cube.Upper[2]-cube.Lower[2]-1))+[W[2,1]]
	W = numpy.meshgrid(zw,yw,xw,indexing='ij')		
	W = W[0]*W[1]*W[2]
	# W3 = zeros(list(W.shape))
	# W3[:,:,:] = W
	return W
	
### WRIGHT SHIM VALUES ##############

#SSAD SHIMS
shim = [1.0000 + 0.0000j,
		0.8259 + 0.5639j,
		0.2643 + 0.9644j,
		-0.2789 + 0.9603j,
		0.9870 - 0.1608j,
		0.9999 - 0.0162j,
		0.9550 + 0.2967j,
		0.8370 + 0.5472j,
		-0.9942 - 0.1071j,
		-0.8373 - 0.5467j,
		-0.2602 - 0.9656j,
		0.3727 - 0.9279j,
		-0.9897 + 0.1433j,
		-0.9998 - 0.0186j,
		-0.9616 - 0.2744j,
		-0.8655 - 0.5009j]
		
#FracD SHIMS		
# shim = [1.0000 + 0.0000j,
		# 0.9745 - 0.2243j,
		# 0.9898 + 0.1422j,
		# 0.5189 + 0.8548j,
		# -0.6674 - 0.7447j,
		# -0.9982 + 0.0607j,
		# -0.9266 + 0.3761j,
		# -0.9982 + 0.0595j]
	
#####################################
	
order = ['P01', 'P02', 'P03', 'P04',
		 'P05', 'P06', 'P07', 'P08'
		 'P09', 'P10', 'P11', 'P12',
		 'P13', 'P14', 'P15', 'P16']

S = numpy.zeros([16,16],'complex')
a = numpy.zeros(16,'complex')
z = numpy.zeros(16,'complex')
z0 = 50
sim = s4l.simulation.GetSimulations()[0]									#### CHANGE SIM HERE
print(sim.Name)
multresults = sim.Results()
order2 = multresults.keys()
P = numpy.zeros(16,'complex')
for ii in range(16):
	results = multresults.GetSimulation(order2[ii])
	order = results.keys()

	
	
	
	input_power_sensor = results['Input Power']
	Psensor = input_power_sensor['EM Input Power(f)']
	Psensor.Update()
	P[ii] = Psensor.Data.GetComponent(0)

	
	sensor = results.GetSensor('SensBox')
	# sensor = results.GetSensor('SensBox Box')
	# sensor = results.GetSensor('Overall Field')
	fieldclass = sensor.GetData('EM E(x,y,z,f0)')
	Efield = fieldclass.Field(0)
	grid = fieldclass.Grid
	new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
	if ii == 0:
		Eall = numpy.zeros(list(Efield.shape),'complex')
		Ec = numpy.zeros(list(Efield.shape)+[16],'complex')
		Ecs = numpy.zeros(list(Efield.shape)+[16],'complex')
		sar = sensor.GetData('SAR(x,y,z,f0)')
		ld = sensor.GetData('El. Loss Density(x,y,z,f0)')
		rho = ld.Field(0)/sar.Field(0)
		rho = [rho[x] if numpy.isnan(rho[x]) == False else 0 for x in range(size(rho))]
		rho = numpy.asarray(rho)
		Ex = Efield[:,0]
		Ey = Efield[:,1]
		Ez = Efield[:,2]
		Eabs = (numpy.abs(numpy.sqrt(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj()))).flatten()
		sig = 2*(ld.Field(0)).flatten()/(numpy.power(Eabs,2))
		mask = mask = [1 if x>0.005 else 0 for x in sig]
		mask = numpy.asarray(mask)
		x = grid.XAxis
		y = grid.YAxis
		z = grid.ZAxis
		del Eabs
		del ld
		del sar
		volmesh = numpy.meshgrid(numpy.diff(z),numpy.diff(y),numpy.diff(x), indexing='ij')
		vol = volmesh[0].flatten()*volmesh[1].flatten()*volmesh[2].flatten()
		vol = numpy.asarray(vol)
	
	Ec[:,:,ii] = (Efield/(numpy.sqrt(P[ii])))*sqrt(2)*zip(mask,mask,mask)
	Ecs[:,:,ii] = Ec[:,:,ii]*shim[ii]
	
	del Efield
	del fieldclass
	del sensor
	# del i
	# del v
	del results
	
	
Eall[:,:] = Ecs[:,:,0] + Ecs[:,:,1] + Ecs[:,:,2] + Ecs[:,:,3] + Ecs[:,:,4] + Ecs[:,:,5] + Ecs[:,:,6] + Ecs[:,:,7] + Ecs[:,:,8] + Ecs[:,:,9] + Ecs[:,:,10] + Ecs[:,:,11] + Ecs[:,:,12] + Ecs[:,:,13] + Ecs[:,:,14] + Ecs[:,:,15]
del Ecs
del Ec

results = multresults.GetSimulation(multresults.keys()[0])					#### CHANGE SIM AND HERE
fieldclass = results.GetSensor('SensBox')
# fieldclass = results.GetSensor('Overall Field')
grid = fieldclass.GetData('EM E(x,y,z,f0)').Grid

import EmPostPro
cubes, lossyrho_fd, labels_fd = EmPostPro.ConstMassCubesGenerator.Generate(grid, target_mass_kg=1E-2, enable_uncertainty_region=False)
print(len(cubes))

LUT = numpy.zeros(len(cubes))
for ii in range(len(cubes)):
	LUT[ii] = cubes[ii].ReferenceCellId
	


SAR = numpy.zeros((Eall.shape[0]),'complex')
for i in range(Eall.shape[0]):
		if sig[i] != 0.0:
			e = Eall[i]
			SAR[i] = (sig[i]/(2*rho[i]))*numpy.dot(e.conj().transpose(),e)*vol[i]
			del e

del Eall
del sig
del vol

SAR = SAR.reshape(list((grid.ZAxis.size-1,grid.YAxis.size-1,grid.XAxis.size-1)))
SAR10g = numpy.zeros([len(cubes)])
for ii in range(len(cubes)):
	print(ii)
	if (cubes[ii].Lower[2]!=cubes[ii].Upper[2] and cubes[ii].Lower[1]!=cubes[ii].Upper[1] and cubes[ii].Lower[0]!=cubes[ii].Upper[0]):
		SAR10g[ii] = Average(getWeights(cubes[ii]),cubes[ii],SAR)



path = r'D:\\Data\\Frac - 3rd Part\\SAR_OPTSHIM\\SAR10g_test'

mdict = {'SAR10g':SAR10g, 'LUT':LUT}
scipy.io.savemat(path,mdict)

# mdict = {'SAR':SAR}
# scipy.io.savemat(path,mdict)

del SAR
del SAR10g

























###
