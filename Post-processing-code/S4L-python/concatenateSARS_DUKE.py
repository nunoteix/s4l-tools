import s4l_v1 as s4l
from scipy.io import savemat 
from scipy.io import loadmat 
import s4l_v1.simulation.emfdtd as fdtd
import numpy as np
#import matplotlib
#matplotlib.rcParams['text.usetex'] = True
#matplotlib.rcParams['text.latex.unicode'] = True

from numpy import linalg as LA
import XPostProcessor

def Average(W,cube,E):
	xl = cube.Lower[0]
	xu = cube.Upper[0]
	yl = cube.Lower[1]
	yu = cube.Upper[1]
	zl = cube.Lower[2]
	zu = cube.Upper[2]
	return sum(sum(sum(W*E[zl:zu+1,yl:yu+1,xl:xu+1],0),0),0)/cube.Volume
	
def getWeights(cube):
	W = zeros((3,2))
	LL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[0])
	LU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[7])
	for ii in range(3):
		W[ii,0] = (LU-cube.Point0)[ii]/(LU-LL)[ii]
	UL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[0])
	UU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[7])
	for ii in range(3):
		W[ii,1] = (cube.Point1-UL)[ii]/(UU-UL)[ii]

	xw =[W[0,0]]+list(numpy.ones(cube.Upper[0]-cube.Lower[0]-1))+[W[0,1]]
	yw =[W[1,0]]+list(numpy.ones(cube.Upper[1]-cube.Lower[1]-1))+[W[1,1]]	
	zw =[W[2,0]]+list(numpy.ones(cube.Upper[2]-cube.Lower[2]-1))+[W[2,1]]
	W = numpy.meshgrid(zw,yw,xw,indexing='ij')		
	W = W[0]*W[1]*W[2]
	W3 = zeros(list(W.shape))
	W3[:,:,:] = W
	return W3
	
	
mat1 = loadmat(r'D:\Data\DUKE - 4th Part\SAR_OPTSHIM\SAR1_onDUKE')
mat2 = loadmat(r'D:\Data\DUKE - 4th Part\SAR_OPTSHIM\SAR2_onDUKE')
mat3 = loadmat(r'D:\Data\DUKE - 4th Part\SAR_OPTSHIM\SAR3_onDUKE')
mat4 = loadmat(r'D:\Data\DUKE - 4th Part\SAR_OPTSHIM\SAR_onDUKE')

SAR1 = mat1.get(mat1.keys()[0])
SAR2 = mat2.get(mat2.keys()[0])
SAR3 = mat3.get(mat3.keys()[0])
SAR4 = mat4.get(mat4.keys()[0])

fill = numpy.zeros(1)
SAR = numpy.concatenate((SAR1, 1, SAR2, 1, SAR3, 1, SAR4), axis=1)

sensor = results.GetSensor('Sens Box')
# sensor = results.GetSensor('Overall Field')
fieldclass = sensor.GetData('EM E(x,y,z,f0)')
# Efield1 = fieldclass.Field(0)
grid = fieldclass.Grid

import EmPostPro
cubes, lossyrho_fd, labels_fd = EmPostPro.ConstMassCubesGenerator.Generate(grid, target_mass_kg=1E-2, enable_uncertainty_region=False)
print(len(cubes))

LUT = numpy.zeros(len(cubes))
for ii in range(len(cubes)):
	LUT[ii] = cubes[ii].ReferenceCellId

SAR = SAR.reshape(list((grid.ZAxis.size-1,grid.YAxis.size-1,grid.XAxis.size-1)))
SAR10g = numpy.zeros([len(cubes)])
for ii in range(len(cubes)):
	print(ii)
	if (cubes[ii].Lower[2]!=cubes[ii].Upper[2] and cubes[ii].Lower[1]!=cubes[ii].Upper[1] and cubes[ii].Lower[0]!=cubes[ii].Upper[0]):
		SAR10g[ii] = Average(getWeights(cubes[ii]),cubes[ii],SAR)
