# -*- coding: utf-8 -*-
import s4l_v1 as s4l
import sys, os
import numpy
import scipy.io

path = 'D:\\sim4life\\fracDipolesOnLiver\\fracDipolesOnLiver3.smash_Results'
simName = 'Liver'

sims=s4l.simulation.GetSimulations()
sim = sims[1] # select the multiport simulation that is needed here
data = {} # create structure to store data
k=0
p=0

results=sim.Results()

#singleSim = results.keys()[0]
for singleSim in results.keys():
#for singleSim in {results.keys()[0]}:

	singleSimResults=results.GetSimulation(singleSim)

	k=k+1
	if (k<10):
		numberString='0'+str(k)
	else:
		numberString=str(k)

	print singleSim
	#replace spaces by underscores
	newName = singleSim.replace(' ','_')
	print newName
	
	if (not singleSimResults.keys()):
		print "Warning! Simulation '",newName,"' does not have any field sensor"
		
	else:
		p=p+1
		sensor = singleSimResults.GetSensor("Regional Field Sensor Box")
		inputPower = singleSimResults.GetSensor(singleSimResults.keys()[0]).GetData(singleSimResults.GetSensor(singleSimResults.keys()[0]).keys()[0]).GetComponent(0)
		print inputPower
	
		#Extract B1 field
		fieldclass = None
		B1field = None
		mdict = None
		data = {}
		fieldclass = sensor.GetData(sensor.keys()[10])
		B1field = fieldclass.Field(0)/sqrt(inputPower)
		new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
		B1field = B1field.reshape(list(new_shape)+[2])
		B1field = B1field.transpose([2, 1, 0, 3])
		mdict = {'field':B1field, 'x_axis':fieldclass.Grid.XAxis, 'y_axis':fieldclass.Grid.YAxis, 'z_axis':fieldclass.Grid.ZAxis}
		filename = 'B1field'+numberString+'.mat'
		data['B1field'+numberString] = mdict
		print 'saving data to: ' + path +'\\'+filename
		scipy.io.savemat(path+'\\'+filename,data) # save data to MATLAB
		print 'Finished...'	

		#Extract E field
		fieldclass = None
		Efield = None
		mdict = None
		data = {}
		fieldclass = sensor.GetData(sensor.keys()[0])
		Efield = fieldclass.Field(0)/sqrt(inputPower)
		new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
		Efield = Efield.reshape(list(new_shape)+[3])
		Efield = Efield.transpose([2, 1, 0, 3])
		mdict = {'field':Efield, 'x_axis':fieldclass.Grid.XAxis, 'y_axis':fieldclass.Grid.YAxis, 'z_axis':fieldclass.Grid.ZAxis}
		filename = 'Efield'+numberString+'.mat'
		data['Efield'+numberString] = mdict
		print 'saving data to: ' + path +'\\'+filename
		scipy.io.savemat(path+'\\'+filename,data) # save data to MATLAB
		print 'Finished...'	

		#Only extract SAR, dPdV and J for first simulation
		if (p==1):
			#Extract SAR field
			fieldclass = None
			SARfield = None
			mdict = None
			data = {}
			fieldclass = sensor.GetData(sensor.keys()[9])
			SARfield = fieldclass.Field(0)/sqrt(inputPower)
			new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
			SARfield = SARfield.reshape(list(new_shape))
			SARfield = SARfield.transpose([2, 1, 0])
			mdict = {'field':SARfield, 'x_axis':fieldclass.Grid.XAxis, 'y_axis':fieldclass.Grid.YAxis, 'z_axis':fieldclass.Grid.ZAxis}
			filename = 'SAR'+numberString+'.mat'
			data['SAR'+numberString] = mdict
			print 'saving data to: ' + path +'\\'+filename
			scipy.io.savemat(path+'\\'+filename,data) # save data to MATLAB
			print 'Finished...'	
		
			#Extract J field
			fieldclass = None
			Jfield = None
			mdict = None
			data = {}
			fieldclass = sensor.GetData(sensor.keys()[5])
			Jfield = fieldclass.Field(0)/sqrt(inputPower)
			new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
			Jfield = Jfield.reshape(list(new_shape)+[3])
			Jfield = Jfield.transpose([2, 1, 0, 3])
			mdict = {'field':Jfield, 'x_axis':fieldclass.Grid.XAxis, 'y_axis':fieldclass.Grid.YAxis, 'z_axis':fieldclass.Grid.ZAxis}
			filename = 'Jfield'+numberString+'.mat'
			data['Jfield'+numberString] = mdict
			print 'saving data to: ' + path +'\\'+filename
			scipy.io.savemat(path+'\\'+filename,data) # save data to MATLAB
			print 'Finished...'	
		
			#Extract dPdV field
			fieldclass = None
			dPdVfield = None
			mdict = None
			data = {}
			fieldclass = sensor.GetData(sensor.keys()[7])
			dPdVfield = fieldclass.Field(0)/sqrt(inputPower)
			new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
			dPdVfield = dPdVfield.reshape(list(new_shape))
			dPdVfield = dPdVfield.transpose([2, 1, 0,])
			mdict = {'field':dPdVfield, 'x_axis':fieldclass.Grid.XAxis, 'y_axis':fieldclass.Grid.YAxis, 'z_axis':fieldclass.Grid.ZAxis}
			filename = 'dPdV'+numberString+'.mat'
			data['dPdV'+numberString] = mdict
			print 'saving data to: ' + path +'\\'+filename
			scipy.io.savemat(path+'\\'+filename,data) # save data to MATLAB
			print 'Finished...'	
	
