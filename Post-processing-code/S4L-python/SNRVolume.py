# -*- coding: utf-8 -*-
import s4l_v1 as s4l
from scipy.io import savemat 
from scipy.io import loadmat 
import s4l_v1.simulation.emfdtd as fdtd
import numpy as np
#import matplotlib
#matplotlib.rcParams['text.usetex'] = True
#matplotlib.rcParams['text.latex.unicode'] = True

from numpy import linalg as LA
import XPostProcessor

#Uncomment section below if you want to load the C_matrix from an external path

# mat = loadmat(r'D:\Data\SSAD - 1st Part\Correlation Matrices\C_matrix 8xSSADtest')
# mat = loadmat(r'D:\Data\Loops - 2nd Part\Correlation Matrices\C_matrix 32xLoopsStraighttest')		# CHANGE LOAD PATH HERE
# mat = loadmat(r'D:\Data\Frac - 3rd Part\Correlation Matrices\C_matrix 8xFracD20mmtest')
mat = loadmat(r'D:\Data\DUKE - 4th Part\Correlation Matrices\C_matrix 24xSmallFracD_onDUKE')

# C_matrix = mat.get(mat.keys()[3])
R_matrix = mat.get(mat.keys()[0])


ents = s4l.model.AllEntities()

sims = list(s4l.document.AllSimulations)

ii = 0 # Select which simulation no. you want (starting from 0 on top of the list)					# CHANGE SIM
sim = sims[ii]
results = sim.Results()
nports = 24 #Select amount of ports you want to extract												# CHANGE NO. PORTS

result = results.GetSimulation(results.keys()[0])

# overall_field_sensor = result['SensBox'] 															# CHANGE SENSOR HERE
overall_field_sensor = result['Sens Box']
# overall_field_sensor = result['Overall Field'] 


B1 = overall_field_sensor['B1(x,y,z,f0)'] # Extract B1 Field
	
B1data = B1.Data # Contains grid and field (and other stuff)
B1grid = B1data.Grid
new_shape = (B1grid.Dimensions[0]-1, B1grid.Dimensions[1]-1, B1grid.Dimensions[2]-1)


B1field_normalized_all = np.zeros((new_shape[0]*new_shape[1]*new_shape[2],nports),dtype='complex')

# For-loop below extracts the B1minus-field for port n, and puts it as a column in an nvoxels*nports size matrix 
for jj in range(1,nports+1): 
		
	
	# Check order of simulations in your specific subsimulation here
	result = results.GetSimulation(results.keys()[jj-1])
		
	# overall_field_sensor = result['Overall Field'] 													# CHANGE SENSOR HERE
	# overall_field_sensor = result['SensBox']
	overall_field_sensor = result['Sens Box']
	input_power_sensor = result['Input Power']
	
	# Psensor = input_power_sensor['EM Input Power(f)']
	# Psensor.Update()
	# P = Psensor.Data.GetComponent(0)
	
	i_in = result.GetSensor(result.keys()[jj]).GetData('EM I(f)').GetComponent(0)
	#v_in = result.GetSensor(result.keys()[jj+2]).GetData('EM U(f)').GetComponent(0)
	#a = .5*(v_in/numpy.sqrt(50)+numpy.sqrt(50)*i_in)
	
	# Selection of result no. is based on this specific S4L Output!
	
	B1 = overall_field_sensor['B1(x,y,z,f0)'] # Extract B1 Field
	
	B1data = B1.Data # Contains grid and field (and other stuff)
	B1grid = B1data.Grid # Select grid data
	xaxis = B1grid.XAxis
	yaxis = B1grid.YAxis
	zaxis = B1grid.ZAxis
	B1.Update() #?
	
	B1field_unshaped = np.array([B1data.Field(0)])
	
	B1field_normalized = (B1field_unshaped/i_in)#*sqrt(4/3)#*sqrt(2)#*2 							# CHANGE POWER

	new_shape = (B1grid.Dimensions[0]-1, B1grid.Dimensions[1]-1, B1grid.Dimensions[2]-1)

	B1field_normalized_all[:,jj-1] = B1field_normalized[:,:,1] #Select B1- here


	

# C_matrix = np.matrix(C_matrix)
R_matrix = np.matrix(R_matrix)
# Uncomment section below if you want to select not all the ports 

#C_matrix_alt = C_matrix[[[2],[3],[4],[5],[6],[7]],[2,3,4,5,6,7]] #Select right rows and columns with broadcasting


lengthall = new_shape[0]*new_shape[1]*new_shape[2]
SNRfield_flat = np.zeros(lengthall)	
R_inv = LA.inv(R_matrix)
# For loop calculates the SNR per voxel		
for ii in range(0,lengthall):
	B1row = np.matrix([B1field_normalized_all[ii,:]])
	B1row = np.transpose(B1row)
	B1rowherm = np.transpose(np.conj(B1row))
	SNRvoxel = np.sqrt(B1rowherm*R_inv*B1row)
	SNRvoxel = np.abs([SNRvoxel]) # Throws away very small imaginary part
	SNRfield_flat[ii] = SNRvoxel
SNRfield_flat = np.double(abs(SNRfield_flat))
SNR = SNRfield_flat#.reshape(new_shape)

# path = r'D:\\Data\\SSAD - 1st Part\\SNR Matrices\\'
# path = r'D:\\Data\\Loops - 2nd Part\\SNR Matrices\\'
# path = r'D:\\Data\\Frac - 3rd Part\\SNR Matrices\\' # Path your exporting to
path = r'D:\\Data\\DUKE - 4th Part\\SNR Matrices\\'
mdict = {'SNR':SNR,}
ext = '.mat'
savemat(path+'SNR_'+'24xSmallFracD_onDUKE'+ext,mdict)


# SNRfield_obj = XPostProcessor.DoubleFieldData()
# SNRfield_obj.Grid = B1grid
# SNRfield_obj.ValueLocation = XPostProcessor.eValueLocation.kCellCenter
# SNRfield_obj.NumberOfComponents = 1
# SNRfield_obj.NumberOfSnapshots = 1
# SNRfield_obj.SetField(0,SNRfield_flat)
# producer1 = XPostProcessor.TrivialProducer()
# producer1.SetDataObject(SNRfield_obj)
# producer1.Description = 'SNR'
# reg = XPostProcessor.AlgorithmRegistry()
# reg.AddAlgorithm(producer1)

del results
del result

	
	