### script to retrieve Surface-J(x,y,z,f0) grid and field ###

import s4l_v1.document as document
import s4l_v1.analysis as analysis
from scipy.io import savemat 
import numpy


for ii in range(1,9):
	# for jj in range(1,13):
	simulation = document.AllSimulations["Huygens Channel "+str(ii)] ##### <--- Rapid HUYGENS
	# simulation = document.AllSimulations["Channel "+str(ii)]		   ##### <--- Rapid Small Sim
	# simulation = document.AllSimulations["Huygens Element "+str(ii)] ##### <--- Hydra HUYGENS
		# simulation = document.AllSimulations["Element #"+str(ii)+' '+str(jj)]		   ##### <--- Hydra Small Sim
	
	simulation_extractor = simulation.Results()

	########## # Adding a new EmSensorExtractor
	em_sensor_extractor = simulation_extractor["sBoxClone  (Model)"] ############## <--- Select for HUYGENS
	# em_sensor_extractor = simulation_extractor["Overall Field"]
	
	# inputs = [simulation_extractor.Outputs["A"+str(ii)]] 			   ##### <--- Rapid Small Sim
		# inputs = [simulation_extractor.Outputs["A00E"+str(ii-1)+"  (E"+str(ii-1)+")"]] 	   ##### <--- Hydra Small Sim
	
	# em_sim_extractor = analysis.extractors.EmPortSimulationExtractor(inputs=inputs)
	# em_sim_extractor.Name = "Port"
	# em_sim_extractor.UpdateAttributes()
	######## document.AllAlgorithms.Add(em_port_simulation_extractor)
	# em_sensor_extractor = em_sim_extractor["Overall Field"]

	
	em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
	em_sensor_extractor.Normalization.Normalize = True
	########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)
	
	########## # Adding a new SurfaceViewer for Surf J
	# surfJ = em_sensor_extractor.Outputs["Surface-J(x,y,z,f0)"]
	# surfJ.Update()
	# surfJ_data = surfJ.Data
	# surfJ_data_grid = surfJ_data.Grid
	# surfJ_data_field = surfJ_data.Field(0)
	
	# n_points = surfJ_data_grid.NumberOfPoints
		
	# points_coord = []
	# for index in range(n_points):
		# p_coord = surfJ_data_grid.GetPoint(index)
		# points_coord.append(p_coord)
		
		
		######## Adding a new SurfaceViewer for B1	
	B1 = em_sensor_extractor.Outputs["B1(x,y,z,f0)"]
	B1.Update()
	B1_data = B1.Data
	B1_data_grid = B1_data.Grid
	xaxis = B1_data_grid.XAxis
	yaxis = B1_data_grid.YAxis
	zaxis = B1_data_grid.ZAxis
	B1_data_field = B1_data.Field(0)
		
	# Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
	E = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]
	E.Update()
	E_data = E.Data
	E_data_grid = E_data.Grid
	xaxis = E_data_grid.XAxis
	yaxis = E_data_grid.YAxis
	zaxis = E_data_grid.ZAxis
	E_data_field = E_data.Field(0)
		
		## end of the script ###
	
	# path = r'G:\\Surface Current\\Final Rapid Coil on Duke 15T Wire StrippedTip\\J_Ch'+str(ii)+'_points'
	# mdict = {'JPoints':points_coord}
	# savemat(path,mdict)
	
	# path = r'G:\\Surface Current\\Final Rapid Coil on Duke 15T Wire StrippedTip\\J_Ch'+str(ii)
	# mdict = {'SurfJ':surfJ_data_field}
	# savemat(path,mdict)
	
	path = r'G:\\E\\E_V2RapidCoil_Duke_15T_RealWire_StrippedTip\\E_channel'+str(ii)
	mdict = {'Snapshot0':E_data_field, 'Axis0':xaxis, 'Axis1':yaxis, 'Axis2':zaxis}
	savemat(path,mdict)
	
	path = r'G:\\B1\\B1_V2RapidCoil_Duke_15T_RealWire_StrippedTip\\B1_channel'+str(ii)
	mdict = {'Snapshot0':B1_data_field, 'Axis0':xaxis, 'Axis1':yaxis, 'Axis2':zaxis}
	savemat(path,mdict)	
	
		
getsar = em_sensor_extractor.Outputs["SAR(x,y,z,f0)"]
getld = em_sensor_extractor.Outputs["El. Loss Density(x,y,z,f0)"]
getE = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]
	
getsar.Update()
getld.Update()
getE.Update()
			
sar = getsar.Data
ld = getld.Data
EData= getE.Data
			
EField= EData.Field(0)
			
rho = ld.Field(0)/sar.Field(0)
rho = [rho[x] if numpy.isnan(rho[x]) == False else 0 for x in range(len(rho))]
rho = numpy.asarray(rho)

path = r'G:\\Mask Properties\\V2RapidCoil_Duke_15T_RealWire_StrippedTip'
mdict = {'rho':rho}
savemat(path,mdict)	

