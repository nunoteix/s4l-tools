# -*- coding: utf-8 -*-

allsims = s4l.simulation.GetSimulations()
for ii in range(numpy.size(allsims)):
	print(allsims[ii].Name)
sources = ['ES1  (coil 1)', 'ES2  (Coil 2)', 'ES3  (Coil 3)', 'ES4  (Coil 4)']
B_all = numpy.zeros([4,181,173,82,3],complex)
multiport = 1	
for ii in range (4):
	results = allsims[ii+3].Results()
	if(multiport ==1):
		resultstemp = allsims[2].Results()
		results = resultstemp.GetSensor(resultstemp.keys()[ii])
	sensor = results.GetSensor('Overall Field')
	fieldclass = sensor.GetData('B(x,y,z,f0)')
	p = 0
	for jj in range(numpy.size(sources)):
		ptemp = results.GetSensor(sources[jj]).GetData('EM P(f)').GetComponent(0)
		p = p+numpy.abs(ptemp.real)
	field = fieldclass.Field(0)
	if(ii==0):
		grid = fieldclass.Grid
	new_shape = (fieldclass.Grid.Dimensions[0]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[2]-1)
	B_all[ii,:,:,:,:] = field.reshape(list(new_shape)+[field.shape[1]])/numpy.sqrt(p)
	###power norm?
	
#pVector = numpy.asarray([1,1j,-1,-1j])/4.
pVector = numpy.asarray([0.247916,0.250582,0.249353,0.252149 ])
Bcombined = numpy.zeros(B_all.shape[1:5],complex)
for ii in range(numpy.size(pVector)):
	Bcombined = Bcombined+numpy.sqrt(numpy.abs(pVector[ii]))*B_all[ii,:,:,:,:]*numpy.exp(1j*numpy.angle(pVector[ii]))

Bnew = numpy.zeros((numpy.size(Bcombined[:,:,:,0]),3),complex)
Bnew[:,0] = Bcombined[:,:,:,0].flatten()
Bnew[:,1] = Bcombined[:,:,:,1].flatten()
Bnew[:,2] = Bcombined[:,:,:,2].flatten()
B1p = .5*(Bnew[:,0]+1j*Bnew[:,1])
B1m = .5*numpy.conj(Bnew[:,0]-1j*Bnew[:,1])
rotbfield = XPostProcessor.ComplexDoubleFieldData()
rotbfield.Grid = grid
rotbfield.ValueLocation = XPostProcessor.eValueLocation.kCellCenter
rotbfield.NumberOfComponents = 2
rotbfield.NumberOfSnapshots = 1
rotbfield.SetField( 0, numpy.asarray(zip(B1p,B1m)) )
producer3 = XPostProcessor.TrivialProducer()
producer3.SetDataObject(rotbfield)
producer3.Description = 'Rotating B-Field'
reg = XPostProcessor.AlgorithmRegistry()
reg.AddAlgorithm(producer3)
