# -*- coding: utf-8 -*-
import s4l_v1 as s4l
import scipy.io
import numpy
import scipy.linalg
from numpy import linalg as LA
import numpy.matlib

def AverageOverQ(W,cube):
	xl = cube.Lower[0]
	xu = cube.Upper[0]
	yl = cube.Lower[1]
	yu = cube.Upper[1]
	zl = cube.Lower[2]
	zu = cube.Upper[2]
	return sum(sum(sum(W*P[zl:zu+1,yl:yu+1,xl:xu+1,:,:],0),0),0)/.01

def getWeights(cube):
	W = zeros((3,2))
	LL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[0])
	LU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[7])
	for ii in range(3):
		W[ii,0] = (LU-cube.Point0)[ii]/(LU-LL)[ii]
	UL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[0])
	UU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[7])
	for ii in range(3):
		W[ii,1] = (cube.Point1-UL)[ii]/(UU-UL)[ii]

	xw =[W[0,0]]+list(numpy.ones(cube.Upper[0]-cube.Lower[0]-1))+[W[0,1]]
	yw =[W[1,0]]+list(numpy.ones(cube.Upper[1]-cube.Lower[1]-1))+[W[1,1]]	
	zw =[W[2,0]]+list(numpy.ones(cube.Upper[2]-cube.Lower[2]-1))+[W[2,1]]
	W = numpy.meshgrid(zw,yw,xw,indexing='ij')		
	W = W[0]*W[1]*W[2]
	W8x8 = zeros(list(W.shape)+[8]+[8])
	for ii in range(8):
		for jj in range(8):
			W8x8[:,:,:,ii,jj] = W
	return W8x8

path =  r'D:\\Data\\SSAD - 1st Part\\P_Matrices\\8FracD.npy'	
P = numpy.load(path)

sim = s4l.simulation.GetSimulations()[0]
multresults = sim.Results()
results = multresults.GetSimulation(multresults.keys()[0])
fieldclass = results.GetSensor('SensBox')
grid = fieldclass.GetData('EM E(x,y,z,f0)').Grid

import EmPostPro
cubes, lossyrho_fd, labels_fd = EmPostPro.ConstMassCubesGenerator.Generate(grid, target_mass_kg=1E-2, enable_uncertainty_region=False)
print(len(cubes))
Qavg = zeros([len(cubes)]+list(shape(P)[1:]),complex)

P = P.reshape(list((grid.ZAxis.size-1,grid.YAxis.size-1,grid.XAxis.size-1))+[8]+[8])
for ii in range(len(cubes)):
	print(ii)
	if (cubes[ii].Lower[2]!=cubes[ii].Upper[2] and cubes[ii].Lower[1]!=cubes[ii].Upper[1] and cubes[ii].Lower[0]!=cubes[ii].Upper[0]):
		#Qavg[cubes[ii].ReferenceCellId,:,:] = AverageOverQ(getWeights(cubes[ii]),cubes[ii])
		Qavg[ii,:,:] = AverageOverQ(getWeights(cubes[ii]),cubes[ii])

LUT = numpy.zeros(len(cubes))
for ii in range(len(cubes)):
	LUT[ii] = cubes[ii].ReferenceCellId
		
path =  r'D:\\Data\\SSAD - 1st Part\\8FracD\\Q10g'
path2 =  r'D:\\Data\\SSAD - 1st Part\\8FracD\\Grid'			
numpy.save(path,Qavg)
import scipy.io
mdict = {'Q10g':Qavg, 'LUT':LUT}
scipy.io.savemat(path,mdict)
scipy.io.savemat(path2,{'GRID':grid})