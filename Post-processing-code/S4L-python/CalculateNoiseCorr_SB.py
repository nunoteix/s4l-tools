# -*- coding: utf-8 -*-
import s4l_v1 as s4l
from scipy.io import savemat  
import s4l_v1.simulation.emfdtd as fdtd
import numpy as np
import sys, os
from pylab import *
from scipy import interpolate

sims = list(s4l.document.AllSimulations)

kk = 0 # Select which simulation no. you want (starting from 0 on top of the list)			# CHANGE SIM
sim = sims[kk]
results = sim.Results()



nports = 24 #Select amount of ports you want to extract										# CHANGE NO. PORTS

# Extract initial results to calculate conductivity

result_ini = results.GetSimulation(results.keys()[0])
overall_field_sensor_ini = result_ini['Sens Box']
# overall_field_sensor_ini = result_ini['SensBox'] 											# CHANGE SENSOR
# overall_field_sensor_ini = result_ini['Overall Field'] 
fieldclass_ini = overall_field_sensor_ini.GetData('EM E(x,y,z,f0)')
Efield_ini = fieldclass_ini.Field(0)
ld_ini = overall_field_sensor_ini.GetData('El. Loss Density(x,y,z,f0)')
Ex_ini = Efield_ini[:,0]
Ey_ini = Efield_ini[:,1]
Ez_ini = Efield_ini[:,2]
Eabs_ini = (numpy.abs(numpy.sqrt(Ex_ini*Ex_ini.conj()+Ey_ini*Ey_ini.conj()+Ez_ini*Ez_ini.conj()))).flatten()

term1 = 2*(ld_ini.Field(0)).flatten()
term2 = (numpy.power(Eabs_ini,2))

sig = np.divide(term1,term2)
sig = np.nan_to_num(sig) #Remove potential nans created by dividing by zero

Efield = overall_field_sensor_ini['EM E(x,y,z,f0)']
Edata_ini = Efield.Data
Egrid_ini = Edata_ini.Grid
		
xaxis = Egrid_ini.XAxis
yaxis = Egrid_ini.YAxis
zaxis = Egrid_ini.ZAxis

dx = np.diff(xaxis)
dy = np.diff(yaxis)
dz = np.diff(zaxis)

DX,DY,DZ = meshgrid(dx,dy,dz)
V_matrix = DX*DY*DZ # Matrix containing volume of every single voxel
V_matrix = np.transpose(V_matrix,(1,0,2)) #Reshape field to match shape E-field

new_shape = (Egrid_ini.Dimensions[0]-1, Egrid_ini.Dimensions[1]-1, Egrid_ini.Dimensions[2]-1)
sig_reshaped = sig.reshape((new_shape[0],new_shape[1],new_shape[2]),order='F')

R_matrix = np.zeros([nports,nports],dtype='complex') # Noise covariance matrix. Will be filled in for loop.
# P = np.zeros(nports,'complex')
for ii in range(1,nports+1): 
		
	result_ii = results.GetSimulation(results.keys()[ii-1])
	
	overall_field_sensor_ii = result_ini['Sens Box'] 
	# overall_field_sensor_ii = result_ii['SensBox'] 
	# overall_field_sensor_ii = result_ii['Overall Field']									# CHANGE SENSOR
	
	
	# input_power_sensor_ii = result_ii['Input Power']
	# Psensor = input_power_sensor_ii['EM Input Power(f)']
	# Psensor.Update()
	# P_ii = Psensor.Data.GetComponent(0)
		
		
	fieldclass_ii = overall_field_sensor_ii.GetData('EM E(x,y,z,f0)')
	Efield_ii = fieldclass_ii.Field(0)
	# Efield_norm_ii = (Efield_ii/(numpy.sqrt(P_ii)))#*2 #sqrt(4/3) #sqrt(2) # CHANGE POWER NORM
	
	Ex_ii = Efield_ii[:,0]
	Ey_ii = Efield_ii[:,1]
	Ez_ii = Efield_ii[:,2]
	
	# Extract current for every port for subsimulation ii
	# CHECK ORDER OF SUBSIMULATIONS IN SIMULATION II HERE
	# disp(result_ii.keys()[ii])
	i_ii = result_ii.GetSensor(result_ii.keys()[ii]).GetData('EM I(f)').GetComponent(0)   	 # CHECK SIM ORDER HERE ii+1/ii

	
	for jj in range(1,nports+1):
		#overall_field_sensor = result['Overall Field'] # Extract overall field sensor
		result_jj = results.GetSimulation(results.keys()[jj-1])
		
		
		
		# overall_field_sensor_jj = result_jj['Overall Field'] 
		# overall_field_sensor_jj = result_jj['SensBox']										# CHANGE SENSOR
		overall_field_sensor_jj = result_ini['Sens Box'] 
		
		
		# input_power_sensor_jj = result_jj['Input Power']
		# Psensor = input_power_sensor_jj['EM Input Power(f)']
		# Psensor.Update()
		# P_jj = Psensor.Data.GetComponent(0)
		
		
	#Psensor = input_power_sensor['EM Input Power(f)']
	#Psensor.Update()
	#P = Psensor.Data.GetComponent(0)
	
		# Extract current for every port for subsimulation jj
		# CHECK ORDER OF SUBSIMULATIONS IN SIMULATION JJ HERE
		# disp(result_jj.keys()[jj])								
		i_jj = result_jj.GetSensor(result_jj.keys()[jj]).GetData('EM I(f)').GetComponent(0) 	# CHECK SIM ORDER HERE ii+1/ii
		
	
			
	#v_in = result.GetSensor(result.keys()[jj+2]).GetData('EM U(f)').GetComponent(0)
	#a = .5*(v_in/numpy.sqrt(50)+numpy.sqrt(50)*i_in)
		
		fieldclass_jj = overall_field_sensor_jj.GetData('EM E(x,y,z,f0)')
		Efield_jj = fieldclass_jj.Field(0)
		# Efield_norm_jj = (Efield_ii/(numpy.sqrt(P_jj)))#*2 #sqrt(4/3) #sqrt(2) #CHANGE POWER NORM
		Ex_jj = Efield_jj[:,0]
		Ey_jj = Efield_jj[:,1]
		Ez_jj = Efield_jj[:,2]
	
		E_combi = (Ex_ii.conj()*Ex_jj+Ey_ii.conj()*Ey_jj+Ez_ii.conj()*Ez_jj).flatten()
		
		E_combi_shaped = E_combi.reshape((new_shape[0],new_shape[1],new_shape[2]),order='F')
		
		R = 1/(i_ii*i_jj)*np.sum((sig_reshaped*E_combi_shaped*V_matrix).flatten()) # Calculate noise covariance
		
		R_matrix[ii-1,jj-1] = R[0] #Fill ii,jjth entry of noise covariance matrix
		
		

C_matrix = np.zeros([nports,nports],dtype='complex')
for ii in range(0,nports):
	for jj in range(0,nports):
		C_matrix[ii,jj] = R_matrix[ii,jj]/(np.sqrt(R_matrix[ii,ii])*np.sqrt(R_matrix[jj,jj])) # Calculate noise correlation from noise covariance

del fieldclass_jj, Efield_jj, Ex_jj, Ey_jj, Ez_jj, result_jj, Efield_ii, Ex_ii, Ey_ii, Ez_ii, result_ii, overall_field_sensor_ii, overall_field_sensor_jj
del fieldclass_ii, result_ini, overall_field_sensor_ini, fieldclass_ini, Efield_ini, Ex_ini, Ey_ini, Ez_ini, ld_ini
# path = r'D:\\Data\\SSAD - 1st Part\\Correlation Matrices\\' # Path your exporting to		#CHANGE PATHWAY
# path = r'D:\\Data\\Loops - 2nd Part\\Correlation Matrices\\' # Path your exporting to
# path = r'D:\\Data\\Frac - 3rd Part\\Correlation Matrices\\' # Path your exporting to
path = r'D:\\Data\\DUKE - 4th Part\\Correlation Matrices\\' # Path your exporting to
mdict = {'C_matrix':C_matrix, 'R_matrix':R_matrix}
ext = '.mat'
# timestamp = datetime.datetime.now().strftime("%Y%m%d%I%M")
savemat(path+'C_matrix'+' '+'24xSmallFracD_onDUKE'+ext,mdict)







