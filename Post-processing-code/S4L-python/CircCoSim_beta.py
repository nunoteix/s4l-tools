'''
	put this in F:\Documents\Sim4Life\1.3\Scripts
'''
import XCore
import XCoreMath
import XCoreUI
import XPostProcessor
import s4l_v1 as s4l
import numpy
import scipy.optimize
import scipy.linalg
import scipy.io
class CoSimulationSettings(XCore.PropertyGroup):
	def __init__(self):
	
		#self._S4ch = (0.2489-0.0935*1j,-0.2740+0.2379*1j,-0.0673+0.0571*1j,-0.2426+0.2983*1j,-0.2437+0.2361*1j,-0.1591+0.1871*1j,-0.1895+0.3240*1j,-0.1012-0.0861*1j,-0.0785+0.0446*1j,-0.2835+0.2951*1j,0.1702-0.0953*1j,-0.1977+0.3383*1j,-0.3097+0.1984*1j,-0.0472-0.0924*1j,-0.2442+0.2748*1j,0.0631+0.2304*1j)
		#self._S4ch = (0.1472 - 0.0877*1j,  -0.2341 + 0.3011*1j,  -0.0983 + 0.0437*1j,  -0.2281 + 0.3214*1j, -0.2495 + 0.3150*1j,  -0.1570 + 0.1767*1j,  -0.2257 + 0.2670*1j,  -0.0959 - 0.0129*1j, -0.1058 + 0.0474*1j,  -0.2353 + 0.2738*1j,   0.2449 - 0.1682*1j,  -0.2404 + 0.2813*1j, -0.2321 + 0.3297*1j,  -0.1074 - 0.0158*1j,-0.2283 + 0.2694*1j,0.0997 + 0.2627*1j)
		#self._S4ch = (0.0757 + 0.0593*1j,  -0.2563 - 0.0032*1j,  -0.1794 + 0.1593*1j,  -0.3429 + 0.1909*1j, -0.2545 + 0.0104*1j,  -0.5672 + 0.1248*1j,  -0.2398 - 0.0732*1j,  -0.0269 - 0.0434*1j, -0.1516 + 0.1478*1j,  -0.2062 - 0.0663*1j,   0.1209 + 0.2681*1j,  -0.3173 + 0.1853*1j, -0.3711 + 0.1988*1j,  -0.0241 - 0.0490*1j,  -0.4017 + 0.2074*1j, 0.2583 + 0.0126*1j)
		#self._S4ch = (   0.0585 - 0.02541j,  -0.3224 + 0.18781j,  -0.2210 + 0.10591j,  -0.2995 + 0.23831j, -0.3304 + 0.17341j,  -0.1454 + 0.04191j,  -0.3578 + 0.15151j,  -0.0462 - 0.11161j, -0.1953 + 0.09781j,  -0.3054 + 0.15051j,   0.1266 + 0.09721j,  -0.2726 + 0.21961j, -0.3445 + 0.22881j,  -0.0465 - 0.12021j,  -0.3508 + 0.24361j,   0.2300 - 0.02861j,)
		self._S4ch =   (-0.0764-0.1370*1j, -0.2277+0.2334*1j, -0.1802+0.1569*1j, -0.2569+0.2924*1j, -0.2318+0.2235*1j, -0.3111-0.0610*1j, -0.2672+0.2310*1j, -0.0350-0.0310*1j, -0.1746+0.1620*1j, -0.2527+0.2434*1j, 0.0325-0.0103*1j, -0.2850+0.3097*1j, -0.2482+0.3095*1j, -0.0227-0.0194*1j, -0.2851+0.3159*1j, 0.1196-0.1712*1j)
		
		XCore.PropertyGroup.__init__(self)
		self.Description = "Optimization Settings"
		# self._psAddress = XCore.PropertyString("crespo@speag.com")
		# self.Add("EmailAddress", self._psAddress)
		# self._psAddress.Description = "Email"
		
		self._starget = None
		self._groups = None
		
		self._niter = XCore.PropertyInt(200)
		self.Add("niter", self._niter)
		self._niter.Description = "Number of Iterations"
		
		self._capvec = XCore.PropertyBool(False)
		self.Add("capvec", self._capvec)
		self._capvec.Description = "Use saved cap values"
		self._capvec.Unit =XCore.Unit.Farad
		
		# self._psNL = XCore.PropertyInt(24)
		# self.Add("NL", self._psNL)
		# self._psNL.Description = "Number of Lumped Elements"
		
		self._psZ0 = XCore.PropertyComplex(50 + 0 * 1j)
		self.Add("Z0", self._psZ0)
		self._psZ0.Description = "Reference Impedance"
		self._psZ0.Unit =XCore.Unit.Ohm
		
		self._psR = XCore.PropertyReal(0.0)
		self.Add("R", self._psR)
		self._psR.Description = "Equivalent Series Resistance of Lumped Elements"
		self._psR.Unit =XCore.Unit.Ohm
		
		# self._psL = XCore.PropertyReal(0)
		# self.Add("L", self._psL)
		# self._psL.Description = "Inductance"
		
		self._psFreq = XCore.PropertyReal(298e6)
		self.Add("SelFreq", self._psFreq)
		self._psFreq.Description = "Frequency for Optimization"		
		self._psFreq.Unit = XCore.Unit.Hertz
		self._psFreq2 = XCore.PropertyReal(298e6)
		self.Add("SelFreq2", self._psFreq2)
		self._psFreq2.Description = "Frequency for Combiner"		
		self._psFreq2.Unit = XCore.Unit.Hertz
		
		self._psPort = XCore.PropertyEnum([],0)
		self.Add("Port1", self._psPort)
		self._psPort.Description = "Port #"
		self._psPort.Visible = False
		#self.Foo0 = XCore.PropertyInt()
		#self.Foo1 = XCore.PropertyComplex()
		#self.Foo2 = XCore.PropertyReal()
		#self.Foo3 = XCoreMath.PropertyVec3()
		#self.Foo4 = XCore.PropertyRealTuple([1,2,3,5])
		#self.Foo5 = XCore.PropertyMultilineString()
		#self.Foo6 = XCore.PropertyGroup()
		#self.Foo6.Child0 = XCore.PropertyFile()
		
		self._psNP = XCore.PropertyInt(2)
		self.Add("NP", self._psNP)
		self._psNP.Description = "Number of Ports"
		self._psNP.OnModified.Connect(self.callback)
		self._psNG = XCore.PropertyInt(1)
		self.Add("NG", self._psNG)
		self._psNG.Description = "Number of Groups"
		self._psNG.OnModified.Connect(self.callback)
		#self.callback1( prop=None, mod=XCore.PropertyModificationTypeEnum.kPropertyModified)
		self.callback( prop=None, mod=XCore.PropertyModificationTypeEnum.kPropertyModified)
	def callback1(self, prop, mod):
		_groups = XCore.PropertyGroup()
		_groups.Description = "Port Group Assignment"
		if self._groups is not None:
			self._groups.Lose()
		self._groups = _groups
		self.Add("groups",self._groups)
		for ii in range(numpy.size(self._psPort.Values.keys())):
			if ii<self._psNP.Value:
				self._groups._g = XCore.PropertyEnum([],ii)
			else:
				self._groups._g = XCore.PropertyEnum([],self._psNP.Value)
			for jj in range(self._psNP.Value+self._psNG.Value):
				if jj<self._psNP.Value:
					self._groups._g.Insert(jj,'Drive Port ' + str(jj+1))
				else:
					self._groups._g.Insert(jj,'Capacitor Group ' + str(jj+1-self._psNP.Value))
			self._groups.Add(str(self._psPort.Values.get(ii)),self._groups._g)
	def callback(self, prop, mod):
		print prop
		if mod == XCore.PropertyModificationTypeEnum.kPropertyModified:
			self.callback1( prop=None, mod=XCore.PropertyModificationTypeEnum.kPropertyModified)
			_starget = XCore.PropertyGroup()
			_starget.Description = "Target S Matrix"
			if self._starget is not None:
				self._starget.Lose()
			self._starget = _starget
			self.Add("starget",self._starget)
			for ii in range(self._psNP.Value):
				for jj in range(self._psNP.Value):
					self._starget._S = XCore.PropertyComplex(self._S4ch[ii*4+jj])
					#self._starget._S = XCore.PropertyComplex(0)
					self._starget.Add("S" + str(ii + 1) + str(jj + 1),self._starget._S)
		
class CircuitCoSim(XPostProcessor.AlgorithmBase):
	class_id = XCore.Uuid("45d48621-c7c8-11e4-8830-0800200c9a66")
	def __init__(self):
		XPostProcessor.AlgorithmBase.__init__(self, 1, 2)
		self.Initialize(CircuitCoSim.class_id)
		self.Description = "CircuitCoSim"
		self.VariableNumberOfInputConnections = False
		options = CoSimulationSettings()
		self.Options = options
		self._scattering = None
		self._ak = None
	def CheckInputConnections(self, inputs):
		print "CheckInputConnections", inputs
		return inputs[0].Data.Description == 'Scattering Sij(f)'
	def UpdateOutputAttributes(self):
		print "UpdateOutputAttributes"
		input = self.GetInputConnection(0)
		np = self.Options.NP.Value
		nl = input.Data.Shape[0] - np
		self._scattering = XPostProcessor.ComplexFloatXYData()
		self._scattering.Axis = input.Data.Axis
		self._scattering.Allocate(self._scattering.Axis.size,[np * np])
		#self._scattering.Description = 'Scattering Sij(f)'
		
		self._ak = XPostProcessor.ComplexFloatXYData()
		self._ak.Allocate(np + nl,[np])
		if numpy.size(self.Options.Port1.Values.values())==0:
			NG = self.Options.NG.Value
			for ii in range(input.Producer.Children[1].Size):
				self.Options.Port1.Insert(ii,input.Producer.Children[1].Children[ii].Children[0].ValueDescription)
			self.Options.NG.Value = 0
			self.Options.NG.Value = NG
		return True
	def ComputeOutputData(self, index):
		print "ComputeOutputData"
		input = self.GetInputConnection(0)
		np = self.Options.NP.Value
		z0 = self.Options.Z0.Value
		R = self.Options.R.Value
		# L = self.Options.L.Value
		L = 0
		selFreq = self.Options.SelFreq.Value
		selFreq2 = self.Options.SelFreq2.Value
		niter = self.Options.niter.Value
		portCapInds = []
		for ii in range(self.Options.NG.Value):
			portCapInds.append([])
		count = 0
		for ii in range(self.Options.groups.Size):
			if self.Options.groups.Children[ii].Value >= np:
				portCapInds[self.Options.groups.Children[ii].Value-np].append(ii-count)
			else:
				count = count+1
		#portCapInds = [1],[0,2,3,4,5,6,7,8,9,10,11],[13],[12,14,15,16,17,18,19,20,21,22,23]
		#portCapInds =
		#[1,13,25,37],[0,2,3,4,5,6,7,8,9,10,11,12,14,15,16,17,18,19,20,21,22,23,24,26,27,28,29,30,31,32,33,34,35,36,38,39,40,41,42,43,44,45,46,47]
		
		S_target = numpy.zeros([np,np],complex)
		for ii in range(np):
			for jj in range(np):
				S_target[ii,jj] = self.Options.starget.Children[ii * np + jj].Value
		
		S_do = input.Data
		freqs = S_do.Axis.tolist()
		if selFreq < freqs[-1]:
			selFreq = numpy.round(numpy.maximum(selFreq-freqs[0],0) / (freqs[1]-freqs[0]))*(freqs[1]-freqs[0])+freqs[0]
		else:
			selFreq = freqs[-1]
		nl = S_do.Shape[0] - np
		S = numpy.zeros([np + nl,np + nl,numpy.size(freqs)],complex)
		for ii in range(np + nl):
			for jj in range(np + nl):
				S[ii,jj,:] = S_do.GetComponent(ii * (np + nl) + jj)
		order = []
		for ii in range(np):
			x = [x for x in range(np+nl) if self.Options.groups.Children[x].Value==ii]
			order.append(x.pop(0))
		for ii in range(np+nl):
			if ii not in order:
				order.append(ii)
		Snew = numpy.zeros([np + nl,np + nl,numpy.size(freqs)],complex)
		for ind1,ii in enumerate(order):
			for ind2,jj in enumerate(order):
				Snew[ind1,ind2,:] = S[ii,jj,:]
		S = Snew
		del Snew
		y0mat = scipy.linalg.inv(numpy.diagflat(numpy.sqrt(z0) * numpy.ones([1,nl])))
		capValsOut = numpy.zeros([niter,numpy.size(portCapInds)])
		costOut = numpy.zeros([niter,1])
		bnds = []
		bnds0 = [0,None]
		for x in range(numpy.size(portCapInds)):
			bnds.append(bnds0)
			
		if(self.Options.capvec.Value == False):
			for ii in range(niter):
				print(ii)
				test = 1
				while(test == 1):
					test = 0
					capValsInit = (15e-12 * numpy.random.rand(numpy.size(portCapInds))).tolist()
					xopt = scipy.optimize.minimize(optimfun_circ, numpy.array(capValsInit), args = (portCapInds,S,y0mat,R,L,np,nl,freqs,selFreq,S_target),bounds=bnds,method='Nelder-Mead')
					for k in range(numpy.size(xopt.x)):
						if(xopt.x[k] <= 0):
							test = 1
				capValsOut[ii] = xopt.x
				costOut[ii] = xopt.fun
			print(capValsOut[numpy.argmin(costOut)])
			Ctemp = capValsOut[numpy.argmin(costOut)]
			for ii in range(niter):
				print(ii)
				test = 1
				while(test == 1):
					test = 0
					capValsInit = Ctemp + numpy.random.random_integers(-1,1,numpy.size(Ctemp))*numpy.random.rand(numpy.size(Ctemp))*.25*Ctemp
					xopt = scipy.optimize.minimize(optimfun_circ, capValsInit, args = (portCapInds,S,y0mat,R,L,np,nl,freqs,selFreq,S_target),bounds=bnds,method='Nelder-Mead')
					for k in range(numpy.size(xopt.x)):
						if(xopt.x[k] <= 0):
							test = 1
				capValsOut[ii] = xopt.x
				costOut[ii] = xopt.fun
			for ii in range(niter):
				print(ii)
				test = 1
				while(test == 1):
					test = 0
					capValsInit = Ctemp + numpy.random.random_integers(-1,1,numpy.size(Ctemp))*numpy.random.rand(numpy.size(Ctemp))*.1*Ctemp
					xopt = scipy.optimize.minimize(optimfun_circ, capValsInit, args = (portCapInds,S,y0mat,R,L,np,nl,freqs,selFreq,S_target),bounds=bnds,method='Nelder-Mead')
					for k in range(numpy.size(xopt.x)):
						if(xopt.x[k] <= 0):
							test = 1
				capValsOut[ii] = xopt.x
				costOut[ii] = xopt.fun
			print(capValsOut[numpy.argmin(costOut)])
			optCapVals = capValsOut[numpy.argmin(costOut)]
			path = r'D:\\ZMT Laptop\\ZMT Documents\\exported fields\\'
			mdict = {'capValsOut':optCapVals, 'portCapInds':portCapInds}
			ext = '.mat'
			scipy.io.savemat(path+'CapValsOut'+ext,mdict)
		else:
			path = r'D:\\ZMT Laptop\\ZMT Documents\\exported fields\\'
			Mat = scipy.io.loadmat(path+'CapValsOut',appendmat=True)
			optCapVals = Mat.get('capValsOut').flatten()
			portCapInds = Mat.get('portCapInds')
			portCapInds = portCapInds.flatten()
			for ii in range(portCapInds.size):
				portCapInds[ii] = portCapInds[ii].flatten()
		S_out = genS(optCapVals,portCapInds,S,y0mat,R,L,np,nl,freqs,selFreq)
		path = r'D:\\ZMT Laptop\\ZMT Documents\\exported fields\\'
		mdict = {'S_out':S_out[:,:,freqs.index(selFreq)]}
		ext = '.mat'
		scipy.io.savemat(path+'Scosim'+ext,mdict)
		a_k = genFieldCombineWeights(optCapVals,portCapInds,S,y0mat,R,L,np,nl,freqs,selFreq2)
		self._scattering.Axis = freqs
		self._scattering.Allocate(self._scattering.Axis.size,[np * np])
		for ii in range(np):
			for jj in range(np):
				self._scattering.SetComponent(ii * np + jj,S_out[ii,jj,:].astype(numpy.complex64))
		print(numpy.min(costOut))
		
		for ii in range(np):
			self._ak.SetComponent(ii,a_k[:,ii].astype(numpy.complex64))
		return True
	def GetOutputData(self, index):
		# print "GetOutputData", index
		return self._ak if index == 1 else self._scattering
	def GetInputSMatrix(self):
		input = self.GetInputConnection(0)
		S_do = input.Data
		return S_do
		
XCore.RegisterClass(CircuitCoSim, CircuitCoSim.class_id)
# Needed to refresh UI
XPostProcessor.RebuildAlgorithmCache()
def optimfun_circ(ValsInit,portCapInds,S,y0mat,R,L,nPorts,nLumped,freqs,selFreq,S_target):
	ind = freqs.index(selFreq)
	S_ports = S[0:nPorts,0:nPorts,ind]
	S_lumped = S[nPorts:nPorts + nLumped,nPorts:nPorts + nLumped,ind]
	S_right = S[0:nPorts,nPorts:nPorts + nLumped,ind]
	S_left = S[nPorts:nPorts + nLumped,0:nPorts,ind]
	
	capValsInit = ValsInit[0:numpy.size(portCapInds)]
	#R = ValsInit[numpy.size(portCapInds):]
	capvec = R + 1 / (1j * 2 * numpy.pi * selFreq * capValsInit) + (1j * 2 * numpy.pi * selFreq * L)
	capvec = capvec.flatten()
	capvec_full = numpy.zeros([1,nLumped],complex)
	capvec_full = capvec_full.flatten()
	for ii in range(numpy.size(portCapInds)):
		for jj in range(numpy.size(portCapInds[ii])):
			capvec_full[portCapInds[ii][jj]] = capvec[ii]
	z_cap = numpy.diagflat(capvec_full)
	sigma = numpy.dot(scipy.linalg.inv(numpy.dot(numpy.dot(y0mat,z_cap),y0mat) + numpy.eye(nLumped)),numpy.dot(numpy.dot(y0mat,z_cap),y0mat) - numpy.eye(nLumped))
	S_out = S_ports + numpy.dot(S_right,numpy.dot(sigma,numpy.dot(scipy.linalg.inv(numpy.eye(nLumped) - numpy.dot(S_lumped,sigma)),S_left)))
	#cost = scipy.linalg.norm(numpy.diag(S_out))
	# max = 0
	# for ii in range(int(numpy.shape(S_out)[0])):
		# for jj in range(int(numpy.shape(S_out)[1])):
			# if(ii != jj):
				# if(numpy.abs(S_out[ii][jj]) > max):
					# max = numpy.abs(S_out[ii][jj])
	# cost = cost + max
	cost = scipy.linalg.norm(S_out- S_target)
	return cost
	
def genS(ValsInit,portCapInds,S,y0mat,R,L,nPorts,nLumped,freqs, selFreq):
	S_out = numpy.zeros([nPorts,nPorts,numpy.size(freqs)],complex)
	for kk in range(numpy.size(freqs)):
		f = freqs[kk]
		S_ports = S[0:nPorts,0:nPorts,kk]
		S_lumped = S[nPorts:nPorts + nLumped,nPorts:nPorts + nLumped,kk]
		S_right = S[0:nPorts,nPorts:nPorts + nLumped,kk]
		S_left = S[nPorts:nPorts + nLumped,0:nPorts,kk]
	
		capValsInit = ValsInit[0:numpy.size(portCapInds)]
		#R = ValsInit[numpy.size(portCapInds):]
		capvec = R + 1 / (1j * 2 * numpy.pi * f * capValsInit) + (1j * 2 * numpy.pi * f * L)
		capvec = capvec.flatten()
		capvec_full = numpy.zeros([1,nLumped],complex)
		capvec_full = capvec_full.flatten()
		for ii in range(numpy.size(portCapInds)):
			for jj in range(numpy.size(portCapInds[ii])):
				capvec_full[portCapInds[ii][jj]] = capvec[ii]
		z_cap = numpy.diagflat(capvec_full)
		sigma = numpy.dot(scipy.linalg.inv(numpy.dot(numpy.dot(y0mat,z_cap),y0mat) + numpy.eye(nLumped)),numpy.dot(numpy.dot(y0mat,z_cap),y0mat) - numpy.eye(nLumped))
		S_out[:,:,kk] = S_ports + numpy.dot(S_right,numpy.dot(sigma,numpy.dot(scipy.linalg.inv(numpy.eye(nLumped) - numpy.dot(S_lumped,sigma)),S_left)))
	return S_out
	
def genFieldCombineWeights(ValsInit,portCapInds,S,y0mat,R,L,nPorts,nLumped,freqs,selFreq):
	ind = freqs.index(selFreq)
	S_ports = S[0:nPorts,0:nPorts,ind]
	S_lumped = S[nPorts:nPorts + nLumped,nPorts:nPorts + nLumped,ind]
	S_right = S[0:nPorts,nPorts:nPorts + nLumped,ind]
	S_left = S[nPorts:nPorts + nLumped,0:nPorts,ind]
	
	capValsInit = ValsInit[0:numpy.size(portCapInds)]
	#R = ValsInit[numpy.size(portCapInds):]
	capvec = R + 1 / (1j * 2 * numpy.pi * selFreq * capValsInit) + (1j * 2 * numpy.pi * selFreq * L)
	capvec = capvec.flatten()
	capvec_full = numpy.zeros([1,nLumped],complex)
	capvec_full = capvec_full.flatten()
	for ii in range(numpy.size(portCapInds)):
		for jj in range(numpy.size(portCapInds[ii])):
			capvec_full[portCapInds[ii][jj]] = capvec[ii]
	z_cap = numpy.diagflat(capvec_full)
	sigma = numpy.dot(scipy.linalg.inv(numpy.dot(numpy.dot(y0mat,z_cap),y0mat) + numpy.eye(nLumped)),numpy.dot(numpy.dot(y0mat,z_cap),y0mat) - numpy.eye(nLumped))
	a_k = numpy.zeros([nLumped + nPorts, nPorts],complex)
	a = numpy.zeros([nPorts,1])
	for ii in range(nPorts):
		a = numpy.zeros([nPorts,1])
		a[ii] = 1
		a_k[0:nPorts,ii] = a.flatten()
		a_k[nPorts:nPorts + nLumped,ii] = (numpy.dot(numpy.dot(sigma,numpy.dot(scipy.linalg.inv(numpy.eye(nLumped) - numpy.dot(S_lumped,sigma)),S_left)),a)).flatten()
	return a_k
	
def combineEfields(S,a_k,excitation,Efield_all,p):
	a = numpy.dot(a_k,excitation)
	E = numpy.zeros(numpy.shape(Efield_all)[1:5])
	for ii in range(int(numpy.shape(S)[0])):
		# anorm = 2*p[ii]/(1-S[ii,ii]*S[ii,ii].conj()) ## 2 because RMS
		E = E + a[ii] * Efield_all[ii,:,:,:,:] ## normalizes to 1W RMS power
	return E

