import numpy
import s4l_v1.analysis as analysis
import s4l_v1.document as document
import s4l_v1.model as model
import s4l_v1.units as units
from s4l_v1 import ReleaseVersion
from s4l_v1 import Unit

mult = 0
port_name = 'Feed 1'
sim_tree = 'PW Channel 1 Phantom_1.75RInsu_ThinCore'
sens_name = 'SensBox Phantom'

ReleaseVersion.set_active(ReleaseVersion.version4_4)

# Adding a new SimulationExtractor
simulation = document.AllSimulations[sim_tree]
simulation_extractor = simulation.Results()

if mult:
	inputs = [simulation_extractor.Outputs[sim_tree+" - "+port_name]]
	simulation_extractor = analysis.extractors.EmPortSimulationExtractor(inputs=inputs)
	simulation_extractor.Name = sim_tree+" - "+port_name
	simulation_extractor.UpdateAttributes()
	document.AllAlgorithms.Add(simulation_extractor)

em_sensor_extractor = simulation_extractor[sens_name] 
em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
em_sensor_extractor.Normalization.Normalize = True
########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
document.AllAlgorithms.Add(em_sensor_extractor)
	
# Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
ELD = em_sensor_extractor.Outputs["El. Loss Density(x,y,z,f0)"]
E = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]

ELD.Update()
E.Update()
	
ELDData = ELD.Data
EData= E.Data

EField= EData.Field(0)
ELDDist = ELDData.Field(0)

Ex = EField[:,0]
Ey = EField[:,1]
Ez = EField[:,2]

xaxis = ELDData.Grid.XAxis; xm = (xaxis[0:-1]+xaxis[1:])/2
yaxis = ELDData.Grid.YAxis; ym = (yaxis[0:-1]+yaxis[1:])/2
zaxis = ELDData.Grid.ZAxis; zm = (zaxis[0:-1]+zaxis[1:])/2

sz = [zaxis.shape[0]-1, yaxis.shape[0]-1, xaxis.shape[0]-1]

Eabs = (numpy.abs(numpy.sqrt(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj()))).flatten()

sig = 2*(ELDDist.flatten()/(numpy.power(Eabs,2)))
# sig = [sig[x] if numpy.isnan(sig[x]) == False else 0 for x in range(len(sig))]
sig = numpy.asarray(sig)

sig_r = numpy.reshape(sig, sz)
wire_vox = numpy.argwhere( numpy.isnan(sig_r) == True)


sig = [sig[x] if numpy.isnan(sig[x]) == False else 0 for x in range(len(sig))]
sig_r = numpy.reshape(sig, sz)

sig_r = numpy.around(sig_r,7)

sig_cubes = [sig_r[wire_vox[x,0]-1:wire_vox[x,0]+2, wire_vox[x,1]-1:wire_vox[x,1]+2, wire_vox[x,2]-1:wire_vox[x,2]+2] for x in range(len(wire_vox))]

trigger = 0
issue_counter = 0
tip_counter = 0
for vv in range(len(sig_cubes)):
	sig_counts = 27 - list(sig_cubes[vv].flatten()).count(0)
	if sig_counts > 0 and sig_counts <= 1:
		print('Wire touches Dielectric on: '+str(wire_vox[vv].tolist()).strip('[]'))
		# break
		issue_counter += 1
	# if sig_cubes[vv].flatten().any() > 0:
		# trigger = 1
	elif sig_counts > 1:
		print('Wire touches Dielectric on: '+str(wire_vox[vv].tolist()).strip('[]')+' , but it might be wiretip, checking further')
		tip_counter += 1
	# if trigger == 1:
		# print('Wire touches Dielectric on: '+str(wire_vox[vv].tolist()).strip('[]'))
		# break
print('Sweep returned: '+str(tip_counter)+' Potential Tip Points and '+str(issue_counter)+' Issue Points.')
		
	
	




# unique_sig_ra = numpy.unique(siga)

# for x in unique_sig:
	# print x
	
# sig_test = sig_r
# sig_test[wire_vox] = 10;
#########
# sig_viewer = ELD
# sig_viewer.Data.SetField(0,sig)


####Adding a new SliceFieldViewer
# inputs = [sig_viewer]
# slice_field_viewer = analysis.viewers.SliceFieldViewer(inputs=inputs)
# slice_field_viewer.Data.Mode = slice_field_viewer.Data.Mode.enum.QuantityRealPart
# slice_field_viewer.Slice.Plane = slice_field_viewer.Slice.Plane.enum.YZ
# slice_field_viewer.Slice.Index = 113
# slice_field_viewer.UpdateAttributes()
# document.AllAlgorithms.Add(slice_field_viewer)







