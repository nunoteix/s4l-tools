import s4l_v1 as s4l
from scipy.io import savemat 
from scipy.io import loadmat 
import s4l_v1.simulation.emfdtd as fdtd
import numpy as np
import sys, os
from pylab import *

sims = s4l.document.AllSimulations

ii = 0 # Select which simulation no. you want (starting from 0 on top of the list)
sim = sims[ii]
multresults = sim.Results()
order = multresults.keys()
nports = 8 #Select amount of ports you want to extract


results = multresults.GetSimulation(order[ii])
overall_field_sensor = results['SensBox']
F1 = overall_field_sensor['B1(x,y,z,f0)'] # CHANGE FIELD YOU WANT TO EXTRACT <--------------------------------------------------
F1data = F1.Data # Contains grid and field (and other info)
F1.Update() #?
F1field = F1data.Field(0)
F1grid = F1data.Grid # Select grid data
xaxis = F1grid.XAxis
yaxis = F1grid.YAxis
zaxis = F1grid.ZAxis

savedShape = F1field.shape
# del results
# del overall_field_sensor
# del B1
# del B1data
# del B1field
# del B1grid

def getF(jj,multresults):
	results = multresults.GetSimulation(order[jj])
	
	input = results['Input Power']
	Psensor = input['EM Input Power(f)']	
	Psensor.Update()
	P = max(Psensor.Data.GetComponent(0))
	
	overall_field_sensor = results['SensBox']
	F1 = overall_field_sensor['B1(x,y,z,f0)']  # CHANGE FIELD YOU WANT TO EXTRACT <-------------------------------------------
	F1.Update() #?
	F1c = (F1.Data.Field(0))/np.sqrt(P)

	return F1c

for jj in range(0,nports):
	F1c = getF(jj,multresults)
	
	path1 = r'G:\\B1\\B1_RapidArray_heartDuke_8p\\' # Path your exporting to
	# path1 = r'G:\\TRASH FIELDS - DELETE AFTER USE IN MATLAB\\E_Hydra_headDuke_64p_50dB_betterSkin_noNorm\\'
	# ^ CHANGE PATH DIRECTION^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ <-----------------------------------------------------------------
	mdict = {'Snapshot0':F1c, 'Axis0':xaxis, 'Axis1':yaxis, 'Axis2':zaxis}
	ext = '.mat'
	savemat(path1+'B1_channel'+str(jj+1)+ext,mdict,True)  # CHANGE NAME OF CHANGED VARIABLE <----------------------------------
	
