import s4l_v1 as s4l
import scipy.io
import numpy
import scipy.linalg
from numpy import linalg as LA
import numpy.matlib
from scipy.io import savemat 
from scipy.io import loadmat 

	
def Average(W,cube,E):
	xl = cube.Lower[0]
	xu = cube.Upper[0]
	yl = cube.Lower[1]
	yu = cube.Upper[1]
	zl = cube.Lower[2]
	zu = cube.Upper[2]
	return sum(sum(sum(W*E[zl:zu+1,yl:yu+1,xl:xu+1],0),0),0)/cube.Volume
	
def getWeights(cube):
	W = zeros((3,2))
	LL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[0])
	LU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[7])
	for ii in range(3):
		W[ii,0] = (LU-cube.Point0)[ii]/(LU-LL)[ii]
	UL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[0])
	UU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[7])
	for ii in range(3):
		W[ii,1] = (cube.Point1-UL)[ii]/(UU-UL)[ii]

	xw =[W[0,0]]+list(numpy.ones(cube.Upper[0]-cube.Lower[0]-1))+[W[0,1]]
	yw =[W[1,0]]+list(numpy.ones(cube.Upper[1]-cube.Lower[1]-1))+[W[1,1]]	
	zw =[W[2,0]]+list(numpy.ones(cube.Upper[2]-cube.Lower[2]-1))+[W[2,1]]
	W = numpy.meshgrid(zw,yw,xw,indexing='ij')		
	W = W[0]*W[1]*W[2]
	W3 = zeros(list(W.shape))
	W3[:,:,:] = W
	return W3

def getSigRhoVol(multresults,order2):
	results = multresults[order2[0]]
	# order = results.keys()

	sensor = results['SensBox']

	fieldclass = sensor['EM E(x,y,z,f0)']
	data = fieldclass.Data
	fieldclass.Update()
	EField = data.Field(0)

	grid = data.Grid

	getsar = sensor['SAR(x,y,z,f0)']
	getld = sensor['El. Loss Density(x,y,z,f0)']
	sar = getsar.Data
	ld = getld.Data
	getsar.Update()
	getld.Update()
	
	
	rho = ld.Field(0)/sar.Field(0)
	rho = [rho[x] if numpy.isnan(rho[x]) == False else 0 for x in range(len(rho))]
	rho = numpy.asarray(rho)
	Ex = EField[:,0]
	Ey = EField[:,1]
	Ez = EField[:,2]
	Eabs = (numpy.abs(numpy.sqrt(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj()))).flatten()
	sig = 2*(ld.Field(0)).flatten()/(numpy.power(Eabs,2))
	# mask = mask = [1 if x>0.005 else 0 for x in sig]
	# mask = numpy.asarray(mask)
	x = grid.XAxis
	y = grid.YAxis
	z = grid.ZAxis

	volmesh = numpy.meshgrid(numpy.diff(z),numpy.diff(y),numpy.diff(x), indexing='ij')
	vol = volmesh[0].flatten()*volmesh[1].flatten()*volmesh[2].flatten()
	vol = numpy.asarray(vol)
	
	return (sig, rho, vol)



### WRIGHT SHIM VALUES ##############	
shim = [1.0000 + 0.0000j,
		0.9745 - 0.2243j,
		0.9898 + 0.1422j,
		0.5189 + 0.8548j,
		-0.6674 - 0.7447j,
		-0.9982 + 0.0607j,
		-0.9266 + 0.3761j,
		-0.9982 + 0.0595j]
	
#####################################

sim = s4l.simulation.GetSimulations()[0]									#### CHANGE SIM HERE
print(sim.Name)
multresults = sim.Results()
order2 = multresults.keys()

(sig, rho, vol) = getSigRhoVol(multresults, order2)

Eall = numpy.zeros(list(sig.shape)+[3]+[8], 'complex')
for ii in xrange(0,8):
	mat = loadmat(r'G:\E for S4L Q-script\E_Hydra_headDuke_64p_50dB_betterSkin_noNorm\E_1uT_channel'+str(ii+1))
	Eall[:,:,ii] = mat.get(mat.keys()[5])
	# Eall[:,:,ii] = Eall[:,:] + Etemp
	
Q = numpy.zeros((list(sig.shape)+[8]+[8]),'complex')

for i in range(Eall.shape[0]):
		if sig[i] != 0.0:
			e = Eall[i]
			Q[i] = (sig[i]/(2*rho[i]))*numpy.dot(e.conj().transpose(),e)#*vol[i]
			del e

path = r'G:\\Q-Matrices\\Hydra_headDuke_64p_50dB_betterSkin_noNorm'
mdict = {'Q-Matrix':Q}
scipy.io.savemat(path,mdict)