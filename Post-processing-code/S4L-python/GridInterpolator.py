import numpy
import s4l_v1.analysis as analysis
import s4l_v1.document as document
import s4l_v1.model as model
import s4l_v1.units as units
from s4l_v1 import ReleaseVersion
from s4l_v1 import Unit
from scipy.interpolate import interpn

# Define the version to use for default values
ReleaseVersion.set_active(ReleaseVersion.version4_4)

sim_type = 'Huygens '#''#''#'Main ' #
dielectric = 'Phantom' #'Duke'#
rfc_descriptor = 'SC' #'PW' #'LC'# 
wire_descriptor =  'Straight'#'Realistic' #'Test' #'' # 
wire_model = ''#'_HighRes'#'_CoreFine0.2' #'_ThickInsu'# '_ThickTip' #'_Thick' #'_Thin' #'_ThinError' #'_ThickTipEdge'#'_ThickTipIn' #   

coilno = 8 #1 # 
sensorno =36 # 15 # 
sim_freq = 64e6

prime_path = 'G:\\Huygens Simulations\\'+dielectric+'\\Extracted Results'
# prime_path = 'G:\\Wire Voxelization\\'+dielectric+'\\Extracted Results'
sim_name = '\\RapidCoils_'+dielectric+'_Guidewire'+wire_model+'_2020_01_06_'
# sim_name = '\\RapidCoils_'+dielectric+'_'+wire_model+'_'
# sim_name = '\\PlaneWave_'+dielectric+'_Testwire'+wire_model+'_'

# Creating the analysis pipeline
# Adding a new SimulationExtractor
for cn in range(1,coilno+1):
	simulation = document.AllSimulations[sim_type+rfc_descriptor+" Channel "+str(cn)+" "+dielectric+wire_model]
	simulation_extractor = simulation.Results()
	
	em_sensor_extractor = simulation_extractor[sim_type+'SensBox '+dielectric] 
	em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
	em_sensor_extractor.Normalization.Normalize = True
	########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)
		
	# Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
	E = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]
	E.Update()
	EData = E.Data
	EField = EData.Field(0)
		
	if cn == 1:
		Eall = numpy.zeros(list(EField.shape)+[8], 'complex')
	Eall[:,:,cn-1] = EField
	print(['Loaded E-Field from Coil #'+str(cn)])
	
	
SAR = em_sensor_extractor.Outputs["SAR(x,y,z,f0)"]
ELD = em_sensor_extractor.Outputs["El. Loss Density(x,y,z,f0)"]
	
SAR.Update()
ELD.Update()

SARData = SAR.Data
ELDData = ELD.Data
	
SARDist = SARData.Field(0)
ELDDist = ELDData.Field(0)
	
rho = ELDDist/SARDist
rho = numpy.asarray([rho[x] if numpy.isnan(rho[x]) == False else 0 for x in range(len(rho))])
	
	
Ex = EField[:,0]
Ey = EField[:,1]
Ez = EField[:,2]
	
xaxis = EData.Grid.XAxis
yaxis = EData.Grid.YAxis
zaxis = EData.Grid.ZAxis

xm = (xaxis[0:-1]+xaxis[1:])/2
ym = (yaxis[0:-1]+yaxis[1:])/2
zm = (zaxis[0:-1]+zaxis[1:])/2

xm_q = numpy.arange(xm[0], xm[-1], 0.0015)
ym_q = numpy.arange(ym[0], ym[-1], 0.0015)
zm_q = numpy.arange(zm[0], zm[-1], 0.0015)
m_q = numpy.meshgrid(xm_q, ym_q, zm_q)
m_qr = numpy.reshape(m_q, [3]+[xm_q.size*ym_q.size*zm_q.size]).transpose()

sz = [xaxis.shape[0]-1, yaxis.shape[0]-1, zaxis.shape[0]-1]
	
Eabs = (numpy.abs(numpy.sqrt(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj())))
sig = 2*(numpy.squeeze(ELDDist))/(numpy.power(Eabs,2))
sig = numpy.asarray([sig[x] if numpy.isnan(sig[x]) == False else 0 for x in range(len(sig))])
	
print('Starting to build Q-Matrices...')

Q_x = numpy.zeros(Eall.shape[0], 'complex')
Q_r = numpy.zeros([xm_q.size]+[ym_q.size]+[zm_q.size]+[coilno]+[coilno], 'complex')
for cn in range(0,1):
	for cnq in range(0, 1):
		for i in range(Eall.shape[0]):
			e = Eall[i]
			e_star = e.conj().transpose()
			if sig[i] != 0.0 and ~numpy.isnan(sig[i]):
				Q_x[i] = (sig[i]/(2*rho[i]))*e_star[cn,:].dot(e[:,cnq])

		Q_xr = numpy.reshape(Q_x, sz)
		Q_xi = interpn((xm,ym,zm), Q_xr, m_qr)
		Q_xri = numpy.reshape(Q_xi, [xm_q.size]+[ym_q.size]+[zm_q.size])
		Q_r[:,:,:,cn,cnq] = Q_xri;