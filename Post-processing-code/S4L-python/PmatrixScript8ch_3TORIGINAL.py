# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import s4l_v1 as s4l
import scipy.io
import numpy
import scipy.linalg
from numpy import linalg as LA


def VOPgenerator(Q10g):
	##Take the norm of all the Q matrices
	Norms = LA.norm(Q10gflat,axis=(1,2)).tolist()
	##Sort them in descending order
	SortedNorms = sorted(Norms, reverse=True)

	##First Iter
	eigs = numpy.zeros(Q10gflat.shape[0])
	temp = Q10gflat[Norms.index(SortedNorms[0]),:,:]-Q10gflat
	for i in range(Q10gflat.shape[0]):
		if LA.norm(Q10gflat[i,:,:]) != 0:
			eigs[i] = numpy.min(scipy.linalg.eigh(temp[i,:,:],eigvals_only=True))
	del temp
	eigs = eigs.tolist()
	sortedeigs = sorted(eigs)
	Zall = []
	u = .05*numpy.max(Norms)
	Z = numpy.zeros(Q10gflat.shape[1:],'complex')
	cluster = []
	cluster.append(Norms.index(SortedNorms[0]))
	for ii in range(sortedeigs.index(0)):
		E,V = scipy.linalg.eig(Q10gflat[Norms.index(SortedNorms[0])]+Z-Q10gflat[eigs.index(sortedeigs[sortedeigs.index(0)-ii-1])])
		E = E.real
		Ep = numpy.zeros(E.shape)
		for n in range(E.size):
			Ep[n] = max(0.,E[n])
		Z = Z + numpy.dot(numpy.dot(V,numpy.diagflat(Ep-E)),V.conj().T)
		if LA.norm(Z) > u:
			Z = Z - numpy.dot(numpy.dot(V,numpy.diagflat(Ep-E)),V.conj().T)
			break
	Zall.append(Z)
	print LA.norm(Z)
	if sortedeigs.index(0)-ii-1 == 0:
		print 'No more points left'
	else:
		print str(sortedeigs.index(0)-ii)+' points left'

	##Second Iter
	##Calculate Remainders
	remainder = []
	for jj in xrange(sortedeigs.index(0)-ii):
		remainder.append(eigs.index(sortedeigs[sortedeigs.index(0)-ii-jj-1]))
	Norms2 = LA.norm(Q10gflat[remainder],axis=(1,2)).tolist()
	SortedNorms = sorted(Norms2, reverse=True)
	##Compute Next Z
	eigs = numpy.zeros(numpy.size(remainder))
	temp = Q10gflat[remainder[Norms2.index(SortedNorms[0])],:,:]-Q10gflat[remainder]
	for i in range(temp.shape[0]):
		eigs[i] = numpy.min(scipy.linalg.eigh(temp[i,:,:],eigvals_only=True))
	del temp
	eigs = eigs.tolist()
	sortedeigs = sorted(eigs)
	Z = numpy.zeros(Q10gflat.shape[1:],'complex')
	cluster.append(remainder[Norms2.index(SortedNorms[0])])
	for ii in range(sortedeigs.index(0)):
		E,V = scipy.linalg.eig(Q10gflat[remainder[Norms2.index(SortedNorms[0])]]+Z-Q10gflat[remainder[eigs.index(sortedeigs[sortedeigs.index(0)-ii-1])]])
		E = E.real
		Ep = numpy.zeros(E.shape)
		for n in range(E.size):
			Ep[n] = max(0.,E[n])
		Z = Z + numpy.dot(numpy.dot(V,numpy.diagflat(Ep-E)),V.conj().T)
		if LA.norm(Z) > u:
			Z = Z - numpy.dot(numpy.dot(V,numpy.diagflat(Ep-E)),V.conj().T)
			break
	Zall.append(Z)
	print LA.norm(Z)
	if sortedeigs.index(0)-ii-1 == 0:
		print 'No more points left'
	else:
		print str(sortedeigs.index(0)-ii)+' points left'

	## Remanining Iters
	while sortedeigs.index(0)-ii-1 != 0:
		##Calculate Remainders
		remainderold = remainder
		remainder = []
		for jj in xrange(sortedeigs.index(0)-ii):
			remainder.append(remainderold[eigs.index(sortedeigs[sortedeigs.index(0)-ii-jj-1])])
		Norms2 = LA.norm(Q10gflat[remainder],axis=(1,2)).tolist()
		SortedNorms = sorted(Norms2, reverse=True)
		##Next Iters
		eigs = numpy.zeros(numpy.size(remainder))
		temp = Q10gflat[remainder[Norms2.index(SortedNorms[0])],:,:]-Q10gflat[remainder]
		for i in range(temp.shape[0]):
			eigs[i] = numpy.min(scipy.linalg.eigh(temp[i,:,:],eigvals_only=True))
		del temp
		eigs = eigs.tolist()
		sortedeigs = sorted(eigs)
		Z = numpy.zeros(Q10gflat.shape[1:],'complex')
		cluster.append(remainder[Norms2.index(SortedNorms[0])])
		for ii in range(sortedeigs.index(0)):
			E,V = scipy.linalg.eig(Q10gflat[remainder[Norms2.index(SortedNorms[0])]]+Z-Q10gflat[remainder[eigs.index(sortedeigs[sortedeigs.index(0)-ii-1])]])
			E = E.real
			Ep = numpy.zeros(E.shape)
			for n in range(E.size):
				Ep[n] = max(0.,E[n])
			Z = Z + numpy.dot(numpy.dot(V,numpy.diagflat(Ep-E)),V.conj().T)
			if LA.norm(Z) > u:
				Z = Z - numpy.dot(numpy.dot(V,numpy.diagflat(Ep-E)),V.conj().T)
				break
		Zall.append(Z)
		print LA.norm(Z)
		if sortedeigs.index(0)-ii-1 == 0:
			print 'No more points left'
			break
		else:
			print str(sortedeigs.index(0)-ii)+' points left'

	#Finish
	Aall = Q10gflat[cluster]+Zall
	return Aall

def makeEmat(e1,e2,e3,e4,e5,e6,e7,e8):
	e = numpy.zeros((3,8),'complex')
	e[0,0] = e1[0]
	e[1,0] = e1[1]
	e[2,0] = e1[2]
	e[0,1] = e2[0]
	e[1,1] = e2[1]
	e[2,1] = e2[2]
	e[0,2] = e3[0]
	e[1,2] = e3[1]
	e[2,2] = e3[2]
	e[0,3] = e4[0]
	e[1,3] = e4[1]
	e[2,3] = e4[2]
	e[0,4] = e5[0]
	e[1,4] = e5[1]
	e[2,4] = e5[2]
	e[0,5] = e6[0]
	e[1,5] = e6[1]
	e[2,5] = e6[2]
	e[0,6] = e7[0]
	e[1,6] = e7[1]
	e[2,6] = e7[2]
	e[0,7] = e8[0]
	e[1,7] = e8[1]
	e[2,7] = e8[2]
	return e

def makePmat(E1,E2,E3,E4,E5,E6,E7,E8,sig,vol):
	P = numpy.zeros((E1.shape[0],8,8),'complex')
	for i in range(E1.shape[0]):
			if sig[i] != 0.0:
				e = makeEmat(E1[i],E2[i],E3[i],E4[i],E5[i],E6[i],E7[i],E8[i])
				P[i,:,:] = (sig[i]/(2*rho[i]))*numpy.dot(e.conj().transpose(),e)*vol[i]
				del e
	return P

def AverageOverQ(W,cube):
	Qsum = zeros(shape(Q)[1:])
	CellCount = 0
	for xx in range(cube.Lower[0],cube.Upper[0]+1):
		for yy in range(cube.Lower[1],cube.Upper[1]+1):
			for zz in range(cube.Lower[2],cube.Upper[2]+1):
				Qsum = Qsum + W[xx-cube.Lower[0],yy-cube.Lower[1],zz-cube.Lower[2]]*Q[grid.ComputeCellIndex(xx,yy,zz),:,:]
				CellCount = CellCount+W[xx-cube.Lower[0],yy-cube.Lower[1],zz-cube.Lower[2]]
	return Qsum/CellCount

def getWeights(cube):
	W = zeros((3,2))
	LL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[0])
	LU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[7])
	for ii in range(3):
		W[ii,0] = (LU-cube.Point0)[ii]/(LU-LL)[ii]
	UL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[0])
	UU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[7])
	for ii in range(3):
		W[ii,1] = (cube.Point1-UL)[ii]/(UU-UL)[ii]

	xw =[W[0,0]]+list(numpy.ones(cube.Upper[0]-cube.Lower[0]-1))+[W[0,1]]
	yw =[W[1,0]]+list(numpy.ones(cube.Upper[1]-cube.Lower[1]-1))+[W[1,1]]	
	zw =[W[2,0]]+list(numpy.ones(cube.Upper[2]-cube.Lower[2]-1))+[W[2,1]]
	W = numpy.meshgrid(xw,yw,zw,indexing='ij')		
	W = W[0]*W[1]*W[2]
	return W

order = ['Match 1  (Bottom elements/Element1)', 'Match 2  (Bottom elements/Element2)', 'Match 3  (Bottom elements/Element3)', 'Match 4  (Bottom elements/Element4)', 'Match 5  (Top elements/Element5)', 'Match 6  (Top elements/Element6)', 'Match 7  (Top elements/Element7)', 'Match 8  (Top elements/Element8)']
S = numpy.zeros([8,8],'complex')
a = numpy.zeros(8,'complex')
z = numpy.zeros(8,'complex')
z0 = 50
sim = s4l.simulation.GetSimulations()[3]
print(sim.Name)
multresults = sim.Results()
order2 = multresults.keys()
for ii in range(8):
	results = multresults.GetSimulation(order2[ii])
	#order = results.keys()
	b = numpy.zeros(8,'complex')
	for jj in range(8):
		i = results.GetSensor(order[jj]).GetData('EM I(f)').GetComponent(0)
		v = results.GetSensor(order[jj]).GetData('EM U(f)').GetComponent(0)
		if order[ii]==order[jj]:
			a[ii] = .5*(v[2500]/numpy.sqrt(z0)+numpy.sqrt(z0)*i[2500])
			z[ii] = v[2500]/i[2500]
		b[jj] = .5*(v[2500]/numpy.sqrt(z0)-numpy.sqrt(z0)*i[2500])
	S[:,ii] = b/a[ii]
	sensor = results.GetSensor('Pelvic Sensor')
	fieldclass = sensor.GetData('EM E(x,y,z,f0)')
	#Bfieldclass = sensor.GetData('B(x,y,z,f0)')
	Efield = fieldclass.Field(0)
	#Bfield = Bfieldclass.Field(0)
	grid = fieldclass.Grid
	new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
	if ii == 0:
		Eall = numpy.zeros(list(Efield.shape)+[32],'complex')
		#Ball = numpy.zeros(list(new_shape)+[3]+[8],'complex64')
		sar = sensor.GetData('SAR(x,y,z,f0)')
		# mask = [1 if x>0 else 0 for x in sar.Field(0)]
		# mask = numpy.asarray(mask)
		ld = sensor.GetData('El. Loss Density(x,y,z,f0)')
		rho = ld.Field(0)/sar.Field(0)
		rho = [rho[x] if numpy.isnan(rho[x]) == False else 0 for x in range(size(rho))]
		rho = numpy.asarray(rho)
		Ex = Efield[:,0]
		Ey = Efield[:,1]
		Ez = Efield[:,2]
		Eabs = (numpy.abs(numpy.sqrt(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj()))).flatten()
		sig = 2*(ld.Field(0)).flatten()/(numpy.power(Eabs,2))
		mask = mask = [1 if x>0.005 else 0 for x in sig]
		mask = numpy.asarray(mask)
		x = grid.XAxis
		y = grid.YAxis
		z = grid.ZAxis
		del Eabs
		del ld
		del sar
		volmesh = numpy.meshgrid(numpy.diff(z),numpy.diff(y),numpy.diff(x), indexing='ij')
		vol = volmesh[0].flatten()*volmesh[1].flatten()*volmesh[2].flatten()
		vol = numpy.asarray(vol)
		
	print(.5*(a[ii]*numpy.conj(a[ii])-b[ii]*numpy.conj(b[ii])))
	Eall[:,:,ii] = Efield/(numpy.sqrt(.5*(a[ii]*numpy.conj(a[ii])-b[ii]*numpy.conj(b[ii]))))*zip(mask,mask,mask)
	#Bfield = Bfield*zip(mask,mask,mask)
	#Bfield = Bfield.reshape(list((fieldclass.Grid.ZAxis.size-1,fieldclass.Grid.YAxis.size-1,fieldclass.Grid.XAxis.size-1))+[3])
	#Ball[:,:,:,:,ii] = Bfield/a[ii]
	del Efield
	#del Bfield
	del fieldclass
	del sensor
	del i
	del v
	del results
	
	
P = makePmat(Eall[:,:,0],Eall[:,:,1],Eall[:,:,2],Eall[:,:,3],Eall[:,:,4],Eall[:,:,5],Eall[:,:,6],Eall[:,:,7],sig,vol)
del Eall
path = r'D:\\ZMT Laptop\\ZMT Documents\\exported fields\\3TDipolesOnDuke\\P'
numpy.save(path, P)

# del Eall

# import EmPostPro
# cubes, lossyrho_fd, labels_fd = EmPostPro.ConstMassCubesGenerator.Generate(grid, target_mass_kg=1E-2, enable_uncertainty_region=False)
# print(len(cubes))
# Qavg = zeros([len(cubes)]+list(shape(Q)[1:]),complex)

# for ii in range(len(cubes)):
	# print(ii)
	# if (cubes[ii].Lower[2]!=cubes[ii].Upper[2] and cubes[ii].Lower[1]!=cubes[ii].Upper[1] and cubes[ii].Lower[0]!=cubes[ii].Upper[0]):
		##Qavg[cubes[ii].ReferenceCellId,:,:] = AverageOverQ(getWeights(cubes[ii]),cubes[ii])
		# Qavg[ii,:,:] = AverageOverQ(getWeights(cubes[ii]),cubes[ii])
		
# VOPs = VOPgenerator(Qavg)

# import scipy.io
# path = r'D:\\ZMT Laptop\\ZMT Documents\\exported fields\\'
# mdict = {'Eall':Eall,'S':S, 'x_axis':x,'y_axis':y,'z_axis':z, 'a':a}
# scipy.io.savemat(path+'DipoleArrayFields_TopElement',mdict, do_compression=True)