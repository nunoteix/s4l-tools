def Do(sn, freq):
	import numpy
	import s4l_v1.analysis as analysis
	import s4l_v1.document as document
	import s4l_v1.model as model
	import s4l_v1.units as units
	from s4l_v1 import ReleaseVersion
	from s4l_v1 import Unit
	
	
	# Define the version to use for default values
	ReleaseVersion.set_active(ReleaseVersion.version3_4)
	
	# Creating the analysis pipeline
	# Adding a new SimulationExtractor
	simulation = document.AllSimulations["800Feng Uncapped"]
	simulation_extractor = simulation.Results()
	
	sensor = model.AllEntities()["HRS "+str(sn)]
	em_sensor_extractor = simulation_extractor["HRS "+str(sn)]

	pos = sensor.Properties.Transformation.Children[2].Value
	
	x = pos[0]
	y = pos[1]
	z = pos[2]
	

	em_sensor_extractor.Normalization.Normalize = True
	em_sensor_extractor.Normalization.AvailableReferences = u"HRS 1 / EM I(f)"
	em_sensor_extractor.Normalization.NewReferenceValue = 1.0, units.Amperes
	document.AllAlgorithms.Add(em_sensor_extractor)
	
	inputs = [em_sensor_extractor.Outputs["EM I(f)"]]
	plot_viewer = analysis.viewers.PlotViewer(inputs=inputs)
	# plot_viewer.Plotter.ComplexOptions = plot_viewer.Plotter.ComplexOptions.enum.Mag
	# plot_viewer.Visible = True
	plot_viewer.UpdateAttributes()
	document.AllAlgorithms.Add(plot_viewer)
	# em_sensor_extractor.UpdateAttributes()
	plot_viewer.Update()
	
	# inputs = em_sensor_extractor.Outputs["EM I(f)"]
	a = inputs[0]
	# a.Update()
	# a.UpdateAttributes()
	# JData = a.Data.GetComponent(0)[freq_ind]
	JFreqs = a.Data.Axis

	freq_ind = JFreqs.searchsorted(freq)
	# J = JData[freq_ind]	
	J = a.Data.GetComponent(0)[freq_ind]
	
	plot_viewer.Visible = False
	
	return J, x, y, z

import numpy
import s4l_v1.analysis as analysis

sensorno = 161
J = numpy.zeros((sensorno,), dtype = 'complex')
Axis = numpy.zeros((3,sensorno))
freq = 64e6


for sn in range(1,sensorno+1):
	[oJ, x, y, z] = Do(sn, freq)
	Axis[:,sn-1] = [x,y,z]
	J[sn-1] = oJ


from scipy.io import savemat
path = r'G:\\Transfer Functions\\Feng Paper Replication\\Current_HR_Uncapped_Dielectric_NormTest'
mdict = {'J':J, 'Axes':Axis}
savemat(path,mdict)	
