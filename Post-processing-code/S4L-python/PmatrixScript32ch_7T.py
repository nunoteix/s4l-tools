# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import s4l_v1 as s4l
import scipy.io
import numpy
import scipy.linalg
from numpy import linalg as LA


def VOPgenerator(Q10g):
	##Take the norm of all the Q matrices
	Norms = LA.norm(Q10gflat,axis=(1,2)).tolist()
	##Sort them in descending order
	SortedNorms = sorted(Norms, reverse=True)

	##First Iter
	eigs = numpy.zeros(Q10gflat.shape[0])
	temp = Q10gflat[Norms.index(SortedNorms[0]),:,:]-Q10gflat
	for i in range(Q10gflat.shape[0]):
		if LA.norm(Q10gflat[i,:,:]) != 0:
			eigs[i] = numpy.min(scipy.linalg.eigh(temp[i,:,:],eigvals_only=True))
	del temp
	eigs = eigs.tolist()
	sortedeigs = sorted(eigs)
	Zall = []
	u = .05*numpy.max(Norms)
	Z = numpy.zeros(Q10gflat.shape[1:],'complex')
	cluster = []
	cluster.append(Norms.index(SortedNorms[1]))
	for ii in range(sortedeigs.index(0)):
		E,V = scipy.linalg.eig(Q10gflat[Norms.index(SortedNorms[0])]+Z-Q10gflat[eigs.index(sortedeigs[sortedeigs.index(0)-ii-1])])
		E = E.real
		Ep = numpy.zeros(E.shape)
		for n in range(E.size):
			Ep[n] = max(0.,E[n])
		Z = Z + numpy.dot(numpy.dot(V,numpy.diagflat(Ep-E)),V.conj().T)
		if LA.norm(Z) > u:
			Z = Z - numpy.dot(numpy.dot(V,numpy.diagflat(Ep-E)),V.conj().T)
			break
	Zall.append(Z)
	print LA.norm(Z)
	if sortedeigs.index(0)-ii-1 == 0:
		print 'No more points left'
	else:
		print str(sortedeigs.index(0)-ii)+' points left'

	##Second Iter
	##Calculate Remainders
	remainder = []
	for jj in xrange(sortedeigs.index(0)-ii):
		remainder.append(eigs.index(sortedeigs[sortedeigs.index(0)-ii-jj-1]))
	Norms2 = LA.norm(Q10gflat[remainder],axis=(1,2)).tolist()
	SortedNorms = sorted(Norms2, reverse=True)
	##Compute Next Z
	eigs = numpy.zeros(numpy.size(remainder))
	temp = Q10gflat[remainder[Norms2.index(SortedNorms[0])],:,:]-Q10gflat[remainder]
	for i in range(temp.shape[0]):
		eigs[i] = numpy.min(scipy.linalg.eigh(temp[i,:,:],eigvals_only=True))
	del temp
	eigs = eigs.tolist()
	sortedeigs = sorted(eigs)
	Z = numpy.zeros(Q10gflat.shape[1:],'complex')
	cluster.append(remainder[Norms2.index(SortedNorms[0])])
	for ii in range(sortedeigs.index(0)):
		E,V = scipy.linalg.eig(Q10gflat[remainder[Norms2.index(SortedNorms[0])]]+Z-Q10gflat[remainder[eigs.index(sortedeigs[sortedeigs.index(0)-ii-1])]])
		E = E.real
		Ep = numpy.zeros(E.shape)
		for n in range(E.size):
			Ep[n] = max(0.,E[n])
		Z = Z + numpy.dot(numpy.dot(V,numpy.diagflat(Ep-E)),V.conj().T)
		if LA.norm(Z) > u:
			Z = Z - numpy.dot(numpy.dot(V,numpy.diagflat(Ep-E)),V.conj().T)
			break
	Zall.append(Z)
	print LA.norm(Z)
	if sortedeigs.index(0)-ii-1 == 0:
		print 'No more points left'
	else:
		print str(sortedeigs.index(0)-ii)+' points left'

	## Remanining Iters
	while sortedeigs.index(0)-ii-1 != 0:
		##Calculate Remainders
		remainderold = remainder
		remainder = []
		for jj in xrange(sortedeigs.index(0)-ii):
			remainder.append(remainderold[eigs.index(sortedeigs[sortedeigs.index(0)-ii-jj-1])])
		Norms2 = LA.norm(Q10gflat[remainder],axis=(1,2)).tolist()
		SortedNorms = sorted(Norms2, reverse=True)
		##Next Iters
		eigs = numpy.zeros(numpy.size(remainder))
		temp = Q10gflat[remainder[Norms2.index(SortedNorms[0])],:,:]-Q10gflat[remainder]
		for i in range(temp.shape[0]):
			eigs[i] = numpy.min(scipy.linalg.eigh(temp[i,:,:],eigvals_only=True))
		del temp
		eigs = eigs.tolist()
		sortedeigs = sorted(eigs)
		Z = numpy.zeros(Q10gflat.shape[1:],'complex')
		cluster.append(remainder[Norms2.index(SortedNorms[0])])
		for ii in range(sortedeigs.index(0)):
			E,V = scipy.linalg.eig(Q10gflat[remainder[Norms2.index(SortedNorms[0])]]+Z-Q10gflat[remainder[eigs.index(sortedeigs[sortedeigs.index(0)-ii-1])]])
			E = E.real
			Ep = numpy.zeros(E.shape)
			for n in range(E.size):
				Ep[n] = max(0.,E[n])
			Z = Z + numpy.dot(numpy.dot(V,numpy.diagflat(Ep-E)),V.conj().T)
			if LA.norm(Z) > u:
				Z = Z - numpy.dot(numpy.dot(V,numpy.diagflat(Ep-E)),V.conj().T)
				break
		Zall.append(Z)
		print LA.norm(Z)
		if sortedeigs.index(0)-ii-1 == 0:
			print 'No more points left'
			break
		else:
			print str(sortedeigs.index(0)-ii)+' points left'

	#Finish
	Aall = Q10gflat[cluster]+Zall
	return Aall

def makeEmat(e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12,e13,e14,e15,e16,e17,e18,e19,e20,e21,e22,e23,e24,e25,e26,e27,e28,e29,e30,e31,e32):
	e = numpy.zeros((3,32),'complex')
	for ii in range(0,2):
		e[ii,0] = e1[ii]
		e[ii,1] = e2[ii]
		e[ii,2] = e3[ii]
		e[ii,3] = e4[ii]
		e[ii,4] = e5[ii]
		e[ii,5] = e6[ii]
		e[ii,6] = e7[ii]
		e[ii,7] = e8[ii]
		e[ii,8] = e9[ii]
		e[ii,9] = e10[ii]
		e[ii,10] = e11[ii]
		e[ii,11] = e12[ii]
		e[ii,12] = e13[ii]
		e[ii,13] = e14[ii]
		e[ii,14] = e15[ii]
		e[ii,15] = e16[ii]
		e[ii,16] = e17[ii]
		e[ii,17] = e18[ii]
		e[ii,18] = e19[ii]
		e[ii,19] = e20[ii]
		e[ii,20] = e21[ii]
		e[ii,21] = e22[ii]
		e[ii,22] = e23[ii]
		e[ii,23] = e24[ii]
		e[ii,24] = e25[ii]
		e[ii,25] = e26[ii]
		e[ii,26] = e27[ii]
		e[ii,27] = e28[ii]
		e[ii,28] = e29[ii]
		e[ii,29] = e30[ii]
		e[ii,30] = e31[ii]
		e[ii,31] = e32[ii]


	return e

# CHOOSE APPROPRIATE PMAT #

# def makePmat(E1,E2,E3,E4,E5,E6,E7,E8,E9,E10,E11,E12,E13,E14,E15,E16,E17,E18,E19,E20,E21,E22,E23,E24,E25,E26,E27,E28,E29,E30,E31,E32,sig,vol):
	# P = numpy.zeros((E1.shape[0],32,32),'complex')
	# for i in range(E1.shape[0]):
			# if sig[i] != 0.0:
				# e = makeEmat(E1[i],E2[i],E3[i],E4[i],E5[i],E6[i],E7[i],E8[i],E9[i],E10[i],E11[i],E12[i],E13[i],E14[i],E15[i],E16[i],E17[i],E18[i],E19[i],E20[i],E21[i],E22[i],E23[i],E24[i],E25[i],E26[i],E27[i],E28[i],E29[i],E30[i],E31[i],E32[i])
				# P[i,:,:] = (sig[i]/(2*rho[i]))*numpy.dot(e.conj().transpose(),e)*vol[i]
				# del e
	# return P
	
def makePmat(E1,E2,E3,E4,E5,E6,E7,E8,E9,E10,E11,E12,E13,E14,E15,E16,E17,E18,E19,E20,E21,E22,E23,E24,E25,E26,E27,E28,E29,E30,E31,E32,sig,vol):
	P = numpy.zeros((E1.shape[0],32,32),'complex')
	for i in range(E1.shape[0]):
			if sig[i] != 0.0:
				e = makeEmat(E1[i],E2[i],E3[i],E4[i],E5[i],E6[i],E7[i],E8[i],E9[i],E10[i],E11[i],E12[i],E13[i],E14[i],E15[i],E16[i],E17[i],E18[i],E19[i],E20[i],E21[i],E22[i],E23[i],E24[i],E25[i],E26[i],E27[i],E28[i],E29[i],E30[i],E31[i],E32[i])
				P[i,:,:] = (sig[i]/(2*rho[i]))*numpy.dot(e.conj().transpose(),e)*vol[i]
				del e
	return P

def AverageOverQ(W,cube):
	Qsum = zeros(shape(Q)[1:])
	CellCount = 0
	for xx in range(cube.Lower[0],cube.Upper[0]+1):
		for yy in range(cube.Lower[1],cube.Upper[1]+1):
			for zz in range(cube.Lower[2],cube.Upper[2]+1):
				Qsum = Qsum + W[xx-cube.Lower[0],yy-cube.Lower[1],zz-cube.Lower[2]]*Q[grid.ComputeCellIndex(xx,yy,zz),:,:]
				CellCount = CellCount+W[xx-cube.Lower[0],yy-cube.Lower[1],zz-cube.Lower[2]]
	return Qsum/CellCount

def getWeights(cube):
	W = zeros((3,2))
	LL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[0])
	LU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[7])
	for ii in range(3):
		W[ii,0] = (LU-cube.Point0)[ii]/(LU-LL)[ii]
	UL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[0])
	UU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[7])
	for ii in range(3):
		W[ii,1] = (cube.Point1-UL)[ii]/(UU-UL)[ii]

	xw =[W[0,0]]+list(numpy.ones(cube.Upper[0]-cube.Lower[0]-1))+[W[0,1]]
	yw =[W[1,0]]+list(numpy.ones(cube.Upper[1]-cube.Lower[1]-1))+[W[1,1]]	
	zw =[W[2,0]]+list(numpy.ones(cube.Upper[2]-cube.Lower[2]-1))+[W[2,1]]
	W = numpy.meshgrid(xw,yw,zw,indexing='ij')		
	W = W[0]*W[1]*W[2]
	return W

order = ['P01', 'P02', 'P03', 'P04',
		 'P05', 'P06', 'P07', 'P08',
		 'P09', 'P10', 'P11', 'P12',
		 'P13', 'P14', 'P15', 'P16', 
		 'P17', 'P18', 'P19', 'P20',
		 'P21', 'P22', 'P23', 'P24',
		 'P25', 'P26', 'P27', 'P28',
		 'P29', 'P30', 'P31', 'P32',]
S = numpy.zeros([32,32],'complex')
a = numpy.zeros(32,'complex')
z = numpy.zeros(32,'complex')
z0 = 50
sim = s4l.simulation.GetSimulations()[0]
print(sim.Name)
multresults = sim.Results()
order2 = multresults.keys()
for ii in range(32):
	results = multresults.GetSimulation(order2[ii])
	#order = results.keys()
	b = numpy.zeros(32,'complex')
	for jj in range(32):
		i = results.GetSensor(order[jj]).GetData('EM I(f)').GetComponent(0)
		v = results.GetSensor(order[jj]).GetData('EM U(f)').GetComponent(0)
		if order[ii]==order[jj]:
			a_temp = .5*(v/numpy.sqrt(z0)+i*numpy.sqrt(z0))
			a[ii] = a_temp[0]
			z_temp = v/i
			z[ii] = z_temp[0]
		b_temp = .5*(v/numpy.sqrt(z0)-numpy.sqrt(z0)*i)
		b[ii] = b_temp[0]
	S[:,ii] = b/a[ii]
	sensor = results.GetSensor('SensBox Box')
	fieldclass = sensor.GetData('EM E(x,y,z,f0)')
	#Bfieldclass = sensor.GetData('B(x,y,z,f0)')
	Efield = fieldclass.Field(0)
	#Bfield = Bfieldclass.Field(0)
	grid = fieldclass.Grid
	new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
	if ii == 0:
		Eall = numpy.zeros(list(Efield.shape)+[32],'complex')
		#Ball = numpy.zeros(list(new_shape)+[3]+[8],'complex64')
		sar = sensor.GetData('SAR(x,y,z,f0)')
		# mask = [1 if x>0 else 0 for x in sar.Field(0)]
		# mask = numpy.asarray(mask)
		ld = sensor.GetData('El. Loss Density(x,y,z,f0)')
		rho = ld.Field(0)/sar.Field(0)
		rho = [rho[x] if numpy.isnan(rho[x]) == False else 0 for x in range(size(rho))]
		rho = numpy.asarray(rho)
		Ex = Efield[:,0]
		Ey = Efield[:,1]
		Ez = Efield[:,2]
		Eabs = (numpy.abs(numpy.sqrt(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj()))).flatten()
		sig = 2*(ld.Field(0)).flatten()/(numpy.power(Eabs,2))
		mask = mask = [1 if x>0.005 else 0 for x in sig]
		mask = numpy.asarray(mask)
		x = grid.XAxis
		y = grid.YAxis
		z = grid.ZAxis
		del Eabs
		del ld
		del sar
		volmesh = numpy.meshgrid(numpy.diff(z),numpy.diff(y),numpy.diff(x), indexing='ij')
		vol = volmesh[0].flatten()*volmesh[1].flatten()*volmesh[2].flatten()
		vol = numpy.asarray(vol)
		
	print(.5*(a[ii]*numpy.conj(a[ii])-b[ii]*numpy.conj(b[ii])))
	Eall[:,:,ii] = Efield/(numpy.sqrt(.5*(a[ii]*numpy.conj(a[ii])-b[ii]*numpy.conj(b[ii]))))*zip(mask,mask,mask)
	#Bfield = Bfield*zip(mask,mask,mask)
	#Bfield = Bfield.reshape(list((fieldclass.Grid.ZAxis.size-1,fieldclass.Grid.YAxis.size-1,fieldclass.Grid.XAxis.size-1))+[3])
	#Ball[:,:,:,:,ii] = Bfield/a[ii]
	del Efield
	#del Bfield
	del fieldclass
	del sensor
	del i
	del v
	del results
	
	
	
# CHOOSE APPROPRIATE makePmat	
# P = makePmat(Eall[:,:,0],Eall[:,:,1],Eall[:,:,2],Eall[:,:,3],Eall[:,:,4],Eall[:,:,5],Eall[:,:,6],Eall[:,:,7],Eall[:,:,8],
			 # Eall[:,:,9],Eall[:,:,10],Eall[:,:,11],Eall[:,:,12],Eall[:,:,13],Eall[:,:,14],Eall[:,:,15],Eall[:,:,16],Eall[:,:,17],
			 # Eall[:,:,18],Eall[:,:,19],Eall[:,:,20],Eall[:,:,21],Eall[:,:,22],Eall[:,:,23],Eall[:,:,24],Eall[:,:,25],Eall[:,:,26],
			 # Eall[:,:,27],Eall[:,:,28],Eall[:,:,29],Eall[:,:,30],Eall[:,:,31],sig,vol)

# P = makePmat(Eall[0:Eall.shape[0]/2+50,:,0],Eall[0:Eall.shape[0]/2+50,:,1],Eall[0:Eall.shape[0]/2+50,:,2],
			 # Eall[0:Eall.shape[0]/2+50,:,3],Eall[0:Eall.shape[0]/2+50,:,4],Eall[0:Eall.shape[0]/2+50,:,5],
			 # Eall[0:Eall.shape[0]/2+50,:,6],Eall[0:Eall.shape[0]/2+50,:,7],Eall[0:Eall.shape[0]/2+50,:,8],
			 # Eall[0:Eall.shape[0]/2+50,:,9],Eall[0:Eall.shape[0]/2+50,:,10],Eall[0:Eall.shape[0]/2+50,:,11],
			 # Eall[0:Eall.shape[0]/2+50,:,12],Eall[0:Eall.shape[0]/2+50,:,13],Eall[0:Eall.shape[0]/2+50,:,14],
			 # Eall[0:Eall.shape[0]/2+50,:,15],Eall[0:Eall.shape[0]/2+50,:,16],Eall[0:Eall.shape[0]/2+50,:,17],
			 # Eall[0:Eall.shape[0]/2+50,:,18],Eall[0:Eall.shape[0]/2+50,:,19],Eall[0:Eall.shape[0]/2+50,:,20],
			 # Eall[0:Eall.shape[0]/2+50,:,21],Eall[0:Eall.shape[0]/2+50,:,22],Eall[0:Eall.shape[0]/2+50,:,23],
			 # Eall[0:Eall.shape[0]/2+50,:,24],Eall[0:Eall.shape[0]/2+50,:,25],Eall[0:Eall.shape[0]/2+50,:,26],
			 # Eall[0:Eall.shape[0]/2+50,:,27],Eall[0:Eall.shape[0]/2+50,:,28],Eall[0:Eall.shape[0]/2+50,:,29],
			 # Eall[0:Eall.shape[0]/2+50,:,30],Eall[0:Eall.shape[0]/2+50,:,31],sig[0:Eall.shape[0]/2+50],vol[0:Eall.shape[0]/2+50])
			 
P = makePmat(Eall[Eall.shape[0]/2-50:Eall.shape[0],:,0],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,1],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,2],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,3],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,4],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,5],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,6],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,7],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,8],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,9],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,10],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,11],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,12],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,13],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,14],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,15],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,16],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,17],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,18],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,19],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,20],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,21],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,22],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,23],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,24],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,25],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,26],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,27],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,28],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,29],
			 Eall[Eall.shape[0]/2-50:Eall.shape[0],:,30],Eall[Eall.shape[0]/2-50:Eall.shape[0],:,31],
			 sig[Eall.shape[0]/2-50:Eall.shape[0]],vol[Eall.shape[0]/2-50:Eall.shape[0]])
# del Eall

# path = r'D:\\Data\\SSAD - 1st Part\\P_Matrices\\32DipsCloserZXTHIN_AIR'
# path = r'D:\\Data\\SSAD - 1st Part\\P_Matrices\\32DipsCloserZXTHIN_AIR_1'
path = r'D:\\Data\\SSAD - 1st Part\\P_Matrices\\32DipsCloserZXTHIN_AIR_2'
numpy.save(path, P)
del P


# import EmPostPro
# cubes, lossyrho_fd, labels_fd = EmPostPro.ConstMassCubesGenerator.Generate(grid, target_mass_kg=1E-2, enable_uncertainty_region=False)
# print(len(cubes))
# Qavg = zeros([len(cubes)]+list(shape(Q)[1:]),complex)

# for ii in range(len(cubes)):
	# print(ii)
	# if (cubes[ii].Lower[2]!=cubes[ii].Upper[2] and cubes[ii].Lower[1]!=cubes[ii].Upper[1] and cubes[ii].Lower[0]!=cubes[ii].Upper[0]):
		##Qavg[cubes[ii].ReferenceCellId,:,:] = AverageOverQ(getWeights(cubes[ii]),cubes[ii])
		# Qavg[ii,:,:] = AverageOverQ(getWeights(cubes[ii]),cubes[ii])
		
# VOPs = VOPgenerator(Qavg)

# import scipy.io
# path = r'D:\\ZMT Laptop\\ZMT Documents\\exported fields\\'
# mdict = {'Eall':Eall,'S':S, 'x_axis':x,'y_axis':y,'z_axis':z, 'a':a}
# scipy.io.savemat(path+'DipoleArrayFields_TopElement',mdict, do_compression=True)