# Voxelize all simulations in the simulation tree 
#
# Author: 	Alexander Raaijmakers (AXR)
# Date:		11.01.2015
# Email:	a.raaijmakers@umcutrecht.nl
#
# UMC Utrecht, Dept. Radiology, 7 Tesla Group, The Netherlands

import s4l_v1 as s4l

print "Start"

sims =  s4l.simulation.GetSimulations() # extract all simulations

# Loop over all simulations	
for s in sims:	
   # Print name of the simulation
   print 'Creating voxels for simulation:'
   print s.Name
   s.CreateVoxels()
   
   