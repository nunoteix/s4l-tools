import s4l_v1 as s4l
import scipy.io
import numpy
import scipy.linalg
from numpy import linalg as LA
import numpy.matlib
from scipy.io import savemat 
from scipy.io import loadmat 
import gc
	

for ii in xrange(1,9):
	simulation = document.AllSimulations["Huygens Channel "+str(ii)] ##### <--- Rapid HUYGENS
	simulation_extractor = simulation.Results()

	########## # Adding a new EmSensorExtractor
	em_sensor_extractor = simulation_extractor["sBox"] ############## <--- Select for HUYGENS
		
	# inputs = [simulation_extractor.Outputs["A"+str(ii)]] 			   ##### <--- Rapid Small Sim
	# inputs = [simulation_extractor.Outputs["A00E"+str(ii-1)+"  (E"+str(ii-1)+")"]] 	   ##### <--- Hydra Small Sim
		
	# em_sim_extractor = analysis.extractors.EmPortSimulationExtractor(inputs=inputs)
	# em_sim_extractor.Name = "Port"
	# em_sim_extractor.UpdateAttributes()
	######## document.AllAlgorithms.Add(em_port_simulation_extractor)
	# em_sensor_extractor = em_sim_extractor["SensBox"]
		
	em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
	em_sensor_extractor.Normalization.Normalize = True
	########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	# document.AllAlgorithms.Add(em_sensor_extractor)


	##


	if ii == 1:
		SAR = em_sensor_extractor.Outputs["SAR(x,y,z,f0)"]
		SAR.Update()
		SAR_data = SAR.Data
		SAR_data_grid = SAR_data.Grid
		xaxis = SAR_data_grid.XAxis
		yaxis = SAR_data_grid.YAxis
		zaxis = SAR_data_grid.ZAxis
		SAR_data_field = SAR_data.Field(0)

		ELD = em_sensor_extractor.Outputs["El. Loss Density(x,y,z,f0)"]
		ELD.Update()
		ELD_data = ELD.Data
		ELD_data_field = ELD_data.Field(0)

		rho = ELD_data_field/SAR_data_field
		rho = [rho[x] if numpy.isnan(rho[x]) == False else 0 for x in range(len(rho))]
		rho = numpy.asarray(rho)

		Ex = EField[:,0]
		Ey = EField[:,1]
		Ez = EField[:,2]
		Eabs = (numpy.abs(numpy.sqrt(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj()))).flatten()
		sig = 2*(ELD_data_field).flatten()/(numpy.power(Eabs,2))

		volmesh = numpy.meshgrid(numpy.diff(zaxis),numpy.diff(yaxis),numpy.diff(xaxis), indexing='ij')
		vol = volmesh[0].flatten()*volmesh[1].flatten()*volmesh[2].flatten()
		vol = numpy.asarray(vol)
		
		Eall = numpy.zeros(list(sig.shape)+[3]+[8], 'complex')
	
	E = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]
	E.Update()
	E_data = E.Data
	Eall[:,:,ii-1] = E_data.Field(0)

Q = numpy.zeros((list(sig.shape)+[8]+[8]),'complex')

for i in range(Eall.shape[0]):
		if sig[i] != 0.0:
			e = Eall[i]
			Q[i] = 0.0002*e.conj().transpose().dot(e)#*vol[i]   (sig[i]/(2*rho[i]))
			del e
			
# Q = Q.reshape(list([(grid.ZAxis.size-1)*(grid.YAxis.size-1)*(grid.XAxis.size-1),8,8]))

# w = numpy.array([0.1525272 - 0.3851685j, # Mode 8
	# -0.3004195 + 0.3713604j,
	# -0.1121617 - 0.05463602j,
	# -0.3144183 - 0.1506209j,
	# 0.01263364 - 0.05302412j,
	# 0.1269290 + 0.1513172j,
	# 0.02213423 + 0.1175130j,
	# 0.6377157 - 0.01224201j])
	
# Mode 1 #
w = numpy.array([[0.2937067 + 0.2601013j, 0.01193572 + 0.1764142j, 0.09612522 - 0.2170972j, 0.05691027 - 0.2527609j, -0.5419959 + 0.03611813j, 0.01585806 + 0.1388739j, 0.3910320 - 0.2284603j, 0.1525272 - 0.3851685j],
[0.04025201 + 0.2048185j, -0.04475570 + 0.1719992j, 0.05394017 + 0.1244815j, -0.09952845 + 0.5721215j, -0.3195322 - 0.04075515j, -0.09232337 - 0.3253956j, 0.1875933 - 0.2961228j, -0.3004195 + 0.3713604j],
[-0.02990488 - 0.2593963j,	-0.005377155 - 0.02912867j, 0.9449484 - 0.004753142j, -0.1130210 - 0.01938986j, 0.02878160 + 0.05256722j, 0.06333353 - 0.01507557j, -0.01529431 - 0.03482269j, -0.1121617 - 0.05463602j],
[-0.08522302 - 0.3652885j,	-0.4244961 - 0.2011421j, -0.08778275 + 0.006246062j, 0.6457119 + 0.02707570j, -0.1000587 - 0.04284121j, 0.1751956 - 0.1006413j, 0.1801628 - 0.08093909j, -0.3144183 - 0.1506209j],
[0.4057802 + 0.2407489j, -0.4065119 + 0.2379382j, 0.06375457 - 0.06390157j,0.03292451 + 0.04326089j, 0.6826628 + 0.01872971j, -0.03595148 - 0.01803388j, 0.2014237 - 0.1813076j, 0.01263364 - 0.05302412j],
[-0.02898972 + 0.2110370j, 0.1645579 + 0.2165319j, 0.04790079 + 0.01516103j, 0.1915788 + 0.07092602j, 0.03357824 + 0.05042084j, 0.8840426 + 0.01050899j, -0.1096128 - 0.008122726j, 0.1269290 + 0.1513172j],
[-0.1993896 - 0.2714965j, 0.4896615 + 0.1536497j, -0.05445429 + 0.02463466j, 0.09385192 - 0.001099353j, 0.2548020 + 0.1745917j, -0.04385620 + 0.03063045j, 0.7055539 + 0.02004144j, 0.02213423 + 0.1175130j],
[-0.1529567 - 0.4344838j, -0.3771053 + 0.1191604j, -0.09915560 + 0.02126543j, -0.2951936 + 0.1555006j, -0.09482270 + 0.1062475j, 0.1123105 - 0.1492023j, 0.06583204 - 0.2085586j, 0.6377157 - 0.01224201j]], dtype = 'complex')

Sh = numpy.zeros(list(sig.shape)+[8], dtype = 'complex')
for s in xrange(1,9):
	wt = w[s-1,:,numpy.newaxis]

	A = w[s-1,:].dot(Q).dot(wt)
	Sh[:,s-1] = A.squeeze()

# dif = numpy.divide(S1, SAR_data_field.squeeze()).tolist()

# dif.count(1)


Shflat = numpy.double(abs(Sh))
import XPostProcessor
SNRfield_obj = XPostProcessor.DoubleFieldData()
SNRfield_obj.Grid = SAR_data_grid
SNRfield_obj.ValueLocation = XPostProcessor.eValueLocation.kCellCenter
SNRfield_obj.NumberOfComponents = 1
SNRfield_obj.NumberOfSnapshots = 1
SNRfield_obj.SetField(0,Shflat)
producer1 = XPostProcessor.TrivialProducer()
producer1.SetDataObject(SNRfield_obj)
producer1.Description = 'SAR Mode '+str(s+1)
reg = XPostProcessor.AlgorithmRegistry()
reg.AddAlgorithm(producer1)

path = r'G:\\Q-Matrices\\Q_LongRapidCoil_LWPhantom'		
mdict = {'Q':Q}
scipy.io.savemat(path,mdict)

path = r'G:\\Point SAR\\pSAR_LongRapidCoil_LWPhantom'		
mdict = {'pSAR':Sh}
scipy.io.savemat(path,mdict)