def Do(cn, sn, freq):
	import numpy
	import s4l_v1.analysis as analysis
	import s4l_v1.document as document
	import s4l_v1.model as model
	import s4l_v1.units as units
	from s4l_v1 import ReleaseVersion
	from s4l_v1 import Unit
	
	
	# Define the version to use for default values
	ReleaseVersion.set_active(ReleaseVersion.version3_4)
	
	# Creating the analysis pipeline
	# Adding a new SimulationExtractor
	simulation = document.AllSimulations["Huygens Channel "+str(cn)]
	simulation_extractor = simulation.Results()
	
	if sn < 10:
		sensor = model.AllEntities()["S0"+str(sn)]
		em_sensor_extractor = simulation_extractor["S0"+str(sn)+"  (Model)"]
	else:
		sensor = model.AllEntities()["S"+str(sn)]
		em_sensor_extractor = simulation_extractor["S"+str(sn)+"  (Model)"]

	pos = sensor.Properties.Transformation.Children[2].Value
	
	x = pos[0]
	y = pos[1]
	z = pos[2]
	

	em_sensor_extractor.Normalization.Normalize = True
	em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)
	
	inputs = [em_sensor_extractor.Outputs["EM I(f)"]]
	plot_viewer = analysis.viewers.PlotViewer(inputs=inputs)
	# plot_viewer.Plotter.ComplexOptions = plot_viewer.Plotter.ComplexOptions.enum.Mag
	# plot_viewer.Visible = True
	plot_viewer.UpdateAttributes()
	document.AllAlgorithms.Add(plot_viewer)
	# em_sensor_extractor.UpdateAttributes()
	plot_viewer.Update()
	
	# inputs = em_sensor_extractor.Outputs["EM I(f)"]
	a = inputs[0]
	# a.Update()
	# a.UpdateAttributes()
	# JData = a.Data.GetComponent(0)[freq_ind]
	JFreqs = a.Data.Axis

	freq_ind = JFreqs.searchsorted(freq)
	# J = JData[freq_ind]	
	J = a.Data.GetComponent(0)[freq_ind]
	
	plot_viewer.Visible = False
	
	return J, x, y, z

import numpy
import s4l_v1.analysis as analysis
coilno = 8
sensorno = 44
J = numpy.zeros((coilno, sensorno), dtype = 'complex')
Axis = numpy.zeros((3,sensorno))
freq = 64e6


cn = 8
for sn in range(1,sensorno+1):
	[oJ, x, y, z] = Do(cn, sn, freq)
	if cn == 1:
		Axis[:,sn-1] = [x,y,z]
	J[cn-1,sn-1] = oJ


from scipy.io import savemat
path = r'G:\\Surface Current\\Final Rapid Coil on Duke 15T Real Wire Stripped Tip\Current'
mdict = {'J':J, 'Axes':Axis}
savemat(path,mdict)	
