### script to retrieve Surface-J(x,y,z,f0) grid and field ###

import s4l_v1.document as document
import s4l_v1.analysis as analysis
from scipy.io import savemat
import numpy

import s4l_v1.model as model
import s4l_v1.units as units
from s4l_v1 import ReleaseVersion
from s4l_v1 import Unit


for ii in range(1,9):
	simulation = document.AllSimulations["Huygens Channel "+str(ii)] ##### <--- Rapid HUYGENS
	# simulation = document.AllSimulations["Channel "+str(ii)]		   ##### <--- Rapid Small Sim
	# simulation = document.AllSimulations["Huygens Element "+str(ii)] ##### <--- Hydra HUYGENS
	# simulation = document.AllSimulations["Element #"+str(ii)]		   ##### <--- Hydra Small Sim
	
	simulation_extractor = simulation.Results()

	########## # Adding a new EmSensorExtractor
	em_sensor_extractor = simulation_extractor["sBoxClone  (Model)"] ############## <--- Select for HUYGENS
	
	# inputs = [simulation_extractor.Outputs["A"+str(ii)]] 			   ##### <--- Rapid Small Sim
	# inputs = [simulation_extractor.Outputs["A00E"+str(ii-1)+"  (E"+str(ii-1)+")"]] 	   ##### <--- Hydra Small Sim
	
	# em_sim_extractor = analysis.extractors.EmPortSimulationExtractor(inputs=inputs)
	# em_sim_extractor.Name = "Port"
	# em_sim_extractor.UpdateAttributes()
	######## document.AllAlgorithms.Add(em_port_simulation_extractor)
	# em_sensor_extractor = em_sim_extractor["Overall Field"]

	
	em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
	em_sensor_extractor.Normalization.Normalize = True
	########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)
	
	# Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
	E = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]
	E.Update()
	E_data = E.Data
	E_data_field = E_data.Field(0)
	
	if ii == 1:
		Eall = numpy.zeros(list(E_data_field.shape)+[8], 'complex')
	Eall[:,:,ii-1] = E_data_field
	
		

grid = E_data.Grid
xaxis = grid.XAxis
yaxis = grid.YAxis
zaxis = grid.ZAxis

getsar = em_sensor_extractor.Outputs["SAR(x,y,z,f0)"]
geteld = em_sensor_extractor.Outputs["El. Loss Density(x,y,z,f0)"]
# getE = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]
	
getsar.Update()
geteld.Update()
E.Update()
			
sar = getsar.Data
eld = geteld.Data
EData = E.Data
			
EField= EData.Field(0)
			
rho = eld.Field(0)/sar.Field(0)
rho = [rho[x] if numpy.isnan(rho[x]) == False else 0 for x in range(len(rho))]
rho = numpy.asarray(rho)

Ex = EField[:,0]
Ey = EField[:,1]
Ez = EField[:,2]
Eabs = (numpy.abs(numpy.sqrt(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj())))
sig = 2*(numpy.squeeze(eld.Field(0)))/(numpy.power(Eabs,2))

volmesh = numpy.meshgrid(numpy.diff(zaxis),numpy.diff(yaxis),numpy.diff(xaxis), indexing='ij')
vol = volmesh[0].flatten()*volmesh[1].flatten()*volmesh[2].flatten()
vol = numpy.asarray(vol)

Q = numpy.zeros((list(sig.shape)+[8]+[8]),'complex')

for i in range(Eall.shape[0]):
		if sig[i] != 0.0 and ~numpy.isnan(sig[i]):
			e = Eall[i]
			Q[i] = (sig[i]/(2*rho[i]))*e.conj().transpose().dot(e)#*vol[i]   (sig[i]/(2*rho[i]))
			del e

import scipy.linalg
from numpy import linalg as LA
import numpy.matlib
from scipy.io import savemat 
from scipy.io import loadmat 
Q1 = Q[0:len(sig)/6,:,:]
Q2 = Q[len(sig)/6:2*len(sig)/6,:,:]
Q3 = Q[2*len(sig)/6:3*len(sig)/6,:,:]
Q4 = Q[3*len(sig)/6:4*len(sig)/6,:,:]
Q5 = Q[4*len(sig)/6:5*len(sig)/6,:,:]
Q6 = Q[5*len(sig)/6:len(sig),:,:]


path = r'G:\\Q-Matrices\\Q_V2RapidCoil_Duke_15T_RealWire_StrippedTip_part6'
mdict = {'Q_Matrix':Q6}
scipy.io.savemat(path,mdict)
			
			
####### 10g Averagin #######
for ii in range(Q.shape[1]):
	for ii in range(Q.shape[2]):

	
simulation = document.AllSimulations["Huygens Channel 8"]
simulation_extractor = simulation.Results()


ii = 3
jj = 0
sect_toAvg = numpy.zeros(list(sig.shape)+[1])
sect_Avged = numpy.zeros(list(sig.shape)+[8]+[8])
sect_toAvg = Q[:,ii,jj].real
sect_toAvg = numpy.float32(abs(sect_toAvg)).reshape(len(sig),1)

em_sensor_extractor = simulation_extractor["sBox"]
em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
em_sensor_extractor.Normalization.Normalize = True
# document.AllAlgorithms.Add(em_sensor_extractor)
	
struct_to_alter = em_sensor_extractor.Outputs["SAR(x,y,z,f0)"]
struct_to_alter.Data.SetField(0,sect_toAvg)
list_toAvg = []
list_toAvg.append(struct_to_alter)
averager = analysis.em_evaluators.AverageSarFieldEvaluator(inputs = list_toAvg)
averager.TargetMass = 10.0, Unit("g")
averager.UpdateAttributes()
document.AllAlgorithms.Add(averager)

inputs = [averager.Outputs["IEEE/IEC62704-1 Avg.SAR(x,y,z,f0)"]]

# SNRfield_obj = XPostProcessor.FloatFieldData()
# SNRfield_obj.Grid = grid
# SNRfield_obj.ValueLocation = XPostProcessor.eValueLocation.kCellCenter
# SNRfield_obj.NumberOfComponents = 1
# SNRfield_obj.NumberOfSnapshots = 1
# SNRfield_obj.SetField(0,inputs.Data.Field(0))
# producer1 = XPostProcessor.TrivialProducer()
# producer1.SetDataObject(SNRfield_obj)
# producer1.Description = 'Averaged Entry'
# reg = XPostProcessor.AlgorithmRegistry()
# reg.AddAlgorithm(producer1)


slice_field_viewer = analysis.viewers.SliceFieldViewer(inputs=inputs)
slice_field_viewer.Data.Mode = slice_field_viewer.Data.Mode.enum.QuantityRealModulus
slice_field_viewer.UpdateAttributes()
document.AllAlgorithms.Add(slice_field_viewer)

sect_Avged[:,ii,jj] = inputs[0].Data.Field(0).squeeze()

del inputs









		
import EmPostPro
cubes, lossyrho_fd, labels_fd = EmPostPro.ConstMassCubesGenerator.Generate(grid, target_mass_kg=1E-2, enable_uncertainty_region=False)
print(len(cubes))

Qavg = numpy.zeros([len(cubes)]+list(Q.shape[1:]),complex)

Q = Q.reshape(list((grid.ZAxis.size-1,grid.YAxis.size-1,grid.XAxis.size-1))+[8]+[8])
massKg = rho*vol
massKg = massKg.reshape(list((grid.ZAxis.size-1,grid.YAxis.size-1,grid.XAxis.size-1))+[1]+[1])


for ii in range(len(cubes)):
	# print(ii)
	if (cubes[ii].Lower[2]!=cubes[ii].Upper[2] and cubes[ii].Lower[1]!=cubes[ii].Upper[1] and cubes[ii].Lower[0]!=cubes[ii].Upper[0]):
		Qavg[ii,:,:] = AverageOverQ(getWeights(cubes[ii]),cubes[ii])

LUT = numpy.zeros(len(cubes))
for ii in range(len(cubes)):
	LUT[ii] = cubes[ii].ReferenceCellId
	
for ii in range(len(cubes)):
	xl = cubes[ii].Lower[0]
	xu = cubes[ii].Upper[0]
	yl = cubes[ii].Lower[1]
	yu = cubes[ii].Upper[1]
	zl = cubes[ii].Lower[2]
	zu = cubes[ii].Upper[2]
	Qavg[ii,:,:] = sum(sum(sum(Q[zl:zu+1,yl:yu+1,xl:xu+1,:,:]*massKg[zl:zu+1,yl:yu+1,xl:xu+1,:,:], 0), 0), 0)/0.01


w = numpy.array([[-0.3501589 - 0.2636127j, 0.1636537 + 0.5065372j, -0.2459631 + 0.1173872j, -0.1773789 + 0.1896803j, -0.1359239 + 0.2491103j, -0.1095467 + 0.2440170j, -0.2273656 + 0.1489687j, -0.3951092 + 0.02722307j],
[-0.3706227 - 0.2217399j, 0.2094591 + 0.2663407j, -0.06969609 - 0.2088608j, 0.2148582 - 0.2953691j, 0.2715667 - 0.3915519j, 0.1818211 - 0.4027811j, 0.1418339 - 0.2226816j, -0.1559999 + 0.01948521j],
[-0.2917419 - 0.2006699j, 0.01538416 - 0.07951252j, 0.8912228 - 7.600814e-05j, -0.1205577 - 0.02450689j, -0.1379141 - 0.01528855j, -0.1333838 + 0.003692285j, -0.1121344 - 0.02806627j, -0.08646902 - 0.02736868j],
[-0.2670780 - 0.1103294j, -0.2028527 - 0.2718835j, -0.1211590 + 0.05646749j, 0.7981472 + 0.02053301j, -0.2407622 + 0.04523825j, -0.2176154 + 0.07622831j, -0.1710106 + 0.008780430j, -0.05885116 - 0.01153173j],
[-0.2666647 - 0.1282851j, -0.2354600 - 0.3946143j, -0.1452783 + 0.05960656j, -0.2476068 + 0.001622430j, 0.6993162 + 0.02600887j, -0.2752704 + 0.06750554j, -0.2052362 - 0.007733485j, -0.05341573 - 0.01466930j],
[-0.2472275 - 0.1627550j, -0.1520854 - 0.3875255j, -0.1460554 + 0.03424061j, -0.2295031 - 0.03305814j, -0.2809984 - 0.01784425j, 0.7363392 + 0.02385047j, -0.1902311 - 0.03585663j, -0.05337893 - 0.02118132j],
[-0.2819107 - 0.1196797j, -0.1595969 - 0.1643409j, -0.1072217 + 0.04791806j, -0.1660753 + 0.02889107j, -0.1933260 + 0.05301404j, -0.1733679 + 0.07592840j, 0.8545424 + 0.01570535j, -0.06955514 - 0.01346839j],
[-0.3274730 - 0.1904448j, 0.02289160 + 0.2067058j, -0.06425884 + 0.003294763j, -0.04048776 + 0.03774412j, -0.02831314 + 0.05617231j, -0.02299969 + 0.05240361j, -0.05559736 + 0.02213398j, 0.8912852 - 0.02888838j]])

Sh = numpy.zeros(list(sig.shape)+[1], dtype = 'float32')
first = int(LUT[0])
last = int(LUT[-1])
lut = numpy.int_(LUT)

count = 0
for ii in range(len(numpy.isnan(wQ))):
	if numpy.isnan(wQ.all()) == 1:
		count += 1

for s in xrange(1,9):
	wt = w[s-1,:,numpy.newaxis]

	A = w[s-1,:].dot(Qavg).dot(wt)  #Q.reshape(list(sig.shape)+[8]+[8])
	Sh[lut,s-1] = A.squeeze()  #Sh[lut,s-1]
	
Sh =  w[0,:].dot(Q).dot(w[0,:,numpy.newaxis])
Sh = S.squeeze()

wSAR = numpy.array([0.1667 + 0.0000j, -0.1054 + 0.1291j, 0.1439 + 0.0841j, -0.1465 + 0.0795j, -0.1665 + 0.0078j, -0.0438 - 0.1608j])
SAR = Sh[:,2:8].dot(wSAR[:,numpy.newaxis])


new_shape = (grid.Dimensions[0]-1, grid.Dimensions[1]-1, grid.Dimensions[2]-1)
Shflat = numpy.float32(abs(Sh))
Shflat = Shflat.reshape(new_shape)
import XPostProcessor
SNRfield_obj = XPostProcessor.DoubleFieldData()
SNRfield_obj.Grid = grid
SNRfield_obj.ValueLocation = XPostProcessor.eValueLocation.kCellCenter
SNRfield_obj.NumberOfComponents = 1
SNRfield_obj.NumberOfSnapshots = 1
SNRfield_obj.SetField(0,Shflat)
producer1 = XPostProcessor.TrivialProducer()
producer1.SetDataObject(SNRfield_obj)
producer1.Description = 'SAR Avg Dark Modes '+str(s+1)
reg = XPostProcessor.AlgorithmRegistry()
reg.AddAlgorithm(producer1)

####
#SAVE Q-MATRICES
import scipy.linalg
from numpy import linalg as LA
import numpy.matlib
from scipy.io import savemat 
from scipy.io import loadmat 
Q1 = Q[0:len(sig)/2+1,:,:]
path = r'G:\\Q-Matrices\\Q_V2RapidCoil_Duke_portByPort_Wire_StrippedTip_part1'
mdict = {'Q_Matrix_part1':Qsave[0:len(sig)/2,:,:]}
scipy.io.savemat(path,mdict)