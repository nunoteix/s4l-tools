# -*- coding: utf-8 -*-
import s4l_v1 as s4l
import sys, os
import numpy
import scipy.io

sims=s4l.simulation.GetSimulations()
data = {} # create structure to store data
k=0
path = r'D:\sim4life\antennaLoopArray3T\10antenna20loopArrayOnBigDuke.smash_Results\matlab'


for sim in sims:
#for	sim in {sims[0]}:
	k=k+1
	if (k<10):
		numberString='0'+str(k)
	else:
		numberString=str(k)

	print sim.Name
	#replace spaces by underscores
	newName = sim.Name.replace(' ','_')
	print newName
	
	results = sim.Results()
	sensor = results.GetSensor("Overall Field")
	inputPower = results.GetSensor(results.keys()[0]).GetData(results.GetSensor(results.keys()[0]).keys()[0]).GetComponent(0)
	
	#Extract B1 field
	fieldclass = None
	B1field = None
	mdict = None
	data = {}
	fieldclass = sensor.GetData(sensor.keys()[10])
	B1field = fieldclass.Field(0)/sqrt(inputPower)
	new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
	B1field = B1field.reshape(list(new_shape)+[2])
	B1field = B1field.transpose([2, 1, 0, 3])
	mdict = {'field':B1field, 'x_axis':fieldclass.Grid.XAxis, 'y_axis':fieldclass.Grid.YAxis, 'z_axis':fieldclass.Grid.ZAxis}
	filename = 'B1field'+newName+'.mat'
	data['B1field'+numberString] = mdict
	print 'saving data to: ' + path +'\\'+filename
	scipy.io.savemat(path+'\\'+filename,data) # save data to MATLAB
	print 'Finished...'	

	#Extract E field
	fieldclass = None
	Efield = None
	mdict = None
	data = {}
	fieldclass = sensor.GetData(sensor.keys()[0])
	Efield = fieldclass.Field(0)/sqrt(inputPower)
	new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
	Efield = Efield.reshape(list(new_shape)+[3])
	Efield = Efield.transpose([2, 1, 0, 3])
	mdict = {'field':Efield, 'x_axis':fieldclass.Grid.XAxis, 'y_axis':fieldclass.Grid.YAxis, 'z_axis':fieldclass.Grid.ZAxis}
	filename = 'Efield'+newName+'.mat'
	data['Efield'+numberString] = mdict
	print 'saving data to: ' + path +'\\'+filename
	scipy.io.savemat(path+'\\'+filename,data) # save data to MATLAB
	print 'Finished...'	

	#Only extract SAR, dPdV and J for first simulation
	if (k==1):
		#Extract SAR field
		fieldclass = None
		SARfield = None
		mdict = None
		data = {}
		fieldclass = sensor.GetData(sensor.keys()[9])
		SARfield = fieldclass.Field(0)/sqrt(inputPower)
		new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
		SARfield = SARfield.reshape(list(new_shape))
		SARfield = SARfield.transpose([2, 1, 0])
		mdict = {'field':SARfield, 'x_axis':fieldclass.Grid.XAxis, 'y_axis':fieldclass.Grid.YAxis, 'z_axis':fieldclass.Grid.ZAxis}
		filename = 'SAR'+newName+'.mat'
		data['SAR'+numberString] = mdict
		print 'saving data to: ' + path +'\\'+filename
		scipy.io.savemat(path+'\\'+filename,data) # save data to MATLAB
		print 'Finished...'	
	
		#Extract J field
		fieldclass = None
		Jfield = None
		mdict = None
		data = {}
		fieldclass = sensor.GetData(sensor.keys()[5])
		Jfield = fieldclass.Field(0)/sqrt(inputPower)
		new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
		Jfield = Jfield.reshape(list(new_shape)+[3])
		Jfield = Jfield.transpose([2, 1, 0, 3])
		mdict = {'field':Jfield, 'x_axis':fieldclass.Grid.XAxis, 'y_axis':fieldclass.Grid.YAxis, 'z_axis':fieldclass.Grid.ZAxis}
		filename = 'Jfield'+newName+'.mat'
		data['Jfield'+numberString] = mdict
		print 'saving data to: ' + path +'\\'+filename
		scipy.io.savemat(path+'\\'+filename,data) # save data to MATLAB
		print 'Finished...'	
	
		#Extract dPdV field
		fieldclass = None
		dPdVfield = None
		mdict = None
		data = {}
		fieldclass = sensor.GetData(sensor.keys()[7])
		dPdVfield = fieldclass.Field(0)/sqrt(inputPower)
		new_shape = (fieldclass.Grid.Dimensions[2]-1, fieldclass.Grid.Dimensions[1]-1, fieldclass.Grid.Dimensions[0]-1)
		dPdVfield = dPdVfield.reshape(list(new_shape))
		dPdVfield = dPdVfield.transpose([2, 1, 0,])
		mdict = {'field':dPdVfield, 'x_axis':fieldclass.Grid.XAxis, 'y_axis':fieldclass.Grid.YAxis, 'z_axis':fieldclass.Grid.ZAxis}
		filename = 'dPdV'+newName+'.mat'
		data['dPdV'+numberString] = mdict
		print 'saving data to: ' + path +'\\'+filename
		scipy.io.savemat(path+'\\'+filename,data) # save data to MATLAB
		print 'Finished...'	
	
