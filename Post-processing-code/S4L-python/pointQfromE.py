import s4l_v1 as s4l
import scipy.io
import numpy
import scipy.linalg
from numpy import linalg as LA
import numpy.matlib
from scipy.io import savemat 
from scipy.io import loadmat 
import gc
	
def AverageOverQ(W,cube):
	xl = cube.Lower[0]
	xu = cube.Upper[0]
	yl = cube.Lower[1]
	yu = cube.Upper[1]
	zl = cube.Lower[2]
	zu = cube.Upper[2]
	return sum(sum(sum(W*Q[zl:zu+1,yl:yu+1,xl:xu+1,:,:],0),0),0)/0.01
	
def getWeights(cube):
	W = numpy.zeros((3,2))
	LL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[0])
	LU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Lower[0],cube.Lower[1],cube.Lower[2]))[7])
	for ii in range(3):
		W[ii,0] = (LU-cube.Point0)[ii]/(LU-LL)[ii]
	UL=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[0])
	UU=grid.GetPoint(grid.GetCellPoints(grid.ComputeCellIndex(cube.Upper[0],cube.Upper[1],cube.Upper[2]))[7])
	for ii in range(3):
		W[ii,1] = (cube.Point1-UL)[ii]/(UU-UL)[ii]

	xw =[W[0,0]]+list(numpy.ones(cube.Upper[0]-cube.Lower[0]-1))+[W[0,1]]
	yw =[W[1,0]]+list(numpy.ones(cube.Upper[1]-cube.Lower[1]-1))+[W[1,1]]	
	zw =[W[2,0]]+list(numpy.ones(cube.Upper[2]-cube.Lower[2]-1))+[W[2,1]]
	W = numpy.meshgrid(zw,yw,xw,indexing='ij')		
	W = W[0]*W[1]*W[2]
	W8x8 = numpy.zeros(list(W.shape)+[8]+[8])
	for ii in range(8):
		for jj in range(8):
			W8x8[:,:,:,ii,jj] = W
	return W8x8

def getSigRhoVol(multresults,order2):
	results = multresults[order2[0]]
	# order = results.keys()

	sensor = results['SensBox']

	fieldclass = sensor['EM E(x,y,z,f0)']
	data = fieldclass.Data
	fieldclass.Update()
	EField = data.Field(0)

	grid = data.Grid

	getsar = sensor['SAR(x,y,z,f0)']
	getld = sensor['El. Loss Density(x,y,z,f0)']
	sar = getsar.Data
	ld = getld.Data
	getsar.Update()
	getld.Update()
	
	
	rho = ld.Field(0)/sar.Field(0)
	rho = [rho[x] if numpy.isnan(rho[x]) == False else 0 for x in range(len(rho))]
	rho = numpy.asarray(rho)
	Ex = EField[:,0]
	Ey = EField[:,1]
	Ez = EField[:,2]
	Eabs = (numpy.abs(numpy.sqrt(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj()))).flatten()
	sig = 2*(ld.Field(0)).flatten()/(numpy.power(Eabs,2))
	# mask = mask = [1 if x>0.005 else 0 for x in sig]
	# mask = numpy.asarray(mask)
	x = grid.XAxis
	y = grid.YAxis
	z = grid.ZAxis

	volmesh = numpy.meshgrid(numpy.diff(z),numpy.diff(y),numpy.diff(x), indexing='ij')
	vol = volmesh[0].flatten()*volmesh[1].flatten()*volmesh[2].flatten()
	vol = numpy.asarray(vol)
	
	return (sig, rho, vol)
	
#####################################

sim = s4l.simulation.GetSimulations()[0]									#### CHANGE SIM HERE
print(sim.Name)
multresults = sim.Results()
order2 = multresults.keys()

(sig, rho, vol) = getSigRhoVol(multresults, order2)

Eall = numpy.zeros(list(sig.shape)+[3]+[8], 'complex')
for ii in xrange(0,8):
	mat = loadmat(r'G:\E for S4L Q-script\E_Hydra_headDuke_64p_50dB_betterSkin_noNorm\E_1uT_channel'+str(ii+1))
	Eall[:,:,ii] = mat.get(mat.keys()[5])
	# Eall[:,:,ii] = Eall[:,:] + Etemp
	
Q = numpy.zeros((list(sig.shape)+[8]+[8]),'complex')

for i in range(Eall.shape[0]):
		if sig[i] != 0.0:
			e = Eall[i]
			Q[i] = (sig[i]/(2*rho[i]))*numpy.dot(e.conj().transpose(),e)*vol[i]
			del e

path = r'G:\\Q-Matrices\\Hydra_headDuke_64p_50dB_betterSkin_noNorm'
mdict = {'Q-Matrix':Q}
scipy.io.savemat(path,mdict)

###########################################################################################################

Q = numpy.load(r'G:\\Q-Matrices\\Hydra_headDuke_64p_50dB_betterSkin_noNorm.npy')

sim = s4l.simulation.GetSimulations()[0]									#### CHANGE SIM HERE
multresults = sim.Results()
order2 = multresults.keys()
results = multresults[order2[0]]
sensor = results['SensBox']
fieldclass = sensor['EM E(x,y,z,f0)']
data = fieldclass.Data
fieldclass.Update()
grid = data.Grid


import EmPostPro
cubes, lossyrho_fd, labels_fd = EmPostPro.ConstMassCubesGenerator.Generate(grid, target_mass_kg=1E-2, enable_uncertainty_region=False)
print(len(cubes))
Qavg = numpy.zeros([len(cubes)]+list(Q.shape[1:]),complex)

Q = Q.reshape(list((grid.ZAxis.size-1,grid.YAxis.size-1,grid.XAxis.size-1))+[8]+[8])
for ii in range(len(cubes)):
	# print(ii)
	if (cubes[ii].Lower[2]!=cubes[ii].Upper[2] and cubes[ii].Lower[1]!=cubes[ii].Upper[1] and cubes[ii].Lower[0]!=cubes[ii].Upper[0]):
		Qavg[ii,:,:] = AverageOverQ(getWeights(cubes[ii]),cubes[ii])

LUT = numpy.zeros(len(cubes))
for ii in range(len(cubes)):
	LUT[ii] = cubes[ii].ReferenceCellId
		
path = r'G:\\Q10gAvg\\Hydra_headDuke_64p_50dB_betterSkin_noNorm'		
# numpy.save(path,Qavg)
# import scipy.io
mdict = {'Q10g':Qavg, 'LUT':LUT}
scipy.io.savemat(path,mdict)