# -*- coding: utf-8 -*-
import s4l_v1 as s4l
from scipy.io import savemat  
import s4l_v1.simulation.emfdtd as fdtd
import numpy
import sys, os
from pylab import *

sims = s4l.simulation.GetSimulations()

ii = 0 # Select which simulation no. you want (starting from 0 on top of the list)
sim = sims[ii]
results = sim.Results
nports = 32 #Select amount of ports you want to extract
for jj in range(0,nports): 
		
	result = results(jj)
	overall_field_sensor = result['Overall Field'] # Extract overall field sensor
	#overall_field_sensor = result['Sensor Block'] 
	input_power_sensor = result['Input Power']
	
	Psensor = input_power_sensor['EM Input Power(f)']
	Psensor.Update()
	P = Psensor.Data.GetComponent(0)
	
	# Selection of result no. is based on this specific S4L Output!
 
	
	
	SAR = overall_field_sensor['SAR(x,y,z,f0)'] # Extract B1 Field
	SARdata = SAR.Data # Contains grid and field (and other stuff)
	SARgrid = SARdata.Grid # Select grid data
	xaxis = SARgrid.XAxis
	yaxis = SARgrid.YAxis
	zaxis = SARgrid.ZAxis
	SAR.Update() #?
	
	SARfield_unshaped = SARdata.Field(0) # Select B1 field from all data, use normalized field
	SARfield_normalized = SARfield_unshaped/sqrt(P)
	
	
	# Misschien alleen reeele deel van vermogen meenemen
	
	path = r'D:\\Data\\SSAD - 1st Part\\Recent\\SAR_32\\' # Path your exporting to
	mdict = {'Snapshot0':SARfield_normalized, 'Axis0':xaxis, 'Axis1':yaxis, 'Axis2':zaxis}
	ext = '.mat'
	savemat(path+sim.Name+str(jj+1)+'SAR'+ext,mdict)
	
	
	