function [A] = distributionGenerator(dist2D, x_axis, y_axis, fig, lowerBound, upperBound, method)

if exist('upperBound') == 0
    upperBound = max(max(dist2D));
end
if exist('lowerBound') == 0
    lowerBound = min(min(dist2D));
end

[X,Y] = meshgrid(y_axis, x_axis);
[Xq,Yq] = meshgrid(linspace(min(y_axis),max(y_axis),300),...
    linspace(min(x_axis),max(x_axis),300));

if exist('method') == 0
    method = 'nearest';
end

A = flipud(imrotate(interp2(X,Y, dist2D, Xq, Yq, method),90));
% figure;
if fig == 1
    imagesc(x_axis, y_axis,A,[lowerBound upperBound]);colormap jet;%axis square
    colorbar
end