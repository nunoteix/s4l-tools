function [B1_shimmed_sum,x1,B1_minus_sum] = ampShim(nports,xmid,ymid,zmid, Bdata,mask,rho)
    %shimmer from data

%     data = getAllDistributionsFromFile; %GET RAW TRANSMIT FIELDS (B1)
    
    s = size(Bdata.B1.B1plus);
    nchan = s(4);
    maxi = 0;
    tB1=zeros(s(1),s(2),s(3),1);
    tDmask = zeros(s(1),s(2),s(3));
    
    xaxis = Bdata.B1.axes.x;
    yaxis = Bdata.B1.axes.y;
    zaxis = Bdata.B1.axes.z;
    
%     side = find(xaxis > -0.15);
%     xmi = side(1);
%     side = find(xaxis > 0.15);
%     xmx = side(1);
%     side = find(yaxis > -0.06);
%     ymi = side(1);
%     side = find(yaxis > 0.06);
%     ymx = side(1);
%     side = find(zaxis > -0.06);
%     zmi = side(1);
%     side = find(zaxis > -0.04);
%     zmx = side(1);
%     tDmask(xmi:xmx, ymi:ymx, zmi:zmx) = 1;

    tDmask = mask;
    
    [~, X0, Nreps] = prepareRFamps(Bdata,xmid, ymid, zmid); %CREATE MASK AND INITIAL CONDITIONS FOR THE OPTIMIZATION PROBLEM
                                                                    
    x1 = optimizeRFamps1(Bdata.B1.B1plus, tDmask,0,'Homo',X0,0,Nreps); %SOLVE THE OPTIMIZATION PROBLEM

    for ii = 1:nports
        B1_shimmed(:,:,:,ii) = Bdata.B1.B1plus(:,:,:,ii)*x1(ii); %SHIM THE RAW TRANSMIT FIELDS WITH THE OBTAINED VALUES
        B1_minus(:,:,:,ii) = Bdata.B1.B1minus(:,:,:,ii)*x1(ii);
    end
    
    B1_shimmed_sum = sum(B1_shimmed,4); %SUM OVER NUMBER OF SIMULATED CHANNELS
    B1_minus_sum = sum(B1_minus,4);
    
    B1_slice = squeeze(abs(B1_shimmed_sum(:,ymid,:)));
%     figure;distributionGenerator(B1_slice,data.B1.axes.x, data.B1.axes.z, 1); %DISPLAY THE SHIMMED B1+ DISTRIBUTION
    
%     B1_slice = squeeze(abs(B1_minus_sum(:,ymid,:)));
%     visualizeDistr2(B1_slice,data.B1.axes.x, data.B1.axes.z)
    
%     B1_slice = squeeze(abs(B1_shimmed_sum(xmid,:,:)));
%     visualizeDistr2(B1_slice,data.B1.axes.y, data.B1.axes.z)
    
    max(max(B1_slice))
    imag = distributionGenerator(abs(B1_shimmed_sum(:,:,zmid)),Bdata.B1.axes.x, Bdata.B1.axes.y,0);
    mask_imag = distributionGenerator(abs(rho(:,:,zmid)),Bdata.B1.axes.x, Bdata.B1.axes.y,0,[],[],'nearest');
    figure;imagesc(Bdata.B1.axes.x, Bdata.B1.axes.y, imag.*mask_imag); colormap('jet'); colorbar
end