%% PUT FIELDS ON LINEAR GRID
load(uigetfile('*.mat', 'Load a File with Correct Axis'));

xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;

load(uigetfile('*.mat', 'Load 4D E-field File'));
load(uigetfile('*.mat', 'Load 4D H-field File'));

xaxisq = linspace(min(xaxis),max(xaxis),length(yaxis));
yaxisq = linspace(min(yaxis),max(yaxis),length(xaxis));
zaxisq = linspace(min(zaxis),max(zaxis),length(zaxis));

[X,Y,Z] = meshgrid(yaxis, xaxis, zaxis);
[Xq,Yq,Zq] = meshgrid(yaxisq,xaxisq,zaxisq);
Hi = H; clear H
Ei = E; clear E
for ii =1:3
    H(:,:,:,ii) = interp3(X,Y,Z,Hi(:,:,:,ii),Xq,Yq,Zq);
end
for ii = 1:3
    E(:,:,:,ii) = interp3(X,Y,Z,Ei(:,:,:,ii),Xq,Yq,Zq);
end
E = permute(E,[2 1 3 4]);
H = permute(H,[2 1 3 4]);
%% DATA TO RECORD
xvals = [xaxis(1) length(xaxis) (xaxis(end)-xaxis(1))/length(xaxis)];
yvals = [yaxis(1) length(yaxis) (yaxis(end)-yaxis(1))/length(yaxis)];
zvals = [zaxis(1) length(zaxis) (zaxis(end)-zaxis(1))/length(zaxis)];

freq = 128000000;
power = 1;
%% WRITE TO FILE
huygFile = fopen('Hydra_onAir_64p_50dB.txt','w');
fprintf(huygFile,'%s\n%s\n','<huygens>','<axes>');
fprintf(huygFile,'%f %f %f\n',xvals);
fprintf(huygFile,'%f %f %f\n',yvals);
fprintf(huygFile,'%f %f %f\n',zvals);
fprintf(huygFile,'%s\n%s\n','</axes>','<frequency>');
fprintf(huygFile,'%f\n',freq);
fprintf(huygFile,'%s\n%s\n','</frequency>','<incident-power');
fprintf(huygFile,'%f\n',power);
fprintf(huygFile,'%s\n%s\n','</incident-power>','<e-field>');
for xx = 1:xvals(2)
    for yy = 1:yvals(2)
        for zz = 1:zvals(2)
            tempE = [real(E(xx,yy,zz,1)); imag(E(xx,yy,zz,1)); real(E(xx,yy,zz,2));...
                imag(E(xx,yy,zz,2)); real(E(xx,yy,zz,3)); imag(E(xx,yy,zz,3))];

            fprintf(huygFile,'%f %f %f %f %f %f\n',tempE);
        end
    end
end
fprintf(huygFile,'%s\n%s\n','</e-field>','<h-field>');
for xx = 1:xvals(2)
    for yy = 1:yvals(2)
        for zz = 1:zvals(2)
            tempH = [real(H(xx,yy,zz,1)); imag(H(xx,yy,zz,1)); real(H(xx,yy,zz,2));...
                imag(H(xx,yy,zz,2)); real(H(xx,yy,zz,3)); imag(H(xx,yy,zz,3))];

            fprintf(huygFile,'%f %f %f %f %f %f\n',tempH);
        end
    end
end
fprintf(huygFile,'%s\n%s\n','</h-field>','<\huygens>');

fclose(huygFile);
%%