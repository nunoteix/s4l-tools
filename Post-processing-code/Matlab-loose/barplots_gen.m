xx = 93;       %CHANGE
yy = 34;        %CHANGE
zz = 114;        %CHANGE
coord = [xx yy zz];

% 8xSSAD % 32xIntercDips_w_LSens_AIRSUB % 32xLoopsStraight % 8xFracD20mm
% 16xFracD2 % 32xFracM+Patch % 32xSmallFracDapart % 32xSmallFracDsamespace
% 24xSmallFracDapart% 24xSmallFracDsamespace


load('SNR_24xSmallFracDaparttest.mat');       %CHANGE
load('B1Shimmed_24xSmallFracDGAP.mat');     %CHANGE
load('SAR10g_24xSmallFracDaparttest.mat');    %CHANGE
J = load('J_24xSmallFracDGAP.mat');         %CHANGE
dim = [length(J.Axis0)-1 length(J.Axis1)-1 length(J.Axis2)-1];

CD = sum(J.Snapshot0,2);
CD(abs(CD)>=0.05)=1;
CD(abs(CD)~=1)=0;
CD = reshape(CD,dim);

Bmat = B1_shimmed_sum;
% Bmat = permute(Bmat, [2 1 3]); %Permute for duke
% CD = permute(CD, [2 1 3]); %Permute for DUKE

[SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,J,0);
% SAR3D = permute(SAR3D, [2 1 3]); %Permute for DUKE
% axs = struct('x', J.Axis0,'y', J.Axis1, 'z', J.Axis2); %for DUKE

mipSAR = max(SAR3D, [], 2);
% mipSAR = max(SAR3D, [], 1);

SNR = reshape(SNR, dim);
% SNR = permute(SNR, [2 1 3]);
%%
FigHandle = figure(1);
set(FigHandle, 'Position', [100, 100, 750, 420]);
visualizeDistr2((abs(Bmat(:,:,zz)).*1e6).*CD(:,:,zz), J.Axis0, J.Axis1, 1, [0 4]);
axis([-0.26 0.26 -0.13 0.13])
% axis([-0 0.54 -0.01 0.2])
% colorbar off
c=colorbar
ylabel(c,'B_1^+ ({\mu}T)') 
set(gca, 'FontSize', 20);
% title('B_1^+')
axis off

FigHandle = figure(2);
set(FigHandle, 'Position', [100, 100, 750, 420]);
visualizeDistr2(((abs(Bmat(:,:,zz)).*1e6).*CD(:,:,zz))./sqrt(pSAR10g), J.Axis0, J.Axis1, 1, [0 0.6]);
axis([-0.26 0.26 -0.13 0.13])
% axis([-0 0.54 -0.01 0.2])
% colorbar off
c=colorbar;
ylabel(c,'B_1^+/{\surd}pSAR_1_0_g ({\mu}T/{\surd}W/kg)') 
set(gca, 'FontSize', 20);
% title('B_0^+/{\surd}SAR_m_a_x (T/\surd{W/kg})')
axis off


FigHandle = figure(3);
set(FigHandle, 'Position', [100, 100, 750, 420]);
visualizeDistr2(SNR(:,:,zz).*CD(:,:,zz), J.Axis0, J.Axis1, 1, [0 2e-6]);
axis([-0.26 0.26 -0.13 0.13])
% colorbar off
c=colorbar;
ylabel(c,'SNR') 
set(gca, 'FontSize', 20);
% title('SNR')
axis off


FigHandle = figure(4)
set(FigHandle, 'Position', [100, 100, 750, 420]);
visualizeDistr2(squeeze(mipSAR), J.Axis0, J.Axis2,1,[0 60]);
% axis([-0.26 0.26 -0.1 0.15])
% colorbar off
c=colorbar;
ylabel(c,'pSAR_1_0_g (W/kg)') 
set(gca, 'FontSize', 20);
% title('SAR10g MIP')
axis off

%%
tDmask = maskGen(dim, coord(1), coord(2), coord(3), axs);
% tDmask = permute(tDmask, [2 1 3]); %permute for DUKE

maskedB1plus = tDmask.*(abs(Bmat).*1e6);
avgB1inROI = sum(maskedB1plus(:))/sum(maskedB1plus(:) > 0);

maskedSNR = tDmask.*SNR;
avgSNR = sum(maskedSNR(:))/sum(maskedSNR(:) > 0);

maskedB1overSAR = maskedB1plus./sqrt(pSAR10g);
avgB1overSARinROI = sum(maskedB1overSAR(:))/sum(maskedB1overSAR(:) > 0);

maskedSARoverB1 = (pSAR10g)./(maskedB1plus(maskedB1plus>0).^2);
avgSARoverB1inROI = sum(maskedSARoverB1(:))/sum(maskedSARoverB1(:) > 0);

avgs(:,1) = [avgB1inROI pSAR10g avgB1overSARinROI];% avgSARoverB1inROI avgSNR];

%%
figure(5);
for ii = 1:3
n = 130+ii;
subplot(n)
p1 = bar(1,avgs(ii,1));
hold on;
p2 = bar(2,avgs(ii,2));
% hold on;
% p3 = bar(3,avgs(ii,3));
% hold on;
% p4 = bar(4,avgs(ii,4));
set(p1,'FaceColor','red');
set(p2,'FaceColor','blue');
% set(p3,'FaceColor','black');
% set(p4,'FaceColor','green');
% % a=axes;
% % set(a,'xticklabel',[]);
end
% B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})
subplot(131)
set(gca,'fontsize', 20);
ylabel('Average B_1^+ in ROI ({\mu}T)')
subplot(132)
set(gca,'fontsize', 20);
ylabel('Maximum Local SAR (W/kg)')
subplot(133)
set(gca,'fontsize', 20);
ylabel('Average B_1^+/{\surd}pSAR_1_0_g ratio in ROI ({\mu}T/{\surd}W/kg)')
% subplot(155)
% set(gca,'fontsize', 20);
% % axis([0 6 0 2e-7])
% ylabel('SNR')
% set(gca,'fontsize', 20);
% subplot(154)
% set(gca,'fontsize', 20);
% % axis([0 6 0 2e-7])
% ylabel('Average pSAR_1_0_g/B_1^+^2 ratio in ROI (W/kg/{\mu}T^2)')
% set(gca,'fontsize', 20);
% legend('32-channel Small Fractionated Dipoles','32-channel Small Fractionated Dipoles (non-linear spacing)','24-channel Fractionated Dipoles','24-channel Fractionated Dipoles (non-linear spacing)')
subplot(132)
title('32-channel Small Fractionated Dipoles (non-linear spacing)')
legend('on Phantom','on DUKE')


