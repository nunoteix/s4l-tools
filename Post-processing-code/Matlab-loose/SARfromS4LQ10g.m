function [SAR,axis,C] = SARfromQ10g(Q10g_1,LUT, Jfile, shim)
    load(Jfile);
    dim = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
    J = sum(Snapshot0,2);
    J(abs(J)>=0.1)=1;
    J(abs(J)~=1)=0;
    J = reshape(J,dim);

    axis = struct('x', Axis0,'y', Axis1, 'z', Axis2);
    %% SAR from S4L Q-Matrix
%     load(Qfile);
    SAR = zeros(1,dim(1)*dim(2)*dim(3));
    
    for ii = 1:length(LUT)
        SAR(LUT(ii)) = shim'*squeeze(Q10g_1(ii,:,:))*shim;
    end

    if real(SAR) == abs(SAR)
        SAR = reshape(abs(SAR),dim(1),dim(2),dim(3));
        [C,I] = max(SAR(:));
        [I1,I2,I3] = ind2sub(size(SAR),I);

        figure;visualizeDistr2(SAR(:,:,I3).*J(:,:,I3),Axis0, Axis1,1,[0 C])
%         figure;visualizeDistr2(SAR(:,:,I3),Axis0, Axis1,1,[0 C])
%         figure;visualizeDistr2(J(:,:,I3),Axis0, Axis1,1,[0 C])
    else
        h = errordlg('Something went wrong - SAR is complex','SAR ERROR');

    end
end



