%% Section Attempting to Create Heart Mask
if ~exist('Axis0','var')
%     load('G:\B1\B1_RapidCoil_Duke_1_5T_portByPort_Wire_StrippedTip\B1_channel1') %RCOIL
%     load('G:\B1\B1_FinalRapidCoil_Duke_portByPort_Wire_StrippedTip_1_5T\B1_channel1') %FINALRCOIL
%     load('G:\B1\B1_V2RapidCoil_Duke_portByPort_Wire_StrippedTip\B1_channel1') %V2RCOIL

%     load('G:\B1\B1_V2RapidCoil_LWPhant3_portByPort_Wire_StrippedTip\B1_channel1.mat')
%     load('G:\B1\B1_FinalRapidCoil_LWPhant3_portByPort_Wire_StrippedTip_1_5T\B1_channel1.mat')
    
    load('G:\B1\B1_FinalRapidCoil_Duke_15T_RealWire_StrippedTip\B1_channel1.mat')
%     load('G:\B1\B1_V2RapidCoil_Duke_15T_RealWire_StrippedTip\B1_channel1.mat')
end

xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
fsz = length(xaxis)*length(yaxis)*length(zaxis);

if ~exist('rho','var')
%     load('G:\Mask Properties\RapidCoil_Duke_15T_Wire_StrippedTip.mat')
%     load('G:\Mask Properties\FinalRapidCoil_Duke_15T_Wire_StrippedTip.mat')
%     load('G:\Mask Properties\V2RapidCoil_Duke_Wire_StrippedTip_2.mat')
    
%     load('G:\Mask Properties\V2RapidCoil_LWPhant3_Wire_StrippedTip.mat')
%     load('G:\Mask Properties\FinalRapidCoil_LWPhant3_15T_Wire_StrippedTip.mat')
    
    load('G:\Mask Properties\FinalRapidCoil_Duke_15T_RealWire_StrippedTip.mat')
%     load('G:\Mask Properties\V2RapidCoil_Duke_15T_RealWire_StrippedTip.mat')
    
    
    if iscell(rho) == 1
        rho = cellfun(@(x) double(x), rho);
    end
end


xtip = 81;
ytip = 73;
ztip = 513;
%% FOR DUKE
maskR = rho; %Load Mask Properties first, if is in cell form run: rho = cellfun(@(x) double(x), rho);
maskR(maskR == 1049.75) = 1; %Density of Heart Lumen (blood)
maskR(maskR == 1.080800048828125e+03) = 1; %Density of Heart Muscle (plot rho and check precis value)
maskR(maskR ~= 1) = 0; % Make everything else 0
maskR = reshape(maskR, sz); %Reshape to 3D

% maskR(14:123,2:106,318:345); % Check in S4L what small box encompasses
% the Heart, as blood as same density you need to make everything else
% outside this box 0, hence the following lines:

% %%%% RCOIL
% maskR(1:14,:,:) = 0;
% maskR(:,1:2,:) = 0;
% maskR(:,:,1:318) = 0;
% maskR(123:end,:,:) = 0;
% maskR(:,106:end,:) = 0;
% maskR(:,:,345:end) = 0;

%%%% V2RCOIL
% maskR(1,:,:) = 0;
% % maskR(:,1:7,:) = 0;
% maskR(:,:,1:318) = 0;
% maskR(101:end,:,:) = 0;
% maskR(:,106:end,:) = 0;
% maskR(:,:,343:end) = 0;

%%%% FRCOIL
% maskR(1:12,:,:) = 0;
% maskR(:,1:2,:) = 0;
% maskR(:,:,1:322) = 0;
% maskR(130:end,:,:) = 0;
% maskR(:,106:end,:) = 0;
% maskR(:,:,349:end) = 0;

%%%% REAL WIRE V2OIL
maskR(1:21,:,:) = 0;
maskR(:,1:28,:) = 0;
maskR(:,:,1:469) = 0;
maskR(125:end,:,:) = 0;
maskR(:,131:end,:) = 0;
maskR(:,:,534:end) = 0;

% Voxelization makes everything patchy. Dilate and Erode to fill patches
se = strel('sphere',2); %Patches are small so small Dilate radius will do
dilmaskR = imdilate(maskR,se); %Dilate
filledmaskR = imerode(dilmaskR,se); %Erode, Heart Region should now be properly masked

wiredmaskR = filledmaskR;
% filledmaskR(50:81, 25:47, 1:333) = 0; % FOR RCOIL
% filledmaskR(34:64, 17:45, 1:333) = 0; % FOR V2RCOIL
% filledmaskR(46:77, 20:50, 1:340) = 0; % FOR FRCOIL
filledmaskR(78:99, 59:90, 505:535) = 0; % FOR REAL WIRE

%% E

filledmask = reshape(filledmaskR, [fsz 1]);
wiredmask = reshape(wiredmaskR, [fsz 1]);


%mask for MLS
% maskidx = find(filledmask);
% maskMLS = filledmask(maskidx);
maskMLS = filledmask(filledmask~=0);
% maskidx2 = find(wiredmask);
% sMask = wiredmask(maskidx2);
sMask = wiredmask(wiredmask ~= 0);

for ii = 1:8
% %%%% HEART SECTION
%     load(['G:\B1\B1_RapidCoil_Duke_1_5T_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)]) %RCOIL
%     load(['G:\B1\B1_FinalRapidCoil_Duke_portByPort_Wire_StrippedTip_1_5T\B1_channel' num2str(ii)]) %FINALRCOIL
%     load(['G:\B1\B1_V2RapidCoil_Duke_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)]) %V2RCOIL
    
%     load(['G:\B1\B1_V2RapidCoil_LWPhant3_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)])
%     load(['G:\B1\B1_FinalRapidCoil_LWPhant3_portByPort_Wire_StrippedTip_1_5T\B1_channel' num2str(ii)])
    
%     load(['G:\B1\B1_FinalRapidCoil_Duke_15T_RealWire_StrippedTip\B1_channel' num2str(ii)])
    load(['G:\E\E_V2RapidCoil_Duke_15T_RealWire_StrippedTip\E_channel' num2str(ii)])
    
    if ii == 1
        xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
        yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
        zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
        sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
        fsz = length(xaxis)*length(yaxis)*length(zaxis);
    end
    normE = sqrt(Snapshot0(:,1).^2 + Snapshot0(:,2).^2 + Snapshot0(:,3).^2);
%     RE(:,ii) = (normE+filledmask).*filledmask;%.*w(ii);
    REwired(:,ii) = (normE);%+wiredmask).*wiredmask;

end
% heartRE = reshape(RE,[sz 8]);
heartREwired = reshape(REwired, [sz 8]);

for ii = 1:8
    sumEwired(:,:,:,ii) = heartREwired(:,:,:,ii).*w(ii);
end


modedEwired = REwired*modes_1s(:,:,15);
for ii = 2:8
    Ewired_1s(:,ii-1) = modedEwired(:,ii).*w1(ii-1,15);
end
sumEwired_1s = reshape(Ewired_1s, [sz 7]);
%% B1

% figure;
for ii = 1:8
% %%%% HEART SECTION
%     load(['G:\B1\B1_RapidCoil_Duke_1_5T_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)]) %RCOIL
%     load(['G:\B1\B1_FinalRapidCoil_Duke_portByPort_Wire_StrippedTip_1_5T\B1_channel' num2str(ii)]) %FINALRCOIL
%     load(['G:\B1\B1_V2RapidCoil_Duke_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)]) %V2RCOIL
    
%     load(['G:\B1\B1_V2RapidCoil_LWPhant3_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)])
%     load(['G:\B1\B1_FinalRapidCoil_LWPhant3_portByPort_Wire_StrippedTip_1_5T\B1_channel' num2str(ii)])
    
%     load(['G:\B1\B1_FinalRapidCoil_Duke_15T_RealWire_StrippedTip\B1_channel' num2str(ii)])
    load(['G:\B1\B1_V2RapidCoil_Duke_15T_RealWire_StrippedTip\B1_channel' num2str(ii)])
    
    if ii == 1
        xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
        yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
        zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
        sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
        fsz = length(xaxis)*length(yaxis)*length(zaxis);
    end
    
%     RB1(:,ii) = (Snapshot0(:,1)+filledmask).*filledmask;%.*w(ii);
    RB1wired(:,ii) = (Snapshot0(:,1));%+wiredmask).*wiredmask;
    
end

% heartRB1 = reshape(RB1,[sz 8]);
heartRB1wired = reshape(RB1wired, [sz 8]);


for ii = 1:8
    sumB1wired(:,:,:,ii) = heartRB1wired(:,:,:,ii).*w(ii);
end

modedB1wired = RB1wired*modes_1s(:,:,15);
for ii = 2:8
    B1wired_1s(:,ii-1) = modedB1wired(:,ii).*w1(ii-1,15);
end
sumB1wired_1s = reshape(B1wired_1s, [sz 7]);

%%
yy = 74;
figure;distributionGenerator(squeeze(abs(sum(sumEwired_1s(:,yy,:,:),4))), xaxis, zaxis,1,0,40,'nearest');
%%
yy = 74;
figure;distributionGenerator(squeeze(abs(sum(sumB1wired_1s(:,yy,:,:),4))), xaxis, zaxis,1,0,2.5e-6,'nearest');
axis normal

%%
