%% Current Density Import
% Change load here to load desired model

% load cDensity_norman_heart_centred_LARGE_MODEL
load cDensity_preg5_fetus_centred

currDensity = cD(:,:,:,1,1)+cD(:,:,:,1,2)+cD(:,:,:,2,1)+cD(:,:,:,2,2)+cD(:,:,:,3,1)+cD(:,:,:,3,2);
% for ii=1:591 imagesc(real(squeeze(currDensity(:,:,ii))),[0 1]);pause(0.1);end

%% Generating Mask
% 
cD_Mask = currDensity;
cD_Mask(cD_Mask ~= 0) = 1;
% for ii=1:591 imagesc(real(squeeze(cD_Mask(:,:,ii))),[0 1]);pause(0.01);end
save('Preg5_Mask','cD_Mask');
%% Interpolating Mask

%Change load here to import desired Mesh and Mask
% load RAW_5mm_Mesh_Data
% load Large_Norman_Mask
load RAW_5mm_Mesh_Data_preg5
load Preg5_Mask

% Dimensions of grid cells
dx=5e-3;
dy=5e-3;
dz=5e-3;

temp_Mask = cD_Mask;

[xi,yi,zi] = meshgrid(xlims(1):dx:xlims(2),ylims(1):dy:ylims(2),zlims(1):dz:zlims(2));

cD_Msk_interp = interp3(x, y, z, temp_Mask, xi, yi, zi,'nearest');

%% Acquiring Heart/Fetus Slice

% L_Norman_Msk_H_Slice = squeeze(cD_Msk_interp(:,:,150));
% Norman_Msk_H_Slice = squeeze(cD_Msk_interp(:,:,150));
% save('L_Norman_Msk_H_Slice','L_Norman_Msk_H_Slice')
% save('Norman_Msk_H_Slice','Norman_Msk_H_Slice')
Preg_Msk_F_Slice = squeeze(cD_Msk_interp(:,:,213));
save('Preg5_Msk_F_Slice','Preg_Msk_F_Slice')