function [r1s, r2s, r3s2m, r3s] = ISMRM19_PLOTTER(points, maxEV_fullQ, maxEV_1s1m, maxEV_2s2m, maxEV_3s2m, maxEV_3s3m, chandle)
kk = 1;
mm = 1;
for ii = 1:length(points)
    for jj = ii+1:length(points)
        points_2s(kk,1:2) = [points(ii) points(jj)];
        for ll = jj+1:length(points)
            points_3s(mm,1:3) = [points(ii) points(jj) points(ll)];
            mm = mm+1;
        end
        kk = kk + 1;
    end
end
%% PROCESS

kk = 0;
for ii = points
    kk = kk+1;
    %% 1-Sensor   
    if kk == 1;
        r1s = [];
    end
    r1s = [r1s; (maxEV_fullQ(kk)-maxEV_1s1m(kk))/maxEV_fullQ(kk) ii];
        
	%% 2-Sensor
    [idx2,~] = find(points_2s(:,2) == ii);
    
    if kk == 1;
        r2s = (maxEV_fullQ(kk)-min(maxEV_2s2m(idx2)))/maxEV_fullQ(kk);
    else
        r2s = [r2s; (maxEV_fullQ(kk)-min(maxEV_2s2m(idx2)))/maxEV_fullQ(kk) ii];
    end
    
    %% 3-Sensor 2-ModesOut
    [idx3s2m,~] = find(points_3s(:,3) == ii);
    
    if kk == 1 || kk == 2;
        r3s2m = (maxEV_fullQ(kk)-min(maxEV_3s2m(idx3s2m)))/maxEV_fullQ(kk);
    else
        r3s2m = [r3s2m; (maxEV_fullQ(kk)-min(maxEV_3s2m(idx3s2m)))/maxEV_fullQ(kk) ii];
    end
    
    %% 3-Sensor
    [idx3,~] = find(points_3s(:,3) == ii);
    
    if kk == 1 || kk == 2;
        r3s = (maxEV_fullQ(kk)-min(maxEV_3s3m(idx3)))/maxEV_fullQ(kk);
    else
        r3s = [r3s; (maxEV_fullQ(kk)-min(maxEV_3s3m(idx3)))/maxEV_fullQ(kk) ii];
    end
    
end
%% PLOTTER
figure;
set(gcf,'Position',[100 100 600 300])
set(gca, 'FontSize', 16)

plot(r1s(:,2),r1s(:,1)*100,'-o',...
    'LineWidth', 3,...
    'MarkerSize', 6,...
    'MarkerEdgeColor','none',...
    'MarkerFaceColor',[0,0,0])
hold on
plot(r2s(:,2),r2s(:,1)*100,'-o',...
    'LineWidth', 3,...
    'MarkerSize', 6,...
    'MarkerEdgeColor','none',...
    'MarkerFaceColor',[0 0 0])
plot(r3s2m(:,2),r3s2m(:,1)*100,'-o',...
    'LineWidth', 3,...
    'MarkerSize', 6,...
    'MarkerEdgeColor','none',...
    'MarkerFaceColor',[0 0 0])
plot(r3s(:,2),r3s(:,1)*100,'-o',...
    'LineWidth', 3,...
    'MarkerSize', 6,...
    'MarkerEdgeColor','none',...
    'MarkerFaceColor',[0 0 0])

xlim([points(1) points(end)+20]);
grid on
grid minor
title(chandle,'FontSize',14)

legend({'1-Sensor | 7-D.o.F.', '2-Sensor | 6-D.o.F.','3-Sensor | 6-D.o.F.', '3-Sensor | 5-D.o.F.'},'Location', 'southeast')

xlabel('Deepest Sensor Position','FontSize',14)
ylabel('Reduction in wcSAR/\muT^{2} (%)','FontSize',14)


end