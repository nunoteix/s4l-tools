function [cmap] = b1weightsMap(b1weights_2s, b1weights_1s, b1weights, points_w)
    map = ones(length(points_w));
    upperTM = triu(map,1);
    upperTM(upperTM~=0) = fliplr(b1weights_2s.*1e12);

    cmap = fliplr(upperTM+tril(map,-1));
%     cmap = fliplr(upperTM+upperTM.');
    
    cmap(cmap==0) = eye(length(points_w))*b1weights_1s'.*1e12;
    cmap(cmap==1) = 0; 
    
    plasmawhite = plasma(4096);
    plasmawhite(1,:) = [1,1,1];
    
    
    figure;
    set(gcf,'Position',[1 1 1020 680])
    set(gca, 'FontSize', 16)
    colormap(plasmawhite)


    
    subplot(1,10,2)
    imagesc(b1weights.*1e12)
    cbh = colorbar('westoutside');
    cbh.Position(3) = 0.025;
    cbh.Position(1) = 0.15;
    caxis([0 max(abs(b1weights.*1e12))])
    ylabel('B1+ per Unit Power Without Nulling', 'FontSize', 20, 'FontWeight', 'bold')
    
    a = get(gca,'XTickLabel');
    set(gca,'XTickLabel',a,'fontsize',18)
    set(gca,'xtick',[])
    set(gca,'ytick',[])
%     
    
    subplot(1,10,4:10)
    imagesc(points_w, fliplr(points_w),abs(cmap))
    set(gca,'Ydir','Normal')
    caxis([0 max(abs(b1weights.*1e12))])
%     caxis([0 0.12])



    a = get(gca,'XTickLabel');
    set(gca,'XTickLabel',a,'fontsize',14)
    set(gca,'Ydir','reverse')
    axis on
    xlabel('Sensor Position 1',  'FontSize', 20)
    ylabel('Sensor Position 2', 'FontSize', 20)
    title('B1+ per Unit Power After Nulling', 'FontSize', 20)
    set(gca,'box','off')    
    
    

end
 