function [A] = Gen3D(dist3D, xaxis, yaxis, zaxis, xquery, yquery, zquery)

[X,Y,Z] = meshgrid(yaxis, xaxis, zaxis);

[Xq,Yq,Zq] = meshgrid(linspace(min(yaxis),max(yaxis),yquery),...
    linspace(min(xaxis),max(xaxis),xquery),...
    linspace(min(zaxis),max(zaxis),zquery));

% [Xq,Yq,Zq] = meshgrid(min(yaxis):yquery:max(yaxis),...
%     min(xaxis):xquery:max(xaxis),...
%     min(zaxis):zquery:max(zaxis));

A = interp3(X,Y,Z, dist3D, Xq, Yq, Zq,'nearest');
% figure;
% if fig == 1
%     imagesc(x_axis, y_axis,A,[lowerBound upperBound]);axis square;colormap jet
%     colorbar
end

