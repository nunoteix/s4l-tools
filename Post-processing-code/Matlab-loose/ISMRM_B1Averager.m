for ii = 1:8
    
%     w(ii) = exp(1i*deg2rad(45*(ii-1)));
    
%     load(['G:\B1\B1_RapidCoil_LWPhant3_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)])
%     RB1(:,ii) = Snapshot0(:,1).*w(ii);
%     
%     load(['G:\B1\B1_V2RapidCoil_LWPhant3_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)])
%     V2RB1(:,ii) = Snapshot0(:,1).*w(ii);
    
    load(['G:\B1\B1_RapidCoil_Duke_1_5T_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)])
    RB1(:,ii) = Snapshot0(:,1);%.*w(ii);
    
end


xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];


%% Section Attempting to Create Heart Mask
maskR = rho; %Load Mask Properties first, if is in cell form run: rho = cellfun(@(x) double(x), rho);
maskR(maskR == 1049.75) = 1; %Density of Heart Lumen (blood)
maskR(maskR == 1.080800048828125e+03) = 1; %Density of Heart Muscle (plot rho and check precis value)
maskR(maskR ~= 1) = 0; % Make everything else 0
maskR = reshape(maskR, sz); %Reshape to 3D

% maskR(14:123,2:106,318:345); % Check in S4L what small box encompasses
% the Heart, as blood as same density you need to make everything else
% outside this box 0, hence the following lines:

maskR(1:14,:,:) = 0;
maskR(:,1:2,:) = 0;
maskR(:,:,1:318) = 0;
maskR(123:end,:,:) = 0;
maskR(:,106:end,:) = 0;
maskR(:,:,345:end) = 0;

% Voxelization makes everything patchy. Dilate and Erode to fill patches
se = strel('sphere',10); %Patches are small so small Dilate radius will do
dilmaskR = imdilate(maskR,se); %Dilate
filledmaskR = imerode(dilmaskR,se); %Erode, Heart Region should now be properly masked

filledmaskR(50:81, 25:47, 1:333) = 0;

%%
% figure;
for ii = 1:8
% %%%% HEART SECTION
    load(['G:\B1\B1_RapidCoil_Duke_1_5T_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)])
    RB1(:,ii) = Snapshot0(:,1);%.*w(ii);
    rsRB1 = reshape(RB1(:,ii), sz);
    heartRB1(:,:,:,ii) = rsRB1.*filledmaskR;
    
    subplot(2,4,ii)
%     distributionGenerator(abs(squeeze(heartRB1(:,35,318:345,ii))),xaxis,zaxis(318:345),1,0,2e-7,'nearest');
    distributionGenerator(angle(squeeze(heartRB1(:,35,318:345,ii))),xaxis,zaxis(318:345),1,-3.14,3.14,'nearest');
end
%%


% figure;
for ii = 1:8
    tempF = heartRB1(:,:,:,ii);
    tempF = tempF(:);
    tempL = find(tempF);
    phase(ii) = mean(angle(tempF(tempL)));
    w(ii) = exp(1i*phase(ii));
    zphi_heartRB1(:,:,:,ii) = heartRB1(:,:,:,ii).*w(ii);
%     
%     subplot(2,4,ii)
%     distributionGenerator(abs(squeeze(zphi_heartRB1(:,35,318:345,ii))),xaxis,zaxis(318:345),1,0,2e-7,'nearest');
%     distributionGenerator(angle(squeeze(zphi_heartRB1(:,35,318:345,ii))),xaxis,zaxis(318:345),1,-3.14,3.14,'nearest');

end

absZPhi = abs(sum(zphi_heartRB1(:,:,:,:),4));

tempL = find(absZPhi);
avgB1 = mean(absZPhi(tempL));
% 
% absNormal = abs(sum(heartRB1(:,:,:,:),4));
% figure;distributionGenerator(squeeze(absNormal(:,35,318:345)),xaxis,zaxis(318:345),1,0,1e-6,'nearest');
% figure;distributionGenerator(squeeze(absZPhi(:,35,318:345)),xaxis,zaxis(318:345),1,0,1e-6,'nearest');

for ii = 1:8
    for jj = 1:8
         E_temp(:,:,:,:,jj) = squeeze(Ftemp(:,:,:,:,jj).*v3(jj,ii));
    end
end
%%

rsRB1 = sum(reshape(RB1, [sz 8]),4); % Short Coil

% %%%% Long Coil
load('G:\Mask Properties\V2RapidCoil_LWPhant3_3T_Wire_StrippedTip')
maskV2R = rho;
maskV2R(maskV2R > 0) = 1;
maskV2R = reshape(maskV2R, sz);

% %%%% Short Coil
load('G:\Mask Properties\RapidCoil_LWPhant3_3T_Wire_StrippedTip')
maskR = rho;
maskR(maskR > 0) = 1;
maskR = reshape(maskR, sz);

maskR(26:55, 26:55, 1:274) = 0; %LWPhant3 Short Coil
maskR(:,:,[235 284]) = 0;

maskV2R(26:55, 26:55, 1:274) = 0; %LWPhant3 Long Coil
maskV2R(:,:,[185 284]) = 0;

maskR(50:81, 25:47, 1:333) = 0; %Duke Short Coil (1.5T)
maskR(:,:,[307 361]) = 0;

% t = distributionGenerator(abs(squeeze(rsRB1(:,32,:))), xaxis, zaxis,1,0,1e-6);
% tm = distributionGenerator(squeeze(maskR(:,32,:)),xaxis,zaxis,0,0,0,'nearest');
% imagesc(imrotate(t.*tm,-90))


rsV2RB1 = sum(reshape(V2RB1, [sz 8]), 4); % Long Coil
rsRB1 = sum(reshape(RB1, [sz 8]),4); % Short Coil

covV2RB1 = rsV2RB1(:,:,185:284).*maskR(:,:,185:284); % Long Coil
covRB1 = rsRB1(:,:,235:284).*maskR(:,:,235:284); % Short Coil
covRB1 = rsRB1(:,:,307:361).*maskR(:,:,307:361); %Short Coil Duke (1.5T)

V2RmB1 = mean(abs(covV2RB1(:))); % Long Coil
RmB1 = mean(abs(covRB1(:))); % Short Coil

%%
t = distributionGenerator(abs(squeeze(rsRB1(:,ytip,:))), xaxis, zaxis,0,0,1e-6);
t2 = distributionGenerator(abs(squeeze(rsV2RB1(:,32,:))), xaxis, zaxis,0,0,1e-6);
tm = distributionGenerator(squeeze(maskR(:,ytip,:)),xaxis,zaxis,0,0,0,'nearest');
tm2 = distributionGenerator(squeeze(maskV2R(:,32,:)),xaxis,zaxis,0,0,0,'nearest');

% t([2970-2845:2970-2840 2970-2355:2970-2350],:) = 0;
% t2([2970-2845:2970-2840 2970-1855:2970-1850],:) = 0;

figure(1); subplot(2,1,1);imagesc(zaxis, xaxis, imrotate(t.*tm,-90))
colormap(jetblack)
caxis([0 0.85e-6])
title(['B1+ Slice || Mean in Coil Coverage = ' num2str(RmB1)])
colorbar
figure(1); subplot(2,1,2);imagesc(zaxis, xaxis, imrotate(t2.*tm2,-90))
colormap(jetblack)
caxis([0 0.85e-6])
colorbar
title(['B1+ Slice || Mean in Coil Coverage = ' num2str(V2RmB1)])
