%% Get Point by Point from SURF GRID (S4Life) (easy straight wire)

[zOP, zOIdx] = sortrows(JPoints,3); % Sort Points by z-coord
zOJ = SurfJ(zOIdx,:); % Sort Current the same way as above

[uni, posIdx, uniIdx] = unique(zOP(:,3)); % Get each unique z-coord
Jaxis = zOP(posIdx,3); % get z-axis

for ii = 1:length(uni)
    
    loopIdx = find(uniIdx == ii); % Find indexes for the first unique value
    fullJ(ii,:) = mean(zOJ(loopIdx,:));
end


%%
chan = 8;
figure(76)
ii = 0;
% for ii = 0:0.2:2*pi
pedge = Jaxis(1)+615;
figure(76)
% subplot(2,4,chan)
% plot(Jaxis, abs(compJ(:,3)),'LineWidth',2)
plot(Jaxis, abs(sum(fullJ,2)),'LineWidth',2)
l1 = line([pedge pedge], [-5 5], 'Color', 'k');
legend([l1],'Phantom Boundary');legend('boxoff')
axis([-1300 200 -0.1 4.5])
xlabel('Length of the Wire (z-axis)')
ylabel('Surface Current Density (J)')
title(['V2RCoil - Phantom - Str. Tip - Channel ' num2str(chan)])
set(gca,'FontSize',12,'FontWeight','bold','LineWidth',2);
hold on
    
%% Get Point by Point from SURF GRID (S4Life) (complicated 90deg curves wires)

clearvars -except JPoints SurfJ test J_Ch1 J_Ch2 J_Ch3 J_Ch4 J_Ch5 J_Ch6 J_Ch7 J_Ch8 pedge
wo = [3 2 3 1 3];
t = [3 1 2];
JPoints1 = JPoints(:,t);
[OP, OIdx] = sortrows(JPoints1);% Sort Points by z-coord
OP = OP(:,[2 3 1]);
OJ = SurfJ(OIdx,:); % Sort Current the same way as above

for ii = 1:length(OP(:,1))
    if OP(ii,3) == OP(ii+20,wo(1))
        zend = ii;
        zsect = 1:zend-1;
        break
    end
end
for ii = zend:length(OP(:,1))
    if OP(ii,2) == OP(ii+20,wo(2))
        yend = ii;
        ysect = zend:yend-1;
        break
    end
end
for ii = yend:length(OP(:,1))
    if OP(ii,3) == OP(ii+20,wo(3))
        zend2 = ii;
        zsect2 = yend:zend2-1;
        break
    end
end
for ii = zend2:length(OP(:,1))
    if OP(ii,1) == OP(ii+20,wo(4))
        xend = ii;
        xsect = zend2:xend-1;
        break
    end
end
zsect3 = xend:length(OP(:,1));


[Zuni, ZposIdx, ZuniIdx] = unique(OP(zsect,wo(1))); % Get each unique z-coord
OPtemp = OP(zsect,3); 
axpatch1 = OPtemp(ZposIdx);
[Yuni, YposIdx, YuniIdx] = unique(OP(ysect,wo(2))); % Get each unique y-coord
OPtemp = OP(ysect,2); 
axpatch2 = OPtemp(YposIdx);
[Zuni2, ZposIdx2, ZuniIdx2] = unique(OP(zsect2,wo(3)));
OPtemp = OP(zsect2,3); 
axpatch3 = OPtemp(ZposIdx2);
[Xuni, XposIdx, XuniIdx] = unique(OP(xsect,wo(4))); % Get each unique x-coord
OPtemp = OP(xsect,1); 
axpatch4 = OPtemp(XposIdx);
[Zuni3, ZposIdx3, ZuniIdx3] = unique(OP(zsect3,wo(5)));
OPtemp = OP(zsect3,3); 
axpatch5 = OPtemp(ZposIdx3);

ax = (axpatch2-axpatch2(1)+axpatch2(2)-axpatch2(1))+axpatch1(end); %start of patch combination
ax2 = (axpatch3-axpatch3(1)+axpatch3(2)-axpatch3(1))+ax(end);
ax3 = (axpatch4-axpatch4(1)+axpatch4(2)-axpatch4(1))+ax2(end);
ax4 = (axpatch5-axpatch5(1)+axpatch5(2)-axpatch5(1))+ax3(end);

Jaxis = cat(1,axpatch1, ax, ax2, ax3, ax4);

OJtemp = OJ(zsect,:);
sp = 0;
for ii = 1:length(Zuni) 
    loopIdx = find(ZuniIdx == ii); % Find indexes for the first unique value
    fullJ(ii+sp,:) = mean(OJtemp(loopIdx,:));
end

OJtemp = OJ(ysect,:);
sp = length(fullJ);
for ii = 1:length(Yuni) 
    loopIdx = find(YuniIdx == ii); % Find indexes for the first unique value
    fullJ(-ii+sp+1+length(Yuni),:) = mean(OJtemp(loopIdx,:));
end

OJtemp = OJ(zsect2,:);
sp = length(fullJ);
for ii = 1:length(Zuni2) 
    loopIdx = find(ZuniIdx2 == ii); % Find indexes for the first unique value
    fullJ(ii+sp,:) = mean(OJtemp(loopIdx,:));
end

OJtemp = OJ(xsect,:);
sp = length(fullJ);
for ii = 1:length(Xuni) 
    loopIdx = find(XuniIdx == ii); % Find indexes for the first unique value
    fullJ(-ii+sp+1+length(Xuni),:) = mean(OJtemp(loopIdx,:));
end

OJtemp = OJ(zsect3,:);
sp = length(fullJ);
for ii = 1:length(Zuni3) 
    loopIdx = find(ZuniIdx3 == ii); % Find indexes for the first unique value
    fullJ(ii+sp,:) = mean(OJtemp(loopIdx,:));
end
clearvars -except Jaxis fullJ test JPoints SurfJ J_Ch1 J_Ch2 J_Ch3 J_Ch4 J_Ch5 J_Ch6 J_Ch7 J_Ch8 pedge
%%
figure(75)
set(gcf,'color','w');


fullJmod = max(fullJ,[],2);
% fullJmod = sum(fullJ,2);
% fullJmod = abs(compJ);
for ii = 31:length(fullJmod)-200
    if abs(fullJmod(ii+1))/abs(fullJmod(ii)) < 0.9
        fullJmod(ii+1) = fullJmod(ii-10);
    end
end

compJ = zeros(length(Jaxis),1);
for ii = 1:length(Jaxis)
    smaller = find(Jaxis < Jaxis(ii)+2.5); 
    bigger = find(Jaxis > Jaxis(ii)-2.5);
    sect = intersect(bigger,smaller);
    compJ(ii) = mean(abs(fullJmod(sect,:))).*exp(1i*(mean(angle(fullJmod(sect,:)))));
%     compJ(ii) = mean(fullJmod(sect,:));
end

% plot(Jaxis,abs(compJ))%, 'LineWidth', 2);

% hold on
plot(Jaxis, abs(fullJmod))
% plot(Jaxis, abs(sum(fullJ,2)))
% hold off
% compJ = fullJ(fullJ~=0);
% compJ = reshape(compJ,length(compJ)/3, 3);