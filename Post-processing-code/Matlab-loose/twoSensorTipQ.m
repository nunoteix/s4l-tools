function [maxEV_redQ_2s2m, maxEV_redQ_2s2m_noNorm, pp_absShimmed, v_all, b1weights_2s, w2, avgB1] = twoSensorTipQ(nModesOut, nChannels, points_pos, l, J, maskMLS, sRB1r, sRB1wiredr, Q_tip)
    nModes = nChannels - nModesOut;
    disp(['For ' num2str(nChannels) ' channels and ' num2str(nModesOut)...
        ' sensor: Calculating maximum Eigenvalue of the Q-Matrix reduced to '...
        num2str(nModes) ' modes.'])
    
%     posidx = round(points_pos/st)+pedge_ind;
    
    for kk = 1:length(points_pos)
        disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_pos))])
        [~,posidx1] = min(abs(l-points_pos(kk,1)));
        [~,posidx2] = min(abs(l-points_pos(kk,2)));
%         plot_pos = (posidx(kk,:)-pedge_ind).*st;
        [~,~,v]=svd(J([posidx1, posidx2],:));
%         sens_pos(kk,:) = plot_pos;
        v_dmss = v(:,nModesOut+1:nChannels);
        v_all(:,:,kk) = v;

%         for ii = 1:nChannels
%             for jj = 1:nChannels
%                  oneMode_B1(:,jj) = sRB1r(:,jj).*v(jj,ii);
%                  oneMode_wiredB1(:,jj) = sRB1wiredr(:,jj).*v(jj,ii);%Transform B1+ per channel into per mode
%             end
%             moded_B1(:,ii,kk) = sum(oneMode_B1, 2);
%             moded_wiredB1(:,ii,kk) = sum(oneMode_wiredB1, 2);% Build the 8-mode matrix
%         end

        moded_B1(:,:,kk) = sRB1r*v;
        moded_wiredB1(:,:,kk) = sRB1wiredr*v;
        % SHIM USING MLS
        [w2(:,kk),~] = MLS(moded_B1(:,nModesOut+1:end,kk).*1e6, maskMLS,[],6,200,1e-4,'quiet','nobox');
        [avgB1(kk),holder] = applyShim(moded_wiredB1(:,nModesOut+1:end,kk),w2(:,kk),nModes);

    %     shimmed_B1 = zeros(length(wiredmask),1);
    %     shimmed_B1(wired_pos) = holder;    
        pp_absShimmed(:,nModesOut,kk) = holder; % Save shimmed B1+ for all sens_pos

        % SECTION FOR EIG VALUES @TIP 
        Q_tip_normed = Q_tip./((avgB1(kk)^2)/(sum(abs(w2(:,kk)).^2)/nChannels)); % Normalise the "WCSAR" to that AvgB1+
        
        Q_dmss = v_dmss'*(Q_tip_normed)*v_dmss; % Reduce Q-matrix to the DMSS
        Q_dmss_noNorm = v_dmss'*(Q_tip)*v_dmss;
        
        maxEV_redQ_2s2m(kk) = max(eig(Q_dmss));
        maxEV_redQ_2s2m_noNorm(kk) = max(eig(Q_dmss_noNorm));
    end
    
    b1weights_2s = ((avgB1.^2)./(sum(abs(w2).^2,1)/8));
end