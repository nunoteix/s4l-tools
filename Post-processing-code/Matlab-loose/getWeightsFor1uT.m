function [uTweights] = getWeightsFor1uT()
[fNames,fPath] = uigetfile('*.mat','MultiSelect', 'on', 'Pick E or H Fields');
path = strcat(fPath, fNames(1));
load(path{1,1})
sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
for ii = 1:8
    path = strcat(fPath, fNames(ii));
    load(path{1,1})
    F(:,:,:,:,ii) = reshape(Snapshot0, [sz 2]);
end
xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;

[~, xind] = min(abs(xaxis));
[~, yind] = min(abs(yaxis));
[~, zind] = min(abs(zaxis));

phaseShifts_neg = exp(degtorad([-45 -90 -135 -180 -225 -270 -315 -360]).*1i);

FNorm = zeros(size(F));
uTweights = zeros(1,8);

for ii = 1:8
    w = 1e-6/abs(F(xind,yind,zind,1,ii));
    uTweights(ii) = w*phaseShifts_neg(ii);
    FNorm(:,:,:,:,ii) = F(:,:,:,:,ii).*(uTweights(ii));
    subplot(2,4,ii)
    distributionGenerator(abs(FNorm(:,:,zind,ii)),xaxis,yaxis,0,1e-5);
end

figure(2)
distributionGenerator(squeeze(abs(sum(FNorm(:,:,zind,1,:),5))),xaxis,yaxis,0,1.65e-5);
