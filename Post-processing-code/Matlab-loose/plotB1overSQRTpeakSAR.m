%% ALL PLOT
disp('Open B1 Shimmed Sum Files')
B1fileName = uigetfile('*.mat','B1','MultiSelect','on');
disp('Open 10g Averaged SAR Files')
SARfileName = uigetfile('*.mat','SAR','MultiSelect','on');
disp('Open J Files')
JfileName = uigetfile('*.mat','J','MultiSelect','on');
cx = [90 75 93 93 102 102 109 109 93 93 76 76];
cz = [100 55 68 68 52 52 63 63 49 49 49 49];

figure; hold on;
axis([-11 11 0 30])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('8-channel (FracD)','8-channel (SSAD)','32-channel (CloserZ)','32-channel (CloserZAIR)','32-channel (CloserZX)',...
    '32-channel (CloserZXAIR)','32-channel (CloserZXTHIN)','32-channel (CloserZXTHINAIR)','32-channel (Interc)',...
    '32-channel (IntercAIR)','32-channel (thInterc)','32-channel (thIntercAIR)','Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')

%% SSAD PLOT
disp('Open B1 Shimmed Sum Files')
B1fileName = uigetfile('*.mat','B1','MultiSelect','on');
disp('Open 10g Averaged SAR Files')
SARfileName = uigetfile('*.mat','SAR','MultiSelect','on');
disp('Open J Files')
JfileName = uigetfile('*.mat','J','MultiSelect','on');
cx = [75 93 93 102 102 109 109 93 93 76 76];
cz = [55 68 68 52 52 63 63 49 49 49 49];

figure; hold on;
axis([-8 8 0 16])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('8-channel (SSAD)','32-channel (CloserZ)','32-channel (CloserZAIR)','32-channel (CloserZX)',...
    '32-channel (CloserZXAIR)','32-channel (CloserZXTHIN)','32-channel (CloserZXTHINAIR)','32-channel (Interc)',...
    '32-channel (IntercAIR)','32-channel (thInterc)','32-channel (thIntercAIR)','Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')

%% SSAD SELECTED PLOT
disp('Open B1 Shimmed Sum Files')
B1fileName = uigetfile('*.mat','B1','MultiSelect','on');
disp('Open 10g Averaged SAR Files')
SARfileName = uigetfile('*.mat','SAR','MultiSelect','on');
disp('Open J Files')
JfileName = uigetfile('*.mat','J','MultiSelect','on');
cx = [75 102 93];
cz = [55 52 49];

figure; hold on;
axis([-8 8 5 10])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('8-channel (SSAD)',...
    '32-channel (CloserZXAIR)',...
    '32-channel (IntercAIR)','Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')

%% SSAD SELECTED + LOOPS PLOT
disp('Open B1 Shimmed Sum Files')
B1fileName = {'ShimmedB1_8xSSAD.mat','ShimmedB1_32xIntercDips_w_LSens_AIRSUB.mat','B1Shimmed_32Loop.mat'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_8SSAD.mat','SAR10g_32IntercDips_AIR.mat','SAR10g_32Loops.mat'};
disp('Open J Files')
JfileName = {'J_8SSAD.mat','J_32IntercDips.mat','J_32xLoop.mat'};
cx = [75 93 133];
cz = [55 49 70];

figure; hold on;
axis([-8 8 5 10])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('8-channel (SSAD)',...
    '32-channel (IntercAIR)','32-channel (Loops)',...
    'Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')
%% FRACD8
disp('Open B1 Shimmed Sum Files')
B1fileName = {'ShimmedB1_32xIntercDips_w_LSens_AIRSUB.mat','ShimmedB1_8xFracD.mat'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_32IntercDips_AIR.mat','SAR10g_8FracD.mat'};
disp('Open J Files')
JfileName = {,'J_32IntercDips.mat','J_8FracD.mat'};
cx = [93 90];
cz = [49 100];

figure; hold on;
axis([-8 8 5 10])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('32-channel (IntercAIR)','8-channel (Frac Dipole)',...
    'Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')

%% FRACD16
disp('Open B1 Shimmed Sum Files')
B1fileName = {'ShimmedB1_8xFracD.mat','B1Shimmed_16FracD.mat'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_8FracD.mat','SAR10g_16FracD.mat'};
disp('Open J Files')
JfileName = {'J_8FracD.mat','J_16FracD.mat'};
cx = [90 91];
cz = [100 98];

figure; hold on;
axis([-8 8 5 10])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('8-channel (Frac Dipole)',...
    '16-channel (Frac Dipole)','Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')

%% FRACM32
disp('Open B1 Shimmed Sum Files')
B1fileName = {'ShimmedB1_8xFracD.mat','B1Shimmed_32FracM.mat'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_8FracD.mat','SAR10g_32FracM.mat'};
disp('Open J Files')
JfileName = {'J_8FracD.mat','J_32FracM.mat'};
cx = [90 99];
cz = [100 101];

figure; hold on;
axis([-8 8 5 10])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('8-channel (Frac Dipole)','32-channel (Frac Monopole)',...
    'Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')

%% LOOPS32
disp('Open B1 Shimmed Sum Files')
B1fileName = {'B1Shimmed_32LoopStraight.mat','B1Shimmed_32FracM.mat'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_32LoopsStraight.mat','SAR10g_32FracM.mat'};
disp('Open J Files')
JfileName = {'J_32xLoopStraight.mat','J_32FracM.mat'};
cx = [125 99];
cz = [68 101];

figure; hold on;
axis([-8 8 5 10])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('32-channel (Loops)','32-channel (Frac Monopole)',...
    'Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')
%% 16FracD2
disp('Open B1 Shimmed Sum Files')
B1fileName = {'B1Shimmed_16FracD2.mat','B1Shimmed_32FracM.mat','B1Shimmed_16FracD'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_16FracD2.mat','SAR10g_32FracM.mat','SAR10g_16FracD.mat'};
disp('Open J Files')
JfileName = {'J_16FracD2.mat','J_32FracM.mat','J_16FracD'};
cx = [91 99 91];
cz = [99 101 98];

figure; hold on;
axis([-8 8 5 10])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('16-channel (Frac Dipole2)','32-channel (Frac Monopole)',...
    '16-channel (Frac Dipole)','Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')
%% 32FracMSourceOut
disp('Open B1 Shimmed Sum Files')
B1fileName = {'B1Shimmed_32FracMSO.mat','B1Shimmed_32FracMSourceOut.mat','B1Shimmed_16FracD'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_32FracMSO.mat','SAR10g_32FracMSourceOut.mat','SAR10g_16FracD.mat'};
disp('Open J Files')
JfileName = {'J_32FracMSO.mat','J_32FracMSourceOut.mat','J_16FracD'};
cx = [100 91 91];
cz = [95 95 98];

figure; hold on;
axis([-8 8 5 10])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('32-channel (Frac MonopoleSO)','32-channel (Frac Monopole)',...
    '16-channel (Frac Dipole)','Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')
%% 8FracD10mm
disp('Open B1 Shimmed Sum Files')
B1fileName = {'B1Shimmed_8FracD10mm.mat','B1Shimmed_16FracD'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_8FracD10mm.mat','SAR10g_16FracD.mat'};
disp('Open J Files')
JfileName = {'J_8FracD10mm.mat','J_16FracD.mat'};
cx = [68 91];
cz = [81 98];

figure; hold on;
axis([-8 8 0 10])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    pSAR10g
    max(abs(B1_shimmed_sum(cx(ii),end/2-4:end/2+4,cz(ii))))
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('8-channel (Frac Dipole 10mm)','16-channel (Frac Dipole 10mm)',...
    'Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')

%% FRACD810mm vs. IntercDipsAir
disp('Open B1 Shimmed Sum Files')
B1fileName = {'ShimmedB1_32xIntercDips_w_LSens_AIRSUB.mat','B1Shimmed_8FracD10mm.mat'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_32IntercDips_AIR.mat','SAR10g_8FracD10mm.mat'};
disp('Open J Files')
JfileName = {,'J_32IntercDips.mat','J_8FracD10mm.mat'};
cx = [93 68];
cz = [49 81];

figure; hold on;
axis([-8 8 5 10])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    pSAR10g
    max(abs(B1_shimmed_sum(cx(ii),end/2-4:end/2+4,cz(ii))))
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('32-channel (IntercAIR)','8-channel (Frac Dipole)',...
    'Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')

%% FRACM32Patch + 
disp('Open B1 Shimmed Sum Files')
B1fileName = {'B1Shimmed_32FracM+PatchGAP.mat','B1Shimmed_8FracD10mm.mat'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_32FracM+PatchGAP.mat','SAR10g_8FracD10mm.mat'};
disp('Open J Files')
JfileName = {'J_32FracM+PatchGAP.mat','J_8FracD10mm.mat'};
cx = [103 68];
cz = [104 81];

figure; hold on;
axis([-8 8 5 13])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    pSAR10g
    max(abs(B1_shimmed_sum(cx(ii),end/2-4:end/2+4,cz(ii))))
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('32-channel (IntercAIR)','8-channel (Frac Dipole)',...
    'Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')
%% FRACD SMALL vs Patch
disp('Open B1 Shimmed Sum Files')
B1fileName = {'B1Shimmed_32xFracM+PatchGAP.mat','B1Shimmed_32xSmallFracDapart.mat','B1Shimmed_8xFracD20mm.mat'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_32xFracM+PatchGAP.mat','SAR10g_32xSmallFracDapart.mat','SAR10g_8xFracD20mm.mat'};
disp('Open J Files')
JfileName = {'J_32xFracM+PatchGAP.mat','J_32xSmallFracDapart.mat','J_8xFracD20mm.mat'};
cx = [103 108 68];
cz = [104 114 81];

figure(01); hold on;
axis([-8 8 5 25])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    J = load(JfileName{ii});
    [SAR3D, axs, pSAR10g(ii)] = OptSAR10g_plotter(SAR10g,LUT,J,0);
    pSAR10g(ii)
    max(abs(B1_shimmed_sum(cx(ii),end/2-4:end/2+4,cz(ii))))
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g(ii)).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('32-channel (Frac Monopole + Patch)','32-channel (Small Frac Dipole)','8-channel (Frac Dipole)',...
    'Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')

figure(02)
load(B1fileName{1})
load(JfileName{1})
J = sum(Snapshot0,2);
J(abs(J)>=0.005)=1;
J(abs(J)~=1)=0;
J = reshape(J,[length(Axis0)-1 length(Axis1)-1 length(Axis2)-1]);
visualizeDistr2((abs(B1_shimmed_sum(:,:,cz(1)))./sqrt(pSAR10g(2))).*J(:,:,cz(1)).*1e6, Axis0, Axis1,1,[0 30])

figure(03)
load(B1fileName{2})
load(JfileName{2})
J = sum(Snapshot0,2);
J(abs(J)>=0.005)=1;
J(abs(J)~=1)=0;
J = reshape(J,[length(Axis0)-1 length(Axis1)-1 length(Axis2)-1]);
visualizeDistr2((abs(B1_shimmed_sum(:,:,cz(2)))./sqrt(pSAR10g(2))).*J(:,:,cz(2)).*1e6, Axis0, Axis1,1,[0 30])

figure(04)
load(B1fileName{3})
load(JfileName{3})
J = sum(Snapshot0,2);
J(abs(J)>=0.005)=1;
J(abs(J)~=1)=0;
J = reshape(J,[length(Axis0)-1 length(Axis1)-1 length(Axis2)-1]);
visualizeDistr2((abs(B1_shimmed_sum(:,:,cz(3)))./sqrt(pSAR10g(3))).*J(:,:,cz(3)).*1e6, Axis0, Axis1,1,[0 30])


%% FRACD SMALL 24 vs Patch
disp('Open B1 Shimmed Sum Files')
B1fileName = {'B1Shimmed_24xSmallFracD.mat','B1Shimmed_32xSmallFracD.mat','B1Shimmed_8xFracD20mm.mat','B1Shimmed_24xSmallFracDGAP.mat', 'B1Shimmed_24SmallFracDFAR.mat'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_24xSmallFracD.mat','SAR10g_32xSmallFracD.mat','SAR10g_8xFracD20mm.mat','SAR10g_24xSmallFracDGAP.mat', 'SAR10g_24SmallFracDFAR.mat'};
disp('Open J Files')
JfileName = {'J_24xSmallFracD.mat','J_32xSmallFracD.mat','J_8xFracD20mm.mat','J_24xSmallFracDGAP.mat', 'J_24xSmallFracDFAR.mat'};
cx = [96 108 68 93 104];
cz = [114 114 81 114 116];

figure(01); hold on;
axis([-8 8 5 15])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g(ii)] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    pSAR10g(ii)
    max(abs(B1_shimmed_sum(cx(ii),end/2-4:end/2+4,cz(ii))))
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g(ii)).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('24-channel (Small Frac Dipole)','32-channel (Small Frac Dipole)','8-channel (Frac Dipole)',...
    '24-channel (Small Frac Dipole GAP)','24-channel (Small Frac Dipole FAR)','Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')
%% FRACD SMALL vs Patch
disp('Open B1 Shimmed Sum Files')
B1fileName = {'B1Shimmed_24SmallFullMndr.mat','B1Shimmed_24SmallFracD.mat','B1Shimmed_8FracD20mm.mat'};
disp('Open 10g Averaged SAR Files')
SARfileName = {'SAR10g_24SmallFullMndr.mat','SAR10g_24SmallFracD.mat','SAR10g_8FracD20mm.mat'};
disp('Open J Files')
JfileName = {'J_24SmallFullMndr.mat','J_24SmallFracD.mat','J_8FracD20mm.mat'};
cx = [94 96 68];
cz = [124 114 81];

figure(01); hold on;
axis([-8 8 5 15])
for ii = 1:length(B1fileName)
    a = round(rand()*10)/10;
    b = round(rand()*10)/10;
    c = round(rand()*10)/10;
    load(B1fileName{ii});
    load(SARfileName{ii});
    [SAR3D, axs, pSAR10g(ii)] = OptSAR10g_plotter(SAR10g,LUT,JfileName{ii},0);
    pSAR10g(ii)
    max(abs(B1_shimmed_sum(cx(ii),end/2-4:end/2+4,cz(ii))))
    plot((axs.y(1:end-1))*100,abs(B1_shimmed_sum(cx(ii),:,cz(ii)))./sqrt(pSAR10g(ii)).*1e6,'LineWidth',2,'Color', [c b a]);
    hold on
    drawnow
    pause(0.1)
end

line([-3 -3],[0 30],'LineWidth',1,'Color','k')
line([3 3],[0 30],'LineWidth',1,'Color','k')

legend('24-channel (Small Meandered Dipole)','24-channel (Small Frac Dipole)','8-channel (Frac Dipole)',...
    'Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')
% 
% figure(02)
% load(B1fileName{1})
% load(JfileName{1})
% J = sum(Snapshot0,2);
% J(abs(J)>=0.005)=1;
% J(abs(J)~=1)=0;
% J = reshape(J,[length(Axis0)-1 length(Axis1)-1 length(Axis2)-1]);
% visualizeDistr2((abs(B1_shimmed_sum(:,:,cz(1)))./sqrt(pSAR10g(2))).*J(:,:,cz(1)).*1e6, Axis0, Axis1,1,[0 30])
% 
% figure(03)
% load(B1fileName{2})
% load(JfileName{2})
% J = sum(Snapshot0,2);
% J(abs(J)>=0.005)=1;
% J(abs(J)~=1)=0;
% J = reshape(J,[length(Axis0)-1 length(Axis1)-1 length(Axis2)-1]);
% visualizeDistr2((abs(B1_shimmed_sum(:,:,cz(2)))./sqrt(pSAR10g(2))).*J(:,:,cz(2)).*1e6, Axis0, Axis1,1,[0 30])
% 
% figure(04)
% load(B1fileName{3})
% load(JfileName{3})
% J = sum(Snapshot0,2);
% J(abs(J)>=0.005)=1;
% J(abs(J)~=1)=0;
% J = reshape(J,[length(Axis0)-1 length(Axis1)-1 length(Axis2)-1]);
% visualizeDistr2((abs(B1_shimmed_sum(:,:,cz(3)))./sqrt(pSAR10g(3))).*J(:,:,cz(3)).*1e6, Axis0, Axis1,1,[0 30])
