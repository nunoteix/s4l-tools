%% For PLANE WAVE BOX TRANSFER FUNCTIONS
npwb = 43;
for ii = 1:npwb
    load(['G:\Transfer Functions\Park Paper Replication\800mm Standing Wave Excitation Uncapped Dielectric\E_noNorm_channel' num2str(ii) '.mat'])
%     if ii == 1
%         xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
%         yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
%         zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
        sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
%     end
        E = reshape(Snapshot0, [sz 3]);
        Etip(:,ii) = squeeze(E(31,31,17,:));
%         clear E sz
end
sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
Eabs = sqrt(abs(Etip(3,:)).^2); %sqrt(abs(Etip(1,:)).^2 + abs(Etip(2,:)).^2 + 
% Epha = sqrt(angle(Etip(1,:)).^2 + angle(Etip(2,:)).^2 + angle(Etip(3,:)).^2);
Epha = (angle(Etip(3,:)));
Epha = unwrap(Epha);
Epha = Epha - Epha(1);
% AxisPlot = AxisPlot;


figure;
subplot(2,1,1)
plot(AxisPlot, Eabs, 'LineWidth', 3)
grid on
axis([AxisPlot(1) AxisPlot(end) 0 1])
title('Park - Plane Wave Boxes')

subplot(2,1,2)
plot(AxisPlot, Epha, 'LineWidth', 3)
grid on
axis([AxisPlot(1) AxisPlot(end) -5 0])

%% Feng - Current Source Reciprocity
load('G:\Transfer Functions\Feng Paper Replication\Current_HR_Uncapped_Dielectric.mat')
ax1 = Axes(3,:);
ax = ax1;
pha = unwrap(angle(J));
pha = pha-pha(1);

figure;


subplot(2,1,1)
plot(ax, abs(J), 'LineWidth', 3)
grid on
axis([ax(1) ax(end) 0 0.4])
title('Feng - Reciprocity')

subplot(2,1,2)
plot(ax, pha, 'LineWidth', 3)
grid on
axis([ax(1) ax(end) -5 0])

%% Compariosn between both
ax = ax1-5/8;
% Park TF =  (1/E1)*(Er(P)/dT) || @P
% Feng TF =  (I/|Itip|*dl) || @Tip


E1 = 17;
factor = 0.02075;
figure(27);
subplot(2,1,1)
plot(AxisPlot, (1/(E1*0.005))*Eabs, '-o','LineWidth', 1.5)
hold on
plot(ax, abs(J)/factor, 'LineWidth', 1.5)
grid on
% axis([AxisPlot(end) AxisPlot(1) 0 1])
% axis([ax(1) ax(end) 0 10])
title(['Transfer Function - Feng/' num2str(factor)])

pfactor = -0.25;
subplot(2,1,2)
plot(AxisPlot, Epha+pfactor, 'LineWidth', 1.5)
hold on
plot(ax, pha, 'LineWidth', 1.5)
grid on
axis([AxisPlot(1) AxisPlot(end) -5 0])
title(['Transfer Function Phase - Additive factor of ' num2str(pfactor)])

legend('Park - Plane Waves', 'Feng - Reciprocity')