for ii = 1:320
    imagesc(squeeze(abs(SAR_f(:,ii,:))))
    pause(0.05)
end

SAR_fin(1:length(SAR3)) = SAR3;
SAR_fin(length(SAR3)+1:length(SAR3)+length(SAR)) = SAR;
SAR_fin(length(SAR3)+length(SAR)+1:length(SAR3)+length(SAR)+length(SAR1)) = SAR1;
SAR_fin(length(SAR3)+length(SAR)+length(SAR1)+1:length(SAR3)+length(SAR)+length(SAR1)+length(SAR2)) = SAR2;
SAR_f = reshape(SAR_fin, [180 320 258]);
mip1 = max(SAR_f, [], 3);
imagesc(squeeze(abs(mip1)))