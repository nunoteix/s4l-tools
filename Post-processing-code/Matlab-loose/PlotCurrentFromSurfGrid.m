%% Straight Wire
step = 5;
for ii = -1384:step:105
    if ii == -1384
        inds = find(JPoints(:,3) >= ii & JPoints(:,3) < ii+step);
        AvgJ(ii+1385,:) = mean(SurfJ(inds,:));
    elseif ii == 105
        inds = find(JPoints(:,3) >= ii);
        AvgJ(ii+1385,:) = mean(SurfJ(inds(1:end-1),:));
    else
        inds = find(JPoints(:,3) >= ii & JPoints(:,3) < ii+step);
        AvgJ(ii+1385,:) = mean(SurfJ(inds,:));
    end
    
end
%% Heart Wire
step = 4;
jj = 0;
for ii = 0:step:329
    jj = jj+1;
    if ii == 0
        inds = find(JPoints(:,3) >= ii & JPoints(:,3) < ii+step);
        AvgJ(jj,:) = mean(SurfJ(inds,:));
    elseif ii == 328
        inds = find(JPoints(:,3) >= ii & JPoints(:,3) < 329);
        AvgJ(jj,:) = mean(SurfJ(inds(1:end-1),:));
    else
        inds = find(JPoints(:,3) > ii & JPoints(:,3) < ii+step);
        AvgJ(jj,:) = mean(SurfJ(inds,:));
    end
end

for ii = 10:step:69
    jj = jj+1;
    if ii == 10
        inds = find(JPoints(:,1) >= ii+1 & JPoints(:,1) < ii+step & JPoints(:,3) >= 329.5 & JPoints(:,3) <= 330.5);
        AvgJ(jj,:) = mean(SurfJ(inds,:));
    elseif ii == 66
        inds = find(JPoints(:,1) >= ii & JPoints(:,1) < 69 & JPoints(:,3) >= 329.5 & JPoints(:,3) <= 330.5);
        AvgJ(jj,:) = mean(SurfJ(inds(1:end-1),:));
    else
        inds = find(JPoints(:,1) > ii & JPoints(:,1) < ii+step);
        AvgJ(jj,:) = mean(SurfJ(inds,:));
    end
end

for ii = 330:step:489
    jj = jj+1;
    if ii == 330
        inds = find(JPoints(:,3) >= ii+1 & JPoints(:,3) < ii+step & JPoints(:,1) >= 69.5 & JPoints(:,1) <= 70.5);
        AvgJ(jj,:) = mean(SurfJ(inds,:));
    elseif ii == 486
        inds = find(JPoints(:,3) >= ii & JPoints(:,3) < 489 & JPoints(:,1) >= 69.5 & JPoints(:,1) <= 70.5);
        AvgJ(jj,:) = mean(SurfJ(inds(1:end-1),:));
    else
        inds = find(JPoints(:,3) > ii & JPoints(:,3) < ii+step);
        AvgJ(jj,:) = mean(SurfJ(inds,:));
    end
end

for ii = 30:step:89
    jj = jj+1;
    if ii == 30
        inds = find(JPoints(:,2) >= ii+1 & JPoints(:,2) < ii+step & JPoints(:,3) >= 489.5 & JPoints(:,3) <= 490.5);
        AvgJ(jj,:) = mean(SurfJ(inds,:));
    elseif ii == 86
        inds = find(JPoints(:,2) >= ii & JPoints(:,2) < 89 & JPoints(:,3) >= 489.5 & JPoints(:,3) <= 490.5);
        AvgJ(jj,:) = mean(SurfJ(inds(1:end-1),:));
    else
        inds = find(JPoints(:,2) > ii & JPoints(:,2) < ii+step);
        AvgJ(jj,:) = mean(SurfJ(inds,:));
    end 
end

for ii = 490:step:1189
    jj = jj+1;
    if ii == 490
        inds = find(JPoints(:,3) >= ii+1 & JPoints(:,3) < ii+step);
        AvgJ(jj,:) = mean(SurfJ(inds,:));
    elseif ii == 1186
        inds = find(JPoints(:,3) >= ii);
        AvgJ(jj,:) = mean(SurfJ(inds(1:end-1),:));
    else
        inds = find(JPoints(:,3) > ii & JPoints(:,3) < ii+step);
        AvgJ(jj,:) = mean(SurfJ(inds,:));
    end
end

%%
figure(76)
set(gcf,'color','w');

compJ = AvgJ(AvgJ~=0);
compJ = reshape(compJ,length(compJ)/3, 3);
%%
ii = 0;
% for ii = 0:0.2:2*pi
figure(76)
subplot(2,4,8)
plot((-105:5:1384)', real(sqrt(sum(abs(compJ).^2,2)).*exp(1i*(angle(sum(compJ,2))+ii))),'LineWidth',2)
l1 = line([884 884], [-5 5], 'Color', 'k');
legend([l1],'Phantom Boundary');legend('boxoff')
axis([-200 1800 -5 5])
xlabel('Length of the Wire (z-axis)')
ylabel('Surface Current Density (J)')
title('HPipe Phantom Stripped Tip Wire - Channel 8')
set(gca,'FontSize',12,'FontWeight','bold','LineWidth',2);


% filename = 'J_RapidCoil_RPhant20cm_StraightWire.gif';
% frame = getframe(1);
% im = frame2im(frame);
% [A,jet] = rgb2ind(im,256); 
% 
% 	if ii == 0;
% 		imwrite(A,jet,filename,'gif','LoopCount',inf,'DelayTime',0.01);
% 	else
% 		imwrite(A,jet,filename,'gif','WriteMode','append','DelayTime',0.01);
%     end
    
% drawnow
% pause(0.01)
% end

% hold on
% plot([-149.5:5:1499.5]', abs(sqrt(sum(abs(compJ).^2,2)).*exp(1i*(angle(compJ(:,3))+pi))))%(:,1).^2+AvgJ(:,2).^2+AvgJ(:,3).^2))
