%%
plot((data8.B1.axes.y-0.0022)*100,abs(sum(B18_shimmed(75,:,55,:),4))./sqrt(0.589).*1e6,'LineWidth',2,'Color',[1 0 1]);
hold on;
plot((data32.B1.axes.y-0.0049)*100,abs(sum(B132_shimmed(116,:,67,:),4))./sqrt(0.00638).*1e6,'LineWidth',2,'Color','r');
% plot((data32.B1.axes.y)*100,abs(data32.B1.B1plus(107,:,69))./sqrt(0.00977).*1e6,'LineWidth',2,'Color','g');
% plot((data32v2.B1.axes.y)*100,abs(data32v2.B1.B1plus(103,:,63))./sqrt(0.326).*1e6,'LineWidth',2,'Color','g');
% plot((data32v3.B1.axes.y+0.004)*100,abs(data32v3.B1.B1plus(110,:,72))./sqrt(0.344).*1e6,'LineWidth',2,'Color','y');
% plot((data32v4.B1.axes.y+0.0085)*-100,abs(data32v4.B1.B1plus(118,:,85))./sqrt(0.431).*1e6,'LineWidth',2,'Color','k');
axis([-11 11 0 0.8])
line([-3 -3],[0 3],'LineWidth',1,'Color','k')
line([3 3],[0 3],'LineWidth',1,'Color','k')
% legend('8','32o','32closeZ','32joint','32smalljoint')
legend('8-channel (SSAD)','32-channel (Matchbox-size SSAD)','Shimmed ROI')
set(gca,'fontsize', 18);
xlabel('Distance from center of ROI (cm)')
ylabel('B_1^+/{\surd}SAR_m_a_x ({\mu}T/\surd{W/kg})')
%%
visualizeDistr2(abs(sum(B18_shimmed(:,:,55,:),4)).*1e6,data8.B1.axes.x*100,data8.B1.axes.y*100,1,[0 0.1])
% ./sqrt(0.589)
axis([-20 20 -8 8])
title('B_1^+/\surd{SAR_m_a_x} ({\mu}T/\surd{W/kg}) Map')
rectangle('Position',[-3 -3 6 6],'Curvature',[1 1],'LineWidth',2,'EdgeColor','k')
line([0 0],[-11.5 12],'Color', [1 0 1],'LineWidth',2)

figure;
visualizeDistr2(abs(sum(B132_shimmed(:,:,67,:),4)).*1e6,data32.B1.axes.x*100,data32.B1.axes.y*100,1,[0 0.1])
% ./sqrt(0.00638)
axis([-20 20 -8 8])
title('B_1^+/\surd{SAR_m_a_x} (\mu T/\surd{W/kg}) Map')
rectangle('Position',[-3 -3 6 6],'Curvature',[1 1],'LineWidth',2,'EdgeColor','k')
line([0 0],[-12.2 11.5],'Color', 'r','LineWidth',2)
%%
fileName = uigetfile('*.mat','SAR','MultiSelect','on');
for ii = 1:32
    load(fileName{ii});
    dim = [length(Axis0) length(Axis1) length(Axis2)];
    SAR(:,:,:,ii) = reshape(Snapshot0, dim-1);
end
SAR_sum = sum(SAR,4);
%%
fileName = uigetfile('*.mat','E','MultiSelect','on');
for ii = 1:32
    load(fileName{ii});
    dim = [length(Axis0) length(Axis1) length(Axis2)];
    E(:,:,:,:,ii) = reshape(Snapshot0, [dim-1 3]);
end
clearvars -except E Axis0 Axis1