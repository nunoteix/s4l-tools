function [tDmask] = maskGen(s,xx,yy,zz,axs)

tDmask = zeros(s(1),s(2),s(3));

x_axis = axs.x;
y_axis = axs.y;
z_axis = axs.z;
    
for ii = 1:s(1)
        for kk = 1:s(2)
            for ll = 1:s(3)
                if (x_axis(ii)-x_axis(xx))^2 + (y_axis(kk)-y_axis(yy))^2 + (z_axis(ll)-z_axis(zz))^2 < 0.03^2
                    tDmask(ii,kk,ll) = 1;
                else
                    tDmask(ii,kk,ll) = 0;
                end
            end
        end
end
end