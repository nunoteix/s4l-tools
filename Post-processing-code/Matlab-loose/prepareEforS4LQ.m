function [] = prepareEforS4LQ(uTweights)

[fNames,fPath] = uigetfile('*.mat','MultiSelect', 'on', 'Pick 8 E-Fields');
outpath = uigetdir('', 'Choose Folder to Export Normalized Fields to');

for ii = 1:8
    disp(['Loading Port #' num2str(ii) '...']);
    path = strcat(fPath, fNames(ii));
    load(path{1,1})
    
    disp(['Normalizing Port #' num2str(ii) '...']);
    Snapshot0 = Snapshot0.*uTweights(ii);

    disp(['Saving Port #' num2str(ii) '...']);
    outfile = ['E_1uT_channel' num2str(ii)];
    save([outpath '\' outfile], 'Axis0', 'Snapshot0', 'Axis2', 'Axis1');
end
disp('Normalized Fields Saved!');