function [power] = w2power(weights,tot_sim_power)
    power = (sum(abs(weights).^2,1)/8);
end