xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];

rho = reshape(rho, sz);
rho(rho>0) = 1;
rho = distributionGenerator(squeeze(rho(:,37,:)), xaxis, zaxis,0,[],[],'nearest');
% b1mask = distributionGenerator(slicemask_4b1, xaxis, zaxis, 0,[],[], 'nearest');

jetblack = jet(4096);
jetblack(1,:) = [0,0,0];

Q_reshaped = reshape(Q_Matrix, [sz 8 8]);
Q_slice = squeeze(Q_reshaped(:,37,:,:,:));
Q_slice = reshape(Q_slice, [sz(1)*sz(3) 8 8]);


eigvecs = zeros(sz(1)*sz(3),8,8);
eigvals = zeros(sz(1)*sz(3),8,8);

for ii = 1:sz(1)*sz(3)
[eigvecs(ii,:,:), eigvals(ii,:,:)] = eig(squeeze(Q_slice(ii,:,:)));
end

dom_eigvecs = zeros(8,sz(1)*sz(3));
for ii = 1:sz(1)*sz(3)
    [~,t] = sort(sum(squeeze(eigvals(ii,:,:))));
    dom_eigvecs(:,ii) = squeeze(eigvecs(ii,:,t(end)));%.*eig;
end
%% Create correlation eig vs. mode maps
points_w = linspace(-150,600,15);


for kk = 1:length(points_w)
    pedge = Jaxis(1) + 615;
    [~, pedge_ind] = min(abs(Jaxis-pedge));

    posidx = [round(points_w(kk)/st)+pedge_ind];% 300+pedge_ind];
    plot_pos = (posidx-pedge_ind).*(Jaxis(2)-Jaxis(1));
    
    [u2,s2,v2]=svd(J(posidx,:));
    ss2 = diag(s2);
    
   
    corr_map_modes = zeros(sz(1)*sz(3),8);
    for ii = 1:sz(1)*sz(3)
        for jj = 1:8
            corr_map_modes(ii,jj) = dot(squeeze(dom_eigvecs(:,ii)), squeeze(v2(:,jj)));
        end
    end
    corr_map_modes = reshape(corr_map_modes,[sz(1) sz(3) 8]);
    
    % a = corr_map_modes(47,333,:);
    tip_eigProj(:,kk) = corr_map_modes(37,274,:);
    sens_pos(kk) = plot_pos;
end
%%
yvals = ones(1,length(points_w))*(maxy/2);
for kk = 1:length(points_w)
    figure(105);
%     if strcmp(opMode,'full') == 0
%         plot((posidx-pedge_ind).*(Jaxis(2)-Jaxis(1)), ones(1,length(posidx))*(maxy/2), 'o', 'Color', 'm', 'LineWidth', 2)
%     end
        plot(points_w(kk), yvals(kk), 'o', 'Color', 'm', 'LineWidth', 1)
        text(points_w(kk), yvals(kk)+0.25, ['P' num2str(kk)], 'FontSize', 6)
    hold on
end
%%
for jj = 1:8
test_modes(:,:,jj) = distributionGenerator(corr_map_modes(:,:,jj),xaxis, zaxis,0,0,1);

% SARm_interp(:,:,jj) = distributionGenerator(SARm_slices(:,:,jj),xaxis, zaxis,0,0,1,'nearest');
end


figure;
for jj = 1:8
subplot(4,2,jj)
imagesc(zaxis, xaxis, imrotate(angle(test_modes(:,:,jj).*rho),-90))
colormap(jetblack)
% mode = abs(test_modes(:,:,jj).*rho);
% idx = find(mode(:));
caxis([0 1])
% caxis([min(mode(idx)) max(mode(idx))])
% if jj > 1
%     caxis([min(mode(idx)) -min(mode(idx))])
% end
axis off
colorbar
% axis([-0.45 0.3 -0.1 0.065])
end
test = sum(abs(test_modes),3).*rho;

%% Eigvals ratio maps
figure;

imagesc(zaxis, xaxis, imrotate(rat_eigvals_interp.*rho,-90))
colormap(jetblack)
caxis([0.5 1])
axis off
title('Dom. Eig. Value / \Sigma Eig. Values')
colorbar
% axis([-0.45 0.3 -0.1 0.065])

%% SAR per current mode
figure(13);
for jj = 1:8
subplot(4,2,jj)
imagesc(zaxis, xaxis, imrotate((SARm_interp(:,:,jj).*rho)./(bavg(jj).^2),-90))
colormap(jetblack)
% mode = abs(test_modes(:,:,jj).*rho);
% idx = find(mode(:));
caxis([0 5e12])
% caxis([min(mode(idx)) max(mode(idx))])
% if jj > 1
%     caxis([min(mode(idx)) -min(mode(idx))])
% end
title(['SAR/B_{1}^{+2} for Mode ' num2str(jj)])
axis off
colorbar
% axis([-0.45 0.3 -0.1 0.065])
end
%% avg B1 in ROI
for jj = 1:8
    tempb1 = reshape(B1_m_interp(:,:,jj), [sz(1)*sz(3)*100 1]);
    idxs_4b1 = find(B1_m_interp(:,:,jj).*b1mask);
    bavg(jj) = mean(tempb1(idxs_4b1));
end
%% tip Eigen projections for different point sensors

figure(105);
subplot(3,2,3:6);
imagesc(1:length(sens_pos),[], abs(tip_eigProj))
caxis([0 1])
xlabel('Sensor #')
set(gca, 'XTick', 1:length(sens_pos))
title('@Tip: dot(Evec,modes)')
colorbar

%%
figure(105)

subplot(3,2,1:2)
nps = length(J_Ch1);
maxy = 3;
phantToCoil = 450;
coilLength = 250;

line([phantToCoil phantToCoil+coilLength], [maxy-1 maxy-1], 'Color', 'k', 'LineWidth', 2);
% line([178 248], [maxy-2 maxy-2], 'Color', 'g', 'LineWidth', 2);
line([Jaxis(1)-Jaxis(pedge_ind) Jaxis(end)-Jaxis(pedge_ind)], [maxy-2 maxy-2], 'Color', 'b', 'LineWidth', 2);
% line([-32 38], [maxy-2 maxy-2], 'Color', 'r', 'LineWidth', 2);
% line([38 178], [maxy-2 maxy-2], 'Color', 'b', 'LineWidth', 2);
% line([178 248], [maxy-2 maxy-2], 'Color', 'g', 'LineWidth', 2);
% line([248 508], [maxy-2 maxy-2], 'Color', 'b', 'LineWidth', 2);
axis([Jaxis(1)-Jaxis(pedge_ind)+10 Jaxis(end)-Jaxis(pedge_ind)+100 0 maxy])
grid on

% legend(leg, 'Location', 'northwest')
xlabel('Wire Length')
ylabel('Wire and Coil')
title('Wire Geometry')
hold on
patchh = patch([0 (nps-pedge_ind) (nps-pedge_ind) 0], [0 0 maxy maxy], 'b');
patchh.FaceAlpha = 0.075;
patchh.EdgeAlpha = 0;
hold on
%%
if strcmp(opMode,'full') == 0
   plot((posidx-pedge_ind).*(Jaxis(2)-Jaxis(1)), ones(1,length(posidx))*(maxy/2), 'o', 'Color', 'm', 'LineWidth', 2)
end

%%
% hold off
% leg={};leg{1} = 'Coil Coverage';leg{2} = 'z-oriented section'; leg{3} = 'y-oriented section';
% leg{4} = 'x-oriented section';leg{5} = 'Phantom'; leg{6} = 'Sensor Point';
% legend(leg, 'Location', 'northwest')

leg={};leg{1} = 'Coil Coverage';leg{2} = 'z-oriented wire'; leg{3} = 'Phantom'; leg{4} = 'Sensor Point';
legend(leg, 'Location', 'northwest')