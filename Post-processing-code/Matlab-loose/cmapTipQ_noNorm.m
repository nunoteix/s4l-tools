function [cmap] = cmapTipQ_noNorm(maxEV_redQ_2s2m, maxEV_redQ_1s1m, maxEV_fullQ, points_w)
    parulawhite = parula(4096);
    parulawhite(1,:) = [1,1,1];
    
    map = ones(length(points_w));
    upperTM = triu(map,1);
    upperTM(upperTM~=0) = fliplr(maxEV_redQ_2s2m);
    
%     cmap = fliplr(upperTM+upperTM.');
    cmap = fliplr(upperTM+tril(map,-1));
    cmap(cmap==0) = eye(length(points_w))*maxEV_redQ_1s1m';
    
    cmap(cmap==1) = 0; 

    figure;
    set(gcf,'Position',[1 1 1020 680])
    set(gca, 'FontSize', 16)
    colormap(parulawhite)

    
    subplot(1,10,2)
    imagesc(maxEV_fullQ)
    cbh = colorbar('westoutside');
    cbh.Position(3) = 0.025;
    cbh.Position(1) = 0.15;
    caxis([0 max(abs(maxEV_fullQ))])
    ylabel('Worst Case SAR Without Nulling', 'FontSize', 20, 'FontWeight', 'bold')
    
%     set(gca,'box','on')
    a = get(gca,'XTickLabel');
    set(gca,'XTickLabel',a,'fontsize',18)
    set(gca,'xtick',[])
    set(gca,'ytick',[])
%     
    
    subplot(1,10,4:10)
    imagesc(points_w, fliplr(points_w),abs(cmap))
    set(gca,'Ydir','Normal')
    
    caxis([0 max(abs(maxEV_fullQ))])
    
    a = get(gca,'XTickLabel');
    set(gca,'XTickLabel',a,'fontsize',14)
    set(gca,'Ydir','reverse')
    axis on
    xlabel('Sensor Position 1', 'FontSize', 20)
    ylabel('Sensor Position 2', 'FontSize', 20)
    title('Worst Case SAR After Nulling', 'FontSize', 20)
    set(gca,'box','off')
    
    

end
 