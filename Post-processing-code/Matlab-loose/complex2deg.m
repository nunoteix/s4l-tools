function [degAngles] = complex2deg(compList)
degAngles = angle(compList).*(180/pi);
end