function [maxEV_redQ, maxEV_redQ_noNorm, v_all, b1weights_1s, w1, avgB1] = fullSVDTipQ(nChannels, Jinterp, maskMLS, sRB1r, sRB1wiredr, Q_tip)

            [~,~,v]=svd(Jinterp); % Single Value Decomposition from Surface Current J (wire)
            v_all = v;
            
            moded_B1= sRB1r*v;
            moded_wiredB1= sRB1wiredr*v;
            
            for nOut = 1:8
                nIn = nChannels - nOut +1;
                v_dmss = v(:,nOut:nChannels); % Creates Dark Mode Subspace (dmss)

                [w,~] = MLS(moded_B1(:,nOut:end).*1e6, maskMLS,[],20,200,1e-4,'quiet','nobox');
                [avgB1(nOut),~] = applyShim(moded_wiredB1(:,nOut:end),w,nIn);
                if nOut > 1
                    w1(:,nOut) = [w;zeros(nOut-1,1)];
                else
                    w1 = w;
                end
%                 pp_absShimmed(:,nOut) = holder; % Save shimmed B1+ for all sens_pos

                % SECTION FOR EIG VALUES @TIP
                Q_tip_normed = Q_tip./((avgB1(nOut)^2)/(sum(abs(w1(:,nOut)).^2)/nChannels)); % Normalise the "WCSAR" to that AvgB1+

                Q_dmss = v_dmss'*(Q_tip_normed)*v_dmss; % Reduce Q-matrix to the DMSS
                Q_dmss_noNorm = v_dmss'*(Q_tip)*v_dmss;

                maxEV_redQ(nOut) = max(eig(Q_dmss)); %Calculate the "WCSAR" for the DMSS
                maxEV_redQ_noNorm(nOut) = max(eig(Q_dmss_noNorm)); %Calculate the "WCSAR" for the DMSS
 
            end
    b1weights_1s = ((avgB1.^2)./(sum(abs(w1).^2,1)/8));    
end