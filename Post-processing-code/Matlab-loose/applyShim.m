function [avgB1, shimmedB1,err] = applyShim(B1byChannel, weights, nChannels)

    if ndims(B1byChannel) > 2
        sz = size(squeeze(B1byChannel));
        B1byChannel = reshape(B1byChannel, [prod(sz(1:end-1)) nChannels]);
    end
    shimmedB1 = B1byChannel*weights;
%     for ii = 1:nChannels
%         shimmedChannels(:,ii) = B1byChannel(:,ii).*weights(ii);
%     end
%     shimmedB1 = sum(shimmedChannels,2);
    err = norm(abs(shimmedB1)-1e-6)/norm(1e-6*ones(size(shimmedB1)));
    avgB1 = mean(abs(shimmedB1));