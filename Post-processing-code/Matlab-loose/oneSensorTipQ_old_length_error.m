function [maxEV_redQ_1s1m, maxEV_redQ_1s1m_noNorm, pp_absShimmed, v_all, b1weights_1s, w1, avgB1] = oneSensorTipQ(nChannels, points_w, pedge_ind, st, J, maskMLS, sRB1r, sRB1wiredr, Q_tip)
    nSensors = 1;
    nModes = nChannels - nSensors;
    disp(['For ' num2str(nChannels) ' channels and ' num2str(nSensors)...
        ' sensor: Calculating maximum Eigenvalue of the Q-Matrix reduced to '...
        num2str(nModes) ' modes.'])
    
    for kk = 1:length(points_w)
        disp(['Running sensor position ' num2str(kk) ' of ' num2str(length(points_w))])
        
        posidx = round(points_w(kk)/st)+pedge_ind; %Sets points in relation to phantom-air border
        plot_pos = (posidx-pedge_ind).*st; % Calculates actual metric distance of the points for plotting
        [u,s,v]=svd(J(posidx,:)); % Single Value Decomposition from Surface Current J (wire)
        sens_pos(kk,:) = plot_pos; % Actual position of sensor vector
        v_dmss = v(:,2:nChannels); % Creates Dark Mode Subspace (dmss)

        v_all(:,:,kk) = v;

%         for ii = 1:nChannels
%             for jj = 1:nChannels
%                  oneMode_B1(:,jj) = sRB1r(:,jj).*v(jj,ii);
%                  oneMode_wiredB1(:,jj) = sRB1wiredr(:,jj).*v(jj,ii);%Transform B1+ per channel into per mode
%             end
%             moded_B1(:,ii,kk) = sum(oneMode_B1, 2);
%             moded_wiredB1(:,ii,kk) = sum(oneMode_wiredB1, 2);% Build the 8-mode matrix
%         end
%         
        moded_B1(:,:,kk) = sRB1r*v;
        moded_wiredB1(:,:,kk) = sRB1wiredr*v;

        [w1(:,kk),z1] = MLS(moded_B1(:,nSensors+1:end,kk).*1e6, maskMLS,[],20,200,1e-4,'quiet','nobox');
        [avgB1(kk),holder] = applyShim(moded_wiredB1(:,nSensors+1:end,kk),w1(:,kk),nModes);

        pp_absShimmed(:,nSensors,kk) = holder; % Save shimmed B1+ for all sens_pos

        % SECTION FOR EIG VALUES @TIP
        Q_tip_normed = Q_tip./((avgB1(kk)^2)/(sum(abs(w1(:,kk)).^2)/nChannels)); % Normalise the "WCSAR" to that AvgB1+
        
        Q_dmss = v_dmss'*(Q_tip_normed)*v_dmss; % Reduce Q-matrix to the DMSS
        Q_dmss_noNorm = v_dmss'*(Q_tip)*v_dmss;
        
        maxEV_redQ_1s1m(kk) = max(eig(Q_dmss)); %Calculate the "WCSAR" for the DMSS
        maxEV_redQ_1s1m_noNorm(kk) = max(eig(Q_dmss_noNorm)); %Calculate the "WCSAR" for the DMSS
    end
    
    b1weights_1s = ((avgB1.^2)./(sum(abs(w1).^2,1)/8));    
end