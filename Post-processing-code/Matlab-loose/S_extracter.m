clear all; close all;

n = 16; %No. of coils
Sij = dlmread('S_16FracD2.txt');
% Sij = xlsread('Sij_12LoopsT.xls','Sheet1','A2:IV5002');
% Sij(:,257:289) = xlsread('Sij_12LoopsT.xls','Sheet2','A2:AG5002');
% Sij = xlsread('Sij_4LoopsCloser.xls','A2:AG5002');
index = find(Sij(:,1)==345400000);
% index = find(Sij(:,1)==2.9416e8);
% index = find(Sij(:,1)==2.98e8);
Sij_r = Sij(index,:)';
for ii = 1:n*n
        Sij_channels(ii) = Sij_r(2*ii) + 1i*Sij_r(2*ii+1);
end
Sij_channels = reshape(Sij_channels,n,n);
Sij_channels_dB = 20*log10(abs(Sij_channels));
figure
colormap('jet')
imagesc(Sij_channels_dB)
colorbar
caxis([-20, -5])
title('S-Matrix @Resonance Frequency')
xlabel('Coil no.')
ylabel('Coil no.')
set(gca,'ytick',0:n)
set(gca,'xtick',0:n)
set(gca, 'XAxisLocation', 'top')
Sij_channels_dB;

%% plotter
% close all;
figure;
axis([2e8 5e8 -60 10])
for ii =2:2:16
hold on;
plot(Sij(:,1),20*log10(abs(Sij(:,ii)+1i*Sij(:,ii+1))),'DisplayName',strcat('1,',num2str(ii/2)),'LineWidth',2)
drawnow
% pause(1)
legend('-DynamicLegend');
end

figure;
axis([2e8 5e8 -60 10])
for ii =16:2:32
hold on;
plot(Sij(:,1),20*log10(abs(Sij(:,ii)+1i*Sij(:,ii+1))),'DisplayName',strcat('1,',num2str(ii/2)),'LineWidth',2)
drawnow
% pause(1)
legend('-DynamicLegend');
end