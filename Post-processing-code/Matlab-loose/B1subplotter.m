clearvars -except avgs; close all;
%% LOAD DATA
xx = 93;
yy = 34;
zz = 114;
coord = [xx yy zz];

load('SNR_24xSmallFracDapart.mat');
load('B1Shimmed_24xSmallFracDGAP.mat');
load('SAR10g_24xSmallFracDGAP.mat');
J = load('J_24xSmallFracDGAP.mat');

dim = [length(J.Axis0)-1 length(J.Axis1)-1 length(J.Axis2)-1];
CD = sum(J.Snapshot0,2);
CD(abs(CD)>=0.05)=1;
CD(abs(CD)~=1)=0;
CD = reshape(CD,dim);

Bmat = B1_shimmed_sum;

[SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,J,0);

SNR = reshape(SNR, dim);

mipSAR = max(SAR3D, [], 2);
%% FIGURES
figure(1)
visualizeDistr2((abs(Bmat(:,:,zz)).*1e6).*CD(:,:,zz), J.Axis0, J.Axis1, 1, [0 4]);
axis([-0.26 0.26 -0.13 0.13])
colorbar off
% set(gca, 'FontSize', 20);
% title('B_1^+')
axis off


figure(2)
visualizeDistr2(((abs(Bmat(:,:,zz)).*1e6).*CD(:,:,zz))./sqrt(pSAR10g), J.Axis0, J.Axis1, 1, [0 20]);
axis([-0.26 0.26 -0.13 0.13])
colorbar off
% set(gca, 'FontSize', 20);
% title('B_1^+/{\surd}SAR_m_a_x (T/\surd{W/kg})')
axis off


figure(3)
visualizeDistr2(SNR(:,:,zz).*CD(:,:,zz), J.Axis0, J.Axis1, 1, [0 2e-6]);
axis([-0.26 0.26 -0.13 0.13])
colorbar off
% set(gca, 'FontSize', 20);
% title('SNR')
axis off


figure(4)
visualizeDistr2(squeeze(mipSAR), J.Axis0, J.Axis2,1,[0 0.05]);
% axis([-0.26 0.26 -0.1 0.15])
colorbar off
% set(gca, 'FontSize', 20);
% title('SAR10g MIP')
axis off

%% AVERAGES IN ROI
s = size(Bmat);

tDmask = maskGen(s, coord(1), coord(2), coord(3), axs);

maskedB1plus = tDmask.*(abs(Bmat).*1e6);
avgB1inROI = sum(maskedB1plus(:))/sum(maskedB1plus(:) > 0);

maskedSNR = tDmask.*SNR;
avgSNR = sum(maskedSNR(:))/sum(maskedSNR(:) > 0);

maskedB1overSAR = maskedB1plus./sqrt(pSAR10g);
avgB1overSARinROI = sum(maskedB1overSAR(:))/sum(maskedB1overSAR(:) > 0);

avgs(:,4) = [avgB1inROI pSAR10g avgB1overSARinROI avgSNR];
%% PLOT AVGS
% figure;

for ii = 1:4
    n = 140+ii;
    subplot(n)

    p1 = bar(1,avgs(ii,1));
    hold on;
    p2 = bar(2,avgs(ii,2));
    hold on;
    p3 = bar(3,avgs(ii,3));
    hold on;
    p4 = bar(4,avgs(ii,4));

    set(p1,'FaceColor','red');
    set(p2,'FaceColor','blue');
    set(p3,'FaceColor','black');
    set(p4,'FaceColor','green');
    set(gca,'xtick',[])
    if ii==1
        ylabel('Average B_1^+ in ROI (\muT)')
    elseif ii==2
        ylabel('Maximum Local SAR (W/kg)')
    elseif ii==3
        ylabel('Average B_1^+/\surd{pSAR10g} ratio in ROI (\muT/\surd{W/kg})')
    elseif ii==4
        ylabel('SNR')
    end
set(gca, 'FontSize', 20);
end
hL = legend('32-channel Small Fractionated Dipoles', '32-channel Small Fractionated Dipoles (non-linear spacing)', '24-channel Small Fractionated Dipoles', '24-channel Small Fractionated Dipoles (non-linear spacing)');
newPosition = [0.4 0.4 0.1 0.1];

newUnits = 'normalized';

set(hL,'Position', newPosition,'Units', newUnits);

