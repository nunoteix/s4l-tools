%% LOAD MATS
% coil_handle = 'FinalRapidCoil_Duke_1_5T_';
% coil_handle = 'V2RapidCoil_Duke_1_5T_';

% coil_handle = 'V2RapidCoil_Duke_15T_RealWire_StrippedTip_';
% coil_handle = 'FinalRapidCoil_Duke_15T_RealWire_StrippedTip_';
% 
load([coil_handle 'tipQ.mat'])
load([coil_handle 'Masks.mat'])
load([coil_handle 'maskMLS.mat'])
load([coil_handle 'heartB1.mat'])
load([coil_handle 'Density.mat'])
load([coil_handle 'Axis.mat'])
load([coil_handle 'J.mat'])

    
%%
npoints = 20;

%%%%%% IF OLD CURRENT METHOD
% pedge = Jaxis(1)+732; %IF DUKE
% Jaxis = Axes(3,:);
% pedge = Jaxis(1)+672; %IF PHANTOM
% 
% points_w = linspace(-100,570,npoints);
% [~, pedge_ind] = min(abs(Jaxis-pedge));
% st = Jaxis(2)-Jaxis(1);
% kk = 1;
% mm = 1;
% linterp_shift = Jaxis - pedge;
% for ii = 1:length(points_w)
%     for jj = ii+1:length(points_w)
%         points_pos(kk,1:2) = [points_w(ii) points_w(jj)];
%         for ll = jj+1:length(points_w)
%                 points_3_pos(mm,1:3) = [points_w(ii) points_w(jj) points_w(ll)];
%                 mm = mm+1;
%         end
%         kk = kk + 1;
%     end
% end
% Jinterp = J;

% %%%%%% IF NEW CURERNT METHOD
ltrue(1) = 0;
for ii = 2:length(Axes)
     ltrue(ii) = ltrue(ii-1) + norm(Axes(:,ii)-Axes(:,ii-1));
end
% lshifted = ltrue - ltrue(19);
for ii = 1:8
    Jinterp_ini(ii,:) = interp1(J(ii,:), 1:36/2000:36);
end
linterp = interp1(ltrue,1:36/2000:36);

points_w = linspace(-100,570,npoints);
pedge = ltrue(18);
linterp_shift = linterp-pedge;
[~, pedge_ind] = min(abs(linterp-pedge));
st = linterp(2)-linterp(1);
kk = 1;
mm = 1;
for ii = 1:length(points_w)
    for jj = ii+1:length(points_w)
        points_pos(kk,1:2) = [points_w(ii) points_w(jj)];
        for ll = jj+1:length(points_w)
                points_3_pos(mm,1:3) = [points_w(ii) points_w(jj) points_w(ll)];
                mm = mm+1;
        end
        kk = kk + 1;
    end
end
Jinterp = permute(Jinterp_ini, [2 1]);


% FOR ALL
jetblack = jet(4096);
jetblack(1,:) = [0,0,0];

%% find WORST CASE (ALL MODES) AVERAGE B1+
nChannels = 8;

% MLS SHIM
nonzero_maskedB1reshaped = reshape(nonzero_maskedB1,[],nChannels);
nonzero_maskedB1wired_reshaped = reshape(nonzero_maskedB1wired,[],nChannels);


[u,s,v] = svd(Jinterp);
scaling_factor = 1e-6/abs(mean(nonzero_maskedB1reshaped(:)));

[w,z] = MLS_modeshim_max_amp(nonzero_maskedB1reshaped.*scaling_factor*1e6,v, maskMLS,1.5,[],0.8,50,0.05);
[avgB1_quad,~] = applyShim(nonzero_maskedB1wired_reshaped, w, nChannels);
  
b1weights = ((avgB1_quad.^2)./(sum(abs(w).^2,1)/8));
% shimmed_B1 = zeros(length(wiredmask),1);
% shimmed_B1(wired_pos(:,1)~=0) = shimmedB1_holder;
% shimmed_B1 = reshape(shimmed_B1, [sz 1]);
%%
Q_tip_normed = Q_tip./((avgB1_quad^2)/(sum(abs(w).^2)/nChannels));
maxEV_fullQ(1:npoints) = max(eig(Q_tip_normed));
maxEV_fullQ_noNorm(1:npoints) = max(eig(Q_tip));

% avgB1 = zeros(15,2);

%% SVD + MODING B1 + MLS + EIG Q
% [maxEV_redQ_1s1m, maxEV_redQ_1s1m_noNorm, shimmedB1_1s, modes_1s, b1weights_1s, w1, avgB1_1s] = oneSensorTipQv2(nChannels, points_w, pedge_ind, st, J, maskMLS, sRB1r, sRB1wiredr, Q_tip);

% [maxEV_redQ_2s2m, maxEV_redQ_2s2m_noNorm, shimmedB1_2s, modes_2s, b1weights_2s, w2, avgB1_2s] = twoSensorTipQv2(nChannels, points_pos, pedge_ind, st, J, maskMLS, sRB1r, sRB1wiredr, Q_tip);


[maxEV_redQ_1s1m, maxEV_redQ_1s1m_noNorm, shimmedB1_1s, modes_1s, b1weights_1s, w1, avgB1_1s] = oneSensorTipQ(nChannels, points_w, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

[maxEV_redQ_2s1m, maxEV_redQ_2s1m_noNorm, shimmedB1_2s1m, modes_2s1m, b1weights_2s1m, w2s1m, avgB1_2s1m] = twoSensorTipQ(1,nChannels, points_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

[maxEV_redQ_2s2m, maxEV_redQ_2s2m_noNorm, shimmedB1_2s, modes_2s, b1weights_2s, w2, avgB1_2s] = twoSensorTipQ(2, nChannels, points_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

[maxEV_redQ_3s3m, maxEV_redQ_3s3m_noNorm, shimmedB1_3s, modes_3s, b1weights_3s, w3, avgB1_3s] = threeSensorTipQ(3, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

[maxEV_redQ_3s2m, maxEV_redQ_3s2m_noNorm, shimmedB1_3s2m, modes_3s2m, b1weights_3s2m, w3s2m, avgB1_3s2m] = threeSensorTipQ(2, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);

[maxEV_redQ_3s1m, maxEV_redQ_3s1m_noNorm, shimmedB1_3s1m, modes_3s1m, b1weights_3s1m, w3s1m, avgB1_3s1m] = threeSensorTipQ(1, nChannels, points_3_pos, linterp_shift, Jinterp, maskMLS, nonzero_maskedB1reshaped, nonzero_maskedB1wired_reshaped, Q_tip);



[u_fullWire,s_fullWire,v_fullWire]=svd(Jinterp(:,:));


%% MAKE MAPS
cmap = cmapTipQ(maxEV_redQ_2s2m, maxEV_redQ_1s1m, maxEV_fullQ, points_w);

b1wMap = b1weightsMap(b1weights_2s, b1weights_1s, b1weights, points_w);

noNormMap = cmapTipQ_noNorm(maxEV_redQ_2s2m_noNorm, maxEV_redQ_1s1m_noNorm, maxEV_fullQ_noNorm, points_w);

%% THREE SENSORS
cmap = cmapTipQ(maxEV_redQ_2s2m, maxEV_redQ_1s1m, maxEV_fullQ, points_w);

b1wMap = b1weightsMap(b1weights_2s, b1weights_1s, b1weights, points_w);

noNormMap = cmapTipQ_noNorm(maxEV_redQ_2s2m_noNorm, maxEV_redQ_1s1m_noNorm, maxEV_fullQ_noNorm, points_w);