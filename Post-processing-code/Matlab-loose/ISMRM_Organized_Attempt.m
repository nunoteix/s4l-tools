%%
% _________________________________ |
% Please LOAD B1+ to get GRID       |
% Please LOAD Mask Properties (rho) |
% Please LOAD Q-Matrix of model     |
% _________________________________ |
%                                   |

%% Please LOAD J p/ CHANNEL + J-Axis
% load SurfJ_8Chnls_Rapid_Wire_LWPHant3 %Small Coil Phantom
% load SurfJ_8Chnls_V2Rapid_Wire_LWPHant3 %Long Coil Phantom
% load SurfJ_8chnls_V2Rapid_ElbowedWire_Duke %Long Coil Duke
load SurfJ_8Chnls_Rapid_ElbowedWire_Duke_15T

% load SurfJ_8Chnls_Rapid_Wire_LWPHant3_3T %Small Coil Phantom
% load SurfJ_8Chnls_V2Rapid_Wire_LWPHant3_3T %Small Coil Phantom
% load SurfJ_8Chnls_V2Rapid_ElbowedWire_Duke_3T %Small Coil Phantom


% if z-oriented
% J = cat(2,J_Ch1(:,3),J_Ch2(:,3),J_Ch3(:,3),J_Ch4(:,3),J_Ch5(:,3),J_Ch6(:,3),J_Ch7(:,3),J_Ch8(:,3));

% if heart-wire (duke) (improved)
J = cat(2,J_Ch1(:,1),J_Ch2(:,1),J_Ch3(:,1),J_Ch4(:,1),J_Ch5(:,1),J_Ch6(:,1),J_Ch7(:,1),J_Ch8(:,1));

%% Get GENERIC Vars READY
xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];

% FOR DUKE
pedge = Jaxis(1)+732;
% FOR PHANT
% pedge = Jaxis(1)+615
points_w = linspace(-130,500,15);

[~, pedge_ind] = min(abs(Jaxis-pedge));
st = Jaxis(2)-Jaxis(1);

mask = reshape(rho, sz);
mask(mask>0) = 1;
mask = distributionGenerator(squeeze(mask(:,ytip,:)), xaxis, zaxis,0,[],[],'nearest');
% b1mask = distributionGenerator(slicemask_4b1, xaxis, zaxis, 0,[],[], 'nearest');

jetblack = jet(4096);
jetblack(1,:) = [0,0,0];

Q_reshaped = reshape(Q_Matrix, [sz 8 8]);
Q_slice = squeeze(Q_reshaped(:,37,:,:,:));
Q_slice = reshape(Q_slice, [sz(1)*sz(3) 8 8]);


eigvecs = zeros(sz(1)*sz(3),8,8);
eigvals = zeros(sz(1)*sz(3),8,8);

for ii = 1:sz(1)*sz(3)
[eigvecs(ii,:,:), eigvals(ii,:,:)] = eig(squeeze(Q_slice(ii,:,:)));
end

dom_eigvecs = zeros(8,sz(1)*sz(3));
for ii = 1:sz(1)*sz(3)
    [~,t] = sort(sum(squeeze(eigvals(ii,:,:))));
    dom_eigvecs(:,ii) = squeeze(eigvecs(ii,:,t(end)));%.*eig;
end

%% Get Correlation MAPS (EIG@TIP vs. MODES) && Q-DARKMODES-SUBSPACE (7x7)
maxy = 7;
for kk = 1:length(points_w)
    % IF 1 SENSOR POINT
    posidx = round(points_w(kk)/st)+pedge_ind;
    
    % IF 2 SENSOR POINTS
%     posidx = [round(-200/st+pedge_ind), round(points_w(kk)/st)+pedge_ind];

    plot_pos = (posidx-pedge_ind).*st;
    
    [u2,s2,v2]=svd(J(posidx,:));
%     ss2 = diag(s2);
    
    % SECTION TO CHECK MODES
%     fullJm = J*v2;
%     Jm = zeros(length(Jaxis),8);
%     for ii = 1:length(Jaxis)
%         smaller = find(Jaxis < Jaxis(ii)+7.5); 
%         bigger = find(Jaxis > Jaxis(ii)-7.5);
%         sect = intersect(bigger,smaller);
%         Jm(ii,:) = mean(abs(fullJm(sect,:))).*exp(1i*(mean(angle(fullJm(sect,:)))));
% %         compJ(ii) = mean(fullJmod(sect,:));
%     end
%     figure(99)
%     subplot(15,1,kk)
%     plot(Jaxis-Jaxis(pedge_ind), abs(Jm(:,1:8)))
% %     axis off
%     grid on
%     hold on
%     plot((posidx-pedge_ind).*(Jaxis(2)-Jaxis(1)), ones(1,length(posidx))*(2), 'o', 'Color', 'm', 'LineWidth', 2)
%     leg={};for ii=1:8,leg{ii}=sprintf('Mode %d',ii);end;
%     if kk == 15
%         legend(leg, 'Location', 'northwest')
%     end
% %     line([phantToCoil2 phantToCoil2+coilLength2], [maxy-1 maxy-1], 'Color', 'k', 'LineWidth', 2);
%     line([phantToCoil phantToCoil+coilLength], [maxy-1 maxy-1], 'Color', 'k', 'LineWidth', 2);
%     axis([Jaxis(1)-Jaxis(pedge_ind)+10 Jaxis(end)-Jaxis(pedge_ind)+100 0 maxy])
%     hold off

    % SECTION FOR CORRELATION
%     corr_map_modes = zeros(sz(1)*sz(3),8);
%     for ii = 1:sz(1)*sz(3)
%         for jj = 1:8
%             corr_map_modes(ii,jj) = dot(squeeze(dom_eigvecs(:,ii)), squeeze(v2(:,jj)));
%         end
%     end
%     corr_map_modes = reshape(corr_map_modes,[sz(1) sz(3) 8]);
%     
%     % a = corr_map_modes(47,333,:);
%     tip_eigProj(:,kk) = corr_map_modes(xtip,ztip,:);


    % SECTION FOR EIG VALUES @TIP
    sens_pos(kk,:) = plot_pos;
    
    Q_tip = squeeze(Q_reshaped(xtip,ytip,ztip,:,:))./(avgB1^2);
    v_dmss = v2(:,2:8);
    Q_dmss = v_dmss'*(Q_tip)*v_dmss;
    maxEV_fullQ(kk) = max(eig(Q_tip));
    maxEV_redQ_1s1m(kk) = max(eig(Q_dmss));
    eigv_ratio(kk) = abs(max(eig(Q_dmss))/max(eig(Q_tip)));
    

end
% figure(44);
% plot(1:15, maxEV_fullQ,'r--o')
% hold on; plot(1:length(sens_pos), abs(maxEV_redQ), 'b--o');
% plot(1:15, eigv_ratio, 'k--o')

%% Plot WIRE GEOMETRY + COIL
%%%% FOR DUKE
% xtip = 47;
% xtip = 65; %duke 1.5T short coil
% xtip = 65; %duke 1.5T final coil
% ytip = 32;
% ytip = 35; %duke 1.5T short coil
% ytip = 36; %duke 1.5T final coil
% ztip = 333;
% ztip = 333; %duke 1.5T short coil
% ztip = 340; %duke 1.5T final coil


% %%% FOR PHANTOM
xtip = 37;
ytip = 37;
ztip = 274;



% %%% FOR DUKE
% pedge = Jaxis(1) + 732;
% [~, pedge_ind] = min(abs(Jaxis-pedge));
% phantToCoil = 38+80; %+250-20; %+250 If short coil // -20 if FR
% coilLength = 500+70; %250 if short coil, 500 if long coil 
% phantToCoil2 = 38+80+70+250;%-20; %+250 If short coil // -20 if FR
% coilLength2 = 250; %250 if short coil, 500 if long coil 


% %%% FOR PHANTOM
pedge = Jaxis(1) + 615;
[~, pedge_ind] = min(abs(Jaxis-pedge));

% %%% FOR PHANTOM V2COIL
phantToCoil = 200;
coilLength = 500;
% %%% FOR PHANTOM FRCOIL
phantToCoil2 = 450;
coilLength2 = 250;

figure(6);
set(gcf,'units','points','position',[200,150,800,550])
% subplot(2,2,1:2)
hold off
nps = length(J_Ch1);
maxy = 3;

hold on
patchh = patch([0 (nps-pedge_ind) (nps-pedge_ind) 0], [0 0 maxy maxy], 'b');
patchh.FaceAlpha = 0.075;
patchh.EdgeAlpha = 0;
hold on

% figure;
r1 = 40;
r2 = 0.35;
t = linspace(0, 2*pi);
r = 1;
x = 615+r1*cos(t);
y = (maxy-2)+r2*sin(t);
patchc = patch(x, y, 'r')
patchc.FaceAlpha = 0.8;
patchc.EdgeAlpha = 0;

set(gca, 'XTick',[-500:100:700]);
set(gca,'ytick',[])

%%%%%% FOR V2RC&DUKE
line([phantToCoil phantToCoil+coilLength], [maxy-0.75 maxy-0.75], 'Color', 'k', 'LineWidth', 2);
line([phantToCoil2 phantToCoil2+coilLength2], [maxy-0.5 maxy-0.5], 'Color', [0.5 0.5 0.5], 'LineWidth', 2);

%%% FOR RC
% line([phantToCoil2 phantToCoil2+coilLength2], [maxy-0.75 maxy-0.75], 'Color', 'k', 'LineWidth', 2);

line([Jaxis(1)-Jaxis(pedge_ind) Jaxis(end)-Jaxis(pedge_ind)], [maxy-2 maxy-2], 'Color', 'b', 'LineWidth', 2);

%FOR DUKE
% line([-32 38], [maxy-2 maxy-2], 'Color', 'r', 'LineWidth', 2);
% line([178 248], [maxy-2 maxy-2], 'Color', 'g', 'LineWidth', 2);

axis([Jaxis(1)-Jaxis(pedge_ind)+10 Jaxis(end)-Jaxis(pedge_ind)+200 0 maxy])
% grid off
set(gca,'ytick',[]);
% set(gca,'ycolor',[1 1 1])
% legend(leg, 'Location', 'northwest')
% xlabel('Wire Length', 'FontSize',16)
% ylabel('Wire and Coil', 'FontSize', 16)
% title('Wire Geometry: Phantom', 'FontSize', 18)



%% PLOT POINTS
yvals = ones(1,length(points_w))*(maxy/2);
for kk = 1:length(points_w)
    figure(6);
%     if strcmp(opMode,'full') == 0
%         plot((posidx-pedge_ind).*(Jaxis(2)-Jaxis(1)), ones(1,length(posidx))*(maxy/2), 'o', 'Color', 'm', 'LineWidth', 2)
%     end
        plot(points_w(kk), yvals(kk), 'o', 'Color', 'm', 'LineWidth', 1)
        text(points_w(kk), yvals(kk)+0.25, num2str(kk), 'FontSize', 6)
    hold on
    % IF 2 SENSOR POINTS
%     plot(50, maxy/2, 'o','Color','r','LineWidth',2)
%     text(50, maxy/2+0.25,'Fixed', 'FontSize', 6)
end

%%%% FOR DUKE
% hold off
% leg={};leg{2} = 'Long Coil Coverage';leg{3} = 'Short Coil Coverage';leg{4} = 'z-oriented section'; leg{5} = 'y-oriented section';
% leg{6} = 'x-oriented section';leg{1} = 'Duke'; leg{7} = 'Sensor Point';
% legend(leg, 'Location', 'northwest')


%%% FOR PHANTOMS
leg={};leg{2} = 'Long Coil Coverage'; leg{3} = 'Short Coil Coverage'; leg{4} = 'z-oriented wire'; 
leg{1} = 'Phantom'; leg{5} = 'Sensor Points';
legend(leg, 'Location', 'northwest')


%% Plot BOTTOM figures
figure(105);
subplot(2,2,3:4);
imagesc(1:length(sens_pos),[], abs(tip_eigProj))
caxis([0 1])
xlabel('Sensor #')
set(gca, 'XTick', 1:length(sens_pos))
title('@Tip: dot(Evec,modes)')
c = colorbar;
c.Location = 'westoutside';
%%
figure(345)
% subplot(2,2,1:2);
% impro_val = eigv_ratio*100;
hold on
plot(1:15, abs(maxEV_fullQ).*1e-12, 'r--o','LineWidth', 2)
% 
% plot(1:length(sens_pos), abs(maxEV_redQ_1s1m), 'k--o','LineWidth', 2)
% plot(1:length(sens_pos), abs(maxEV_redQ_2s1m).*1e-12, 'm--o','LineWidth', 2)
plot(1:15, abs(maxEV_redQ_2s2m_inside2).*1e-12, 'b--o','LineWidth', 2)
% plot(1:length(sens_pos), (abs(maxEV_redQ_2s2m)./maxEV_fullQ)*100, 'r--x','LineWidth', 2)
% plot(1:length(sens_pos), (abs(maxEV_redQ_1s1m)./maxEV_fullQ)*100, 'r--x','LineWidth', 2)

% xvals = 1:15;
% for kk = 1:15
%     text(xvals(kk)-0.9, double(impro_val(kk)+0.25), ['~=' num2str(round(impro_val(kk))) '%'], 'FontSize', 8, 'Color', 'r')
% end
% 
patchh = patch([4 16 16 4], [-10 -10 100 100], 'b');
patchh.FaceAlpha = 0.075;
patchh.EdgeAlpha = 0;
hold on

% plot(1:length(sens_pos), abs(maxEV_redQ_3s3m), 'm--o','LineWidth', 2)

% axis([0 16 0 40])
axis([0 16 0 0.4])
xlabel('Sensor #', 'FontSize', 16)
% ylabel('W/Kg/\muT^2', 'FontSize', 16)
ylabel('W/Kg', 'FontSize', 16)
% ylabel('% of Remaining SAR', 'FontSize', 16)
set(gca, 'XTick', 1:length(sens_pos))

% title('Short 1st Coil Efficiency After Nulling', 'FontSize', 18)
title('Remaining Worst Case SAR - 1W Input Power', 'FontSize', 18)

% title('Worst Case SAR - Dark Mode Subspace - Normalised to Average B1+ in the Heart', 'FontSize', 18)
% title('Worst Case SAR - Dark Mode Subspace - 1W Input Power', 'FontSize', 18)
% title('~rwcSAR_{TIP}', 'FontSize', 18)
% colorbar
grid on
% hold off

%%
% leg={}; leg{1} = 'Worst Case SAR'; leg{2} = '1 Sensor - 1 Coupling Mode Excluded'; %leg{3} = '2 Sensors - 1 Coupling Mode Excluded';
% leg{3} = '2 Sensors - 2 Coupling Modes Excluded';
leg={}; leg{1} = 'No nulling'; leg{2} = '1 Sensor - 1 Coupling Mode Excluded'; %leg{3} = '2 Sensors - 1 Coupling Mode Excluded';
leg{3} = '2 Sensors - 2 Coupling Modes Excluded'; leg{4} = 'Phantom';
leg{5} = '3 Sensors - 3 Coupling Modes Excluded';
legend(leg, 'Location', 'east', 'FontSize', 13)
legend('boxoff')
%%
leg={}; leg{1} = 'Short Coil';  leg{2} = 'Long Coil'; leg{3} = 'Phantom';
legend(leg, 'Location', 'northeast', 'FontSize', 14);
legend('boxoff')
% hold off

suptitle('Long Coil by Sensor Position')
%% Plot weights

figure(110)
w3vec = ones(1,15).*sum(abs(w3).^2);
plot(1:length(sens_pos), w3vec, 'r--o','LineWidth', 2)
hold on
plot(1:length(sens_pos), sum(abs(w1).^2), 'k--o','LineWidth', 2)
grid on
plot(1:length(sens_pos), sum(abs(w2).^2), 'b--o','LineWidth', 2)
% plot(1:length(sens_pos), sum(abs(w4).^2), 'm--o','LineWidth', 2)
leg={}; leg{1} = 'No nulling'; leg{2} = '1 Sensor - 1 Coupling Mode Excluded'; %leg{3} = '2 Sensors - 1 Coupling Mode Excluded';
leg{3} = '2 Sensors - 2 Coupling Modes Excluded';
% leg{4} = '3 Sensors - 3 Coupling Modes Excluded';
legend(leg, 'Location', 'northwest', 'FontSize', 10)
legend('boxoff')
axis([0 16 min(sum(abs(w1).^2))-8 max(sum(abs(w2).^2))+8])
xlabel('Sensor #', 'FontSize', 16)
title('Short Coil - \Sigma ||weights||^2', 'FontSize', 16)
ylabel('\Sigma ||weights||^2', 'FontSize', 16)



