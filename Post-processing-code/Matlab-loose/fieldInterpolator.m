function [A] = fieldInterpolator(dist3D, x_axis, y_axis, z_axis)

[X,Y,Z] = meshgrid(y_axis, x_axis, z_axis);
[Xq,Yq,Zq] = meshgrid(linspace(min(x_axis),max(x_axis),500),...
    linspace(min(y_axis),max(y_axis),500),...
    linspace(min(z_axis),max(z_axis),500));
A = interp3(X,Y,Z, dist3D, Xq, Yq, Zq, 'linear');