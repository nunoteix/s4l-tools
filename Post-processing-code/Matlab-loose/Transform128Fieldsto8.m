%% CONVERT 128 CIRC CO-SIM FIELDS INTO REAL 8
nports = 64;
[fNames,fPath] = uigetfile('*.mat','MultiSelect', 'on', 'Pick 128 Fields');

load(uigetfile('*.mat', 'Choose Correct Weights'));

field = load([fPath fNames{1}]);
Axis0 = field.Axis0;
Axis1 = field.Axis1;
Axis2 = field.Axis2;

outpath = uigetdir('', 'Choose Folder to Export 8-Fields to');

Fc = squeeze(zeros([size(field.Snapshot0(:)) 8]));

for jj = 1:nports
    disp(['Converting Circ Co-Sim Port #' num2str(jj) '...']);
	field = load([fPath fNames{jj}]);
	B1temp = field.Snapshot0(:)*weights(jj,:);
	Fc(:,:) = Fc(:,:) + B1temp;
end
Fc = squeeze(reshape(Fc, [size(field.Snapshot0) 8]));
for ii = 1:8
    disp(['Saving Port #' num2str(ii) '...']);
    outfile = ['E_channel' num2str(ii)];           % CHANGE FIELD SAVING NAME <------------------------------------
    Snapshot0 = Fc(:,:,ii);
    save([outpath '\' outfile], 'Axis0', 'Snapshot0', 'Axis2', 'Axis1');
end

