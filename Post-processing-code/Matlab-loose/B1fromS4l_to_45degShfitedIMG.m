%% IMPORT B1
%%
% $x^2+e^{\pi i}$
data = getAllDistributionsFromFile;

%% IMPORT E and H FIELDS
[fNames,fPath] = uigetfile('*.mat','MultiSelect', 'on', 'Pick E or H Fields');
path = strcat(fPath, fNames(1));
load(path{1,1})
sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
for ii = 1:8
    path = strcat(fPath, fNames(ii));
    load(path{1,1})
    F(:,:,:,:,ii) = reshape(Snapshot0, [sz 2]);
end
xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;

%% Fields Scaling Normalization

for ii = 1:8
    centerVoxel = data.B1.B1plus(134,117,113,ii);
    B1plusNorm(:,:,:,ii) = data.B1.B1plus(:,:,:,ii).*((0.1250e-6/abs(centerVoxel))*exp(1i*-angle(centerVoxel)));
%     B1minusNorm(:,:,:,ii) = data.B1.B1minus(:,:,:,ii).*((0.1250e-6/abs(centerVoxel))*exp(1i*-angle(centerVoxel)));
%     E(:,:,:,:,ii) = E(:,:,:,:,ii).*((0.1250e-6/abs(centerVoxel))*exp(1i*-angle(centerVoxel)));
%     norm(ii) = abs(B1plusNorm(115,107,58,ii));
%     degs(ii) = complex2deg(B1plusNorm(115,107,58,ii));
end

% data.E.Ex = squeeze(E(:,:,:,1,:));
% data.E.Ey = squeeze(E(:,:,:,2,:));
% data.E.Ez = squeeze(E(:,:,:,3,:));
% data.B1.B1plus = B1plusNorm;
% data.B1.B1minus = B1minusNorm;

%% Generate 45deg shifts

angles = [45 90 135 180 225 270 315 360];
angles_neg = [-45 -90 -135 -180 -225 -270 -315 -360];
anglesRad_pos = degtorad(angles);
anglesRad_neg = degtorad(angles_neg);
phaseShifts_pos = exp(anglesRad_pos.*1i);
phaseShifts_neg = exp(anglesRad_neg.*1i);

%% APPLY 45 PHASE SHIFTS

for ii = 1:8
B1plus45pos(:,:,:,ii) = data.B1.B1plus(:,:,:,ii).*phaseShifts_pos(ii);
end
for ii = 1:8
Fplus45neg(:,:,:,:,ii) = F(:,:,:,:,ii).*phaseShifts_neg(ii);
end

B1plus45Sum_pos = sum(B1plus45pos,4);
F45Sum_neg = sum(Fplus45neg,5);

%%
figure(2)
for ii = 1:8
    subplot(2,4,ii)
    distributionGenerator(squeeze(abs(F(50,:,:,ii))), yaxis, zaxis,0,8e-7);
    colorbar;
    title(['Channel #' num2str(ii)])
end
%%
% B1toImg = abs(sum(B1plusNorm1(:,:,79,:),4));
% B1toImg2 = abs(sum(B1plusNorm2(:,:,58,:),4));
% B1diff = abs((B1toImg2-B1toImg))./B1toImg2;

figure(2)
distributionGenerator(abs(sum(B1plusNorm(:,:,79,:),4)), data.B1.axes.x, data.B1.axes.y, 0, 2.5e-6);
title('B1+ Quad. Mode')
line([-0.130 0.130],[-0.045 -0.045])
line([-0.130 0.130],[0.045 0.045])
line([-0.130 -0.130],[-0.045 0.045])
line([0.130 0.130],[-0.045 0.045])
colorbar;

figure(3)
for ii = 1:8
    subplot(2,4,ii)
    distributionGenerator(complex2deg(squeeze(B128_25(:,:,79,ii))), data128_25.B1.axes.x, data128_25.B1.axes.y);%,0,2.5e-7);
    colorbar;
    title(['Channel #' num2str(ii)])
end

figure(4)
distributionGenerator(abs(B1plus45Sum_neg(:,:,113)), data.B1.axes.x, data.B1.axes.y,0,5e-7);
title('B1+ Quad. Mode')
% line([-0.130 0.130],[-0.045 -0.045])
% line([-0.130 0.130],[0.045 0.045])
% line([-0.130 -0.130],[-0.045 0.045])
% line([0.130 0.130],[-0.045 0.045])
colorbar;

% figure(5)
% distributionGenerator(abs(B1plus45Sum_pos(:,:,75)), data.B1.axes.x, data.B1.axes.y, 0, 5e-7);
% title('B1+ Quad. Mode')
% colorbar;

% 
% figure(4)
% distributionGenerator(abs(sum(data.B1.B1plus(:,:,58,:),4)), data.B1.axes.x, data.B1.axes.y, 0, 1e-7);
% title('B1+ Quad. Mode')
% colorbar;



%% COMPARISON BETWEEN 128p vs. 64p & -50dB vs. -25dB
figure(1)
subplot(2,2,1)
distributionGenerator(abs(B128_50sum(:,:,79)),data128_50.B1.axes.x, data128_50.B1.axes.y,0, 5e-7);
title('B1+ Quad Mode - 128Ports -50dB (\muT)')

subplot(2,2,2)
distributionGenerator(abs(B128_25sum(:,:,79)),data128_25.B1.axes.x, data128_25.B1.axes.y,0, 5e-7);
title('B1+ Quad Mode - 128Ports -25dB (\muT)')

subplot(2,2,3)
distributionGenerator(abs(B64_50sum(:,:,58)),data64_50.B1.axes.x, data64_50.B1.axes.y,0, 5e-7);
title('B1+ Quad Mode - 64Ports -50dB (\muT)')

subplot(2,2,4)
distributionGenerator(abs(B64_25sum(:,:,58)),data64_25.B1.axes.x, data64_25.B1.axes.y,0, 5e-7);
title('B1+ Quad Mode - 64Ports -25dB (\muT)')

figure(2)
B11 = B64_50sum./max(max(B64_50sum(:,:,58)));
B12 = B128_50sum./max(max(B128_50sum(:,:,79)));
distributionGenerator(abs(B12(:,:,79)),data128_50.B1.axes.x,data128_50.B1.axes.y,0,0.05);
figure(3)
distributionGenerator(abs(B11(:,:,58)),data64_50.B1.axes.x,data64_50.B1.axes.y,0,0.05);
