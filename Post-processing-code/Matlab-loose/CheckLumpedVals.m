L_1 = load('LumpedVals_final_parallel_64_air.mat');
L_2 = load('LumpedVals_final_parallel_64_duke.mat');
L_1 = L_1.lumpedVals(:,2);
L_2 = L_2.lumpedVals(:,2);

figure(1)
scatter(1:length(L_1),L_1,[],'filled')
hold on
scatter(1:length(L_2),L_2)
hold off

for ii = 1:length(L_2)
line([ii ii], [L_1(ii) L_2(ii)])
end

figure(2)
scatter(1:length(L_2),L_1-L_2)
hold on
line([0 60],[0 0])
for ii = 1:length(L_2)
line([ii ii], [0 L_1(ii)-L_2(ii)])
end
hold off