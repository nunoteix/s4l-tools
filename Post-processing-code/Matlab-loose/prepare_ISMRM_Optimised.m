%% Please LOAD J p/ CHANNEL + J-Axis
% load SurfJ_8Chnls_FinalRapid_Wire_LWPHant3 %Small Coil Phantom
% load SurfJ_8Chnls_V2Rapid_Wire_LWPHant3 %Long Coil Phantom

% load SurfJ_8chnls_V2Rapid_ElbowedWire_Duke %Long Coil Duke
% load SurfJ_8Chnls_Rapid_ElbowedWire_Duke_15T
% load SurfJ_8chnls_FinalRapid_ElbowedWire_Duke_15T

% load SurfJ_8Chnls_Rapid_Wire_LWPHant3_3T %Small Coil Phantom
% load SurfJ_8Chnls_V2Rapid_Wire_LWPHant3_3T %Small Coil Phantom
% load SurfJ_8Chnls_V2Rapid_ElbowedWire_Duke_3T %Small Coil Phantom

%
%


% if z-oriented
% J = cat(2,J_Ch1(:,3),J_Ch2(:,3),J_Ch3(:,3),J_Ch4(:,3),J_Ch5(:,3),J_Ch6(:,3),J_Ch7(:,3),J_Ch8(:,3));

% if heart-wire (duke) (improved)
% J = cat(2,J_Ch1(:,1),J_Ch2(:,1),J_Ch3(:,1),J_Ch4(:,1),J_Ch5(:,1),J_Ch6(:,1),J_Ch7(:,1),J_Ch8(:,1));

% if new current method
load('G:\Surface Current\Final Rapid Coil on Duke 15T Real Wire Stripped Tip\Current.mat')
% load('G:\Surface Current\V2Rapid Coil on Duke 15T Real Wire Stripped Tip\Current.mat')
%% Section Attempting to Create Heart Mask
if ~exist('Axis0','var')
%     load('G:\B1\B1_RapidCoil_Duke_1_5T_portByPort_Wire_StrippedTip\B1_channel1') %RCOIL
%     load('G:\B1\B1_FinalRapidCoil_Duke_portByPort_Wire_StrippedTip_1_5T\B1_channel1') %FINALRCOIL
%     load('G:\B1\B1_V2RapidCoil_Duke_portByPort_Wire_StrippedTip\B1_channel1') %V2RCOIL

%     load('G:\B1\B1_V2RapidCoil_LWPhant3_portByPort_Wire_StrippedTip\B1_channel1.mat')
%     load('G:\B1\B1_FinalRapidCoil_LWPhant3_portByPort_Wire_StrippedTip_1_5T\B1_channel1.mat')
    
    load('G:\B1\B1_FinalRapidCoil_Duke_15T_RealWire_StrippedTip\B1_channel1.mat')
%     load('G:\B1\B1_V2RapidCoil_Duke_15T_RealWire_StrippedTip\B1_channel1.mat')
end

xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
fsz = length(xaxis)*length(yaxis)*length(zaxis);

if ~exist('rho','var')
%     load('G:\Mask Properties\RapidCoil_Duke_15T_Wire_StrippedTip.mat')
%     load('G:\Mask Properties\FinalRapidCoil_Duke_15T_Wire_StrippedTip.mat')
%     load('G:\Mask Properties\V2RapidCoil_Duke_Wire_StrippedTip_2.mat')
    
%     load('G:\Mask Properties\V2RapidCoil_LWPhant3_Wire_StrippedTip.mat')
%     load('G:\Mask Properties\FinalRapidCoil_LWPhant3_15T_Wire_StrippedTip.mat')
    
    load('G:\Mask Properties\FinalRapidCoil_Duke_15T_RealWire_StrippedTip.mat')
%     load('G:\Mask Properties\V2RapidCoil_Duke_15T_RealWire_StrippedTip.mat')
    
    
    if iscell(rho) == 1
        rho = cellfun(@(x) double(x), rho);
    end
end


xtip = 81;
ytip = 73;
ztip = 513;

%% FOR PHANTOM

maskR = reshape(rho, [sz 1]);
temprho = rho;
temprho(rho~=0) = 1;
for xx = 1:sz(1)
    for yy = 1:sz(2)
        for zz = 1:sz(3)
            if (xaxis(xx)-xaxis(xtip))^2+(yaxis(yy)-yaxis(ytip))^2+(zaxis(zz)-zaxis(ztip))^2 <= 0.05^2
                maskR(xx,yy,zz) = 1;
            else
                maskR(xx,yy,zz) = 0;
            end
        end
    end
end

filledmaskR = maskR.*reshape(temprho, [sz 1]);
wiredmaskR = filledmaskR;
filledmaskR(25:50, 25:50, 1:275) = 0; % FOR V2RCOIL

%% FOR DUKE
maskR = rho; %Load Mask Properties first, if is in cell form run: rho = cellfun(@(x) double(x), rho);
maskR(maskR == 1049.75) = 1; %Density of Heart Lumen (blood)
maskR(maskR == 1.080800048828125e+03) = 1; %Density of Heart Muscle (plot rho and check precis value)
maskR(maskR ~= 1) = 0; % Make everything else 0
maskR = reshape(maskR, sz); %Reshape to 3D

% maskR(14:123,2:106,318:345); % Check in S4L what small box encompasses
% the Heart, as blood as same density you need to make everything else
% outside this box 0, hence the following lines:

% %%%% RCOIL
% maskR(1:14,:,:) = 0;
% maskR(:,1:2,:) = 0;
% maskR(:,:,1:318) = 0;
% maskR(123:end,:,:) = 0;
% maskR(:,106:end,:) = 0;
% maskR(:,:,345:end) = 0;

%%%% V2RCOIL
% maskR(1,:,:) = 0;
% % maskR(:,1:7,:) = 0;
% maskR(:,:,1:318) = 0;
% maskR(101:end,:,:) = 0;
% maskR(:,106:end,:) = 0;
% maskR(:,:,343:end) = 0;

%%%% FRCOIL
% maskR(1:12,:,:) = 0;
% maskR(:,1:2,:) = 0;
% maskR(:,:,1:322) = 0;
% maskR(130:end,:,:) = 0;
% maskR(:,106:end,:) = 0;
% maskR(:,:,349:end) = 0;

%%%% REAL WIRE V2OIL
maskR(1:21,:,:) = 0;
maskR(:,1:28,:) = 0;
maskR(:,:,1:469) = 0;
maskR(125:end,:,:) = 0;
maskR(:,131:end,:) = 0;
maskR(:,:,534:end) = 0;

% Voxelization makes everything patchy. Dilate and Erode to fill patches
se = strel('sphere',2); %Patches are small so small Dilate radius will do
dilmaskR = imdilate(maskR,se); %Dilate
filledmaskR = imerode(dilmaskR,se); %Erode, Heart Region should now be properly masked

wiredmaskR = filledmaskR;
% filledmaskR(50:81, 25:47, 1:333) = 0; % FOR RCOIL
% filledmaskR(34:64, 17:45, 1:333) = 0; % FOR V2RCOIL
% filledmaskR(46:77, 20:50, 1:340) = 0; % FOR FRCOIL
filledmaskR(78:99, 59:90, 505:535) = 0; % FOR REAL WIRE

%% BOTH

filledmask = reshape(filledmaskR, [fsz 1]);
wiredmask = reshape(wiredmaskR, [fsz 1]);


%mask for MLS
% maskidx = find(filledmask);
% maskMLS = filledmask(maskidx);
maskMLS = filledmask(filledmask~=0);
% maskidx2 = find(wiredmask);
% sMask = wiredmask(maskidx2);
sMask = wiredmask(wiredmask ~= 0);

%%
% figure;
for ii = 1:8
% %%%% HEART SECTION
%     load(['G:\B1\B1_RapidCoil_Duke_1_5T_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)]) %RCOIL
%     load(['G:\B1\B1_FinalRapidCoil_Duke_portByPort_Wire_StrippedTip_1_5T\B1_channel' num2str(ii)]) %FINALRCOIL
%     load(['G:\B1\B1_V2RapidCoil_Duke_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)]) %V2RCOIL
    
%     load(['G:\B1\B1_V2RapidCoil_LWPhant3_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)])
%     load(['G:\B1\B1_FinalRapidCoil_LWPhant3_portByPort_Wire_StrippedTip_1_5T\B1_channel' num2str(ii)])
    
%     load(['G:\B1\B1_FinalRapidCoil_Duke_15T_RealWire_StrippedTip\B1_channel' num2str(ii)])
    load(['G:\B1\B1_V2RapidCoil_Duke_15T_RealWire_StrippedTip\B1_channel' num2str(ii)])
    
    if ii == 1
        xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
        yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
        zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
        sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
        fsz = length(xaxis)*length(yaxis)*length(zaxis);
    end
    
    RB1(:,ii) = (Snapshot0(:,1)+filledmask).*filledmask;%.*w(ii);
    RB1wired(:,ii) = (Snapshot0(:,1)+wiredmask).*wiredmask;
    
%     rsRB1 = reshape(RB1(:,ii), sz);
%     heartRB1(:,:,:,ii) = rsRB1.*filledmaskR;
    
%     subplot(2,4,ii)
%     distributionGenerator(abs(squeeze(heartRB1(:,35,318:345,ii))),xaxis,zaxis(318:345),1,0,2e-7,'nearest');
%     distributionGenerator(angle(squeeze(heartRB1(:,35,318:345,ii))),xaxis,zaxis(318:345),1,-3.14,3.14,'nearest');
end
sRB1 = RB1(RB1~=0);
sRB1 = sRB1 - repmat(maskMLS(:),8,1);
wired_pos = find(RB1wired(:,1));
sRB1wired = RB1wired(RB1wired~=0);
sRB1wired = sRB1wired - repmat(sMask(:),8,1);

heartRB1 = reshape(RB1,[sz 8]);
heartRB1wired = reshape(RB1wired, [sz 8]);


%% GET Q-TIP
% xtip = 47; %duke 1.5T long coil
% ytip = 32; %duke 1.5T long coil
% ztip = 333; %duke 1.5T long coil

% xtip = 65; %duke 1.5T short coil
% ytip = 35; %duke 1.5T short coil
% ztip = 333; %duke 1.5T short coil
% 
% xtip = 65; %duke 1.5T final coil
% ytip = 36; %duke 1.5T final coil
% ztip = 340; %duke 1.5T final coil

if ~exist('Q_Matrix','var')
%     load('Q_RapidCoil_Duke_15T_portByPort_Wire_StrippedTip.mat') %RCOIL
%     load('Q_V2RapidCoil_Duke_portByPort_Wire_StrippedTip.mat') %V2RCOIL
%     load('Q_FinalRapidCoil_Duke_15T_portByPort_Wire_StrippedTip.mat') %FRCOIL

%     load('G:\Q-Matrices\Q_V2RapidCoil_LWPhant3_portByPort_Wire_StrippedTip.mat')
%     load('G:\Q-Matrices\Q_FinalRapidCoil_LWPhant3_15T_portByPort_Wire_StrippedTip.mat')
    
    load('G:\Q-Matrices\Q_V2RapidCoil_Duke_15T_RealWire_StrippedTip.mat')
%     load('G:\Q-Matrices\Q_FinalRapidCoil_Duke_15T_RealWire_StrippedTip.mat')
end

Q_reshaped = reshape(Q_Matrix, [sz 8 8]);
Q_tip = squeeze(Q_reshaped(xtip,ytip,ztip,:,:));

%%
handle = 'FinalRapidCoil_Duke_15T_RealWire_StrippedTip';
save(['G:/ISMRM2019/mats for Optimised script/' handle '_tipQ'],'Q_tip','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_Masks'],'filledmask', 'wiredmask','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_maskMLS'],'maskMLS','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_J'],'J','Jaxis','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_heartB1'],'sRB1','sRB1wired','wired_pos','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle '_Density'],'rho','-v7.3')
save(['G:/ISMRM2019/mats for Optimised script/' handle 'p_Axis'],'fsz','sz','xaxis','yaxis','zaxis','-v7.3')
