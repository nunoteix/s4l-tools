w = 118e6:1e5:138e6;
R_s = 2;
R_p = 900;

load('C:\Users\nt16\Documents\PERINATAL033-BACKUP\Hydra Simulations\LumpedVals3.0_final.mat')
C_t = lumpedVals(:,2);
C_s(1) = C_t(1); C_s(2) = C_t(9); C_s(3) = C_t(10); C_s(4) = C_t(11); C_s(5) = C_t(12); C_s(6) = C_t(13); C_s(7) = C_t(13);
C_s(8) = C_t(14); C_s(9) = C_t(15); C_s(10) = C_t(16); C_s(11) = C_t(25); C_s(12) = C_t(26); C_s(13) = C_t(27); C_s(14) = C_t(28);
C_s(15) = C_t(29); C_s(16) = C_t(30); C_s(17) = C_t(31); C_s(18) = C_t(32); C_s(19) = C_t(41); C_s(20) = C_t(42); C_s(21) = C_t(43);
C_s(22) = C_t(44); C_s(23) = C_t(45); C_s(24) = C_t(46); C_s(25) = C_t(47); C_s(26) = C_t(48); C_s(27) = C_t(49); C_s(28) = C_t(113);
C_s(29) = C_t(114); C_s(30) = C_t(115); C_s(31) = C_t(116); C_s(32) = C_t(117); C_s(33) = C_t(118); C_s(34) = C_t(19); C_s(35) = C_t(120);

load('C:\Users\nt16\Documents\PERINATAL033-BACKUP\Hydra Simulations\LumpedVals3.0_final_parallel.mat')
C_t = lumpedVals(:,2);
C_p(1) = C_t(1); C_p(2) = C_t(9); C_p(3) = C_t(10); C_p(4) = C_t(11); C_p(5) = C_t(12); C_p(6) = C_t(13); C_p(7) = C_t(13);
C_p(8) = C_t(14); C_p(9) = C_t(15); C_p(10) = C_t(16); C_p(11) = C_t(25); C_p(12) = C_t(26); C_p(13) = C_t(27); C_p(14) = C_t(28);
C_p(15) = C_t(29); C_p(16) = C_t(30); C_p(17) = C_t(31); C_p(18) = C_t(32); C_p(19) = C_t(41); C_p(20) = C_t(42); C_p(21) = C_t(43);
C_p(22) = C_t(44); C_p(23) = C_t(45); C_p(24) = C_t(46); C_p(25) = C_t(47); C_p(26) = C_t(48); C_p(27) = C_t(49); C_p(28) = C_t(113);
C_p(29) = C_t(114); C_p(30) = C_t(115); C_p(31) = C_t(116); C_p(32) = C_t(117); C_p(33) = C_t(118); C_p(34) = C_t(19); C_p(35) = C_t(120);

for ii = 1:35
    z_p(ii,:) = 1./(1./R_p + 1i.*w.*C_p(ii));
    z_s(ii,:) = R_s + 1./(1i*w.*C_s(ii));
end
for ii = 1:35
    figure(1)
    subplot(6,6,ii)
    plot(w, imag(z_p(ii,:)), 'b--'); hold on; plot(w, imag(z_s(ii,:)),'r');
    title(['Imag. Impedance ' num2str(ii)])
    axis([118e6 138e6 -1200 0])

    figure(2)
    subplot(6,6,ii)
    plot(w, real(z_p(ii,:)), 'b--'); hold on; plot(w, real(z_s(ii,:)),'r');
    title(['Real Impedance ' num2str(ii)])
    
%     axis off;
end
figure(1)
legend('Parallel RC', 'Series RC')
figure(2)
legend('Parallel RC', 'Series RC')

