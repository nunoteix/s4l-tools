function [tDmask X0 Nreps] = prepareRFamps(data2,xx,yy,zz, tDmask)
clearvars -except data2 tDmask xx yy zz
check = exist('tDmask');

if check == 1
    resp = input('A mask already exists, do you want to create another one? (Y/N)\n','s');
else
    resp = 'Y';
end

if resp == 'Y'
    s = size(data2.B1.B1plus);
    nchan = s(4);
    maxi = 0;
    tB1=zeros(s(1),s(2),s(3),1);
    tDmask = zeros(s(1),s(2),s(3));
    
    for ii = 1:nchan
        tB1(:,:,:,1) = tB1(:,:,:,1) + data2.B1.B1plus(:,:,:,ii);
    end    
    for ii=1:s(3)        
        if max(max(abs(tB1(:,:,ii)),[],2)) > maxi
            maxi = max(max(abs(tB1(:,:,ii)),[],2));
            jj=ii;
        end
    end
    
    x_axis = data2.B1.axes.x;
    y_axis = data2.B1.axes.y;
    z_axis = data2.B1.axes.z;
    
    for ii = 1:s(1)
        for kk = 1:s(2)
            for ll = 1:s(3)
                if (x_axis(ii)-x_axis(xx))^2 + (y_axis(kk)-y_axis(yy))^2 + (z_axis(ll)-z_axis(zz))^2 < 0.03^2
                    tDmask(ii,kk,ll) = 1;
                else
                    tDmask(ii,kk,ll) = 0;
                end
            end
        end
    end
%     for ii = 1:s(1)
%         for kk = 1:s(2)
%             for ll = 1:s(3)
%                 if (x_axis(ii)-x_axis(179))^2 + (y_axis(kk)-y_axis(round(s(2)/2)))^2 + (z_axis(ll)-z_axis(jj))^2 < 0.03^2
%                     tDmask(ii,kk,ll) = 1;
%                 else
%                     tDmask(ii,kk,ll) = 0;
%                 end
%             end
%         end
%     end

    X0 = ones(1,8);
    X0 = input('Type initial conditions (Amp -> Phase) \n');
    Nreps = input('Set the number of repetitions for ideal initial conditions \n');
    
else
    X0 = ones(1,8);
    X0 = input('Type initial conditions (Amp -> Phase) \n');
    Nreps = input('Set the number of repetitions for ideal initial conditions \n');
end
% jj
% round(s(1)/2)
% round(s(2)/2)

end

        
        