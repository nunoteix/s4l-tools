%% Please LOAD J p/ CHANNEL + J-Axis
load SurfJ_8Chnls_Rapid_Wire_LWPHant3 %Small Coil Phantom
% load SurfJ_8Chnls_V2Rapid_Wire_LWPHant3 %Long Coil Phantom

% if z-oriented
J = cat(2,J_Ch1(:,3),J_Ch2(:,3),J_Ch3(:,3),J_Ch4(:,3),J_Ch5(:,3),J_Ch6(:,3),J_Ch7(:,3),J_Ch8(:,3));

% if heart-wire (duke) (improved)
% J = cat(2,J_Ch1(:,1),J_Ch2(:,1),J_Ch3(:,1),J_Ch4(:,1),J_Ch5(:,1),J_Ch6(:,1),J_Ch7(:,1),J_Ch8(:,1));

%% Section Attempting to Create Heart Mask

xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
fsz = length(xaxis)*length(yaxis)*length(zaxis);

if ~exist('rho','var')
%     load('G:\Mask Properties\RapidCoil_LWPhant3_Wire_StrippedTip.mat')

    load('G:\Mask Properties\V2RapidCoil_LWPhant3_Wire_StrippedTip.mat')
%     load('G:\Mask Properties\FinalRapidCoil_LWPhant3_Wire_StrippedTip_1_5T.mat')

%     rho = cellfun(@(x) double(x), rho);
end

xtip = 38;
ytip = 38;
ztip = 275;

maskR = reshape(rho, [sz 1]);
for xx = 1:sz(1)
    for yy = 1:sz(2)
        for zz = 1:sz(3)
            if (xaxis(xx)-xaxis(xtip))^2+(yaxis(yy)-yaxis(ytip))^2+(zaxis(zz)-zaxis(ztip))^2 <= 0.05^2
                maskR(xx,yy,zz) = 1;
            else
                maskR(xx,yy,zz) = 0;
            end
        end
    end
end

wiredmaskR = maskR;
maskR(25:50, 25:50, 1:275) = 0; % FOR V2RCOIL
filledmask = reshape(maskR, [fsz 1]);
wiredmask = reshape(wiredmaskR, [fsz 1]);


%mask for MLS
maskidx = find(filledmask);
maskMLS = filledmask(maskidx);
maskidx2 = find(wiredmask);
sMask = wiredmask(maskidx2);

%%
% figure;
for ii = 1:8
% %%%% HEART SECTION
%     load(['B1_RapidCoil_LWPhant3_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)]) %RCOIL
    
    load(['B1_V2RapidCoil_LWPhant3_portByPort_Wire_StrippedTip\B1_channel' num2str(ii)]) %V2RCOIL
%     load(['B1_FinalRapidCoil_LWPhant3_portByPort_Wire_StrippedTip_1_5T\B1_channel' num2str(ii)]) %RCOIL
    
    if ii == 1
        xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
        yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
        zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
        sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
        fsz = length(xaxis)*length(yaxis)*length(zaxis);
    end
    
    RB1(:,ii) = Snapshot0(:,1).*filledmask;%.*w(ii);
    RB1wired(:,ii) = Snapshot0(:,1).*wiredmask;
%     rsRB1 = reshape(RB1(:,ii), sz);
%     heartRB1(:,:,:,ii) = rsRB1.*filledmaskR;
    
%     subplot(2,4,ii)
%     distributionGenerator(abs(squeeze(heartRB1(:,35,318:345,ii))),xaxis,zaxis(318:345),1,0,2e-7,'nearest');
%     distributionGenerator(angle(squeeze(heartRB1(:,35,318:345,ii))),xaxis,zaxis(318:345),1,-3.14,3.14,'nearest');
end
heartRB1 = reshape(RB1,[sz 8]);
heartRB1wired = reshape(RB1wired, [sz 8]);
% xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
% yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
% zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
% sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];

%% find WORST CASE (ALL MODES) AVERAGE B1+
% for ii = 1:8
%     tempF = heartRB1(:,:,:,ii);
%     tempF = tempF(:);
%     tempL = find(tempF);
%     phase(ii) = mean(angle(tempF(tempL)));
%     w(ii) = exp(1i*phase(ii));
%     zphi_heartRB1(:,:,:,ii) = heartRB1(:,:,:,ii).*w(ii);
% end

% absZPhi_quad = abs(sum(zphi_heartRB1,4));

% test1 = abs(sum(heartRB1,4));
% test = mean(test1(tempL));

% tempL = find(absZPhi_quad);
% avgB1_quad = mean(absZPhi_quad(tempL));

% MLS SHIM
    idxs = find(heartRB1);

    B1MLS = squeeze(reshape(heartRB1(idxs),[size(maskMLS) 8]));

    [w3,z3] = MLS(B1MLS.*1e6, maskMLS,[],2,100,1e-4);

    for ii = 1:8
        temp_shimmedB1(:,ii) = B1MLS(:,ii).*w3(ii);
    end
    shimmedB1_holder = sum(temp_shimmedB1,2);
    clear temp_shimmedB1
    
    testrep = find(filledmask);
    shimmed_B1 = zeros(length(filledmask),1);
    shimmed_B1(testrep) = shimmedB1_holder;
    shimmed_B1 = reshape(shimmed_B1, [sz 1]);
    
%     figure(150)
%     subplot(3,5,kk)
%     distributionGenerator(abs(shimmed_B1(:,:,330)),xaxis,yaxis,1,0,2e-6,'nearest');


    absShimmed = abs(shimmed_B1); % Sum Shimmed B1+
    tempL = find(absShimmed); % Find values inside heart (>0, because already masked)
    avgB1_quad = mean(absShimmed(tempL));

%%

%%% FOR PHANTOM
xtip = 37;
ytip = 37;
ztip = 275;
pedge = Jaxis(1) + 615;

% % % FOR PHANTOM
% phantToCoil2 = 450;
% coilLength2 = 250;

% % % FOR PHANTOM V2COIL
% phantToCoil = 200;
% coilLength = 500;

points_w = linspace(-130,500,15);
[~, pedge_ind] = min(abs(Jaxis-pedge));
st = Jaxis(2)-Jaxis(1);

jetblack = jet(4096);
jetblack(1,:) = [0,0,0];

if ~exist('Q_Matrix','var')
%     load('Q_V2RapidCoil_LWPhant3_portByPort_Wire_StrippedTip.mat')
    load('Q_RapidCoil_LWPhant3_portByPort_Wire_StrippedTip.mat')
end

Q_reshaped = reshape(Q_Matrix, [sz 8 8]);
Q_slice = squeeze(Q_reshaped(:,37,:,:,:));
Q_slice = reshape(Q_slice, [sz(1)*sz(3) 8 8]);

Q_tip = squeeze(Q_reshaped(xtip,ytip,ztip,:,:))./((avgB1_quad^2)/(sum(abs(w3).^2)/8));
maxEV_fullQ(1:15) = max(eig(Q_tip));

maxy = 7;

avgB1 = zeros(15,2);

%% SVD + MODING B1 + MLS + EIG Q

nsensors = 2;
% b1z = 272;
for kk = 1:length(points_w)
    
    % %%%% THIS SETS THE SENSOR POINTS ACCORDING TO THE # OF SENSOR CHOSEN
    if nsensors == 1
        % IF 1 SENSOR POINT
        posidx = round(points_w(kk)/st)+pedge_ind; %Sets points in relation to phantom-air border
        plot_pos = (posidx-pedge_ind).*st; % Calculates actual metric distance of the points for plotting
        [u,s,v]=svd(J(posidx,:)); % Single Value Decomposition from Surface Current J (wire)
        sens_pos(kk,:) = plot_pos; % Actual position of sensor vector
        v_dmss = v(:,2:8); % Creates Dark Mode Subspace (dmss)
    
    elseif nsensors == 2
        % IF 2 SENSOR POINTS
        posidx = [round(-200/st+pedge_ind), round(points_w(kk)/st)+pedge_ind];
        plot_pos = (posidx-pedge_ind).*st;
        [u,s,v]=svd(J(posidx,:));
        sens_pos(kk,:) = plot_pos;
        v_dmss = v(:,3:8);
    end
    v_all(:,:,kk) = v;
    
    
    for ii = 1:8
        for jj = 1:8
             oneMode_B1(:,:,:,jj) = heartRB1(:,:,:,jj).*v(jj,ii);
             oneMode_wiredB1(:,:,:,jj) = heartRB1wired(:,:,:,jj).*v(jj,ii);%Transform B1+ per channel into per mode
        end
        moded_B1(:,:,:,ii,kk) = sum(oneMode_B1, 4);
        moded_wiredB1(:,:,:,ii,kk) = sum(oneMode_wiredB1, 4);% Build the 8-mode matrix
    end
    
    
    %SIMPLE PHASE SHIM
%     for ii = 1:length(v_dmss(1,:))
%         tempF = squeeze(moded_B1(:,:,:,8-length(v_dmss(1,:))+ii,kk)); % Get Dark Mode B1+
%         tempF = tempF(:);
%         tempL = find(tempF); % Find values inside Heart (>0, because it's already masked)
%         phase(ii) = mean(angle(tempF(tempL))); % Find average phase inside heart
%         w(ii) = exp(1i*phase(ii)); % Build phase shim vector
%         shimmed_B1(:,:,:,ii) = squeeze(moded_B1(:,:,:,8-length(v_dmss(1,:))+ii,kk)).*w(ii); % Apply phase shim to B1+
%         shimmed_B1 = sum(shimmed_B1,4);
%     end
%     clear tempL

    % SHIM USING MLS
    darkmodeB1 = reshape(squeeze(moded_B1(:,:,:,nsensors+1:8,kk)),[fsz*(8-nsensors) 1]);
    test = find(darkmodeB1);

    darkmodeB1MLS = squeeze(reshape(darkmodeB1(test),[size(maskMLS) (8-nsensors)]));
    
    darkmodewiredB1 = reshape(squeeze(moded_wiredB1(:,:,:,nsensors+1:8,kk)),[fsz*(8-nsensors) 1]);
    test = find(darkmodewiredB1);
    
    if kk == 1
        nzsz = length(test)/(8-nsensors);
    end

    darkmodewiredB1MLS = squeeze(reshape(darkmodewiredB1(test),[nzsz (8-nsensors)]));
    
    
    if nsensors == 1
        [w1t,z1] = MLS(darkmodeB1MLS.*1e6, maskMLS,[],2,100,1e-4);
        for ii = 1:(8-nsensors)
            temp_shimmedB1(:,ii) = darkmodewiredB1MLS(:,ii).*w1t(ii);
        end
        w1(:,kk) = w1t;
    elseif nsensors == 2
        [w2t,z1] = MLS(darkmodeB1MLS.*1e6, maskMLS,[],2,100,1e-4);

        for ii = 1:(8-nsensors)
            temp_shimmedB1(:,ii) = darkmodewiredB1MLS(:,ii).*w2t(ii);
        end
        w2(:,kk) = w2t;
    end
    
    shimmedB1_holder = sum(temp_shimmedB1,2);

    testrep = find(darkmodewiredB1(1:end/(8-nsensors)));
    shimmed_B1 = zeros(length(wiredmask),1);
    shimmed_B1(testrep) = shimmedB1_holder;
    shimmed_B1 = reshape(shimmed_B1, [sz 1]);
    
%     figure(151)
%     subplot(3,5,kk)
%     distributionGenerator(abs(shimmed_B1(:,:,b1z)),xaxis,yaxis,1,0,1.5e-6,'nearest');


    absShimmed = abs(shimmed_B1); % Sum Shimmed B1+
    
    pp_absShimmed(:,:,:,nsensors,kk) = absShimmed; % Save phase shimmed B1+ for all sens_pos

    tempL = find(absShimmed); % Find values inside heart (>0, because already masked)
    avgB1(kk,nsensors) = mean(absShimmed(tempL)); % Calculate average B1+ inside heart
%     
    % SECTION FOR EIG VALUES @TIP

    
    if nsensors == 1
        
        Q_tip = squeeze(Q_reshaped(xtip,ytip,ztip,:,:))./((avgB1(kk,nsensors)^2)/(sum(abs(w1(:,kk)).^2)/8)); % Normalise the "WCSAR" to that AvgB1+
        Q_dmss = v_dmss'*(Q_tip)*v_dmss; % Reduce Q-matrix to the DMSS
        maxEV_redQ_1s1m(kk) = max(eig(Q_dmss)); %Calculate the "WCSAR" for the DMSS
    elseif nsensors == 2
        
        Q_tip = squeeze(Q_reshaped(xtip,ytip,ztip,:,:))./((avgB1(kk,nsensors)^2)/(sum(abs(w2(:,kk)).^2)/8)); % Normalise the "WCSAR" to that AvgB1+
        Q_dmss = v_dmss'*(Q_tip)*v_dmss; % Reduce Q-matrix to the DMSS
        maxEV_redQ_2s2m(kk) = max(eig(Q_dmss));
    end
disp(kk)

end
clear tempF tempL w phase shimmedB1_holder testrep shimmed_B1 absShimmed

[u_fullWire,s_fullWire,v_fullWire]=svd(J(:,:));

%%
for ii = 1:8
    tempF = heartRB1(:,:,:,ii);
    tempF = tempF(:);
    tempL = find(tempF);
    phase(ii) = mean(angle(tempF(tempL)));
    w(ii) = exp(1i*phase(ii));
    zphi_heartRB1(:,:,:,ii) = heartRB1(:,:,:,ii).*w(ii);
%     
%     subplot(2,4,ii)
%     distributionGenerator(abs(squeeze(zphi_heartRB1(:,35,318:345,ii))),xaxis,zaxis(318:345),1,0,2e-7,'nearest');
%     distributionGenerator(angle(squeeze(zphi_heartRB1(:,35,318:345,ii))),xaxis,zaxis(318:345),1,-3.14,3.14,'nearest');

end

absShimmed = abs(sum(zphi_heartRB1(:,:,:,:),4));

tempL = find(absShimmed);
avgB1 = mean(absShimmed(tempL));
% 
% absNormal = abs(sum(heartRB1(:,:,:,:),4));
% figure;distributionGenerator(squeeze(absNormal(:,35,318:345)),xaxis,zaxis(318:345),1,0,1e-6,'nearest');
% figure;distributionGenerator(squeeze(absZPhi(:,35,318:345)),xaxis,zaxis(318:345),1,0,1e-6,'nearest');

for ii = 1:8
    for jj = 1:8
         E_temp(:,:,:,:,jj) = squeeze(Ftemp(:,:,:,:,jj).*v3(jj,ii));
    end
end