function [improv3s] = scatterTipQ_3sv2(points, maxEV_2s2m, maxEV_3s3m, chandle)
kk = 1;
mm = 1;
for ii = 1:length(points)
    for jj = ii+1:length(points)
        points_2s(kk,1:2) = [points(ii) points(jj)];
        for ll = jj+1:length(points)
            points_3s(mm,1:3) = [points(ii) points(jj) points(ll)];
            mm = mm+1;
        end
        kk = kk + 1;
    end
end
%%

kk = 0;
for ii = points
    kk = kk+1;
    [idx1,~] = find(points_2s(:,2) == ii);
    [idx2,~] = find(points_3s(:,3) == ii);
    
    if kk == 1 || kk == 2;
        improv3s = [(-1*(min(maxEV_3s3m(idx2))-min(maxEV_2s2m(idx1)))./min(maxEV_2s2m(idx1)))];
    else
        improv3s = [improv3s; (-1*(min(maxEV_3s3m(idx2))-min(maxEV_2s2m(idx1)))./min(maxEV_2s2m(idx1))) ii];
    end
end
figure; 
set(gcf,'Position',[1 1 600 270])
    set(gca, 'FontSize', 16)
plot(improv3s(:,2),improv3s(:,1)*100,'-o',...
        'LineWidth', 3,...
        'MarkerSize', 10,...
        'MarkerEdgeColor','w',...
        'MarkerFaceColor',[0,0,0])
xlim([points(1) points(end)+20]);
grid on
grid minor
title({'Effect of extracting a third mode from the system'; chandle},'FontSize',14)

    
xlabel('Deepest Sensor Position','FontSize',14)
ylabel('Change in wcSAR (%)','FontSize',14)


end