function [SAR, axis] = SARfromEShimmed(Jfile,shim)
    load(Jfile);
    dim = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
    J = sum(Snapshot0,2);
    J(abs(J)>=0.1)=1;
    J(abs(J)~=1)=0;
    J = reshape(J,dim);
    axis = struct('x', Axis0,'y', Axis1, 'z', Axis2);
%% get E_Shimmed
    Nc = length(shim);
    fileName = uigetfile('*.mat','E','MultiSelect','on');
  
    Eshimmed = zeros(dim(1), dim(2), dim(3), 3, Nc);
    for ii = 1:Nc
        Snapshot0 = Snapshot0*shim(ii);
        Eshimmed(:,:,:,:,ii) = reshape(Snapshot0, [dim 3]);
        for jj = 1:3
            E_forSAR(:,:,:,jj,ii) = Eshimmed(:,:,:,jj,ii).*J;
        end
    end
    E_forSAR = reshape(E_forSAR, [dim(1)*dim(2)*dim(3) 3 32]);  
%% get SAR
    siz = size(E_forSAR);
    for kk = 1:siz(1)
        if E_forSAR(kk,:,:) ~= 0
            SAR(kk,:,:) = (0.47/(2*1000)).*((E_forSAR(kk,:,:)')*E_forSAR(kk,:,:));
        else
            SAR(kk,:,:) = 0;
        end
    end
end
