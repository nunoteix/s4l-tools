xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;

% tQ = reshape(Q, [length(xaxis), length(yaxis), length(zaxis), 8, 8]);
tpSAR = reshape(dpSAR, [length(xaxis), length(yaxis), length(zaxis), 8]);
rho = reshape(rho, [length(xaxis), length(yaxis), length(zaxis)]);

rho_interp = Gen3D(rho(39:59,28:48,18:end), yaxis(28:48), xaxis(39:59), zaxis(18:end));

k = 0;
for ii = 1:8
%     for jj = 1:8
%         k = k+1;
%         Q_interp(:,:,:,ii,jj) = Gen3D(tQ(39:59,28:48,18:end,ii,jj), yaxis(28:48),xaxis(39:59),zaxis(18:end));
        SAR_interp(:,:,:,ii) = Gen3D(tpSAR(39:59,28:48,18:end,ii), yaxis(28:48),xaxis(39:59),zaxis(18:end));
        figure(1)
%         subplot(8,8,k)
        subplot(2,4,ii)
        imagesc(abs(imrotate(SAR_interp(:,:,19,ii),90)))
%     end
end
sz = size(rho_interp)
dx = (max(xaxis(39:59))-min(xaxis(39:59)))/sz(1);

dy = (max(yaxis(28:48))-min(yaxis(28:48)))/sz(2);

dz = (max(zaxis(18:end))-min(zaxis(18:end)))/sz(3);