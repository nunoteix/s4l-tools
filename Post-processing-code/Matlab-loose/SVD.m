%% multichannel data
% load FullSurfJ_8chnls_V2RapidCoil_STipWire_HDuke
load SurfJ_8Chnls_Rapid_Wire_LWPHant3

% z-oriented
J = cat(2,J_Ch1(:,3),J_Ch2(:,3),J_Ch3(:,3),J_Ch4(:,3),J_Ch5(:,3),J_Ch6(:,3),J_Ch7(:,3),J_Ch8(:,3));

% heart-wire
% J = cat(2,cat(1, J_Ch1(1:83,3), J_Ch1(84:98,1), J_Ch1(99:138,3), J_Ch1(139:153,2), J_Ch1(154:end,3)),...
%     cat(1, J_Ch2(1:83,3), J_Ch2(84:98,1), J_Ch2(99:138,3), J_Ch2(139:153,2), J_Ch2(154:end,3)),...
%     cat(1, J_Ch3(1:83,3), J_Ch3(84:98,1), J_Ch3(99:138,3), J_Ch3(139:153,2), J_Ch3(154:end,3)),...
%     cat(1, J_Ch4(1:83,3), J_Ch4(84:98,1), J_Ch4(99:138,3), J_Ch4(139:153,2), J_Ch4(154:end,3)),...
%     cat(1, J_Ch5(1:83,3), J_Ch5(84:98,1), J_Ch5(99:138,3), J_Ch5(139:153,2), J_Ch5(154:end,3)),...
%     cat(1, J_Ch6(1:83,3), J_Ch6(84:98,1), J_Ch6(99:138,3), J_Ch6(139:153,2), J_Ch6(154:end,3)),...
%     cat(1, J_Ch7(1:83,3), J_Ch7(84:98,1), J_Ch7(99:138,3), J_Ch7(139:153,2), J_Ch7(154:end,3)),...
%     cat(1, J_Ch8(1:83,3), J_Ch8(84:98,1), J_Ch8(99:138,3), J_Ch8(139:153,2), J_Ch8(154:end,3)));

% heart-wire improved
% J1 = cat(2,J_Ch1(:,1),J_Ch2(:,1),J_Ch3(:,1),J_Ch4(:,1),J_Ch5(:,1),J_Ch6(:,1),J_Ch7(:,1),J_Ch8(:,1));
% 
%% SVD
pedge = Jaxis(1) + 615;
[~, pedge_ind] = min(abs(Jaxis-pedge));

[u,s,v]=svd(J1(:,:));

ss = diag(s);

figure(3)
%semilogy(ss)
stem(ss)
grid on
ylabel('singular value')
xlabel('mode number')

st= Jaxis(2) - Jaxis(1);
%%
posidx = [round((100)/st)+pedge_ind, round((450)/st)+pedge_ind];% 300+pedge_ind];
plot_pos = (posidx-pedge_ind).*(Jaxis(2)-Jaxis(1));
% sens_pos(11) = plot_pos;

% posidx = [30 35];
[u2,s2,v2]=svd(J(posidx,:));
% ss2 = diag(s2);

% figure(3)
% %semilogy(ss)
% stem(ss2)
% grid on
% ylabel('singular value')
% xlabel('mode number')
J_modes = J1*v;

figure;
hold on
plot(abs(J_modes(:,1)),'LineWidth',2)
plot(abs(J_modes(:,2)),'LineWidth',2)
plot(abs(J_modes(:,3)),'LineWidth',2)
plot(abs(J_modes(:,4)),'LineWidth',2)
plot(abs(J_modes(:,5)),'LineWidth',2)
plot(abs(J_modes(:,6)),'LineWidth',2)
plot(abs(J_modes(:,7)),'LineWidth',2)
plot(abs(J_modes(:,8)),'LineWidth',2)
for ii = 1:8
leg{ii} = ['Mode ' num2str(ii)];
end
legend(leg,'Location', 'northwest')
set(gca,'box','off')
% axis off
axis on

%% Now look at current in mode space

nps = length(J_Ch1);
maxy = 8;
phantToCoil = 450;
coilLength = 200;
opMode = 'full';
if strcmp(opMode,'full') == 1
    fullJm8 = J*v3;
else
    fullJm8 = J*v2;
end

fullJm = zeros(length(Jaxis),8);
fullJm(:,1:8) = fullJm8;
% fullJm(:,9) = fullJm8*c;
% fullJm(:,10) = J*eig_tip;

Jm = zeros(length(Jaxis),8);
for ii = 1:length(Jaxis)
    smaller = find(Jaxis < Jaxis(ii)+7.5); 
    bigger = find(Jaxis > Jaxis(ii)-7.5);
    sect = intersect(bigger,smaller);
    Jm(ii,:) = mean(abs(fullJm(sect,:))).*exp(1i*(mean(angle(fullJm(sect,:)))));
%     compJ(ii) = mean(fullJmod(sect,:));
end


figure(12)

% subplot(5,2,9:10)
plot(Jaxis-Jaxis(pedge_ind), abs(Jm(:,:)))
hold on
% plot(Jaxis-Jaxis(pedge_ind), abs(Jm(:,10)), 'Color', 'k', 'LineWidth', 2)
% hold off
line([phantToCoil phantToCoil+coilLength], [maxy-2 maxy-2], 'Color', 'k', 'LineWidth', 2);
% line([178 248], [maxy-2 maxy-2], 'Color', 'g', 'LineWidth', 2);
% line([-720 -32], [maxy-1 maxy-1], 'Color', 'b', 'LineWidth', 2);
% line([-32 38], [maxy-1 maxy-1], 'Color', 'r', 'LineWidth', 2);
% line([38 178], [maxy-1 maxy-1], 'Color', 'b', 'LineWidth', 2);
% line([178 248], [maxy-1 maxy-1], 'Color', 'g', 'LineWidth', 2);
% line([248 508], [maxy-1 maxy-1], 'Color', 'b', 'LineWidth', 2);
axis([Jaxis(1)-Jaxis(pedge_ind)+10 Jaxis(end)-Jaxis(pedge_ind)+100 0 maxy])
grid on
leg={};for ii=1:8,leg{ii}=sprintf('Mode %d',ii);end;leg{9} = 'Coil'; leg{10} = 'Phantom';
% legend(leg, 'Location', 'northwest')
xlabel('z')
ylabel('J')
title('Wire current distribution per mode')
hold on
patchh = patch([0 (nps-pedge_ind) (nps-pedge_ind) 0], [0 0 maxy maxy], 'b');
patchh.FaceAlpha = 0.075;
patchh.EdgeAlpha = 0;
if strcmp(opMode,'full') == 0
   plot((posidx-pedge_ind).*(Jaxis(2)-Jaxis(1)), ones(1,length(posidx))*(maxy/2), 'o')
end
hold off
legend(leg, 'Location', 'northwest')

%%

%%% inset plot for tip
axes('Position',[.3 .8 .1 .1])
plot(abs(Jm(:,1:3)),'o-')
ylim([0 0.01])
xlim([nps-1 nps+1])
grid on


%%
figure(4)
subplot(2,2,3)
imagesc(abs(vq))
xlabel('mode index')
ylabel('Coil number')
title('Mode shim amplitude')
colorbar
subplot(2,2,4)
imagesc(180*(angle(vq)/pi))
xlabel('mode index')
ylabel('Coil number')
title('Mode shim phase/deg')
colorbar

%% Now apply the modes to the B1+ Distributions
% IMPORT B1 FIELDS

[fNames,fPath] = uigetfile('*.mat','MultiSelect', 'on', 'Pick E or H Fields');

for ii = 1:8
        path = strcat(fPath, fNames(ii));
        load(path{1,1})
        xaxis =(Axis0(1:end-1)+Axis0(2:end))'/2;
        yaxis =(Axis1(1:end-1)+Axis1(2:end))'/2;
        zaxis =(Axis2(1:end-1)+Axis2(2:end))'/2;
        sz = [length(Axis0)-1 length(Axis1)-1 length(Axis2)-1];
        if ii == 1
            Ftemp = zeros(sz(1),sz(2),sz(3),3,8);
        end
        Ftemp(:,:,:,:,ii) = reshape(Snapshot0, [sz 3]);
        
        
%         if ii == 1
% %             load('V2RapidCoil_Duke_Wire_StrippedTip.mat')
% %             rho = cell2mat(rho);
%             rho = reshape(rho, sz);
%             rho(rho>0) = 1;
%             rho = distributionGenerator(squeeze(rho(:,32,:)), xaxis, zaxis,0,[],[],'nearest');
%         end
%         F(:,:,ii) = distributionGenerator(squeeze(abs(Ftemp(:,31,:,1,ii))), xaxis, zaxis,0,0,2e-7); 
%     F(:,:,:,ii) = fieldInterpolator(Ftemp(:,:,:,1), xaxis, yaxis, zaxis);
end
%%
clear vars E_temp Ez_m Ez_m_interp
for ii = 1:8
    for jj = 1:8
         E_temp(:,:,:,:,jj) = squeeze(Ftemp(:,:,:,:,jj).*v3(jj,ii));
    end
%     
    Ez_m(:,:,ii) = sum(squeeze(E_temp(:,37,:,3,:)), 3);
% 
    Ez_m_interp(:,:,ii) = distributionGenerator(squeeze(abs(Ez_m(:,:,ii))), xaxis, zaxis,0,0,20);
    
%     B1_byElement(:,:,ii) = distributionGenerator(squeeze(abs(Ftemp(:,:,326,1,ii))),xaxis, yaxis, 0);
end


%%
 for ii = 1:8      
    figure(201)
    subplot(4,2,ii)
%     clearvars F
    imagesc(imrotate(abs((B1_m_interp(:,:,ii))).*b1mask,-90))
%     imagesc(xaxis, zaxis, imrotate(abs(F(:,:,ii).*rho),-90))
%     axis([-0.0167 0.0247 -1.089 0.193])
    colormap jet;colorbar;%axis square;
    if ii == 1
        caxis([0 2e-7]) % for B1+
    else
        caxis([0 2e-7])
    end
%     caxis([0 40]) % for Ez
%    caxis([0 2e-7])
%     xlabel('(z-axis)')
%     ylabel('(x-axis)')
%     axis off
    title(['Mode ' num2str(ii) ' B1+ field'])% - PSF:' num2str(scaleF(ii)^2)])
    set(gca,'FontSize',12,'FontWeight','bold','LineWidth',1);
 end

%%
for ii = 1:8
    Fshift(:,:,ii) = F(:,:,ii).*exp(1i*(degtorad(-45)*(ii-1)));
end
clear path fNames fPath Snapshot0 Axis0 Axis1 Axis2

sz = [length(xaxis) length(yaxis) length(zaxis)];


%% to plot E-field unshimmed
if size(F) == [sz 3 8]
    E = reshape(F, [sz(1)*sz(2)*sz(3) 3 8]);
    for ii = 1:8
        Eabs(:,ii) = sqrt(abs(E(:,1,ii)).^2 + abs(E(:,2,ii)).^2); %+ abs(E(:,3,ii)).^2);
    end
    Eabs = reshape(Eabs, [sz 8]);
end
%%
figure(106)
for  ii = 1:8
    subplot(2,4,ii)
%     scaleF(ii) = abs(max(max(B1_m(:,1:15,340,1,1).*rho(:,1:15,340))))/abs(max(max(B1_m(:,1:15,340,1,ii).*rho(:,1:15,340))));
    distributionGenerator(squeeze(abs(F(:,:,50,ii))), xaxis, yaxis,1,0,20);%.*scaleF(ii) //  .*rho(:,:,340))
    
    axis([-0.22 0.22 -0.14 0.14]) %for XY plane
%     axis([-0.21 0.21 zaxis(1)-0.025 zaxis(1)+1.025]) %for XZ plane
    
    xlabel('(x-axis)')
    ylabel('(y-axis)')
    title(['Element ' num2str(ii) ' B1+ field'])% - PSF:' num2str(scaleF(ii)^2)])
    set(gca,'FontSize',12,'FontWeight','bold','LineWidth',1);
end
%%
for ii = 1:8
    B1p_intp(:,:,:,ii) = fieldInterpolator(squeeze(F(:,:,:,1,ii)),xaxis, yaxis, zaxis);
end
%% Apply modes to B1+
for ii = 1:8
    for jj = 1:8
         E_temp(:,:,jj) = squeeze(Ftemp(:,32,:,1,jj)).*v(jj,ii);
    end
    Ez_m(:,:,ii) = sum(E_temp, 3);
end
%% Apply modes to E-field
for ii = 1:8
    for jj = 1:8
         E_temp(:,:,:,:,jj) = squeeze(F(:,:,:,:,jj)).*v(jj,ii);
    end
    E_m(:,:,:,:,ii) = sum(E_temp, 5);
end
% Sum them up
E_m = reshape(E_m, [sz(1)*sz(2)*sz(3) 3 8]);
    for ii = 1:8
        Eabs_m(:,ii) = sqrt(abs(E_m(:,1,ii)).^2 + abs(E_m(:,2,ii)).^2);% + abs(E_m(:,3,ii)).^2);
    end
Eabs_m = reshape(Eabs_m, [sz 8]);

%%
for ii = 1:8
    % % Plot the B1+ distributions correspondant to the 8 modes (3 Coupled, 5 decoupled)
    figure(5)
    set(gcf,'color','w');
    subplot(2,4,ii)

    distributionGenerator(squeeze(abs(Ez_m(:,48,:,ii)).*rho(:,48,:)), xaxis, zaxis,1,0, 2.5e-7);
%     axis([-0.14 0.14 -0.14 0.14])
    axis([-0.14 0.14 0.6 1.2]) %for XZ plane
    
    xlabel('(x-axis)')
    ylabel('(y-axis)')
    if ii <3
        title(['Coupled Mode #' num2str(ii)])
    elseif ii>3
        title(['Decoupled Mode #' num2str(ii)])
    end
    % % Plot the Unshimmed 1W/channel B1+
    figure(6)
    set(gcf,'color','w');
    subplot(2,4,ii)

    distributionGenerator(squeeze(abs(F(:,48,:,1,ii)).*rho(:,48,:)), xaxis, zaxis,1,0, 2.5e-7);
%     axis([-0.14 0.14 -0.14 0.14])
    axis([-0.14 0.14 0.6 1.2]) %for XZ plane
     
    xlabel('(x-axis)')
    ylabel('(z-axis)')
    title(['Channel ' num2str(ii)])
    set(gca,'FontSize',12,'FontWeight','bold','LineWidth',1);
    
%     axis([-0.14 0.14 0.6 1.2]) %for XZ plane
end
    
    
    
    
    