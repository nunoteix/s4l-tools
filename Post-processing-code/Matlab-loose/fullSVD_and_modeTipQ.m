modes  = (Jinterp.*(pi*0.001))*v_fullWire;%
figure;
set(gcf,'Position',[100 100 1220 320])
set(gca, 'FontSize', 20)
subplot(1,18,1:17)

for ii = 1:8
    plot((Jaxis-pedge)*st,smooth(smooth(smooth(abs(modes(:,ii))))), 'LineWidth', 3)
    hold on
    leg{ii} = ['Mode ' num2str(ii)];
end

set(gca,'xtick',[0])
ylabel('J(A)', 'FontSize',20)
ylim([0 0.012])

a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',14)
a = get(gca,'YTickLabel');
set(gca,'YTickLabel',a,'fontsize',14)

title('Long Coil Simplistic Wire', 'FontSize', 14)
legend(leg, 'Location', 'northwest')
% xlabel('Distance along the wire', 'FontSize', 10)


subplot(1,18,18)
% [maxEV_redQ, maxEV_redQ_noNorm, v_all, b1weights_1s, w1, avgB1] = fullSVDTipQ(nChannels, Jinterp, maskMLS, sRB1r, sRB1wiredr, Q_tip);
imagesc(abs((maxEV_redQ_noNorm/maxEV_redQ_noNorm(1))*100)');
axis off
cbh = colorbar('eastoutside');
cbh.Position(3) = 0.025;
cbh.Position(1) = 0.92;

a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',14)
a = get(gca,'YTickLabel');
set(gca,'YTickLabel',a,'fontsize',14)
refresh;