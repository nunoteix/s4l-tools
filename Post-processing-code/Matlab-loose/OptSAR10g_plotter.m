function [SAR,axis,C] = OptSAR10g_plotter(OptSAR,LUT, Jfile,img)
%     load(Jfile);
    dim = [length(Jfile.Axis0)-1 length(Jfile.Axis1)-1 length(Jfile.Axis2)-1];
    J = sum(Jfile.Snapshot0,2);
    J(abs(J)>=0.05)=1;
    J(abs(J)~=1)=0;
    J = reshape(J,dim);

    axis = struct('x', Jfile.Axis0,'y', Jfile.Axis1, 'z', Jfile.Axis2);
    %% SAR from S4L Q-Matrix
%     load(Qfile);
    SAR = zeros(1,dim(1)*dim(2)*dim(3));
    
    for ii = 1:length(LUT)
        SAR(LUT(ii)) = OptSAR(ii);
    end

    if real(SAR) == abs(SAR)
        SAR = reshape(abs(SAR),dim(1),dim(2),dim(3));
        [C,I] = max(SAR(:));
        [I1,I2,I3] = ind2sub(size(SAR),I);
        if img
            figure;visualizeDistr2(SAR(:,:,I3).*J(:,:,I3),Jfile.Axis0, Jfile.Axis1,1,[0 C])
%           figure;visualizeDistr2(SAR(:,:,I3),Axis0, Axis1,1,[0 C])
%           figure;visualizeDistr2(J(:,:,I3),Axis0, Axis1,1,[0 C])
        end
    else
        real(SAR) == abs(SAR);
        A = find(ans == 0);
        SAR(A(:)) = 0;
        SAR = reshape(abs(SAR),dim(1),dim(2),dim(3));
        [C,I] = max(SAR(:));
        [I1,I2,I3] = ind2sub(size(SAR),I);
        if img
            figure;visualizeDistr2(SAR(:,:,I3).*J(:,:,I3),Jfile.Axis0, Jfile.Axis1,1,[0 C])
%           figure;visualizeDistr2(SAR(:,:,I3),Axis0, Axis1,1,[0 C])
%           figure;visualizeDistr2(J(:,:,I3),Axis0, Axis1,1,[0 C])
        end
%         h = errordlg('Something went wrong - SAR is complex','SAR ERROR');

    end
end



