%%B1_minus MAP
clear all;
zmid = 55; %CHANGE HERE
data = getAllDistributionsFromFile;
nametag = uigetfile();
load(nametag);
figure;
visualizeDistr2(abs(B1_minus(:,:,zmid)),data.B1.axes.x, data.B1.axes.y,1,[0 2.5e-6])
axis([-0.25 0.25 -0.095 0.095])

%% B1+ MAP
cz = 104;
figure(04)
load('B1Shimmed_32FracM+PatchGAP')
load('J_32FracM+PatchGAP')
J = sum(Snapshot0,2);
J(abs(J)>=0.005)=1;
J(abs(J)~=1)=0;
J = reshape(J,[length(Axis0)-1 length(Axis1)-1 length(Axis2)-1]);
load('SAR10g_32FracM+PatchGAP');
[SAR3D, axs, pSAR10g] = OptSAR10g_plotter(SAR10g,LUT,'J_32FracM+PatchGAP',1);
figure(02)
visualizeDistr2((abs(B1_shimmed_sum(:,:,cz))./sqrt(pSAR10g)).*J(:,:,cz).*1e6, Axis0, Axis1)
figure(03)
visualizeDistr2(abs(B1_shimmed_sum(:,:,cz)).*J(:,:,cz).*1e6, Axis0, Axis1)