function [map3s] = scatterTipQ_3s(points_pos, points_3_pos, maxEV_redQ_2s2m, maxEV_redQ_3s3m)
    for ii = 1:length(points_pos)
        [idx,~] = find(points_3_pos(:,1) == points_pos(ii,1) & points_3_pos(:,3) == points_pos(ii,2));

        if ii == 1;
        map3s = [(-1*(maxEV_redQ_3s3m(idx)-maxEV_redQ_2s2m(ii))./maxEV_redQ_2s2m(ii))' points_3_pos(idx,3)];
        else
        map3s = [map3s; (-1*(maxEV_redQ_3s3m(idx)-maxEV_redQ_2s2m(ii))./maxEV_redQ_2s2m(ii))' points_3_pos(idx,3)];
        end
    end
    figure; scatter(map3s(:,2),map3s(:,1)*100,10);axis([-100 500 -100 100])
    grid on
    title('Effect of extracting a third mode from the system','FontSize',14)
    xlabel('Sensor Position 3','FontSize',14)
    ylabel('Change in wcSAR (%)','FontSize',14)
end