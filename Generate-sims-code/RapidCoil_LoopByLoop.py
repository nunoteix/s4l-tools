import s4l_v1.document as document
import s4l_v1.model as model
from s4l_v1 import Vec3
import s4l_v1.simulation.emfdtd as fdtd
import s4l_v1.units as units
import numpy
pi = numpy.pi

# Start New Document
document.New()

def CreateHardware():
	# Ground Plate Parameters
	gc_radius = 675.0/2
	gc_length = 1000.0
	gc_depth = 0.0		
	gc_geo = [gc_radius,gc_length,gc_depth]

	# Create Ground Plate
	rf_shield = model.CreateSolidTube( Vec3(0,0,-gc_length/2.0),Vec3(0,0,gc_length),gc_radius+gc_depth,gc_radius)
	rf_shield.Name = 'RFS'

	# Create Metal Loop

	leg1 = model.CreateSolidBlock(Vec3(-75.625/2, 0, -250/2), Vec3(-75.625/2 + 3, 0, 250/2), False)

	leg2 = model.CreateSolidBlock(Vec3(75.625/2, 0, -250/2), Vec3(75.625/2 - 3, 0, 250/2), False)

	arm1 = model.CreateSolidBlock(Vec3(-75.625/2, 0, -250/2), Vec3(75.625/2, 0, -250/2 + 3), False)

	arm2 = model.CreateSolidBlock(Vec3(-75.625/2, 0, 250/2), Vec3(75.625/2, 0, 250/2 - 3), False)

	rect = model.Unite([leg1, leg2, arm1, arm2])

	capgap1 = model.CreateSolidBlock(Vec3(-1, 0, 250/2), Vec3(+1, 0, 250/2 - 3), False)
	capgap2 = model.CreateSolidBlock(Vec3(-1, 0, -250/2), Vec3(+1, 0, -250/2 + 3), False)
	capgap3 = model.CreateSolidBlock(Vec3(-75.625/2, 0, -1), Vec3(-75.625/2 + 3, 0, +1), False)
	capgap4 = model.CreateSolidBlock(Vec3(+75.625/2, 0, -1), Vec3(+75.625/2 - 3, 0, +1), False)

	# Create 8 Loop Coils

	struct1 = model.Subtract([rect, capgap1, capgap2, capgap3, capgap4], False); struct1.Name = 'SLoop1'
	P1 = model.CreatePolyLine([Vec3(-1, 0, -250/2 + 0.75), Vec3(+1, 0, -250/2 + 0.75)]); P1.Name = 'A1'
	LE101 = model.CreatePolyLine([Vec3(-1, 0, -250/2 + 2.25), Vec3(+1, 0, -250/2 + 2.25)]); LE101.Name = 'LE011'
	LE102 = model.CreatePolyLine([Vec3(+75.625/2 - 1.5, 0, +1), Vec3(+75.625/2 - 1.5, 0, -1)]); LE102.Name = 'LE021'
	LE103 = model.CreatePolyLine([Vec3(-1, 0, 250/2 - 1.5), Vec3(+1, 0, 250/2 - 1.5)]); LE103.Name = 'LE031'
	LE104 = model.CreatePolyLine([Vec3(-75.625/2 + 1.5, 0, -1), Vec3(-75.625/2 + 1.5, 0, +1)]); LE104.Name = 'LE041'
	coil1 = model.Unite([struct1, P1, LE101, LE102, LE103, LE104], True)
	coil1.Name = 'Circuit 1'

	struct2 = struct1.Clone(); struct2.Name = 'SLoop2'
	P2 = model.CreatePolyLine([Vec3(-1, 0, -250/2 + 0.75), Vec3(+1, 0, -250/2 + 0.75)]); P2.Name = 'A2'
	LE201 = model.CreatePolyLine([Vec3(-1, 0, -250/2 + 2.25), Vec3(+1, 0, -250/2 + 2.25)]); LE201.Name = 'LE012'
	LE202 = model.CreatePolyLine([Vec3(+75.625/2 - 1.5, 0, +1), Vec3(+75.625/2 - 1.5, 0, -1)]); LE202.Name = 'LE022'
	LE203 = model.CreatePolyLine([Vec3(+1, 0, 250/2 - 1.5), Vec3(-1, 0, 250/2 - 1.5)]); LE203.Name = 'LE032'
	LE204 = model.CreatePolyLine([Vec3(-75.625/2 + 1.5, 0, -1), Vec3(-75.625/2 + 1.5, 0, +1)]); LE204.Name = 'LE042'
	coil2 = model.Unite([struct2, P2, LE201, LE202, LE203, LE204], True)
	coil2.Name = 'Circuit 2'

	struct3 = struct1.Clone(); struct3.Name = 'SLoop3'
	P3 = model.CreatePolyLine([Vec3(-1, 0, -250/2 + 0.75), Vec3(+1, 0, -250/2 + 0.75)]); P3.Name = 'A3'
	LE301 = model.CreatePolyLine([Vec3(-1, 0, -250/2 + 2.25), Vec3(+1, 0, -250/2 + 2.25)]); LE301.Name = 'LE013'
	LE302 = model.CreatePolyLine([Vec3(+75.625/2 - 1.5, 0, -1), Vec3(+75.625/2 - 1.5, 0, +1)]); LE302.Name = 'LE023'
	LE303 = model.CreatePolyLine([Vec3(-1, 0, 250/2 - 1.5), Vec3(+1, 0, 250/2 - 1.5)]); LE303.Name = 'LE033'
	LE304 = model.CreatePolyLine([Vec3(-75.625/2 + 1.5, 0, +1), Vec3(-75.625/2 + 1.5, 0, -1)]); LE304.Name = 'LE043'
	coil3 = model.Unite([struct3, P3, LE301, LE302, LE303, LE304], True)
	coil3.Name = 'Circuit 3'

	struct4 = struct1.Clone(); struct4.Name = 'SLoop4'
	P4 = model.CreatePolyLine([Vec3(-1, 0, -250/2 + 0.75), Vec3(+1, 0, -250/2 + 0.75)]); P4.Name = 'A4'
	LE401 = model.CreatePolyLine([Vec3(-1, 0, -250/2 + 2.25), Vec3(+1, 0, -250/2 + 2.25)]); LE401.Name = 'LE014'
	LE402 = model.CreatePolyLine([Vec3(+75.625/2 - 1.5, 0, +1), Vec3(+75.625/2 - 1.5, 0, -1)]); LE402.Name = 'LE024'
	LE403 = model.CreatePolyLine([Vec3(+1, 0, 250/2 - 1.5), Vec3(-1, 0, 250/2 - 1.5)]); LE403.Name = 'LE034'
	LE404 = model.CreatePolyLine([Vec3(-75.625/2 + 1.5, 0, +1), Vec3(-75.625/2 + 1.5, 0, -1)]); LE404.Name = 'LE044'
	coil4 = model.Unite([struct4, P4, LE401,LE402, LE403, LE404], True)
	coil4.Name = 'Circuit 4'

	struct5 = struct1.Clone(); struct5.Name = 'SLoop5'
	P5 = P1.Clone(); P5.Name = 'A5'
	LE501 = LE101.Clone(); LE501.Name = 'LE015'
	LE502 = LE102.Clone(); LE502.Name = 'LE025'
	LE503 = LE103.Clone(); LE503.Name = 'LE035'
	LE504 = LE104.Clone(); LE504.Name = 'LE045'
	coil5 = model.Unite([struct5, P5, LE501, LE502, LE503, LE504], True)
	coil5.Name = 'Circuit 5'

	struct6 = struct1.Clone(); struct6.Name = 'SLoop6'
	P6 = P1.Clone(); P6.Name = 'A6'
	LE601 = LE201.Clone(); LE601.Name = 'LE016'
	LE602 = LE202.Clone(); LE602.Name = 'LE026'
	LE603 = LE203.Clone(); LE603.Name = 'LE036'
	LE604 = LE204.Clone(); LE604.Name = 'LE046'
	coil6 = model.Unite([struct6, P6, LE601, LE602, LE603, LE604], True)
	coil6.Name = 'Circuit 6'

	struct7 = struct1.Clone(); struct7.Name = 'SLoop7'
	P7 = P1.Clone(); P7.Name = 'A7'
	LE701 = LE301.Clone(); LE701.Name = 'LE017'
	LE702 = LE302.Clone(); LE702.Name = 'LE027'
	LE703 = LE303.Clone(); LE703.Name = 'LE037'
	LE704 = LE304.Clone(); LE704.Name = 'LE047'
	coil7 = model.Unite([struct7, P7, LE701, LE702, LE703, LE704], True)
	coil7.Name = 'Circuit 7'

	struct8 = struct1.Clone(); struct8.Name = 'SLoop8'
	P8 = P1.Clone(); P8.Name = 'A8'
	LE801 = LE401.Clone(); LE801.Name = 'LE018'
	LE802 = LE402.Clone(); LE802.Name = 'LE028'
	LE803 = LE403.Clone(); LE803.Name = 'LE038'
	LE804 = LE404.Clone(); LE804.Name = 'LE048'
	coil8 = model.Unite([struct8, P8, LE801, LE802, LE803, LE804], True)
	coil8.Name = 'Circuit 8'

	# Get Array into position

	T1 = model.Transform(Vec3(1,1,1), Vec3(pi,0,0), Vec3(250/2 - 75.625/2,0 + 100 + 7,0))
	T2 = model.Transform(Vec3(1,1,1), Vec3(pi,0,0), Vec3(250/2 - 75.625/2 - (75.625 - 17.5), 1 + 100 + 7, 0))
	T3 = model.Transform(Vec3(1,1,1), Vec3(pi,0,0), Vec3(250/2 - 75.625/2 - (75.625 - 17.5)*2, 0 + 100 + 7, 0))
	T4 = model.Transform(Vec3(1,1,1), Vec3(pi,0,0), Vec3(250/2 - 75.625/2 - (75.625 - 17.5)*3, 1 + 100 + 7, 0))
	T5 = model.Transform(Vec3(1,1,1), Vec3(pi,0,0), Vec3(250/2 - 75.625/2, -1 - 100 - 7,0))
	T6 = model.Transform(Vec3(1,1,1), Vec3(pi,0,0), Vec3(250/2 - 75.625/2 - (75.625 - 17.5), 0 - 100 - 7, 0))
	T7 = model.Transform(Vec3(1,1,1), Vec3(pi,0,0), Vec3(250/2 - 75.625/2 - (75.625 - 17.5)*2, -1 - 100 - 7, 0))
	T8 = model.Transform(Vec3(1,1,1), Vec3(pi,0,0), Vec3(250/2 - 75.625/2 - (75.625 - 17.5)*3, 0 - 100 - 7, 0))

	coil1.ApplyTransform(T8); coil2.ApplyTransform(T7); coil3.ApplyTransform(T6); coil4.ApplyTransform(T5)
	coil5.ApplyTransform(T4); coil6.ApplyTransform(T3); coil7.ApplyTransform(T2); coil8.ApplyTransform(T1)

def BigSim():
	# Create Multiport Simulation
	#Setup Setttings
	sim = fdtd.MultiportSimulation()
	sim.Name = 'Channel '+str(ElemNo)
	sim.SetupSettings.SimulationTime = 50., units.Periods
	options = sim.SetupSettings.GlobalAutoTermination.enum
	sim.SetupSettings.GlobalAutoTermination = options.GlobalAutoTerminationUserDefined
	#sim.SetupSettings.ConvergenceLevel = -50
	# Import all model elements
	entities = model.AllEntities()

	#Sort Entities into type
	ShieldList = []
	PhantomList = []
	PortList = []
	LoopList = []
	PECList = []
	# DCList = []
	LE1List = []
	LE2List = []
	LE3List = []
	LE4List = []

	LEList = []
	WireList = []

	# WireList.append(entities['WireCore'])
	# WireList.append(entities['WireInsulation'])
	# PECList.append(entities['WireCore'])
	
	##### Choose Phantom or Duke
	# PhantomList.append(entities['Phantom'])
	
	for i in range(0,len(entities)):
		tmp = entities[i]
		
		# if ('Phantom' in tmp.Name):
			# PhantomList.append(tmp)
			
		if ('RFS' in tmp.Name) or ('DR' in tmp.Name):
			ShieldList.append(tmp)
			
		if ('SLoop' in tmp.Name):
			LoopList.append(tmp)

		if (tmp.Name == 'LE01'+str(ElemNo)):
			LE1List.append(tmp)
			# PortList.append(tmp)
		if (tmp.Name == 'LE02'+str(ElemNo)):
			LE2List.append(tmp)
			# PortList.append(tmp)
		if (tmp.Name == 'LE03'+str(ElemNo)):
			LE3List.append(tmp)
			# PortList.append(tmp)
		if (tmp.Name == 'LE04'+str(ElemNo)):
			LE4List.append(tmp)
			# PortList.append(tmp)
			
		if (tmp.Name == 'A'+str(ElemNo)):
			PortList.append(tmp)
	
	LEList = LE1List+LE2List+LE3List+LE4List

	# CHANGE LOOP HERE ------------------------------------------------------->
	
	# Materials
	pec_settings = sim.AddMaterialSettings(LoopList+ShieldList)
	pec_settings.Name = 'PEC' 
	options = pec_settings.MaterialType.enum
	pec_settings.MaterialType = options.PEC

	# Phantom
	# dielectric_settings = sim.AddMaterialSettings(PhantomList)
	# dielectric_settings.Name = 'Phantom'
	# dielectric_settings.MaterialType = options.Dielectric
	# dielectric_settings.ElectricProps.Conductivity = 0.4 # S/m
	# dielectric_settings.ElectricProps.RelativePermittivity = 30
	
	# dielectric_settings = sim.AddMaterialSettings(entities['WireInsulation'])
	# dielectric_settings.Name = 'Dielectric'
	# dielectric_settings.MaterialType = options.Dielectric
	# dielectric_settings.ElectricProps.Conductivity = 5e-12 # S/m
	# dielectric_settings.ElectricProps.RelativePermittivity = 2.3

	# Ports
	edge_port_settings = sim.AddEdgePortSettings(PortList)
	edge_port_settings.Name = 'Sources'
	options = edge_port_settings.ExcitationType.enum
	edge_port_settings.ExcitationType = options.Gaussian
	edge_port_settings.CenterFrequency = 64e6, units.Hz
	edge_port_settings.Bandwidth = 100e6, units.Hz
	edge_port_settings.ReferenceLoad = 50, units.Ohms
	

	LE_P_settings = sim.AddLumpedElementSettings(LE1List)
	LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
	LE_P_settings.Resistance = 1500, units.Ohms
	LE_P_settings.Capacitance = 56e-12, units.Farads
	
	LE_P_settings2 = sim.AddLumpedElementSettings(LE2List)
	LE_P_settings2.Type = LE_P_settings2.Type.enum.ResistorParallelCapacitor
	LE_P_settings2.Resistance = 1500, units.Ohms
	LE_P_settings2.Capacitance = 250e-12, units.Farads
	
	LE_P_settings3 = sim.AddLumpedElementSettings(LE4List)
	LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
	LE_P_settings3.Resistance = 1500, units.Ohms
	LE_P_settings3.Capacitance = 250e-12, units.Farads
		
	if ElemNo == 1:		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE3List)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 14.40e-12, units.Farads
	if ElemNo == 2:		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE3List)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 14.40e-12, units.Farads
	if ElemNo == 3:		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE3List)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 14.41e-12, units.Farads
	if ElemNo == 4:		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE3List)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 14.43e-12, units.Farads
	if ElemNo == 5:		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE3List)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 14.37e-12, units.Farads
	if ElemNo == 6:		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE3List)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 14.34e-12, units.Farads
	if ElemNo == 7:		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE3List)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 14.34e-12, units.Farads
	if ElemNo == 8:		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE3List)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 14.38e-12, units.Farads
	

	# Sensors
	edgesensor_settings = sim.AddEdgeSensorSettings(PortList)
	options = edgesensor_settings.AutoTermination.enum
	edgesensor_settings.AutoTermination = options.AutoTerminationUseGlobal

	edgesensor_settings2 = sim.AddEdgeSensorSettings(LEList)
	options = edgesensor_settings2.AutoTermination.enum
	edgesensor_settings2.AutoTermination = options.AutoTerminationUseGlobal

	settings2 = sim.AddFieldSensorSettings(entities['SensBox'])
	settings2.ExtractedFrequencies = 64e6
	
	settings3 = sim.AddFieldSensorSettings(entities['Box'])
	settings3.ExtractedFrequencies = 64e6

	# overall_field_sensor = sim.AddOverallFieldSensorSettings()
	# overall_field_sensor.ExtractedFrequencies = 64e6

	# Boundary Conditions
	global_boundary_settings = fdtd.BoundarySettings()
	global_boundary_settings.BoundaryType = global_boundary_settings.BoundaryType.enum.ABC
	global_boundary_settings.PmlStrength = global_boundary_settings.PmlStrength.enum.Medium

	sim.GlobalGridSettings.PaddingMode = sim.GlobalGridSettings.PaddingMode.enum.Manual
	sim.GlobalGridSettings.BottomPadding = sim.GlobalGridSettings.TopPadding = (100,)*3
	
	# Grid Settings
	manual_grid_settings = sim.AddManualGridSettings(ShieldList)
	manual_grid_settings.MaxStep = (15, )*3 # model units
	manual_grid_settings.Resolution = (3.5, )*3
	
	manual_grid_settings = sim.AddManualGridSettings(LoopList)
	manual_grid_settings.MaxStep = (10, )*3 # model units
	manual_grid_settings.Resolution = (0, )*3
	
	###### Phantom
	# manual_grid_settings = sim.AddManualGridSettings(PhantomList)
	# manual_grid_settings.MaxStep = (8, )*3 # model units
	# manual_grid_settings.Resolution = (3.5, )*3

	manual_grid_settings2 = sim.AddManualGridSettings(PortList)
	manual_grid_settings2.MaxStep = (15, )*3 # model units
	manual_grid_settings2.Resolution = (3.5, )*3

	manual_grid_settings3 = sim.AddAutomaticGridSettings(LEList)
	manual_grid_settings3.MaxStep = (15, )*3 # model units
	manual_grid_settings3.Resolution = (3.5, )*3

	manual_grid_settings5 = sim.AddManualGridSettings(entities['SensBox'])
	manual_grid_settings5.MaxStep = (15, )*3 # model units
	manual_grid_settings5.Resolution = (3.5, )*3
	
	manual_grid_settings6 = sim.AddManualGridSettings(entities['Box'])
	manual_grid_settings6.MaxStep = (15, )*3 # model units
	manual_grid_settings6.Resolution = (3.5, )*3
	
	# Voxels
	default_voxel = next(s for s in sim.AllSettings if isinstance(s, fdtd.AutomaticVoxelerSettings) )
	sim.Add(default_voxel, ShieldList)
	sim.Add(default_voxel, LoopList)
	sim.Add(default_voxel, PhantomList)
	sim.Add(default_voxel, PortList)
	sim.Add(default_voxel, LEList)  # UNCOMMENT IF THERE ARE FIXED LE'S
	sim.Add(default_voxel, entities['SensBox'])
	sim.Add(default_voxel, entities['Box'])
	
	# Solver settings
	sim.SolverSettings.Kernel = sim.SolverSettings.Kernel.enum.Cuda
	
	# Test
	# for cls in [fdtd.FieldSensorSettings, fdtd.AutomaticGridSettings, fdtd.AutomaticVoxelerSettings]:
		# src_comp = sim.Add(cls(), SBox)[0]
		# print "Field sensor component in source simulatiom?", src_comp in sim.AllComponents
	
	# Actually create sim and run
	document.AllSimulations.Add(sim)
	sim.UpdateGrid()
	# sim.CreateVoxels('C:\\Users\\nt16\\Documents\\PERINATAL033-BACKUP\\Hydra Simulations\\Starting Sims - Vernickels\\2017_01_18_8ElementTest_64Ports.smash')
	#sim.RunSimulation()
	
	return sim, settings2
	
def HuygSim():
	sim = fdtd.Simulation()
	sim.Name = 'Huygens Channel '+str(ElemNo)
	sim.SetupSettings.SimulationTime = 30., units.Periods
	options = sim.SetupSettings.GlobalAutoTermination.enum
	sim.SetupSettings.GlobalAutoTermination = options.GlobalAutoTerminationUserDefined
	#sim.SetupSettings.ConvergenceLevel = -50
	# Import all model elements
	entities = model.AllEntities()
	
	PECList = []
	InsulationList = []
	GridList = []; GridList.append(entities['WireCore']); GridList.append(entities['WireInsulation'])
	DielectricList = []
	
	PECList.append(entities['WireCore'])
	InsulationList.append(entities['WireInsulation'])
	# DielectricList.append(entities['Phantom'])	
	
	# Sensors
	local_field_sensor = sim.AddFieldSensorSettings(entities['sBox'])
	local_field_sensor.ExtractedFrequencies = 64e6

	# overall_field_sensor = sim.AddOverallFieldSensorSettings()
	# overall_field_sensor.ExtractedFrequencies = 64e6
	
	# Materials
	pec_settings = sim.AddMaterialSettings(PECList)
	pec_settings.Name = 'PEC' 
	options = pec_settings.MaterialType.enum
	pec_settings.MaterialType = options.PEC

	# Insulation
	dielectric_settings = sim.AddMaterialSettings(InsulationList)
	dielectric_settings.Name = 'Dielectric'
	dielectric_settings.MaterialType = options.Dielectric
	dielectric_settings.ElectricProps.Conductivity = 5e-12 # S/m
	dielectric_settings.ElectricProps.RelativePermittivity = 2.3
	
	# Phantom
	# dielectric_settings2 = sim.AddMaterialSettings(DielectricList)
	# dielectric_settings2.Name = 'Dielectric'
	# dielectric_settings2.MaterialType = options.Dielectric
	# dielectric_settings2.ElectricProps.Conductivity = 0.4 # S/m
	# dielectric_settings2.ElectricProps.RelativePermittivity = 30
	
	#Sources
	huyg_source_settings = sim.AddHuygensSourceSettings(entities['Box'], BigS.AllComponents['SensBox'])
	options = huyg_source_settings.ExcitationType.enum
	huyg_source_settings.ExcitationType = options.Gaussian
	huyg_source_settings.CenterFrequency = 64e6, units.Hz
	huyg_source_settings.Bandwidth = 100e6, units.Hz

	# Sensors
	# overall_field_sensor = sim.AddOverallFieldSensorSettings()
	# overall_field_sensor.ExtractedFrequencies = 64e6
	
	# Boundary Conditions
	global_boundary_settings = fdtd.BoundarySettings()
	global_boundary_settings.BoundaryType = global_boundary_settings.BoundaryType.enum.ABC
	global_boundary_settings.PmlStrength = global_boundary_settings.PmlStrength.enum.Medium

	sim.GlobalGridSettings.PaddingMode = sim.GlobalGridSettings.PaddingMode.enum.Manual
	sim.GlobalGridSettings.BottomPadding = sim.GlobalGridSettings.TopPadding = (100,)*3
	
	# Grid
	manual_grid_settings = sim.AddManualGridSettings(entities['WireInsulation'])
	manual_grid_settings.MaxStep = (1, 1, 5) # model units
	manual_grid_settings.Resolution = (0.09, )*3
	
	manual_grid_settings = sim.AddManualGridSettings(entities['WireCore'])
	manual_grid_settings.MaxStep = (1, 1, 5) # model units
	manual_grid_settings.Resolution = (0.1, )*3
	manual_grid_settings.Priority = 100
	
	manual_grid_settings = sim.AddManualGridSettings(entities['sBox'])
	manual_grid_settings.MaxStep = (15, )*3 # model units
	manual_grid_settings.Resolution = (3, )*3
	
	manual_grid_settings = sim.AddManualGridSettings(entities['Box'])
	manual_grid_settings.MaxStep = (15,)*3 # model units
	manual_grid_settings.Resolution = (3.5, )*3
	
	# manual_grid_settings = sim.AddManualGridSettings(DielectricList)
	# manual_grid_settings.MaxStep = (8, )*3 # model units
	# manual_grid_settings.Resolution = (3.5, )*3
	
	# Voxels
	default_voxel = next(s for s in sim.AllSettings if isinstance(s, fdtd.AutomaticVoxelerSettings) )
	sim.Add(default_voxel, entities['WireInsulation'])
	# sim.Add(default_voxel, entities['Phantom'])
	sim.Add(default_voxel, entities['sBox'])
	sim.Add(default_voxel, entities['Box'])
	
	cores = entities['WireCore']
	wCore_voxel = sim.AddManualVoxelerSettings(cores)
	wCore_voxel.Priority = 100
	
	insuls = entities['WireInsulation']
	wIns_voxel = sim.AddManualVoxelerSettings(insuls)
	wIns_voxel.Priority = 50
	
	document.AllSimulations.Add(sim)
	sim.UpdateGrid()
	
	sim.SolverSettings.Kernel = sim.SolverSettings.Kernel.enum.Cuda
	
	return sim

	

# Create Hardware
CreateHardware()

# Create Rectangular Phantom
rectphant = model.CreateSolidBlock(Vec3(-160,-100,225), Vec3(160,100,-575))
rectphant.Name = 'Phantom'

# Create Smaller Sensor Box
# SBox = model.CreateWireBlock(Vec3(-400,-400,-600), Vec3(400,400,1600), True)
# SBox.Name = 'SensBox'

SBox = model.CreateWireBlock(Vec3(-180,-120,255), Vec3(180,120,-1260), True)
SBox.Name = 'SensBox'

sbox = model.CreateWireBlock(Vec3(-160,-100,225), Vec3(160,100,-1230), True)
sbox.Name = 'Box'

fsbox = sbox.Clone()
fsbox.Name = 'sBox'

# Create Wire and Huygens Box
wire = model.CreateSolidCylinder(Vec3(10,30,-90), Vec3(10,30,-330.5), 0.5, True)
wire.Name = 'WireCore'

housing = model.CreateSolidCylinder(Vec3(10,30,-95.5), Vec3(10,30,-330.6), 0.6, True);
housing.Name = 'WireInsulation'

# Rest of Wires for DUKE sim
wire2 = model.CreateSolidCylinder(Vec3(10,30,-330), Vec3(80.5,30,-330), 0.5, True)
wire2.Name = 'WireCore2'

housing2 = model.CreateSolidCylinder(Vec3(9.4,30,-330), Vec3(80.6,30,-330), 0.6, True);
housing2.Name = 'WireInsulation2'

wire3 = model.CreateSolidCylinder(Vec3(80,30,-330), Vec3(80,30,-490.5), 0.5, True)
wire3.Name = 'WireCore3'

housing3 = model.CreateSolidCylinder(Vec3(80,30,-329.4), Vec3(80,30,-490.6), 0.6, True);
housing3.Name = 'WireInsulation3'

wire4 = model.CreateSolidCylinder(Vec3(80,30,-490), Vec3(80,100.5,-490), 0.5, True)
wire4.Name = 'WireCore4'

housing4 = model.CreateSolidCylinder(Vec3(80,29.4,-490), Vec3(80,101,-490), 0.6, True);
housing4.Name = 'WireInsulation4'

wire5 = model.CreateSolidCylinder(Vec3(80,100,-490), Vec3(80,100,-1190.5), 0.5, True)
wire5.Name = 'WireCore5'

housing5 = model.CreateSolidCylinder(Vec3(80,100,-489.4), Vec3(80,100,-1190.6), 0.6, True);
housing5.Name = 'WireInsulation5'

Wire = model.Unite([wire, wire2, wire3, wire4, wire5])
Wire.Name = 'WireCore'
Housing = model.Unite([housing, housing2, housing3, housing4, housing5])
Housing.Name = 'WireInsulation'



# box = model.CreateWireBlock(Vec3(-130,-40,+150), Vec3(150,100,-1250), True)
# box.Name = 'Box'

# sbox = box.Clone()
# sbox.Name = 'sBox'

# Relocate Wire+Huygens

T1 = model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(-40,20,50))
wire.ApplyTransform(T1);
housing.ApplyTransform(T1);
# box.ApplyTransform(T1);

for ElemNo in range(1,9):
	# Create Simulation
	(BigS, sens) = BigSim()

	# Create Huygens Simulation
	HuygS = HuygSim()