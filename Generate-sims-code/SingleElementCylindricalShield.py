def CreateHardware():
	from s4l_v1 import Vec3
	import s4l_v1.model as model
	import CreateVernickelElement as CVE
	import MakePorts as MP
	from s4l_v1 import Vec3, Transform, Scaling, Translation, Rotation
	import numpy
	
	# Need to reload functions when debugging so changes are actually run
	CVE = reload(CVE)
	MP = reload(MP)
	# Ground Plate Parameters
	gc_radius = 675.0
	gc_length = 1000.0
	gc_depth = 0.0		
	gc_geo = [gc_radius,gc_length,gc_depth]
	
	# Create Ground Plate
	rf_shield = model.CreateSolidTube( Vec3(0,0,-gc_length/2.0),Vec3(0,0,gc_length),gc_radius+gc_depth,gc_radius)
	rf_shield.Name = 'RF Shield'
	# Decoupling Ring Parameters
	dr_gap = 10.0
	dr_length = 20.0
	dr_depth = 0.0		
	dr_pos_from_top = 20.0
	
	dr_geo = [dr_gap,dr_length,dr_depth,dr_pos_from_top]
	
	# Do not shift shield as we'll shift each element
	dist_from_shield = [35,30,35,40,40,35,30,35]
	
	# Make Transmitters
	Nt = 1
	for i in range(0,Nt):
		
		# Make group for each element
		CoilGroup = model.CreateGroup('Element ' + str(i))
		CoilGroup.Name = 'Element ' + str(i)
		
		# Define Transformation of each element
		trans_dist = gc_radius - dist_from_shield[i]
		trans = Translation(Vec3(trans_dist,0,0))
		rot = Rotation(Vec3(0,0,1),0)
		
		# Make Vernickel element of each transmitter 
		coil_geo = CVE.Do(CoilGroup,trans,rot)
		
		# Make Ports
		MP.Do(coil_geo,gc_geo,CoilGroup,trans,rot,trans_dist,dr_geo)
	
	# Create Decoupling Ring
	dr_shield = model.CreateSolidTube(Vec3(0,0,-dr_length/2.0),Vec3(0,0,dr_length/2.0),gc_radius-dr_gap+dr_depth,gc_radius-dr_gap)
	dr_shield.Name = 'Decoupling Ring'
	
	# Transform the ring
	dr_pos = coil_geo[1]/2.0 + dr_pos_from_top
	trans = Translation(Vec3(0,0,dr_pos))
	dr_shield.ApplyTransform(trans)
	
	# Subtract objects to make holes
	cap_gap = coil_geo[5]
	for i in range(0,Nt/2):
		print i
		subtract_block = model.CreateSolidBlock(Vec3(-cap_gap/2.0,-(gc_radius-dr_gap),-gc_length/2.0),Vec3(cap_gap/2.0,gc_radius-dr_gap,gc_length/2.0))
		rot = Rotation(Vec3(0,0,1),(3.1415/180)*(45*i))
		subtract_block.ApplyTransform(rot)
		dr_shield = model.Subtract([dr_shield,subtract_block])
		
		# Now make decoupling caps
	
	
	return