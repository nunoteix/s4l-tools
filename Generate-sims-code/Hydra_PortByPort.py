import s4l_v1.document as document
import s4l_v1.model as model
from s4l_v1 import Vec3
import s4l_v1.simulation.emfdtd as fdtd
import s4l_v1.units as units
import numpy

def CreateHardware(Nt):
	from s4l_v1 import Vec3, Transform, Scaling, Translation, Rotation

	# Ground Plate Parameters
	gc_radius = 675.0/2
	gc_length = 1000.0
	gc_depth = 0.0		
	gc_geo = [gc_radius,gc_length,gc_depth]
	
	# Create Ground Plate
	rf_shield = model.CreateSolidTube( Vec3(0,0,-gc_length/2.0),Vec3(0,0,gc_length),gc_radius+gc_depth,gc_radius)
	rf_shield.Name = 'RFS'
	# Decoupling Ring Parameters
	dr_gap = 10.0
	dr_length = 20.0
	dr_depth = 0.0		
	dr_pos_from_top = 20.0
	
	dr_geo = [dr_gap,dr_length,dr_depth,dr_pos_from_top]
	
	# Do not shift shield as we'll shift each element
	dist_from_shield = [40.0, 40.0,35.0,30.0,35.0,35.0,30.0,35.0]
	# dist_from_shield = [35.0,35.0,35.0,35.0,35.0,35.0,35.0,35.0]
	
	# Make Transmitters
	for i in range(0,Nt):
		
		# Make group for each element
		CoilGroup = model.CreateGroup('E' + str(i))
		CoilGroup.Name = 'E' + str(i)
		
		# Define Transformation of each element
		trans_dist = gc_radius - dist_from_shield[i]
		trans = Translation(Vec3(trans_dist,0,0))
		ang = (numpy.pi/180.0)*(45.0*-i + 5.5*45.0)
		rot = Rotation(Vec3(0,0,1),ang)
		print ang
		# Make Vernickel element of each transmitter 
		coil_geo = CVEDo(CoilGroup,trans,rot)
		
		# Make Ports
		# if i == 0:
		MPDo(coil_geo,gc_geo,CoilGroup,trans,rot,trans_dist,dr_geo,dist_from_shield,Nt)
		
	# if Nt != 1:
		# Create Decoupling Ring
		# dr_shield = model.CreateSolidTube(Vec3(0,0,-dr_length/2.0),Vec3(0,0,dr_length/2.0),gc_radius-dr_gap+dr_depth,gc_radius-dr_gap)
		# dr_shield.Name = 'DR'
		
		# Transform the ring
		# dr_pos = coil_geo[1]/2.0 + dr_pos_from_top
		# trans = Translation(Vec3(0,0,dr_pos))
		# dr_shield.ApplyTransform(trans)
	
		# Subtract objects to make holes
		# cap_gap = coil_geo[5]
		# for i in range(0,Nt):
			# print i
			# subtract_block = model.CreateSolidBlock(Vec3(-cap_gap/2.0,-(gc_radius-2*dr_gap),-gc_length/2.0),Vec3(cap_gap/2.0,gc_radius-dr_gap,gc_length/2.0))
			# rot = model.Rotation(Vec3(0,0,1),(numpy.pi/180.0)*(45.0*i))
			# subtract_block.ApplyTransform(rot)
			# dr_shield = model.Subtract([dr_shield,subtract_block])
			
			# Now make decoupling caps
			# ang = numpy.arcsin((cap_gap/2.0)/(gc_radius-dr_gap))
			# d2r = (numpy.pi/180.0)
			# innerrad = gc_radius-dr_gap
			# dr_cap = model.CreatePolyLine([Vec3(innerrad*numpy.cos(ang),-innerrad*numpy.sin(ang),dr_pos),Vec3(innerrad*numpy.cos(ang),innerrad*numpy.sin(ang),dr_pos)])
			# rot = Rotation(Vec3(0,0,1),d2r*(135.0-i*45.0))
			# dr_cap.ApplyTransform(rot)
			# if i<7:
				# dr_cap.Name = 'ZDC'+str(i)+str(i+1)
			# else:
				# dr_cap.Name = 'ZDC'+str(i)+str(0)
	
	
	return coil_geo

def CVEDo(CoilGroup, trans, rot):
	import s4l_v1.model as model
	from s4l_v1 import Vec3 
	
	# Define dimensions of coil element
	# Meanings defined in ppt in PythonRepo/Models
	width = 100.0
	length = (4.0*5.0) + 20.0 + 35.0 + 70.0 + 20.0 + 110.0 + 20.0 + 70.0 + 35.0 + 20.0
	depth = 0.0
	cond_width = 20.0
	hole_length = 110.0
	cap_gap = 5.0
	cap_dist_middle1 = 55.0
	cap_dist_middle2 = 150.0
	
	geometry = [width,length,depth,cond_width,hole_length,cap_gap,cap_dist_middle1,cap_dist_middle2]
	
	d2 = depth/2.0
	l2 = length/2.0
	w2 = width/2.0
	
	# Create coil element body 
	coil_base =  model.CreateSolidBlock( Vec3(-d2,-w2,-l2),Vec3(d2,w2,l2))
	
	# Create holes to be subtractedr
	coil_hole1 = model.CreateSolidBlock( Vec3(-d2,-w2+cond_width,-l2+cond_width),Vec3(d2,w2-cond_width,-l2+cond_width+hole_length))
	coil_hole2 = model.CreateSolidBlock( Vec3(-d2,-w2+cond_width,-hole_length/2.0),Vec3(d2,w2-cond_width,hole_length/2.0))
	coil_hole3 = model.CreateSolidBlock( Vec3(-d2,-w2+cond_width,l2-cond_width), Vec3(d2,w2-cond_width,l2-cond_width-hole_length))
	
	# Create capacitor blockes to be subtracted
	cap_gap1 = model.CreateSolidBlock( Vec3(-d2,-w2,-cap_dist_middle1),	Vec3(d2,w2,-cap_dist_middle1-cap_gap))
	cap_gap2 = model.CreateSolidBlock( Vec3(-d2,-w2, cap_dist_middle1),	Vec3(d2,w2, cap_dist_middle1+cap_gap))
	cap_gap3 = model.CreateSolidBlock( Vec3(-d2,-w2,-cap_dist_middle2),	Vec3(d2,w2,-cap_dist_middle2-cap_gap))
	cap_gap4 = model.CreateSolidBlock( Vec3(-d2,-w2, cap_dist_middle2),	Vec3(d2,w2, cap_dist_middle2+cap_gap))
	
	# Subtract holes from main coil body
	coil_base = model.Subtract([coil_base,coil_hole1,coil_hole2,coil_hole3])
	coil_base = model.Subtract([coil_base,cap_gap1,cap_gap2,cap_gap3,cap_gap4])
	coil_base.Name = CoilGroup.Name+'Coil'
	
	coil_base.ApplyTransform(trans)
	coil_base.ApplyTransform(rot)
	
	CoilGroup.Add(coil_base)
	
	return geometry

def MPDo(coil_geo,gc_geo,CoilGroup, trans,rot,trans_dist,dr_geo,dist_from_shield,Nt):
	
	import s4l_v1.model as model
	from s4l_v1 import Vec3
	import numpy as np
	
	# Element Geometry
	width = coil_geo[0]
	length = coil_geo[1]
	depth = coil_geo[2]
	cond_width = coil_geo[3]
	hole_length = coil_geo[4]
	cap_gap = coil_geo[5]
	cap_dist_middle1 = coil_geo[6]
	cap_dist_middle2 = coil_geo[7]
	
	# RF Shield Geometry
	gc_radius = gc_geo[0]
	gc_length = gc_geo[1]
	gc_depth = gc_geo[2]
	
	# Decoupling Ring
	dr_gap = dr_geo[0]
	dr_length = dr_geo[1]
	dr_depth = dr_geo[2]		
	dr_pos_from_top = dr_geo[3]
	# Variables for convenience
	d2 = depth/2.0
	l2 = length/2.0
	w2 = width/2.0
	
	D_act = d2+trans_dist
	D = d2+(gc_radius-dist_from_shield[0])
	r1 = np.sqrt(np.power(D,2.0)+np.power(w2,2.0))
	theta = np.arctan(w2/D)
	# Create port1 - driving port
	port = model.CreatePolyLine([Vec3(D_act+(gc_radius-D_act)/2,0,l2), Vec3(gc_radius,0,l2)])
	# Cap right next to driving port
	port0 = model.CreatePolyLine([Vec3(D_act,0,l2), Vec3(D_act+(gc_radius-D_act)/2,0,l2)])
	# Caps on corner to ground plate
	port1 = model.CreatePolyLine([Vec3(D_act, w2,-l2), Vec3(gc_radius*np.cos(theta), gc_radius*np.sin(theta),-l2)])
	port2 = model.CreatePolyLine([Vec3(D_act,-w2,-l2), Vec3(gc_radius*np.cos(theta),-gc_radius*np.sin(theta),-l2)])
	port3 = model.CreatePolyLine([Vec3(D_act, w2, l2), Vec3(gc_radius*np.cos(theta), gc_radius*np.sin(theta), l2)])
	port4 = model.CreatePolyLine([Vec3(D_act,-w2, l2), Vec3(gc_radius*np.cos(theta),-gc_radius*np.sin(theta), l2)])
	
	# Element Caps
	port5 = model.CreatePolyLine([Vec3(0,-w2, -cap_dist_middle1),  Vec3(0,-w2,-cap_dist_middle1-cap_gap)])
	port6 = model.CreatePolyLine([Vec3(0,-w2,  cap_dist_middle1),  Vec3(0,-w2, cap_dist_middle1+cap_gap)])
	port7 = model.CreatePolyLine([Vec3(0,-w2, -cap_dist_middle2),  Vec3(0,-w2,-cap_dist_middle2-cap_gap)])
	port8 = model.CreatePolyLine([Vec3(0,-w2,  cap_dist_middle2),  Vec3(0,-w2, cap_dist_middle2+cap_gap)])
	port9 = model.CreatePolyLine([Vec3(0, w2, -cap_dist_middle1), Vec3(0, w2,-cap_dist_middle1-cap_gap)])
	port10 = model.CreatePolyLine([Vec3(0, w2, cap_dist_middle1), Vec3(0, w2, cap_dist_middle1+cap_gap)])
	port11 = model.CreatePolyLine([Vec3(0, w2,-cap_dist_middle2), Vec3(0, w2,-cap_dist_middle2-cap_gap)])
	port12 = model.CreatePolyLine([Vec3(0, w2, cap_dist_middle2), Vec3(0, w2, cap_dist_middle2+cap_gap)])
	
	# Name all the ports
	port.Name = 'A00'+CoilGroup.Name	# Source
	
	port0.Name = 'LE01'+CoilGroup.Name	# Cap right after driving port
	port1.Name = 'LE02'+CoilGroup.Name		# Ports  2-5 are corner to groundplate caps
	port2.Name = 'LE03'+CoilGroup.Name
	port3.Name = 'LE04'+CoilGroup.Name
	port4.Name = 'LE05'+CoilGroup.Name
	# port0.Name = 'P01'+CoilGroup.Name	# or select this if you want this element to be part of the circ. co-sim.
	# port1.Name = 'P02'+CoilGroup.Name		
	# port2.Name = 'P03'+CoilGroup.Name
	# port3.Name = 'P04'+CoilGroup.Name
	# port4.Name = 'P05'+CoilGroup.Name
	
	
	port5.Name = 'LE06'+CoilGroup.Name		# Ports 6-13 are intra-element caps
	port6.Name = 'LE07'+CoilGroup.Name
	port7.Name = 'LE08'+CoilGroup.Name
	port8.Name = 'LE09'+CoilGroup.Name
	port9.Name = 'LE10'+CoilGroup.Name
	port10.Name = 'LE11'+CoilGroup.Name
	port11.Name = 'LE12'+CoilGroup.Name
	port12.Name = 'LE13'+CoilGroup.Name
	# port5.Name = 'P06'+CoilGroup.Name		# or select this if you want this element to be part of the circ. co-sim.
	# port6.Name = 'P07'+CoilGroup.Name
	# port7.Name = 'P08'+CoilGroup.Name
	# port8.Name = 'P09'+CoilGroup.Name
	# port9.Name = 'P10'+CoilGroup.Name
	# port10.Name = 'P11'+CoilGroup.Name
	# port11.Name = 'P12'+CoilGroup.Name
	# port12.Name = 'P13'+CoilGroup.Name

	
	# Translate some ports
	port5.ApplyTransform(trans)
	port6.ApplyTransform(trans)
	port7.ApplyTransform(trans)
	port8.ApplyTransform(trans)
	port9.ApplyTransform(trans)
	port10.ApplyTransform(trans)
	port11.ApplyTransform(trans)
	port12.ApplyTransform(trans)
	
	
	
	# Rotate all ports
	port.ApplyTransform(rot)
	port0.ApplyTransform(rot)
	port1.ApplyTransform(rot)
	port2.ApplyTransform(rot)
	port3.ApplyTransform(rot)
	port4.ApplyTransform(rot)
	port5.ApplyTransform(rot)
	port6.ApplyTransform(rot)
	port7.ApplyTransform(rot)
	port8.ApplyTransform(rot)
	port9.ApplyTransform(rot)
	port10.ApplyTransform(rot)
	port11.ApplyTransform(rot)
	port12.ApplyTransform(rot)
	
	
	#
	CoilGroup.Add(port)
	CoilGroup.Add(port0)
	CoilGroup.Add(port1)
	CoilGroup.Add(port2)
	CoilGroup.Add(port3)
	CoilGroup.Add(port4)
	CoilGroup.Add(port5)
	CoilGroup.Add(port6)
	CoilGroup.Add(port7)
	CoilGroup.Add(port8)
	CoilGroup.Add(port9)
	CoilGroup.Add(port10)
	CoilGroup.Add(port11)
	CoilGroup.Add(port12)
	
	return

document.New()

# Build the hardware 
Nt = 8
coil_geo = CreateHardware(Nt)

# model.CreateSolidBlock(Vec3(-250, 95, -550), Vec3(250, 300, 550))
# Create Rectangular Phantom
# rectphant = model.CreateSolidBlock(Vec3(-130,-100,-225), Vec3(130,100,225))
# rectphant.Name = 'Phantom'

# Create Smaller Sensor Box
SBox = model.CreateWireBlock(Vec3(-130,-108,700), Vec3(90,52,-1595), True)
SBox.Name = 'SensBox'

# Create Wire and Huygens Box
wire = model.CreateSolidCylinder(Vec3(0,0,0), Vec3(0,0,-1489), 0.35, True)
wire.Name = 'WireCore'

housing = model.CreateSolidCylinder(Vec3(0,0,-5.5), Vec3(0,0,-1490), 0.4, True);
housing.Name = 'WireInsulation'

box = model.CreateWireBlock(Vec3(-90,-68,600), Vec3(50,12,-1495), True)
box.Name = 'Box'

sbox = box.Clone()
sbox.Name = 'sBox'

T1 = model.Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(-20,-12-18.5,105))
wire.ApplyTransform(T1);
housing.ApplyTransform(T1);
#### Looped Wire

# line_wire = model.CreateSpline([Vec3(10-95,0,50), Vec3(0-95,0,0), Vec3(50-95,0,-100), Vec3(150-95,0,-100), Vec3(200-95,0,-25), Vec3(150-95,0,50)])
# line_wire2 = model.CreateSpline([Vec3(10-95,0,49.75), Vec3(25-95,0,150), Vec3(0-95,0,1650)])
# line_insu = line_wire.Clone()
# line_insu2 = line_wire2.Clone()

# wire = model.Unite([line_wire, line_wire2])
# wire.Name = 'WireCoreLine'
# insu = model.Unite([line_insu, line_insu2])
# insu.Name = 'WireInsulationLine'

def BigSim():
	# Create Multiport Simulation
	#Setup Setttings
	sim = fdtd.MultiportSimulation()
	sim.Name = 'Element #'+str(ElemNo+1) #+' '+str(n) #<---- 				CHANGE ELEMENT NUMBER HERE
	sim.SetupSettings.SimulationTime = 50., units.Periods
	options = sim.SetupSettings.GlobalAutoTermination.enum
	sim.SetupSettings.GlobalAutoTermination = options.GlobalAutoTerminationUserDefined
	#sim.SetupSettings.ConvergenceLevel = -50
	# Import all model elements
	entities = model.AllEntities()

	#Sort Entities into type
	PECList = []
	DielectricList = []
	PortList = []
	DCList = []
	LE1List = []
	LE_backcornerList = []
	LE_frontcornerList = []
	LE_midList = []
	LEList = []

	# HBox = entities['HuygBox']

	for i in range(0,len(entities)):
		tmp = entities[i]
		
		if ('Phantom' in tmp.Name):
			DielectricList.append(tmp)
			
		if ('RFS' in tmp.Name) or ('DR' in tmp.Name):
			PECList.append(tmp)
			
		if ('Coil' in tmp.Name):
			PECList.append(tmp)
			
		if ('E'+str(ElemNo) in tmp.Name):
			if (('L' in tmp.Name[0]) and ('E' in tmp.Name[4]) and (str(ElemNo) in tmp.Name[5])):
				if (('0' in tmp.Name[2]) and ('1' in tmp.Name[3])):
					LE1List.append(tmp)
				elif (('0' in tmp.Name[2]) and (('2' in tmp.Name[3]) or ('3' in tmp.Name[3]))):
					LE_backcornerList.append(tmp)
				elif (('4' in tmp.Name[3]) or ('5' in tmp.Name[3])):
					LE_frontcornerList.append(tmp)
				else:
					LE_midList.append(tmp)
					
				LEList.append(tmp)
			if ((len(tmp.Name)>3) and ('A' in tmp.Name[0]) and ('E' in tmp.Name[3])):
				PortList.append(tmp)
			


	# Materials
	pec_settings = sim.AddMaterialSettings(PECList)
	pec_settings.Name = 'PEC' 
	options = pec_settings.MaterialType.enum
	pec_settings.MaterialType = options.PEC

	# Phantom
	dielectric_settings = sim.AddMaterialSettings(DielectricList)
	dielectric_settings.Name = 'Dielectric'
	dielectric_settings.MaterialType = options.Dielectric
	dielectric_settings.ElectricProps.Conductivity = 0.4 #0.35 + sigma*0.05 # S/m
	dielectric_settings.ElectricProps.RelativePermittivity = 30 #+ perm*20

	# Ports
	edge_port_settings = sim.AddEdgePortSettings(PortList)
	edge_port_settings.Name = 'Sources'
	options = edge_port_settings.ExcitationType.enum
	edge_port_settings.ExcitationType = options.Gaussian
	edge_port_settings.CenterFrequency = 128e6, units.Hz
	edge_port_settings.Bandwidth = 100e6, units.Hz
	edge_port_settings.ReferenceLoad = 50, units.Ohms
	
	if ElemNo == 0:
	
		LE_P_settings = sim.AddLumpedElementSettings(LE1List)
		LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
		LE_P_settings.Resistance = 1500, units.Ohms
		LE_P_settings.Capacitance = 40.06, units.PicoFarads
		
		LE_P_settings2 = sim.AddLumpedElementSettings(LE_frontcornerList)
		LE_P_settings2.Type = LE_P_settings2.Type.enum.ResistorParallelCapacitor
		LE_P_settings2.Resistance = 1500, units.Ohms
		LE_P_settings2.Capacitance = 23.48, units.PicoFarads
		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE_backcornerList)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 50.00, units.PicoFarads
		
		LE_P_settings4 = sim.AddLumpedElementSettings(LE_midList)
		LE_P_settings4.Type = LE_P_settings4.Type.enum.ResistorParallelCapacitor
		LE_P_settings4.Resistance = 1500, units.Ohms
		LE_P_settings4.Capacitance = 29.10, units.PicoFarads
		
	if ElemNo == 1:
	
		LE_P_settings = sim.AddLumpedElementSettings(LE1List)
		LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
		LE_P_settings.Resistance = 1500, units.Ohms
		LE_P_settings.Capacitance = 40.05, units.PicoFarads
		
		LE_P_settings2 = sim.AddLumpedElementSettings(LE_frontcornerList)
		LE_P_settings2.Type = LE_P_settings2.Type.enum.ResistorParallelCapacitor
		LE_P_settings2.Resistance = 1500, units.Ohms
		LE_P_settings2.Capacitance = 23.38, units.PicoFarads
		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE_backcornerList)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 50.00, units.PicoFarads
		
		LE_P_settings4 = sim.AddLumpedElementSettings(LE_midList)
		LE_P_settings4.Type = LE_P_settings4.Type.enum.ResistorParallelCapacitor
		LE_P_settings4.Resistance = 1500, units.Ohms
		LE_P_settings4.Capacitance = 29.00, units.PicoFarads

	if ElemNo == 2:
	
		LE_P_settings = sim.AddLumpedElementSettings(LE1List)
		LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
		LE_P_settings.Resistance = 1500, units.Ohms
		LE_P_settings.Capacitance = 44.50, units.PicoFarads
		
		LE_P_settings2 = sim.AddLumpedElementSettings(LE_frontcornerList)
		LE_P_settings2.Type = LE_P_settings2.Type.enum.ResistorParallelCapacitor
		LE_P_settings2.Resistance = 1500, units.Ohms
		LE_P_settings2.Capacitance = 26.40, units.PicoFarads
		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE_backcornerList)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 50.00, units.PicoFarads
		
		LE_P_settings4 = sim.AddLumpedElementSettings(LE_midList)
		LE_P_settings4.Type = LE_P_settings4.Type.enum.ResistorParallelCapacitor
		LE_P_settings4.Resistance = 1500, units.Ohms
		LE_P_settings4.Capacitance = 31.60, units.PicoFarads
		
	if ElemNo == 3:
	
		LE_P_settings = sim.AddLumpedElementSettings(LE1List)
		LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
		LE_P_settings.Resistance = 1500, units.Ohms
		LE_P_settings.Capacitance = 51.52, units.PicoFarads
		
		LE_P_settings2 = sim.AddLumpedElementSettings(LE_frontcornerList)
		LE_P_settings2.Type = LE_P_settings2.Type.enum.ResistorParallelCapacitor
		LE_P_settings2.Resistance = 1500, units.Ohms
		LE_P_settings2.Capacitance = 30.51, units.PicoFarads
		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE_backcornerList)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 50.00, units.PicoFarads
		
		LE_P_settings4 = sim.AddLumpedElementSettings(LE_midList)
		LE_P_settings4.Type = LE_P_settings4.Type.enum.ResistorParallelCapacitor
		LE_P_settings4.Resistance = 1500, units.Ohms
		LE_P_settings4.Capacitance = 35.60, units.PicoFarads

	if ElemNo == 4:
	
		LE_P_settings = sim.AddLumpedElementSettings(LE1List)
		LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
		LE_P_settings.Resistance = 1500, units.Ohms
		LE_P_settings.Capacitance = 44.21, units.PicoFarads
		
		LE_P_settings2 = sim.AddLumpedElementSettings(LE_frontcornerList)
		LE_P_settings2.Type = LE_P_settings2.Type.enum.ResistorParallelCapacitor
		LE_P_settings2.Resistance = 1500, units.Ohms
		LE_P_settings2.Capacitance = 26.61, units.PicoFarads
		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE_backcornerList)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 50.00, units.PicoFarads
		
		LE_P_settings4 = sim.AddLumpedElementSettings(LE_midList)
		LE_P_settings4.Type = LE_P_settings4.Type.enum.ResistorParallelCapacitor
		LE_P_settings4.Resistance = 1500, units.Ohms
		LE_P_settings4.Capacitance = 31.70, units.PicoFarads

	if ElemNo == 5:
	
		LE_P_settings = sim.AddLumpedElementSettings(LE1List)
		LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
		LE_P_settings.Resistance = 1500, units.Ohms
		LE_P_settings.Capacitance = 44.26, units.PicoFarads
		
		LE_P_settings2 = sim.AddLumpedElementSettings(LE_frontcornerList)
		LE_P_settings2.Type = LE_P_settings2.Type.enum.ResistorParallelCapacitor
		LE_P_settings2.Resistance = 1500, units.Ohms
		LE_P_settings2.Capacitance = 26.38, units.PicoFarads
		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE_backcornerList)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 50.00, units.PicoFarads
		
		LE_P_settings4 = sim.AddLumpedElementSettings(LE_midList)
		LE_P_settings4.Type = LE_P_settings4.Type.enum.ResistorParallelCapacitor
		LE_P_settings4.Resistance = 1500, units.Ohms
		LE_P_settings4.Capacitance = 31.80, units.PicoFarads		

	if ElemNo == 6:
	
		LE_P_settings = sim.AddLumpedElementSettings(LE1List)
		LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
		LE_P_settings.Resistance = 1500, units.Ohms
		LE_P_settings.Capacitance = 52.48, units.PicoFarads
		
		LE_P_settings2 = sim.AddLumpedElementSettings(LE_frontcornerList)
		LE_P_settings2.Type = LE_P_settings2.Type.enum.ResistorParallelCapacitor
		LE_P_settings2.Resistance = 1500, units.Ohms
		LE_P_settings2.Capacitance = 30.29, units.PicoFarads
		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE_backcornerList)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 50.00, units.PicoFarads
		
		LE_P_settings4 = sim.AddLumpedElementSettings(LE_midList)
		LE_P_settings4.Type = LE_P_settings4.Type.enum.ResistorParallelCapacitor
		LE_P_settings4.Resistance = 1500, units.Ohms
		LE_P_settings4.Capacitance = 35.60, units.PicoFarads			

	if ElemNo == 7:
	
		LE_P_settings = sim.AddLumpedElementSettings(LE1List)
		LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
		LE_P_settings.Resistance = 1500, units.Ohms
		LE_P_settings.Capacitance = 44.28, units.PicoFarads
		
		LE_P_settings2 = sim.AddLumpedElementSettings(LE_frontcornerList)
		LE_P_settings2.Type = LE_P_settings2.Type.enum.ResistorParallelCapacitor
		LE_P_settings2.Resistance = 1500, units.Ohms
		LE_P_settings2.Capacitance = 26.42, units.PicoFarads
		
		LE_P_settings3 = sim.AddLumpedElementSettings(LE_backcornerList)
		LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
		LE_P_settings3.Resistance = 1500, units.Ohms
		LE_P_settings3.Capacitance = 50.00, units.PicoFarads
		
		LE_P_settings4 = sim.AddLumpedElementSettings(LE_midList)
		LE_P_settings4.Type = LE_P_settings4.Type.enum.ResistorParallelCapacitor
		LE_P_settings4.Resistance = 1500, units.Ohms
		LE_P_settings4.Capacitance = 31.70, units.PicoFarads

		
	# Sensors
	edgesensor_settings = sim.AddEdgeSensorSettings(PortList)
	options = edgesensor_settings.AutoTermination.enum
	edgesensor_settings.AutoTermination = options.AutoTerminationUseGlobal

	edgesensor_settings2 = sim.AddEdgeSensorSettings(LEList)
	options = edgesensor_settings2.AutoTermination.enum
	edgesensor_settings2.AutoTermination = options.AutoTerminationUseGlobal

	settings2 = sim.AddFieldSensorSettings(SBox)
	settings2.ExtractedFrequencies = 128e6

	# overall_field_sensor = sim.AddOverallFieldSensorSettings()
	# overall_field_sensor.ExtractedFrequencies = 128e6

	# Boundary Conditions
	global_boundary_settings = fdtd.BoundarySettings()
	global_boundary_settings.BoundaryType = global_boundary_settings.BoundaryType.enum.ABC
	global_boundary_settings.PmlStrength = global_boundary_settings.PmlStrength.enum.Medium

	sim.GlobalGridSettings.PaddingMode = sim.GlobalGridSettings.PaddingMode.enum.Manual
	sim.GlobalGridSettings.BottomPadding = sim.GlobalGridSettings.TopPadding = (100,)*3
	
	# Grid Settings
	manual_grid_settings = sim.AddManualGridSettings(PECList)
	manual_grid_settings.MaxStep = (15, )*3 # model units
	manual_grid_settings.Resolution = (3.5, )*3
	
	manual_grid_settings = sim.AddManualGridSettings(DielectricList)
	manual_grid_settings.MaxStep = (3.5, 3.5, 10) # model units
	manual_grid_settings.Resolution = (3.5, )*3

	manual_grid_settings3 = sim.AddManualGridSettings(PortList)
	manual_grid_settings3.MaxStep = (4, )*3 # model units
	manual_grid_settings3.Resolution = (3.5, )*3

	manual_grid_settings3 = sim.AddAutomaticGridSettings(LEList)
	manual_grid_settings3.MaxStep = (4, )*3 # model units
	manual_grid_settings3.Resolution = (3.5, )*3

	aut_grid_settings5 = sim.AddAutomaticGridSettings(SBox)

	# Voxels
	default_voxel = next(s for s in sim.AllSettings if isinstance(s, fdtd.AutomaticVoxelerSettings) )
	sim.Add(default_voxel, PECList)
	sim.Add(default_voxel, DielectricList)
	sim.Add(default_voxel, PortList)
	sim.Add(default_voxel, LEList)  # UNCOMMENT IF THERE ARE FIXED LE'S
	sim.AddAutomaticVoxelerSettings(SBox)

	# Solver settings
	sim.SolverSettings.Kernel = sim.SolverSettings.Kernel.enum.Cuda
	
	# Test
	# for cls in [fdtd.FieldSensorSettings, fdtd.AutomaticGridSettings, fdtd.AutomaticVoxelerSettings]:
		# src_comp = sim.Add(cls(), box)[0]
		# print "Field sensor component in source simulatiom?", src_comp in sim.AllComponents
	
	# Actually create sim and run
	document.AllSimulations.Add(sim)
	sim.UpdateGrid()
	# sim.CreateVoxels('C:\\Users\\nt16\\Documents\\PERINATAL033-BACKUP\\Hydra Simulations\\Starting Sims - Vernickels\\2017_01_18_8ElementTest_64Ports.smash')
	#sim.RunSimulation()
	
	return sim

def HuygSim():
	sim = fdtd.Simulation()
	sim.Name = 'Huygens Element '+str(ElemNo+1)
	sim.SetupSettings.SimulationTime = 40., units.Periods
	options = sim.SetupSettings.GlobalAutoTermination.enum
	sim.SetupSettings.GlobalAutoTermination = options.GlobalAutoTerminationUserDefined
	#sim.SetupSettings.ConvergenceLevel = -50
	# Import all model elements
	entities = model.AllEntities()
	
	PECList = []
	InsulationList = []
	GridList = []; GridList.append(entities['WireCore']); GridList.append(entities['WireInsulation'])
	DielectricList = []
	
	PECList.append(entities['WireCore'])
	InsulationList.append(entities['WireInsulation'])
	DielectricList.append(entities['Phantom'])	
	
	# Sensors
	local_field_sensor = sim.AddFieldSensorSettings(sbox)
	local_field_sensor.ExtractedFrequencies = 128e6

	# overall_field_sensor = sim.AddOverallFieldSensorSettings()
	# overall_field_sensor.ExtractedFrequencies = 128e6
	
	# Materials
	pec_settings = sim.AddMaterialSettings(PECList)
	pec_settings.Name = 'PEC' 
	options = pec_settings.MaterialType.enum
	pec_settings.MaterialType = options.PEC

	# Insulation
	dielectric_settings = sim.AddMaterialSettings(InsulationList)
	dielectric_settings.Name = 'Dielectric'
	dielectric_settings.MaterialType = options.Dielectric
	dielectric_settings.ElectricProps.Conductivity = 5e-12 # S/m
	dielectric_settings.ElectricProps.RelativePermittivity = 2.3
	
	# Phantom
	dielectric_settings2 = sim.AddMaterialSettings(DielectricList)
	dielectric_settings2.Name = 'Dielectric'
	dielectric_settings2.MaterialType = options.Dielectric
	dielectric_settings2.ElectricProps.Conductivity = 0.4 # S/m
	dielectric_settings2.ElectricProps.RelativePermittivity = 30
	
	#Sources
	huyg_source_settings = sim.AddHuygensSourceSettings(box, BigS.AllComponents['SensBox'])
	options = huyg_source_settings.ExcitationType.enum
	huyg_source_settings.ExcitationType = options.Gaussian
	huyg_source_settings.CenterFrequency = 128e6, units.Hz
	huyg_source_settings.Bandwidth = 100e6, units.Hz

	# Sensors
	# overall_field_sensor = sim.AddOverallFieldSensorSettings()
	# overall_field_sensor.ExtractedFrequencies = 128e6
	
	# Grid
	manual_grid_settings = sim.AddManualGridSettings(entities['WireInsulation'])
	manual_grid_settings.MaxStep = (1, 1, 2.5) # model units
	manual_grid_settings.Resolution = (0.04, )*3
	
	manual_grid_settings = sim.AddManualGridSettings(entities['WireCore'])
	manual_grid_settings.MaxStep = (1, 1, 2.5) # model units
	manual_grid_settings.Resolution = (0.05, )*3
	manual_grid_settings.Priority = 100
	
	# manual_grid_settings = sim.AddManualGridSettings(entities['Box'])
	# manual_grid_settings.MaxStep = (15, )*3 # model units
	# manual_grid_settings.Resolution = (3, )*3
	
	manual_grid_settings = sim.AddManualGridSettings(entities['Box'])
	manual_grid_settings.MaxStep = (15,)*3 # model units
	manual_grid_settings.Resolution = (3.5, )*3
	
	manual_grid_settings = sim.AddManualGridSettings(DielectricList)
	manual_grid_settings.MaxStep = (8, )*3 # model units
	manual_grid_settings.Resolution = (3.5, )*3
	
	# Voxels
	default_voxel = next(s for s in sim.AllSettings if isinstance(s, fdtd.AutomaticVoxelerSettings) )
	sim.Add(default_voxel, entities['WireInsulation'])
	sim.Add(default_voxel, entities['Phantom'])
	sim.Add(default_voxel, entities['Box'])
	sim.Add(default_voxel, entities['sBox'])
	
	wCore_voxel = sim.AddManualVoxelerSettings(entities['WireCore'])
	wCore_voxel.Priority = 100
	
	document.AllSimulations.Add(sim)
	sim.UpdateGrid()
	
	sim.SolverSettings.Kernel = sim.SolverSettings.Kernel.enum.Cuda
	
	return sim


for ElemNo in range(0,8):
	# n = 1
	# for sigma in range(0,3):
	# ElemNo = 6
		# for perm in range(0,4):
		# Create Simulation
			BigS = BigSim()
			# n += 1;

	# Create Huygens Simulation
			HuygS = HuygSim()
	
import s4l_v1 as s4l
print "Start"

sims =  s4l.simulation.GetSimulations() # extract all simulations

# Loop over all simulations	
for s in sims:	
   # Print name of the simulation
   print 'Creating voxels for simulation:'
   print s.Name
   s.CreateVoxels()
   
   