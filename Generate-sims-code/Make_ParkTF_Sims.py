# -*- coding: utf-8 -*-
# This script was auto-generated by Sim4Life version 3.4.2.2426

import numpy
import s4l_v1.document as document
import s4l_v1.materials.database as database
import s4l_v1.model as model
import s4l_v1.simulation.emfdtd as emfdtd
import s4l_v1.units as units
from s4l_v1 import ReleaseVersion
from s4l_v1 import Unit

for ii in range(2, 44):
	# Define the version to use for default values
	ReleaseVersion.set_active(ReleaseVersion.version3_4)
	
	# Creating the simulation
	simulation = emfdtd.Simulation()
	simulation.Name = "800Park Uncapped "+str(ii)

	# Mapping the components and entities
	component__plane_x = simulation.AllComponents["Plane X+"]
	component__plane_x_1 = simulation.AllComponents["Plane X-"]
	component__background = simulation.AllComponents["Background"]
	component__plane_y = simulation.AllComponents["Plane Y+"]
	component__plane_y_1 = simulation.AllComponents["Plane Y-"]
	component__plane_z = simulation.AllComponents["Plane Z+"]
	component__plane_z_1 = simulation.AllComponents["Plane Z-"]
	component_missing = simulation.AllComponents["missing"]
	entity_800_fes = model.AllEntities()["800FES"]
	entity_pwe1 = model.AllEntities()["PWE "+str(ii)]
	entity__insu_fes = model.AllEntities()["InsuFES"]
	entity_ipwe1 = model.AllEntities()["IPWE "+str(ii)]
	entity__field_sensor = model.AllEntities()["Field Sensor"]
	entity__source_fes = model.AllEntities()["SourceFES"]
	entity__dielectric = model.AllEntities()["Dielectric"]

	# Editing SetupSettings "Setup"
	setup_settings = simulation.SetupSettings
	setup_settings.SimulationTime = 5.0, units.Periods
	setup_settings.GlobalAutoTermination = setup_settings.GlobalAutoTermination.enum.GlobalAutoTerminationUserDefined

	# Adding a new MaterialSettings
	material_settings = emfdtd.MaterialSettings()
	components = [entity__insu_fes]
	material_settings.Name = "Insulation"
	material_settings.ElectricProps.RelativePermittivity = 3.0
	simulation.Add(material_settings, components)

	# Adding a new MaterialSettings
	material_settings = emfdtd.MaterialSettings()
	components = [entity_800_fes]
	material_settings.Name = "PEC"
	material_settings.MaterialType = material_settings.MaterialType.enum.PEC
	simulation.Add(material_settings, components)

	# Adding a new MaterialSettings
	material_settings = emfdtd.MaterialSettings()
	components = [entity__dielectric]
	material_settings.ElectricProps.Conductivity = 0.46, Unit("S/m")
	material_settings.ElectricProps.RelativePermittivity = 77.0
	simulation.Add(material_settings, components)

	# Adding a new PlaneWaveSourceSettings
	plane_wave_source_settings = emfdtd.PlaneWaveSourceSettings()
	components = [entity_pwe1]
	plane_wave_source_settings.Theta = 90.0, units.Degrees
	plane_wave_source_settings.Phi = 90.0, units.Degrees
	plane_wave_source_settings.Psi = 180.0, units.Degrees
	plane_wave_source_settings.CenterFrequency = 64000000.0, units.Hz
	plane_wave_source_settings.Amplitude = 0.5, units.Volts
	simulation.Add(plane_wave_source_settings, components)

	# Adding a new PlaneWaveSourceSettings
	plane_wave_source_settings = emfdtd.PlaneWaveSourceSettings()
	components = [entity_ipwe1]
	plane_wave_source_settings.Name = "Plane Wave Settings 1"
	plane_wave_source_settings.Theta = 90.0, units.Degrees
	plane_wave_source_settings.Phi = 270.0, units.Degrees
	plane_wave_source_settings.Psi = 180.0, units.Degrees
	plane_wave_source_settings.CenterFrequency = 64000000.0, units.Hz
	plane_wave_source_settings.Amplitude = 0.5, units.Volts
	simulation.Add(plane_wave_source_settings, components)

	# Adding a new EdgeSensorSettings
	edge_sensor_settings = emfdtd.EdgeSensorSettings()
	components = [entity__source_fes]
	edge_sensor_settings.Name = "Edge Sensor Settings 1"
	simulation.Add(edge_sensor_settings, components)

	# Editing FieldSensorSettings "Field Sensor Settings"
	field_sensor_settings = [x for x in simulation.AllSettings if isinstance(x, emfdtd.FieldSensorSettings) and x.Name == "Field Sensor Settings"][0]
	components = [entity__field_sensor]
	simulation.Add(field_sensor_settings, components)

	# Editing GlobalGridSettings "Grid (Empty)"
	global_grid_settings = simulation.GlobalGridSettings
	global_grid_settings.PaddingMode = global_grid_settings.PaddingMode.enum.Manual
	global_grid_settings.BottomPadding = numpy.array([200.0, 200.0, 200.0]), units.MilliMeters
	global_grid_settings.TopPadding = numpy.array([200.0, 200.0, 200.0]), units.MilliMeters

	# Editing AutomaticGridSettings "Automatic"
	automatic_grid_settings = [x for x in simulation.AllSettings if isinstance(x, emfdtd.AutomaticGridSettings) and x.Name == "Automatic"][0]
	components = [entity__field_sensor, entity_ipwe1, entity_pwe1, entity__dielectric]
	simulation.Add(automatic_grid_settings, components)

	# Adding a new ManualGridSettings
	manual_grid_settings = simulation.AddManualGridSettings([entity_800_fes, entity__insu_fes])
	manual_grid_settings.MaxStep = numpy.array([0.25, 0.25, 1.5]), units.MilliMeters
	manual_grid_settings.Resolution = numpy.array([0.0, 0.0, 0.0]), units.MilliMeters

	# Editing AutomaticVoxelerSettings "Automatic Voxeler Settings"
	automatic_voxeler_settings = [x for x in simulation.AllSettings if isinstance(x, emfdtd.AutomaticVoxelerSettings) and x.Name == "Automatic Voxeler Settings"][0]
	components = [entity__field_sensor, entity__insu_fes, entity_ipwe1, entity_pwe1, entity__dielectric]
	simulation.Add(automatic_voxeler_settings, components)

	# Adding a new ManualVoxelerSettings
	manual_voxeler_settings = emfdtd.ManualVoxelerSettings()
	components = [entity_800_fes]
	manual_voxeler_settings.Priority = 1
	simulation.Add(manual_voxeler_settings, components)

	# Editing SolverSettings "Solver"
	solver_settings = simulation.SolverSettings
	solver_settings.Kernel = solver_settings.Kernel.enum.Cuda

	# Update the materials with the new frequency parameters
	simulation.UpdateAllMaterials()

	# Update the grid with the new parameters
	simulation.UpdateGrid()

	# Add the simulation to the UI
	document.AllSimulations.Add( simulation )
