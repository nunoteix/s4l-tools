## Import Modules and Functions
import s4l_v1.document as document
import s4l_v1.model as model
from s4l_v1 import Vec3
import s4l_v1.simulation.emfdtd as fdtd
import s4l_v1.units as units
import CreateVernickelElement as CVE
import MultipleElementsCylindricalShield as MECS
import BasicTorsoPhantom as BTP
# Reload functions I'm changing
CVE = reload(CVE)
MECS = reload(MECS)
BTP = reload(BTP)
# Initialise Sim4Life
document.New()
# Build the hardware 
Nt = 8
coil_geo = MECS.CreateHardware(Nt)

sensbox = model.CreateSolidBlock(Vec3(-400,-400,-600), Vec3(400,400,600))
sensbox.Name = 'SensBox'

# Create the model
phant_dist = 500.0
t_x = 0.0
t_y = 0.0
t_z = 0.0
BTP.Do(t_x,t_y,t_z)
# Create Multiport Simulation
#Setup Setttings
sim = fdtd.MultiportSimulation()
sim.Name = 'FullSim'
sim.SetupSettings.SimulationTime = 15., units.Periods
options = sim.SetupSettings.GlobalAutoTermination.enum
sim.SetupSettings.GlobalAutoTermination = options.GlobalAutoTerminationUserDefined
#sim.SetupSettings.ConvergenceLevel = -50
# Import all model elements
entities = model.AllEntities()
#Sort Entities into type
PECList = []
DielectricList = []
PortList = []
DCList = []
LEList = []

box = entities['SensBox']

for i in range(0,len(entities)):
	tmp = entities[i]
	
	if ('RFS' in tmp.Name) or ('Coil' in tmp.Name) or ('DR' in tmp.Name):
		PECList.append(tmp)
	if ('Phantom' in tmp.Name):
		DielectricList.append(tmp)
	if ((len(tmp.Name)>3) and ('P' in tmp.Name[0]) and ('E' in tmp.Name[3])) or ('DC' in tmp.Name):
		PortList.append(tmp)
	if ((len(tmp.Name)>3) and ('L' in tmp.Name[0]) and ('E' in tmp.Name[1])):
		LEList.append(tmp)
	if ('DC' in tmp.Name):
		DCList.append(tmp)
		
		

# Materials
pec_settings = sim.AddMaterialSettings(PECList)
pec_settings.Name = 'PEC' 
options = pec_settings.MaterialType.enum
pec_settings.MaterialType = options.PEC
# Phantom
dielectric_settings = sim.AddMaterialSettings(DielectricList)
dielectric_settings.Name = 'Dielectric'
dielectric_settings.MaterialType = options.Dielectric
dielectric_settings.ElectricProps.Conductivity = 0.47 # S/m
dielectric_settings.ElectricProps.RelativePermittivity = 80

# Ports
edge_port_settings = sim.AddEdgePortSettings(PortList)
edge_port_settings.Name = 'Sources'
options = edge_port_settings.ExcitationType.enum
edge_port_settings.ExcitationType = options.Gaussian
edge_port_settings.CenterFrequency = 128e6, units.Hz
edge_port_settings.Bandwidth = 100e6, units.Hz
edge_port_settings.ReferenceLoad = 50, units.Ohms

LE_settings = sim.AddLumpedElementSettings(LEList)
LE_settings.Type = LE_settings.Type.enum.Capacitor

# edge_port_settings.Amplitude = 100
# edge_port_settings.AmplitudeProp = 100
# edge_port_settings = sim.AddEdgePortSettings(PortList)
# edge_port_settings.Name = 'Sources'
# options = edge_port_settings.ExcitationType.enum
# edge_port_settings.ExcitationType = options.Harmonic
# edge_port_settings.CenterFrequency = 128e6, units.Hz
# edge_port_settings.ReferenceLoad = 50, units.Ohms
# Sensors
# overall_field_sensor = sim.AddOverallFieldSensorSettings()
# overall_field_sensor.ExtractedFrequencies = 128e6
edgesensor_settings = sim.AddEdgeSensorSettings(PortList)
options = edgesensor_settings.AutoTermination.enum
edgesensor_settings.AutoTermination = options.AutoTerminationUseGlobal

# edgesensor_settings2 = sim.AddEdgeSensorSettings(LEList)  # UNCOMMENT IF THERE ARE FIXED LE'S
# options = edgesensor_settings2.AutoTermination.enum
# edgesensor_settings2.AutoTermination = options.AutoTerminationUseGlobal

settings2 = sim.AddFieldSensorSettings(box)
settings2.ExtractedFrequencies = 128e6
# edgesensor_settings.AutoTermination = options.AutoTerminationNone
# Boundary Conditions
global_boundary_settings = fdtd.BoundarySettings()
global_boundary_settings.BoundaryType = global_boundary_settings.BoundaryType.enum.ABC
global_boundary_settings.PmlStrength = global_boundary_settings.PmlStrength.enum.Medium

# Grid Settings
manual_grid_settings = sim.AddManualGridSettings(PECList)
manual_grid_settings.MaxStep = (15, )*3 # model units
manual_grid_settings.Resolution = (3.5, )*3

manual_grid_settings2 = sim.AddAutomaticGridSettings(DielectricList)
manual_grid_settings2.MaxStep = (15, )*3 # model units
manual_grid_settings2.Resolution = (3.5, )*3

manual_grid_settings3 = sim.AddAutomaticGridSettings(PortList)
manual_grid_settings3.MaxStep = (10, )*3 # model units
manual_grid_settings3.Resolution = (2.5, )*3

# manual_grid_settings3 = sim.AddAutomaticGridSettings(LEList)  # UNCOMMENT IF THERE ARE FIXED LE'S
# manual_grid_settings3.MaxStep = (10, )*3 # model units
# manual_grid_settings3.Resolution = (2.5, )*3

manual_grid_settings5 = sim.AddManualGridSettings(box)
manual_grid_settings5.MaxStep = (15, )*3 # model units
manual_grid_settings5.Resolution = (3.5, )*3

if Nt != 1:
	man_grid_DR = sim.AddManualGridSettings(entities['DR'])
	man_grid_DR.MaxStep = [7, 7, 7]
	man_grid_PortList = sim.AddManualGridSettings(PortList)
	man_grid_PortList.MaxStep = [4, 4, 4]
	man_grid_DCList = sim.AddManualGridSettings(DCList)
	man_grid_DCList.MaxStep = [2, 2, 2]
# Voxels
default_voxel = next(s for s in sim.AllSettings if isinstance(s, fdtd.AutomaticVoxelerSettings) )
sim.Add(default_voxel, PECList)
sim.Add(default_voxel, DielectricList)
sim.Add(default_voxel, PortList)
# sim.Add(default_voxel, LEList)  # UNCOMMENT IF THERE ARE FIXED LE'S
sim.AddAutomaticVoxelerSettings(box)
# Solver settings
sim.SolverSettings.Kernel = sim.SolverSettings.Kernel.enum.Cuda
	
# Actually create sim and run
document.AllSimulations.Add(sim)
sim.UpdateGrid()
# sim.CreateVoxels('C:\\Users\\nt16\\Documents\\Hydra Simulations\\2016_11_15_1ElementTest.smash')
#sim.RunSimulation()
