import s4l_v1.document as document
import s4l_v1.model as model
import s4l_v1.units as units
from s4l_v1 import ReleaseVersion
from s4l_v1 import Unit
from s4l_v1 import Vec3
from s4l_v1 import Transform

# a = model.AllEntities()["HRS1"]
T = Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(0,0,5))
for ii in range(2,200):
	a = model.AllEntities()["HRS"+str(ii-1)]
	b = a.Clone()
	b.Name = "HRS"+str(ii)

	b.ApplyTransform(T)
	
	if b.Transform.Translation[2] == 950:
		break





ii = 1

for z in range(155, 935, 20):
	ii += 1
	T = Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(0,0,20*(ii-1)))
	
	if z == 155:
		box = model.AllEntities()["PWE"+str(ii-1)]
		ibox = model.AllEntities()["IPWE"+str(ii-1)]
		nbox = box.Clone()
		nibox = ibox.Clone()
		nbox.Name = "PWE"+str(ii)
		nibox.Name = "IPWE"+str(ii)
		T1 = Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(0,0,5))
		nbox.ApplyTransform(T1)
		nibox.ApplyTransform(T1)
	
	box = model.AllEntities()["PWE1"]
	ibox = model.AllEntities()["IPWE1"]
	nbox = box.Clone()
	nibox = ibox.Clone()
	
	nbox.Name = "PWE"+str(ii+1)
	nibox.Name = "IPWE"+str(ii+1)

	nbox.ApplyTransform(T)
	nibox.ApplyTransform(T)
	
	if z == 915:
		box = model.AllEntities()["PWE"+str(ii+1)]
		ibox = model.AllEntities()["IPWE"+str(ii+1)]
		
		nbox = box.Clone()
		nibox = ibox.Clone()
		nbox.Name = "PWE"+str(ii+2)
		nibox.Name = "IPWE"+str(ii+2)
		T1 = Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(0,0,10))
		nbox.ApplyTransform(T1)
		nibox.ApplyTransform(T1)
		
		nbox = box.Clone()
		nibox = ibox.Clone()
		nbox.Name = "PWE"+str(ii+3)
		nibox.Name = "IPWE"+str(ii+3)
		T2 = Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(0,0,15))
		nbox.ApplyTransform(T2)
		nibox.ApplyTransform(T2)
	

	


ii = 0
for z in range(155, 950, 20):
	ii += 1
	if ii == 1:
		box = model.CreateWireBlock(Vec3(-50,-50,150), Vec3(50,50,155))
		ibox = box.Clone()
		box.Name = "PWE1"
		ibox.Name = "IPEW1"
		
		box = model.CreateWireBlock(Vec3(-50,-50,945), Vec3(50,50,950))
		ibox = box.Clone()
		box.Name = "PWE2"
		ibox.Name = "IPEW2"
		
		box = model.CreateWireBlock(Vec3(-50,-50,940), Vec3(50,50,945))
		ibox = box.Clone()
		box.Name = "PWE2"
		ibox.Name = "IPEW2"
	
	box = model.CreateWireBlock(Vec3(-50,-50,z), Vec3(50,50,z+5))
	ibox = box.Clone()
	
	box.Name = "PWE"+str(ii+3)
	ibox.Name = "IPEW"+str(ii+3)
	
	