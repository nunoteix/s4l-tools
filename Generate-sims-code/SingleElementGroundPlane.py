def CreateHardware(Nt):
	from s4l_v1 import Vec3
	import s4l_v1.model as model
	import CreateVernickelElement as CVE
	import MakePorts as MP
	
	# Need to reload functions when debugging so changes are actually run
	CVE = reload(CVE)
	MP = reload(MP)
	# Ground Plate Parameters
	gp_width = 1000.0
	gp_height = 600.0
	gp_depth = 2.0		
	gp_VE_dist = 35.0
	gp_geo = [gp_width,gp_height,gp_depth,gp_VE_dist]
	
	# Create Ground Plate
	ground_plate = model.CreateSolidBlock( Vec3(-gp_depth/2.0,-gp_width/2.0,-gp_height/2.0),Vec3(gp_depth/2.0,gp_width/2.0,gp_height/2.0))
	ground_plate.Name = 'Ground Plate'
	
	# Shift Ground Plate (using knowledge that elements are made at isocentre) 
	gp_shift = model.Translation(Vec3(-gp_VE_dist,0,0))
	ground_plate.ApplyTransform(gp_shift)
	
	# Make Transmitters
	for i in range(0,Nt):
		# Make group for each element
		CoilGroup = model.CreateGroup('Element ' + str(i))
		CoilGroup.Name = 'Element ' + str(i)
		
		# Make Vernickel element of each transmitter and add to group
		coil_geo = CVE.Do(CoilGroup)
		
		# Make Ports

	return