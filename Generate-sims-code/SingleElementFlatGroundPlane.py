## Import Modules and Functions
import s4l_v1.document as document
import s4l_v1.model as model
from s4l_v1 import Vec3
import CreateVernickelElement as CVE
import SingleElementGroundPlane as SEGP
import BasicTorsoPhantom as BTP
# Reload functions I'm changing
CVE = reload(CVE)
SEGP = reload(SEGP)
BTP = reload(BTP)
# Initialise Simulations
document.New()
# Build the hardware 
Nt = 1
SEGP.CreateHardware(Nt)
# Create the model
phant_dist = 500.0
BTP.Do(phant_dist)
