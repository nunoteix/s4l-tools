# This script Needs several things modeled to run properly:

# Needs to have DUKE imported.
# Needs to have lumped elements and source created in the format: LE0XC (X = LE number, C = Coil number), source = AC (C = Coil number)
# Needs RF Shield and Coil modeled... for Loops: SLoopC (C = Coil number)
	# FLAG: Need to change this to be more versatile.
# Needs Sensing Boxes: For huygens named Box , For sensor SensBox

import numpy
import s4l_v1.document as document
import s4l_v1.materials.database as database
import s4l_v1.model as model
import s4l_v1.simulation.emfdtd as emfdtd
import s4l_v1.units as units
from s4l_v1 import ReleaseVersion
from s4l_v1 import Unit

import AddLoopCoilToSim as COIL
COIL = reload(COIL)
import AddSourceAndLumpedElementsToSim as SLE
SLE = reload(SLE)
import AddNormalBoxesToSim as NBOXES
NBOXES = reload(NBOXES)
import AddDukeToSim64MHz as DUKE
DUKE = reload(DUKE)

# Define the version to use for default values
ReleaseVersion.set_active(ReleaseVersion.version3_4)

coilno = 8
huygens = 0
# Creating the simulation
simulation = emfdtd.MultiportSimulation()
simulation.Name = "Channel "+str(coilno)

simulation = COIL.Do(simulation)
simulation = SLE.Do(simulation, coilno)
simulation = NBOXES.Do(simulation)
simulation = DUKE.Do(simulation, huygens)

# Editing SetupSettings "Setup"
setup_settings = simulation.SetupSettings
setup_settings.SimulationTime = 40.0, units.Periods
setup_settings.GlobalAutoTermination = setup_settings.GlobalAutoTermination.enum.GlobalAutoTerminationUserDefined
	
# Editing GlobalGridSettings "Grid (Empty)"
global_grid_settings = simulation.GlobalGridSettings
global_grid_settings.PaddingMode = global_grid_settings.PaddingMode.enum.Manual
global_grid_settings.BottomPadding = numpy.array([100.0, 100.0, 100.0]), units.MilliMeters
global_grid_settings.TopPadding = numpy.array([100.0, 100.0, 100.0]), units.MilliMeters

# Editing MultiportSolverSettings "Solver"
multiport_solver_settings = [x for x in simulation.AllSettings if isinstance(x, emfdtd.MultiportSolverSettings) and x.Name == "Solver"][0]
multiport_solver_settings.Kernel = multiport_solver_settings.Kernel.enum.Cuda

# Update the materials with the new frequency parameters
simulation.UpdateAllMaterials()

# Update the grid with the new parameters
simulation.UpdateGrid()

# Add the simulation to the UI
document.AllSimulations.Add( simulation )

