## Import Modules and Functions
import s4l_v1.document as document
import s4l_v1.model as model
from s4l_v1 import Vec3
import s4l_v1.simulation.emfdtd as fdtd
import s4l_v1.units as units
import CreateVernickelElement as CVE
import MultipleElementsCylindricalShield as MECS
import BasicTorsoPhantom as BTP
# Reload functions I'm changing
CVE = reload(CVE)
MECS = reload(MECS)
BTP = reload(BTP)
# Initialise Sim4Life
document.New()
# Build the hardware 
Nt = 8
coil_geo = MECS.CreateHardware(Nt)

sensbox = model.CreateSolidBlock(Vec3(-400,-400,-600), Vec3(400,400,1800))
sensbox.Name = 'SensBox'

# retphant = model.CreateSolidBlock(Vec3(-130,-45,-225), Vec3(130,45,225))
# retphant.Name = 'Phantom'

# Create the model
phant_dist = 500.0
t_x = 0.0
t_y = 0.0
t_z = 0.0
# BTP.Do(t_x,t_y,t_z)
# Create Multiport Simulation
#Setup Setttings
sim = fdtd.MultiportSimulation()
sim.Name = 'FullSim'
sim.SetupSettings.SimulationTime = 30., units.Periods
options = sim.SetupSettings.GlobalAutoTermination.enum
sim.SetupSettings.GlobalAutoTermination = options.GlobalAutoTerminationUserDefined
#sim.SetupSettings.ConvergenceLevel = -50
# Import all model elements
entities = model.AllEntities()

#Sort Entities into type
PECList = []
DielectricList = []
PortList = []
DCList = []
LEList = []
LE_P_List = []

LE_E0_1_List = []
LE_E1_1_List = []
LE_E2_1_List = []
LE_E3_1_List = []
LE_E4_1_List = []
LE_E5_1_List = []
LE_E6_1_List = []
LE_E7_1_List = []
LE_E0_2_List = []
LE_E1_2_List = []
LE_E2_2_List = []
LE_E3_2_List = []
LE_E4_2_List = []
LE_E5_2_List = []
LE_E6_2_List = []
LE_E7_2_List = []

LE_midList = []


box = entities['SensBox']

for i in range(0,len(entities)):
	tmp = entities[i]
	
	if ('RFS' in tmp.Name) or ('Coil' in tmp.Name) or ('DR' in tmp.Name):
		PECList.append(tmp)

	if ('Phantom' in tmp.Name):
		DielectricList.append(tmp)

	if ((len(tmp.Name)>3) and ('P' in tmp.Name[0]) and ('E' in tmp.Name[3])) or ('DC' in tmp.Name):
		PortList.append(tmp)

	if ((len(tmp.Name)>3) and ('L' in tmp.Name[0]) and ('E' in tmp.Name[1])):# or ('DC' in tmp.Name):
		LEList.append(tmp)
		if (('0' in tmp.Name[2]) and ('1' in tmp.Name[3])):
			LE_P_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('2' in tmp.Name[3])) and ('0' in tmp.Name[5]):
			LE_E0_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('2' in tmp.Name[3])) and ('1' in tmp.Name[5]):
			LE_E1_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('2' in tmp.Name[3])) and ('2' in tmp.Name[5]):
			LE_E2_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('2' in tmp.Name[3])) and ('3' in tmp.Name[5]):
			LE_E3_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('2' in tmp.Name[3])) and ('4' in tmp.Name[5]):
			LE_E4_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('2' in tmp.Name[3])) and ('5' in tmp.Name[5]):
			LE_E5_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('2' in tmp.Name[3])) and ('6' in tmp.Name[5]):
			LE_E6_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('2' in tmp.Name[3])) and ('7' in tmp.Name[5]):
			LE_E7_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('3' in tmp.Name[3])) and ('0' in tmp.Name[5]):
			LE_E0_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('3' in tmp.Name[3])) and ('1' in tmp.Name[5]):
			LE_E1_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('3' in tmp.Name[3])) and ('2' in tmp.Name[5]):
			LE_E2_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('3' in tmp.Name[3])) and ('3' in tmp.Name[5]):
			LE_E3_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('3' in tmp.Name[3])) and ('4' in tmp.Name[5]):
			LE_E4_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('3' in tmp.Name[3])) and ('5' in tmp.Name[5]):
			LE_E5_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('3' in tmp.Name[3])) and ('6' in tmp.Name[5]):
			LE_E6_1_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('3' in tmp.Name[3])) and ('7' in tmp.Name[5]):
			LE_E7_1_List.append(tmp)

		elif (('0' in tmp.Name[2]) and ('4' in tmp.Name[3])) and ('0' in tmp.Name[5]):
			LE_E0_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('4' in tmp.Name[3])) and ('1' in tmp.Name[5]):
			LE_E1_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('4' in tmp.Name[3])) and ('2' in tmp.Name[5]):
			LE_E2_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('4' in tmp.Name[3])) and ('3' in tmp.Name[5]):
			LE_E3_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('4' in tmp.Name[3])) and ('4' in tmp.Name[5]):
			LE_E4_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('4' in tmp.Name[3])) and ('5' in tmp.Name[5]):
			LE_E5_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('4' in tmp.Name[3])) and ('6' in tmp.Name[5]):
			LE_E6_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('4' in tmp.Name[3])) and ('7' in tmp.Name[5]):
			LE_E7_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('5' in tmp.Name[3])) and ('0' in tmp.Name[5]):
			LE_E0_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('5' in tmp.Name[3])) and ('1' in tmp.Name[5]):
			LE_E1_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('5' in tmp.Name[3])) and ('2' in tmp.Name[5]):
			LE_E2_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('5' in tmp.Name[3])) and ('3' in tmp.Name[5]):
			LE_E3_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('5' in tmp.Name[3])) and ('4' in tmp.Name[5]):
			LE_E4_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('5' in tmp.Name[3])) and ('5' in tmp.Name[5]):
			LE_E5_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('5' in tmp.Name[3])) and ('6' in tmp.Name[5]):
			LE_E6_2_List.append(tmp)
		elif (('0' in tmp.Name[2]) and ('5' in tmp.Name[3])) and ('7' in tmp.Name[5]):
			LE_E7_2_List.append(tmp)

		elif (('0' in tmp.Name[2]) and ('6' in tmp.Name[3])):
			LE_midList.append(tmp)
		elif (('0' in tmp.Name[2]) and ('7' in tmp.Name[3])):
			LE_midList.append(tmp)
		elif (('0' in tmp.Name[2]) and ('8' in tmp.Name[3])):
			LE_midList.append(tmp)
		elif (('0' in tmp.Name[2]) and ('9' in tmp.Name[3])):
			LE_midList.append(tmp)
		elif (('1' in tmp.Name[2]) and ('0' in tmp.Name[3])):
			LE_midList.append(tmp)
		elif (('1' in tmp.Name[2]) and ('1' in tmp.Name[3])):
			LE_midList.append(tmp)
		elif (('1' in tmp.Name[2]) and ('2' in tmp.Name[3])):
			LE_midList.append(tmp)
		elif (('1' in tmp.Name[2]) and ('3' in tmp.Name[3])):
			LE_midList.append(tmp)
	if ('DC' in tmp.Name):
		DCList.append(tmp)
		
# dc_cap0 = entities['LE14E0']
# dc_cap1 = entities['LE14E1']
# dc_cap2 = entities['LE14E2']
# dc_cap3 = entities['LE14E3']
# dc_cap4 = entities['LE14E4']
# dc_cap5 = entities['LE14E5']
# dc_cap6 = entities['LE14E6']
# dc_cap7 = entities['LE14E7']

# r_cap01 = entities['ZDC01']
# r_cap12 = entities['ZDC12']
# r_cap23 = entities['ZDC23']
# r_cap34 = entities['ZDC34']
# r_cap45 = entities['ZDC45']
# r_cap56 = entities['ZDC56']
# r_cap67 = entities['ZDC67']
# r_cap70 = entities['ZDC70']

# Materials
pec_settings = sim.AddMaterialSettings(PECList)
pec_settings.Name = 'PEC' 
options = pec_settings.MaterialType.enum
pec_settings.MaterialType = options.PEC
# Phantom
dielectric_settings = sim.AddMaterialSettings(DielectricList)
dielectric_settings.Name = 'Dielectric'
dielectric_settings.MaterialType = options.Dielectric
dielectric_settings.ElectricProps.Conductivity = 0.4 # S/m
dielectric_settings.ElectricProps.RelativePermittivity = 80

# Ports
# edge_port_settings.Amplitude = 100
# edge_port_settings.AmplitudeProp = 100
edge_port_settings = sim.AddEdgePortSettings(PortList)
edge_port_settings.Name = 'Sources'
options = edge_port_settings.ExcitationType.enum
edge_port_settings.ExcitationType = options.Gaussian
edge_port_settings.CenterFrequency = 128e6, units.Hz
edge_port_settings.Bandwidth = 100e6, units.Hz
edge_port_settings.ReferenceLoad = 50, units.Ohms

# LE_P_settings = sim.AddLumpedElementSettings(LE_P_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 21.10170245441289, units.PicoFarads



# LE_P_settings = sim.AddLumpedElementSettings(LE_E0_1_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 7.739236586991397, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E1_1_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 8.506781011252995, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E2_1_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 11.33512500081137, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E3_1_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 8.568987700561991, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E4_1_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 5.880157357086625, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E5_1_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 7.127049253533369, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E6_1_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 7.921978370707633, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E7_1_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 11.01953345167197, units.PicoFarads



# LE_P_settings = sim.AddLumpedElementSettings(LE_E0_2_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 22.97907519107763, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E1_2_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 36.43462026440594, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E2_2_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 43.28658623655743, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E3_2_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 24.55278309941415, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E4_2_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 9.132042343430833, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E5_2_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 16.26433884124736, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E6_2_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 35.01230886566697, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(LE_E7_2_List)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 750, units.Ohms
# LE_P_settings.Capacitance = 47.05905618110063, units.PicoFarads

#

LE_P_settings = sim.AddLumpedElementSettings(LE_midList)
LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
LE_P_settings.Resistance = 750, units.Ohms
LE_P_settings.Capacitance = 43.97418428259952, units.PicoFarads

#

# LE_P_settings = sim.AddLumpedElementSettings(dc_cap0)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 129.9104833447918, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(dc_cap1)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 102.8763740234086, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(dc_cap2)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 214.6928208679664, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(dc_cap3)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 165.0572828161734, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(dc_cap4)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 145.5346343832133, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(dc_cap5)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 158.6778543995835, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(dc_cap6)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 160.2511540264288, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(dc_cap7)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 52.02485615028566, units.PicoFarads



# LE_P_settings = sim.AddLumpedElementSettings(r_cap01)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 21.34273527135985, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(r_cap12)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 16.22935443596647, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(r_cap23)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 14.07122797489815, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(r_cap34)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 15.96102436347029, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(r_cap45)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 15.87027579463068, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(r_cap56)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 12.96680119865523, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(r_cap67)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 10.54151804252919, units.PicoFarads

# LE_P_settings = sim.AddLumpedElementSettings(r_cap70)
# LE_P_settings.Type = LE_P_settings.Type.enum.ResistorParallelCapacitor
# LE_P_settings.Resistance = 900, units.Ohms
# LE_P_settings.Capacitance = 7.34014761998624, units.PicoFarads


# Sensors
# overall_field_sensor = sim.AddOverallFieldSensorSettings()
# overall_field_sensor.ExtractedFrequencies = 128e6
edgesensor_settings = sim.AddEdgeSensorSettings(PortList)
options = edgesensor_settings.AutoTermination.enum
edgesensor_settings.AutoTermination = options.AutoTerminationUseGlobal

edgesensor_settings2 = sim.AddEdgeSensorSettings(LEList)
options = edgesensor_settings2.AutoTermination.enum
edgesensor_settings2.AutoTermination = options.AutoTerminationUseGlobal

settings2 = sim.AddFieldSensorSettings(box)
settings2.ExtractedFrequencies = 128e6
# edgesensor_settings.AutoTermination = options.AutoTerminationNone
# Boundary Conditions
global_boundary_settings = fdtd.BoundarySettings()
global_boundary_settings.BoundaryType = global_boundary_settings.BoundaryType.enum.ABC
global_boundary_settings.PmlStrength = global_boundary_settings.PmlStrength.enum.Medium

sim.GlobalGridSettings.PaddingMode = sim.GlobalGridSettings.PaddingMode.enum.Manual
sim.GlobalGridSettings.BottomPadding = sim.GlobalGridSettings.TopPadding = (0,)*3
# Grid Settings
manual_grid_settings = sim.AddManualGridSettings(PECList)
manual_grid_settings.MaxStep = (15, )*3 # model units
manual_grid_settings.Resolution = (3.5, )*3

manual_grid_settings = sim.AddManualGridSettings(entities['DR'])
manual_grid_settings.MaxStep = (10, )*3 # model units
manual_grid_settings.Resolution = (3, )*3

manual_grid_settings2 = sim.AddAutomaticGridSettings(DielectricList)
manual_grid_settings2.MaxStep = (15, )*3 # model units
manual_grid_settings2.Resolution = (3.5, )*3

manual_grid_settings3 = sim.AddManualGridSettings(PortList)
manual_grid_settings3.MaxStep = (4, )*3 # model units
manual_grid_settings3.Resolution = (3.5, )*3

manual_grid_settings3 = sim.AddManualGridSettings(DCList)
manual_grid_settings3.MaxStep = (1.3, )*3 # model units
manual_grid_settings3.Resolution = (2, )*3

					#########################################
					#########################################
					### 								  ###
					### UNCOMMENT IF THERE ARE FIXED LE'S ###
					###									  ###
					#########################################
					#########################################

		 
manual_grid_settings3 = sim.AddAutomaticGridSettings(LEList)
manual_grid_settings3.MaxStep = (4, )*3 # model units
manual_grid_settings3.Resolution = (3.5, )*3

manual_grid_settings5 = sim.AddManualGridSettings(box)
manual_grid_settings5.MaxStep = (15, )*3 # model units
manual_grid_settings5.Resolution = (3.5, )*3

# if Nt != 1:
	# man_grid_DR = sim.AddManualGridSettings(entities['DR'])
	# man_grid_DR.MaxStep = [7, 7, 7]
	# man_grid_PortList = sim.AddManualGridSettings(PortList)
	# man_grid_PortList.MaxStep = [4, 4, 4]
	# man_grid_DCList = sim.AddManualGridSettings(DCList)
	# man_grid_DCList.MaxStep = [2, 2, 2]
	
# Voxels
default_voxel = next(s for s in sim.AllSettings if isinstance(s, fdtd.AutomaticVoxelerSettings) )
sim.Add(default_voxel, PECList)
sim.Add(default_voxel, DielectricList)
sim.Add(default_voxel, PortList)
sim.Add(default_voxel, LEList)  # UNCOMMENT IF THERE ARE FIXED LE'S
sim.AddAutomaticVoxelerSettings(box)

# Solver settings
sim.SolverSettings.Kernel = sim.SolverSettings.Kernel.enum.Cuda
	
# Actually create sim and run
document.AllSimulations.Add(sim)
# sim.UpdateGrid()
# sim.CreateVoxels('C:\\Users\\nt16\\Documents\\PERINATAL033-BACKUP\\Hydra Simulations\\Starting Sims - Vernickels\\2017_01_18_8ElementTest_64Ports.smash')
#sim.RunSimulation()
