# This script Needs several things modeled to run properly:

# Needs to have DUKE imported.
# Needs to have Circles for Current Sensing modeled - their names must be S01 to SXX.
# Needs an insulated conductor: PEC must be named WireCore and Dielectric must be named WireInsulation
	# FLAG: Need to change this to be more versatile.
# Needs Sensing Boxes to be created. First one must be the same as Prep, Sim. and named Box , Second is huygens box and named sBox

import numpy
import s4l_v1.document as document
import s4l_v1.materials.database as database
import s4l_v1.model as model
import s4l_v1.simulation.emfdtd as emfdtd
import s4l_v1.units as units
from s4l_v1 import ReleaseVersion
from s4l_v1 import Unit

import AddDukeToSim64MHz as DUKE
DUKE = reload(DUKE)
import AddCurrentSensorsToSim as CSENSORS
CSENSORS = reload(CSENSORS)
import AddConductorToSim as CONDUCTOR
CONDUCTOR = reload(CONDUCTOR)
import AddHuygensBoxesToSim as BOXES
BOXES = reload(BOXES)

# Define the version to use for default values
ReleaseVersion.set_active(ReleaseVersion.version3_4)
	
coilno = 8
n = 44
huygens = 1

# Creating the simulation
simulation = emfdtd.Simulation()
simulation.Name = "Huygens Channel "+str(coilno)


# Simulation links
link__box = document.AllSimulations["Channel "+str(coilno)].AllComponents["Box  (Model)"]

simulation = DUKE.Do(simulation, huygens)
simulation = CSENSORS.Do(simulation, n)
simulation = CONDUCTOR.Do(simulation)
simulation = BOXES.Do(simulation, link__box)

# Editing SetupSettings "Setup"
setup_settings = simulation.SetupSettings
setup_settings.SimulationTime = 40.0, units.Periods
setup_settings.GlobalAutoTermination = setup_settings.GlobalAutoTermination.enum.GlobalAutoTerminationUserDefined

# Editing GlobalGridSettings "Grid (Empty)"
global_grid_settings = simulation.GlobalGridSettings
global_grid_settings.PaddingMode = global_grid_settings.PaddingMode.enum.Manual
# global_grid_settings.BottomPadding = numpy.array([200.0, 200.0, 200.0]), units.MilliMeters
# global_grid_settings.TopPadding = numpy.array([200.0, 200.0, 200.0]), units.MilliMeters
global_grid_settings.BottomPadding = numpy.array([-100.0, -30.0, -150.0]), units.MilliMeters
global_grid_settings.TopPadding = numpy.array([-115.0, -30.0, -185.0]), units.MilliMeters

# Editing SolverSettings "Solver"
solver_settings = simulation.SolverSettings
solver_settings.Kernel = solver_settings.Kernel.enum.Cuda

# Update the materials with the new frequency parameters
simulation.UpdateAllMaterials()

# Update the grid with the new parameters
simulation.UpdateGrid()

# Add the simulation to the UI
document.AllSimulations.Add( simulation )



