import sys, os
import numpy as np
import math

import s4l_v1.model as model
import s4l_v1.simulation.emfdtd as fdtd
import s4l_v1.analysis as analysis
import s4l_v1.analysis.viewers as viewers

import s4l_v1.units as units
from scipy.optimize import fsolve

import CreateVernickelElement as CVE
import MultipleElementsCylindricalShield as MECS
import BasicTorsoPhantom as BTP

BTP = reload(BTP)

def CreateModel():
	from s4l_v1.model import Vec3, CreateSolidBlock, Entity, Translation, Rotation, Transform
	# Define dimensions of coil element
	# Meanings defined in ppt in PythonRepo/Models
	width = 100.0
	length = (4.0*5.0) + 20.0 + 35.0 + 70.0 + 20.0 + 110.0 + 20.0 + 70.0 + 35.0 + 20.0
	depth = 0.0
	cond_width = 20.0
	hole_length = 110.0
	cap_gap = 5.0
	cap_dist_middle1 = 55.0
	cap_dist_middle2 = 150.0
	
	geometry = [width,length,depth,cond_width,hole_length,cap_gap,cap_dist_middle1,cap_dist_middle2]
	
	d2 = depth/2.0
	l2 = length/2.0
	w2 = width/2.0
	
	# Create coil element body 
	coil_base =  model.CreateSolidBlock( Vec3(-d2,-w2,-l2),Vec3(d2,w2,l2))
	
	# Create holes to be subtractedr
	coil_hole1 = model.CreateSolidBlock( Vec3(-d2,-w2+cond_width,-l2+cond_width),Vec3(d2,w2-cond_width,-l2+cond_width+hole_length))
	coil_hole2 = model.CreateSolidBlock( Vec3(-d2,-w2+cond_width,-hole_length/2.0),Vec3(d2,w2-cond_width,hole_length/2.0))
	coil_hole3 = model.CreateSolidBlock( Vec3(-d2,-w2+cond_width,l2-cond_width), Vec3(d2,w2-cond_width,l2-cond_width-hole_length))
	
	# Create capacitor blockes to be subtracted
	cap_gap1 = model.CreateSolidBlock( Vec3(-d2,-w2,-cap_dist_middle1),	Vec3(d2,w2,-cap_dist_middle1-cap_gap))
	cap_gap2 = model.CreateSolidBlock( Vec3(-d2,-w2, cap_dist_middle1),	Vec3(d2,w2, cap_dist_middle1+cap_gap))
	cap_gap3 = model.CreateSolidBlock( Vec3(-d2,-w2,-cap_dist_middle2),	Vec3(d2,w2,-cap_dist_middle2-cap_gap))
	cap_gap4 = model.CreateSolidBlock( Vec3(-d2,-w2, cap_dist_middle2),	Vec3(d2,w2, cap_dist_middle2+cap_gap))
	
	# Subtract holes from main coil body
	coil_base = model.Subtract([coil_base,coil_hole1,coil_hole2,coil_hole3])
	coil_base = model.Subtract([coil_base,cap_gap1,cap_gap2,cap_gap3,cap_gap4])
	# coil_base.Name = CoilGroup.Name+'Coil'
	coil_base.Name = 'CoilElement01'
	coil_base.Transform = Transform(Vec3(1,1,1), Vec3(0,0,0), Vec3(675/2-15,0,0))
	
	# Get torso Phantom in
	t_x = 0.0
	t_y = 0.0
	t_z = 0.0
	BTP.Do(t_x,t_y,t_z)
	
	# Get shield in
	# Ground Plate Parameters
	gc_radius = 675.0/2
	gc_length = 1000.0
	gc_depth = 0.0		
	gc_geo = [gc_radius,gc_length,gc_depth]
	rf_shield = model.CreateSolidTube( Vec3(0,0,-gc_length/2.0),Vec3(0,0,gc_length),gc_radius+gc_depth,gc_radius)
	rf_shield.Name = 'RFS'
	
	# coil_base.ApplyTransform(trans)
	# coil_base.ApplyTransform(rot)
	
	# Antenna01.Name = 'M01'; Antenna02.Name = 'M02'; #Antenna03.Name = 'M03'; Antenna04.Name = 'M04';
	# Antenna05.Name = 'M05'; Antenna06.Name = 'M06'; Antenna07.Name = 'M07'; Antenna08.Name = 'M08';
	# Antenna09.Name = 'M09'; Antenna10.Name = 'M10'; Antenna11.Name = 'M11'; Antenna12.Name = 'M12';
	# Antenna13.Name = 'M13'; Antenna14.Name = 'M14'; Antenna15.Name = 'M15'; Antenna16.Name = 'M16';
	# Antenna17.Name = 'M17'; Antenna18.Name = 'M18'; Antenna19.Name = 'M19'; Antenna20.Name = 'M20';
	# Antenna21.Name = 'M21'; Antenna22.Name = 'M22'; Antenna23.Name = 'M23'; Antenna24.Name = 'M24';
	
	# source01.Name = 'P01'; source02.Name = 'P02'; #source03.Name = 'P03'; source04.Name = 'P04';
	# source05.Name = 'P05'; source06.Name = 'P06'; source07.Name = 'P07'; source08.Name = 'P08';
	# source09.Name = 'P09'; source10.Name = 'P10'; source11.Name = 'P11'; source12.Name = 'P12';
	# source13.Name = 'P13'; source14.Name = 'P14'; source15.Name = 'P15'; source16.Name = 'P16';
	# source17.Name = 'P17'; source18.Name = 'P18'; source19.Name = 'P19'; source20.Name = 'P20';
	# source21.Name = 'P21'; source22.Name = 'P22'; source23.Name = 'P23'; source24.Name = 'P24';
	
def Run( smash_path, use_graphcard ):
	import s4l_v1.document
	
	s4l_v1.document.New()

	CreateModel()
	
	# sim = CreateSimulation(use_graphcard) # center frequency [MHz]
	# s4l_v1.document.AllSimulations.Add(sim)
	# sim.UpdateGrid()
	# sim.CreateVoxels(smash_path)
	
def main(data_path=None, project_dir=r'C:\\Users\\nt16\\Documents\\Hydra Simulations\\', use_graphcard=True):
	"""
		data_path = path to a folder that contains data for this simulation (e.g. model files)
		project_dir = path to a folder where this project and its results will be saved
	"""
	import sys
	import os

	if not os.path.exists(project_dir):
		os.makedirs(project_dir)

	fname = '2016_11_15_1ElementTest.smash'
	project_path = os.path.join(project_dir, fname)

	Run( project_path, use_graphcard )	

if __name__ == '__main__':
	main(use_graphcard=False)