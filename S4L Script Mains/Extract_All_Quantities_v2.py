import numpy
import s4l_v1.analysis as analysis
import FieldExtractorPackage_v2 as FEP
from scipy.interpolate import interpn
import gc
reload(FEP)

interpolate = False
mult = False

sim_tree = 'Main BC Duke'
field_sensor_name = 'Main SensBox Duke'
current_sensor_group = ''#'Current Sensors T'
port_name = ''

cn = 1
coilno = 1 # 8  # 
sensorno = 15 # 37 # 36 # 
sim_freq = 64e6 #63.882e6 # 
normalization = 'EM Input Power(f)' # 'Conducted Power(f)' # 

prime_path = 'C:\\Users\\nt16\\Desktop\\Coils_Data\\Birdcage Coil'
# prime_path = 'G:\\Huygens Simulations\\Duke\\Extracted Results'
# prime_path = 'G:\\Wire Voxelization\\Phantom\\Extracted Results'
# prime_path = 'G:\\FakePort Simulations\\Duke\\Extracted Results'

if mult:	
	sim_name = '\\'+sim_tree+' '+port_name+'_2021_06_17_'
else:
	sim_name = '\\'+sim_tree+'_2021_06_17_'


#GET CURRENTS
cn = 1
J = numpy.zeros((coilno, sensorno), dtype = 'complex')
Axis = numpy.zeros((3,sensorno))

for sn in range(1,sensorno+1):
	[oJ, x, y, z] = FEP.GetCurrentFromSensors(
	sensorno, sim_tree, field_sensor_name, current_sensor_group, sn, sim_freq, normalization, mult, port_name)
	
	if cn == 1:
		Axis[:,sn-1] = [x,y,z]
	J[cn-1,sn-1] = oJ
	analysis.ResetAnalysis()

from scipy.io import savemat
path = prime_path+'\\EM Current'+sim_name+'EMCurrent'
mdict = {'J':J, 'Axes':Axis}
savemat(path,mdict)

#GET GRID
[xaxis, yaxis, zaxis, xm, ym, zm, m_qr, xm_q, ym_q, zm_q] = FEP.GetGrid(
sim_tree, field_sensor_name, sim_freq, normalization, 0.001, 0.001, 0.001, mult, port_name)

sz = [zaxis.shape[0]-1, yaxis.shape[0]-1, xaxis.shape[0]-1]
if interpolate: 
	from scipy.io import savemat
	path = prime_path+'\\Grid'+sim_name+'Axes'
	mdict = {'XAxis':xm_q, 'YAxis':ym_q, 'ZAxis':zm_q}
	savemat(path,mdict)
else:
	from scipy.io import savemat
	path = prime_path+'\\Grid'+sim_name+'Axes'
	mdict = {'XAxis':xaxis, 'YAxis':yaxis, 'ZAxis':zaxis}
	savemat(path,mdict)

# GET E-Field

E = FEP.GetEFields(sim_tree, field_sensor_name, sim_freq, normalization, mult, port_name)

if interpolate:

	Ei = numpy.c_[interpn((zm,ym,xm), numpy.reshape(E[:,0], sz), m_qr, method='nearest'),
	interpn((zm,ym,xm), numpy.reshape(E[:,1], sz), m_qr, method='nearest'),
	interpn((zm,ym,xm), numpy.reshape(E[:,2], sz), m_qr, method='nearest')]
	
	from scipy.io import savemat
	path = prime_path+'\\E Field'+sim_name+'EFields_C'+str(cn)
	mdict = {'E_'+str(cn):Ei}
	savemat(path,mdict)
else:
	from scipy.io import savemat
	path = prime_path+'\\E Field'+sim_name+'EFields_C'+str(cn)
	mdict = {'E_'+str(cn):E}
	savemat(path,mdict)
	
analysis.ResetAnalysis()
gc.collect()

#GET B1+ FIELDS

B1 = FEP.GetB1Fields(sim_tree, field_sensor_name, sim_freq, normalization, mult, port_name)

if interpolate:
	B1i = numpy.c_[interpn((zm,ym,xm), numpy.reshape(B1[:,0], sz), m_qr, method='nearest'),
	interpn((zm,ym,xm), numpy.reshape(B1[:,1], sz), m_qr, method='nearest')]
	
	from scipy.io import savemat
	path = prime_path+'\\B1 Field'+sim_name+'B1Fields_C'+str(cn)
	mdict = {'B1_'+str(cn):B1i}
	savemat(path,mdict)	
else:
	from scipy.io import savemat
	path = prime_path+'\\B1 Field'+sim_name+'B1Fields_C'+str(cn)
	mdict = {'B1_'+str(cn):B1}
	savemat(path,mdict)
	
analysis.ResetAnalysis()
gc.collect()

# GET B-Field

B = FEP.GetBFields(sim_tree, field_sensor_name, sim_freq, normalization, mult, port_name)

if interpolate:

	Bi = numpy.c_[interpn((zm,ym,xm), numpy.reshape(B[:,0], sz), m_qr, method='nearest'),
	interpn((zm,ym,xm), numpy.reshape(B[:,1], sz), m_qr, method='nearest'),
	interpn((zm,ym,xm), numpy.reshape(B[:,2], sz), m_qr, method='nearest')]
	
	from scipy.io import savemat
	path = prime_path+'\\B Field'+sim_name+'BFields_C'+str(cn)
	mdict = {'B_'+str(cn):Bi}
	savemat(path,mdict)
else:
	from scipy.io import savemat
	path = prime_path+'\\B Field'+sim_name+'BFields_C'+str(cn)
	mdict = {'B_'+str(cn):B}
	savemat(path,mdict)
	
analysis.ResetAnalysis()
gc.collect()


#GET DENSITY
rho = FEP.GetRhoDistribution(sim_tree, field_sensor_name, sim_freq, normalization, mult, port_name)

if interpolate:
	rhoi = interpn((zm,ym,xm), numpy.reshape(rho, sz), m_qr, method='nearest')

	from scipy.io import savemat
	path = prime_path+'\\Density'+sim_name+'Density'
	mdict = {'rho':rhoi}
	savemat(path,mdict)

else:
	from scipy.io import savemat
	path = prime_path+'\\Density'+sim_name+'Density'
	mdict = {'rho':rho}
	savemat(path,mdict)

analysis.ResetAnalysis()
gc.collect()


#GET INTERPOLATED Q-MATRICES
Qi = FEP.GetInterpQMatrices(
sim_tree, coilno, field_sensor_name, sim_freq, normalization, sim_freq, sz, xm, ym, zm, m_qr, xm_q, ym_q, zm_q, mult, port_name)

from scipy.io import savemat
for i in range(1,9):
	for j in range(1,9):
		path = prime_path+'\\Interpolated Q Matrices'+sim_name+'InterpolatedQMatrices_pt'+str(i)+str(j)
		mdict = {'QMatrix_pt'+str(i)+str(j):Qi[:,i-1,j-1]}
		savemat(path,mdict)

# #GET POINTWISE Q-MATRICES
# [Q, LUT] = FEP.GetQMatrices(sim_tree, coilno, field_sensor_name, sim_freq, normalization, sim_freq)

# from scipy.io import savemat
# for i in range(0,8):
	# path = prime_path+'\\Point Q Matrices'+sim_name+'PointQMatrices_pt'+str(i)
	# mdict = {'QMatrix_pt'+str(i):Q[:,:,i]}
	# savemat(path,mdict)
	
# #GET Conductivity
# sig = FEP.GetSigDistribution(sim_type, rfc_descriptor, dielectric, wire_descriptor, wire_model, sim_freq)

# from scipy.io import savemat
# path = prime_path+'\\Conductivity'+sim_name+'Conductivity'
# mdict = {'sig':sig}
# savemat(path,mdict)

# #GET SAR DISTR
# for cn in range(1,coilno+1):
	# SAR = FEP.GetSARDistribution(sim_type,rfc_descriptor, cn, dielectric, wire_descriptor, wire_model, sim_freq)
	
	# from scipy.io import savemat
	# path = prime_path+'\\SAR'+sim_name+'SAR_C'+str(cn)
	# mdict = {'SAR_'+str(cn):SAR}
	# savemat(path,mdict)	
	
#GET SAR DISTR
SAR = FEP.GetSARDistribution(sim_tree, field_sensor_name, sim_freq, normalization, mult, port_name)

if interpolate:
	rhoi = interpn((zm,ym,xm), numpy.reshape(rho, sz), m_qr, method='nearest')

	from scipy.io import savemat
	path = prime_path+'\\SAR'+sim_name+'SAR_C'+str(cn)
	mdict = {'SAR_'+str(cn):SAR}
	savemat(path,mdict)

else:
	from scipy.io import savemat
	path = prime_path+'\\SAR'+sim_name+'SAR_C'+str(cn)
	mdict = {'SAR_'+str(cn):rho}
	savemat(path,mdict)

analysis.ResetAnalysis()
gc.collect()





# import matplotlib.pyplot as plt
# plt.plot(numpy.transpose(abs(J)))
# plt.ylabel('EM I(A)')
# plt.legend(['1','2','3','4','5','6','7','8'])
# plt.show()
