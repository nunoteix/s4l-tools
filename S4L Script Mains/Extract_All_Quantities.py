import numpy
import s4l_v1.analysis as analysis
import FieldExtractorPackage as FEP
from scipy.interpolate import interpn
import gc
reload(FEP)

sim_type = ''#'Series Matched ' #'Resonance Finder ' #'Huygens '#''#'Main ' #
dielectric = 'Phantom' #'Duke'#
rfc_descriptor = 'PW' #'FP' #'BC' #'SC' #'LC'#  
wire_descriptor =  'Test'# 'Realistic' #'Straight'##'' # 
wire_model = '_InsuFine2' #'_1.75RInsu_ThinCore' # ''#''#'_HighRes'#'_CoreFine0.2' #'_ThickInsu'#''# '_ThickTip' #'_Thick' #'_Thin' #'_ThinError' #'_ThickTipEdge'#'_ThickTipIn' #   
csens_extra = ''#
extra = ''#'_Capacitor' #'_Inductor'#


coilno = 1 # 8  # 
sensorno = 15 # 36 # 
sim_freq = 64e6 #63.882e6 # 

# prime_path = 'G:\\Huygens Simulations\\'+dielectric+'\\Extracted Results'
prime_path = 'G:\\Wire Voxelization\\'+dielectric+'\\Extracted Results'
# prime_path = 'G:\\FakePort Simulations\\'+dielectric+'\\Extracted Results'

# sim_name = '\\RapidCoils_'+dielectric+'_Guidewire'+wire_model+'_2020_02_12_'
# sim_name = '\\RapidCoils_'+dielectric+'_'+wire_model+'_'
sim_name = '\\PlaneWave_'+dielectric+'_Testwire'+wire_model+'_'
# sim_name = '\\BodyCoil_'+dielectric+'_Guidewire'+wire_model+extra+'_2020_07_28_'
# sim_name = '\\FakePortSeriesMatchingStep1_'+dielectric+'_Guidewire'+wire_model+extra+'_2020_08_04_'


interpolate = True
#GET CURRENTS
J = numpy.zeros((coilno, sensorno), dtype = 'complex')
Axis = numpy.zeros((3,sensorno))

for cn in range(1, coilno+1):
	for sn in range(1,sensorno+1):
		[oJ, x, y, z] = FEP.GetCurrentFromSensors(sim_type, rfc_descriptor, sensorno, cn, dielectric, wire_descriptor, wire_model, csens_extra, extra, sn, sim_freq)
		if cn == 1:
			Axis[:,sn-1] = [x,y,z]
		J[cn-1,sn-1] = oJ
		analysis.ResetAnalysis()

from scipy.io import savemat
path = prime_path+'\\EM Current'+sim_name+'EMCurrent'
mdict = {'J':J, 'Axes':Axis}
savemat(path,mdict)

#GET GRID
[xaxis, yaxis, zaxis, xm, ym, zm, m_qr, xm_q, ym_q, zm_q] = FEP.GetGrid(
sim_type, rfc_descriptor, dielectric, wire_descriptor, wire_model, extra, sim_freq, 0.001, 0.001, 0.001)

sz = [zaxis.shape[0]-1, yaxis.shape[0]-1, xaxis.shape[0]-1]

from scipy.io import savemat
path = prime_path+'\\Grid'+sim_name+'Axes'
mdict = {'XAxis':xm_q, 'YAxis':ym_q, 'ZAxis':zm_q}
savemat(path,mdict)	

# GET E-Field
for cn in range(1,coilno+1):
# cn = 8
# try:
	E = FEP.GetEFields(sim_type,rfc_descriptor, cn, dielectric, wire_descriptor, wire_model, extra, sim_freq)
	# if cn == 1:
		# Eall = numpy.zeros(list(E.shape)+[8], 'complex')
	# Eall[:,:,cn-1] = E
	
	if interpolate:
		# Ex = interpn((xm,ym,zm), numpy.reshape(E[:,0], sz), m_qr)
		# Ey = interpn((xm,ym,zm), numpy.reshape(E[:,1], sz), m_qr)
		# Ez = interpn((xm,ym,zm), numpy.reshape(E[:,2], sz), m_qr)
		Ei = numpy.c_[interpn((zm,ym,xm), numpy.reshape(E[:,0], sz), m_qr, method='nearest'),
		interpn((zm,ym,xm), numpy.reshape(E[:,1], sz), m_qr, method='nearest'),
		interpn((zm,ym,xm), numpy.reshape(E[:,2], sz), m_qr, method='nearest')]
		
		from scipy.io import savemat
		path = prime_path+'\\E Field'+sim_name+'EFields_C'+str(cn)
		mdict = {'E_'+str(cn):Ei}
		savemat(path,mdict)
	else:
		from scipy.io import savemat
		path = prime_path+'\\E Field'+sim_name+'EFields_C'+str(cn)
		mdict = {'E_'+str(cn):E}
		savemat(path,mdict)
		
	analysis.ResetAnalysis()
	gc.collect()

#GET B1+ FIELDS
for cn in range(1,coilno+1):

	B1 = FEP.GetB1Fields(sim_type,rfc_descriptor, cn, dielectric, wire_descriptor, wire_model, extra, sim_freq)
	# if cn == 1:
		# B1all = numpy.zeros(list(B1.shape)+[8], 'complex')
	# B1all[:,:,cn-1] = B1
	if interpolate:
		# B1p = interpn((xm,ym,zm), numpy.reshape(B1[:,0], sz), m_qr)
		# B1m = interpn((xm,ym,zm), numpy.reshape(B1[:,1], sz), m_qr)
		B1i = numpy.c_[interpn((zm,ym,xm), numpy.reshape(B1[:,0], sz), m_qr, method='nearest'),
		interpn((zm,ym,xm), numpy.reshape(B1[:,1], sz), m_qr, method='nearest')]
		
		from scipy.io import savemat
		path = prime_path+'\\B1 Field'+sim_name+'B1Fields_C'+str(cn)
		mdict = {'B1_'+str(cn):B1i}
		savemat(path,mdict)	
	else:
		from scipy.io import savemat
		path = prime_path+'\\B1 Field'+sim_name+'B1Fields_C'+str(cn)
		mdict = {'B1_'+str(cn):B1}
		savemat(path,mdict)
		
	analysis.ResetAnalysis()
	gc.collect()
	
# except NameError:
		# cn = None

#GET DENSITY
rho = FEP.GetRhoDistribution(sim_type, rfc_descriptor, dielectric, wire_descriptor, wire_model, extra, sim_freq)

if interpolate:
	rhoi = interpn((zm,ym,xm), numpy.reshape(rho, sz), m_qr, method='nearest')

	from scipy.io import savemat
	path = prime_path+'\\Density'+sim_name+'Density'
	mdict = {'rho':rhoi}
	savemat(path,mdict)

else:
	from scipy.io import savemat
	path = prime_path+'\\Density'+sim_name+'Density'
	mdict = {'rho':rho}
	savemat(path,mdict)

analysis.ResetAnalysis()
gc.collect()


#GET INTERPOLATED Q-MATRICES
Qi = FEP.GetInterpQMatrices(
sim_type, rfc_descriptor, coilno, dielectric, wire_descriptor, wire_model, extra, sim_freq, sz, xm, ym, zm, m_qr, xm_q, ym_q, zm_q)

from scipy.io import savemat
for i in range(1,9):
	for j in range(1,9):
		path = prime_path+'\\Interpolated Q Matrices'+sim_name+'InterpolatedQMatrices_pt'+str(i)+str(j)
		mdict = {'QMatrix_pt'+str(i)+str(j):Qi[:,i-1,j-1]}
		savemat(path,mdict)

# #GET POINTWISE Q-MATRICES
# [Q, LUT] = FEP.GetQMatrices(sim_type, rfc_descriptor, coilno, dielectric, wire_descriptor, wire_model, sim_freq)

# from scipy.io import savemat
# for i in range(0,8):
	# path = prime_path+'\\Point Q Matrices'+sim_name+'PointQMatrices_pt'+str(i)
	# mdict = {'QMatrix_pt'+str(i):Q[:,:,i]}
	# savemat(path,mdict)
	
# #GET Conductivity
# sig = FEP.GetSigDistribution(sim_type, rfc_descriptor, dielectric, wire_descriptor, wire_model, sim_freq)

# from scipy.io import savemat
# path = prime_path+'\\Conductivity'+sim_name+'Conductivity'
# mdict = {'sig':sig}
# savemat(path,mdict)

# #GET SAR DISTR
# for cn in range(1,coilno+1):
	# SAR = FEP.GetSARDistribution(sim_type,rfc_descriptor, cn, dielectric, wire_descriptor, wire_model, sim_freq)
	
	# from scipy.io import savemat
	# path = prime_path+'\\SAR'+sim_name+'SAR_C'+str(cn)
	# mdict = {'SAR_'+str(cn):SAR}
	# savemat(path,mdict)	
	





# import matplotlib.pyplot as plt
# plt.plot(numpy.transpose(abs(J)))
# plt.ylabel('EM I(A)')
# plt.legend(['1','2','3','4','5','6','7','8'])
# plt.show()
