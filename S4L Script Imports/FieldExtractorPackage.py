def GetCurrentFromSensors(sim_type, rfc_descriptor, sensorno, cn, dielectric, wire_descriptor, wire_model, csens_extra, extra, sn, sim_freq):
	import numpy
	import s4l_v1.analysis as analysis
	import s4l_v1.document as document
	import s4l_v1.model as model
	import s4l_v1.units as units
	from s4l_v1 import ReleaseVersion
	from s4l_v1 import Unit
	import gc
	
	
	# Define the version to use for default values
	ReleaseVersion.set_active(ReleaseVersion.version4_4)
	
	# Creating the analysis pipeline
	# Adding a new SimulationExtractor
	simulation = document.AllSimulations[sim_type+rfc_descriptor+" Channel "+str(cn)+" "+dielectric+wire_model+extra]
	simulation_extractor = simulation.Results()
	
	if wire_descriptor == 'Straight':
		wire_initial_letter = 'S'
	elif wire_descriptor == 'Realistic':
		wire_initial_letter = 'R'
	elif wire_descriptor == 'Test':
		wire_initial_letter = 'T'
	elif wire_descriptor == 'AWire':
		wire_initial_letter = 'A'
	elif wire_descriptor == 'BWire':
		wire_initial_letter = 'B'
	
	# if sn < 10:
		# sensor = model.AllEntities()[wire_descriptor+" C Sensor 0"+str(sn)]
		# em_sensor_extractor = simulation_extractor[wire_descriptor+" C Sensor 0"+str(sn)+"  (Current Sensors "+wire_initial_letter+")"]
		
		# sensor = model.AllEntities()["Current Sensors "+wire_model+" "+wire_descriptor]
		# em_sensor_extractor = simulation_extractor[wire_descriptor+" C Sensor 0"+str(sn)+"  (Current Sensors "+wire_model+" "+wire_initial_letter+")"]
	# else:
		# sensor = model.AllEntities()[wire_descriptor+" C Sensor "+str(sn)]
		# em_sensor_extractor = simulation_extractor[wire_descriptor+" C Sensor "+str(sn)+"  (Current Sensors "+wire_initial_letter+")"]
	
		# ensor = model.AllEntities()["Current Sensors "+wire_model+" "+wire_descriptor]
		# em_sensor_extractor = simulation_extractor[wire_descriptor+" C Sensor "+str(sn)+"  (Current Sensors "+wire_model+" "+wire_initial_letter+")"]
	# Quick Fixed for Angled Wire
	# if sn < 10:
		# sensor = model.AllEntities()[wire_descriptor+" C Sensor 0"+str(sn)+' 1']
		# em_sensor_extractor = simulation_extractor[wire_descriptor+" C Sensor 0"+str(sn)+" 1  (Current Sensors "+wire_initial_letter+" 1)"]
	# else:
		# sensor = model.AllEntities()[wire_descriptor+" C Sensor "+str(sn)+' 1']
		# em_sensor_extractor = simulation_extractor[wire_descriptor+" C Sensor "+str(sn)+" 1  (Current Sensors "+wire_initial_letter+" 1)"]
	
	sensor = model.AllEntities()["Current Sensors"+csens_extra+" "+wire_initial_letter]
	sensor_name = sensor.Entities[sensorno-sn].Name
	em_sensor_extractor = simulation_extractor[sensor_name+"  (Current Sensors"+csens_extra+" "+wire_initial_letter+")"]

	# pos = sensor.Properties.Transformation.Children[2].Value
	pos = sensor.Entities[sensorno-sn].Properties.Transformation.Children[2].Value
	
	x = pos[0]
	y = pos[1]
	z = pos[2]
	

	em_sensor_extractor.Normalization.Normalize = True
	em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)
	
	inputs = [em_sensor_extractor.Outputs["EM I(f)"]]
	plot_viewer = analysis.viewers.PlotViewer(inputs=inputs)
	# plot_viewer.Plotter.ComplexOptions = plot_viewer.Plotter.ComplexOptions.enum.Mag
	# plot_viewer.Visible = True
	plot_viewer.UpdateAttributes()
	document.AllAlgorithms.Add(plot_viewer)
	# em_sensor_extractor.UpdateAttributes()
	plot_viewer.Update()
	
	# inputs = em_sensor_extractor.Outputs["EM I(f)"]
	a = inputs[0]
	# a.Update()
	# a.UpdateAttributes()
	# JData = a.Data.GetComponent(0)[freq_ind]
	JFreqs = a.Data.Axis

	freq_ind = JFreqs.searchsorted(sim_freq)
	# J = JData[freq_ind]	
	J = a.Data.GetComponent(0)[freq_ind]
	
	plot_viewer.Visible = False
	
	return J, x, y, z


def GetEFields(sim_type, rfc_descriptor, cn, dielectric, wire_descriptor, wire_model, extra, sim_freq):
	import numpy
	import s4l_v1.analysis as analysis
	import s4l_v1.document as document
	import s4l_v1.model as model
	import s4l_v1.units as units
	from s4l_v1 import ReleaseVersion
	from s4l_v1 import Unit
	
	
	# Define the version to use for default values
	ReleaseVersion.set_active(ReleaseVersion.version4_4)
	
	# Creating the analysis pipeline
	# Adding a new SimulationExtractor
	simulation = document.AllSimulations[sim_type+rfc_descriptor+" Channel "+str(cn)+" "+dielectric+wire_model]
	simulation_extractor = simulation.Results()

	
	em_sensor_extractor = simulation_extractor['Huygens '+'SensBox '+dielectric] 
	em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
	em_sensor_extractor.Normalization.Normalize = True
	########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)
	
	# Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
	E = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]
	E.Update()
	E_data = E.Data
	E_data_field = E_data.Field(0)
	
	return E_data_field
	
	
def GetGrid(sim_type, rfc_descriptor, dielectric, wire_descriptor, wire_model, extra, sim_freq, xstep, ystep, zstep):
	import numpy
	import s4l_v1.analysis as analysis
	import s4l_v1.document as document
	import s4l_v1.model as model
	import s4l_v1.units as units
	from s4l_v1 import ReleaseVersion
	from s4l_v1 import Unit
	import gc
	
	
	# Define the version to use for default values
	ReleaseVersion.set_active(ReleaseVersion.version4_4)
	
	# Creating the analysis pipeline
	# Adding a new SimulationExtractor
	simulation = document.AllSimulations[sim_type+rfc_descriptor+" Channel 1 "+dielectric+wire_model]
	simulation_extractor = simulation.Results()

	
	em_sensor_extractor = simulation_extractor['Huygens '+'SensBox '+dielectric] 
	em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
	em_sensor_extractor.Normalization.Normalize = True
	########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)
	
	# Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
	E = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]
	E.Update()
	E_data = E.Data
	xaxis = E_data.Grid.XAxis
	yaxis = E_data.Grid.YAxis
	zaxis = E_data.Grid.ZAxis
	
	xm = (xaxis[0:-1]+xaxis[1:])/2
	ym = (yaxis[0:-1]+yaxis[1:])/2
	zm = (zaxis[0:-1]+zaxis[1:])/2

	xm_q = numpy.arange(xm[0], xm[-1], xstep)
	ym_q = numpy.arange(ym[0], ym[-1], ystep)
	zm_q = numpy.arange(zm[0], zm[-1], zstep)
	m_q = numpy.meshgrid(zm_q, ym_q, xm_q)
	m_qr = numpy.reshape(m_q, [3]+[zm_q.size*ym_q.size*xm_q.size]).transpose()
	
	analysis.ResetAnalysis()
	gc.collect()
	
	return xaxis, yaxis, zaxis, xm, ym, zm, m_qr, xm_q, ym_q, zm_q

	
def GetB1Fields(sim_type, rfc_descriptor, cn, dielectric, wire_descriptor, wire_model, extra, sim_freq):
	import numpy
	import s4l_v1.analysis as analysis
	import s4l_v1.document as document
	import s4l_v1.model as model
	import s4l_v1.units as units
	from s4l_v1 import ReleaseVersion
	from s4l_v1 import Unit
	import gc
	
	
	# Define the version to use for default values
	ReleaseVersion.set_active(ReleaseVersion.version4_4)
	
	# Creating the analysis pipeline
	# Adding a new SimulationExtractor
	simulation = document.AllSimulations[sim_type+rfc_descriptor+" Channel "+str(cn)+" "+dielectric+wire_model]
	simulation_extractor = simulation.Results()

	
	em_sensor_extractor = simulation_extractor[sim_type+'SensBox '+dielectric] 
	em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
	em_sensor_extractor.Normalization.Normalize = True
	########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)
	
	# Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
	B1 = em_sensor_extractor.Outputs["B1(x,y,z,f0)"]
	B1.Update()
	B1_data = B1.Data
	B1_data_field = B1_data.Field(0)
	
	return B1_data_field	

	
def GetRhoDistribution(sim_type, rfc_descriptor, dielectric, wire_descriptor, wire_model, extra, sim_freq):
	import numpy
	import s4l_v1.analysis as analysis
	import s4l_v1.document as document
	import s4l_v1.model as model
	import s4l_v1.units as units
	from s4l_v1 import ReleaseVersion
	from s4l_v1 import Unit
	import gc
	
	
	# Define the version to use for default values
	ReleaseVersion.set_active(ReleaseVersion.version4_4)
	
	# Creating the analysis pipeline
	# Adding a new SimulationExtractor
	simulation = document.AllSimulations[sim_type+rfc_descriptor+" Channel 1 "+dielectric+wire_model]
	simulation_extractor = simulation.Results()

	
	em_sensor_extractor = simulation_extractor[sim_type+'SensBox '+dielectric] 
	em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
	em_sensor_extractor.Normalization.Normalize = True
	########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)
	
	# Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
	SAR = em_sensor_extractor.Outputs["SAR(x,y,z,f0)"]
	ELD = em_sensor_extractor.Outputs["El. Loss Density(x,y,z,f0)"]
	
	SAR.Update()
	ELD.Update()
	
	SARData = SAR.Data
	ELDData = ELD.Data

	SARDist = SARData.Field(0)
	ELDDist = ELDData.Field(0)
	
	rho = ELDDist/SARDist
	rho = [numpy.asscalar(rho[x]) if numpy.isnan(rho[x]) == False else 0 for x in range(len(rho))]

	analysis.ResetAnalysis()
	gc.collect()
	
	return rho


def GetSigDistribution(sim_type, rfc_descriptor, dielectric, wire_descriptor, wire_model, extra, sim_freq):
	import numpy
	import s4l_v1.analysis as analysis
	import s4l_v1.document as document
	import s4l_v1.model as model
	import s4l_v1.units as units
	from s4l_v1 import ReleaseVersion
	from s4l_v1 import Unit
	import gc
	
	
	# Define the version to use for default values
	ReleaseVersion.set_active(ReleaseVersion.version4_4)
	
	# Creating the analysis pipeline
	# Adding a new SimulationExtractor
	simulation = document.AllSimulations[sim_type+rfc_descriptor+" Channel 1 "+dielectric+wire_model]
	simulation_extractor = simulation.Results()

	
	em_sensor_extractor = simulation_extractor[sim_type+'SensBox '+dielectric] 
	em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
	em_sensor_extractor.Normalization.Normalize = True
	########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)
	
	# Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
	ELD = em_sensor_extractor.Outputs["El. Loss Density(x,y,z,f0)"]
	E = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]

	ELD.Update()
	E.Update()
	
	ELDData = ELD.Data
	EData= E.Data
	
	EField= EData.Field(0)
	
	Ex = EField[:,0]
	Ey = EField[:,1]
	Ez = EField[:,2]
	
	Eabs = (numpy.abs(numpy.sqrt(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj()))).flatten()
	sig = 2*(ELDData.Field(0)).flatten()/(numpy.power(Eabs,2))
	
	analysis.ResetAnalysis()
	gc.collect()
	
	return sig


def GetSARDistribution(sim_type, rfc_descriptor, cn, dielectric, wire_descriptor, wire_model, extra, sim_freq):
	import numpy
	import s4l_v1.analysis as analysis
	import s4l_v1.document as document
	import s4l_v1.model as model
	import s4l_v1.units as units
	from s4l_v1 import ReleaseVersion
	from s4l_v1 import Unit
	
	
	# Define the version to use for default values
	ReleaseVersion.set_active(ReleaseVersion.version4_4)
	
	# Creating the analysis pipeline
	# Adding a new SimulationExtractor
	simulation = document.AllSimulations[sim_type+rfc_descriptor+" Channel "+str(cn)+" "+dielectric+wire_model]
	simulation_extractor = simulation.Results()

	
	em_sensor_extractor = simulation_extractor[sim_type+'SensBox '+dielectric] 
	em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
	em_sensor_extractor.Normalization.Normalize = True
	########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
	document.AllAlgorithms.Add(em_sensor_extractor)
	
	# Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
	SAR = em_sensor_extractor.Outputs["SAR(x,y,z,f0)"]
	SAR.Update()
	SAR_data = SAR.Data
	SAR_data_field = SAR_data.Field(0)
	
	return SAR_data_field	
	
	
def GetQMatrices(sim_type, rfc_descriptor, coilno, dielectric, wire_descriptor, wire_model, extra, sim_freq):
	import numpy
	import s4l_v1.analysis as analysis
	import s4l_v1.document as document
	import s4l_v1.model as model
	import s4l_v1.units as units
	from s4l_v1 import ReleaseVersion
	from s4l_v1 import Unit
	
	# Define the version to use for default values
	ReleaseVersion.set_active(ReleaseVersion.version4_4)
	
	# Creating the analysis pipeline
	# Adding a new SimulationExtractor
	for cn in range(1,coilno+1):
		simulation = document.AllSimulations[sim_type+rfc_descriptor+" Channel "+str(cn)+" "+dielectric+wire_model]
		simulation_extractor = simulation.Results()
		
		em_sensor_extractor = simulation_extractor[sim_type+'SensBox '+dielectric] 
		em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
		em_sensor_extractor.Normalization.Normalize = True
		########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
		document.AllAlgorithms.Add(em_sensor_extractor)
		
		# Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
		E = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]
		E.Update()
		EData = E.Data
		EField = EData.Field(0)
		
		if cn == 1:
			Eall = numpy.zeros(list(EField.shape)+[8], 'complex')
		Eall[:,:,cn-1] = EField
		print(['Loaded E-Field from Coil #'+str(cn)])
	
	
	SAR = em_sensor_extractor.Outputs["SAR(x,y,z,f0)"]
	ELD = em_sensor_extractor.Outputs["El. Loss Density(x,y,z,f0)"]
	
	SAR.Update()
	ELD.Update()

	SARData = SAR.Data
	ELDData = ELD.Data
	
	SARDist = SARData.Field(0)
	ELDDist = ELDData.Field(0)
	
	rho = ELDDist/SARDist
	rho = numpy.asarray([rho[x] if numpy.isnan(rho[x]) == False else 0 for x in range(len(rho))])
	
	
	Ex = EField[:,0]
	Ey = EField[:,1]
	Ez = EField[:,2]
	Eabs = (numpy.abs(numpy.sqrt(Ex*Ex.conj()+Ey*Ey.conj()+Ez*Ez.conj())))
	sig = 2*(numpy.squeeze(ELDDist))/(numpy.power(Eabs,2))
	sig = numpy.asarray([sig[x] if numpy.isnan(sig[x]) == False else 0 for x in range(len(sig))])
	
	print('Starting to build Q-Matrices...')
	
	Q = numpy.zeros((list(sig.shape)+[8]+[8]),'complex')
	idx = 1
	LUT = []
	for i in range(Eall.shape[0]):
		if sig[i] != 0.0 and ~numpy.isnan(sig[i]):
			e = Eall[i]
			Q[i] = (sig[i]/(2*rho[i]))*e.conj().transpose().dot(e)#*vol[i]   (sig[i]/(2*rho[i]))
			LUT.append(i)
			del e
	
	LUT = numpy.asarray(LUT)
	return Q, LUT
	

def GetInterpQMatrices(sim_type, rfc_descriptor, coilno, dielectric, wire_descriptor, wire_model, extra, sim_freq, sz, xm, ym, zm, m_qr, xm_q, ym_q, zm_q):
	import numpy
	import s4l_v1.analysis as analysis
	import s4l_v1.document as document
	import s4l_v1.model as model
	import s4l_v1.units as units
	from s4l_v1 import ReleaseVersion
	from s4l_v1 import Unit
	from scipy.interpolate import interpn
	import gc
	
	# Define the version to use for default values
	ReleaseVersion.set_active(ReleaseVersion.version4_4)
	
	# Creating the analysis pipeline
	# Adding a new SimulationExtractor
	for cn in range(1,coilno+1):
		simulation = document.AllSimulations[sim_type+rfc_descriptor+" Channel "+str(cn)+" "+dielectric+wire_model]
		simulation_extractor = simulation.Results()
			
		em_sensor_extractor = simulation_extractor[sim_type+'SensBox '+dielectric] 
		em_sensor_extractor.FrequencySettings.ExtractedFrequency = u"All"
		em_sensor_extractor.Normalization.Normalize = True
			########## # em_sensor_extractor.Normalization.AvailableReferences = u"EM Input Power(f)"
		document.AllAlgorithms.Add(em_sensor_extractor)
			
			# Adding a new SurfaceViewer for Matrix Quantities (E,B,B1,H,etc...)	
		E = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]
		E.Update()
		EField = E.Data.Field(0)
			# print(['Loaded E-Field from Coil #'+str(cn)])
			
		Ei = numpy.c_[interpn((zm,ym,xm), numpy.reshape(EField[:,0], sz), m_qr, method = 'nearest'),
		interpn((zm,ym,xm), numpy.reshape(EField[:,1], sz), m_qr, method = 'nearest'),
		interpn((zm,ym,xm), numpy.reshape(EField[:,2], sz), m_qr, method = 'nearest')]
			
		if cn == 1:
			Eall = numpy.zeros(list(Ei.shape)+[8], 'complex')
		Eall[:,:,cn-1] = Ei
			
			# print(['Interpolated E-Field from Coil #'+str(cn)])
		analysis.ResetAnalysis()
		gc.collect()
		
		
	SAR = em_sensor_extractor.Outputs["SAR(x,y,z,f0)"]
	ELD = em_sensor_extractor.Outputs["El. Loss Density(x,y,z,f0)"]
	E = em_sensor_extractor.Outputs["EM E(x,y,z,f0)"]

	SAR.Update()
	ELD.Update()
	E.Update()
		
	SARDist = SAR.Data.Field(0)
	ELDDist = ELD.Data.Field(0)
	EField = E.Data.Field(0)

	rho = ELDDist/SARDist
	rho = numpy.asarray([rho[x] if numpy.isnan(rho[x]) == False else 0 for x in range(len(rho))])
	rhoi = interpn((zm,ym,xm), numpy.reshape(rho, sz), m_qr, method = 'nearest')

	Eabs = (numpy.abs(numpy.sqrt(EField[:,0]*EField[:,0].conj()+EField[:,1]*EField[:,1].conj()+EField[:,2]*EField[:,2].conj())))
	sig = 2*(numpy.squeeze(ELDDist))/(numpy.power(Eabs,2))
	sig = numpy.asarray([sig[x] if numpy.isnan(sig[x]) == False else 0 for x in range(len(sig))])
	sigi = interpn((zm,ym,xm), numpy.reshape(sig, sz), m_qr, method = 'nearest')


	analysis.ResetAnalysis()
	gc.collect()
		# print('Starting to build Q-Matrices...')
		
	Q_x = numpy.zeros((list(sigi.shape)+[8]+[8]),'complex')
		# Q_r = numpy.zeros([xm_q.size]+[ym_q.size]+[zm_q.size]+[coilno]+[coilno], 'complex')

	for i in range(sigi.shape[0]):
		e = Eall[i]
		e_star = e.conj().transpose()
		if sigi[i] != 0.0 and ~numpy.isnan(sigi[i]):
			Q_x[i] = (sigi[i]/(2*rhoi[i]))*e_star.dot(e)

	return Q_x
