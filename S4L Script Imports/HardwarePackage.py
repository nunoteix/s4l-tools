##############################################################
#
# IMPORTANT: READ BEFORE CHANGING ANYTHING
#
#
# ONLY CHANGE THIS SCRIPT IF OPENED FROM IMPORT IN SIM4LIFE
#
#
# DO NOT CHANGE ANYTHING IF OPENED FROM DROPBOX BACKUP
#
#
##############################################################


import s4l_v1.document as document
import s4l_v1.model as model
from s4l_v1 import Vec3
import s4l_v1.simulation.emfdtd as fdtd
import s4l_v1.units as units
import numpy

def CreateRFShield(rfs_geo):
	rf_shield = model.CreateSolidTube(Vec3(0,0,-rfs_geo[1]/2.0),Vec3(0,0,rfs_geo[1]),rfs_geo[0]+rfs_geo[2],rfs_geo[0])
	rf_shield.Name = 'RFS'
	return rf_shield
	
def CreateLoopCoil(rfc_geo):
	exts = rfc_geo[2]
	
	leg1 = model.CreateSolidBlock(Vec3(-rfc_geo[0]/2, 0, -rfc_geo[1]/2), Vec3(-rfc_geo[0]/2 + 5, 0, rfc_geo[1]/2), False)
	leg2 = model.CreateSolidBlock(Vec3(rfc_geo[0]/2, 0, -rfc_geo[1]/2), Vec3(rfc_geo[0]/2 - 5, 0, rfc_geo[1]/2), False)
	arm1 = model.CreateSolidBlock(Vec3(-rfc_geo[0]/2, 0, -rfc_geo[1]/2), Vec3(rfc_geo[0]/2, 0, -rfc_geo[1]/2 + 5), False)
	arm2 = model.CreateSolidBlock(Vec3(-rfc_geo[0]/2, 0, rfc_geo[1]/2), Vec3(rfc_geo[0]/2, 0, rfc_geo[1]/2 - 5), False)
	rect = model.Unite([leg1, leg2, arm1, arm2])

	capgap1 = model.CreateSolidBlock(Vec3(-1, 0, rfc_geo[1]/2), Vec3(+1, 0, rfc_geo[1]/2 - 5), False)
	capgap2 = model.CreateSolidBlock(Vec3(-1, 0, -rfc_geo[1]/2), Vec3(+1, 0, -rfc_geo[1]/2 + 5), False)
	capgap3 = model.CreateSolidBlock(Vec3(-rfc_geo[0]/2, 0, -1), Vec3(-rfc_geo[0]/2 + 5, 0, +1), False)
	capgap4 = model.CreateSolidBlock(Vec3(+rfc_geo[0]/2, 0, -1), Vec3(+rfc_geo[0]/2 - 5, 0, +1), False)

	# Create 8 Loop Coils

	struct1 = model.Subtract([rect, capgap1, capgap2, capgap3, capgap4], False); struct1.Name = exts+'Loop1'
	P1 = model.CreatePolyLine([Vec3(-1, 0, rfc_geo[1]/2 - 1.25), Vec3(+1, 0, rfc_geo[1]/2 - 1.25)]); P1.Name = 'A'+exts+'1'
	LE101 = model.CreatePolyLine([Vec3(-1, 0, rfc_geo[1]/2 - 3.75), Vec3(+1, 0, rfc_geo[1]/2 - 3.75)]); LE101.Name = exts+'LE011'
	LE102 = model.CreatePolyLine([Vec3(+rfc_geo[0]/2 - 2.5, 0, +1), Vec3(+rfc_geo[0]/2 - 2.5, 0, -1)]); LE102.Name = exts+'LE021'
	LE103 = model.CreatePolyLine([Vec3(-1, 0, -rfc_geo[1]/2 + 2.5), Vec3(+1, 0, -rfc_geo[1]/2 + 2.5)]); LE103.Name = exts+'LE031'
	LE104 = model.CreatePolyLine([Vec3(-rfc_geo[0]/2 + 2.5, 0, -1), Vec3(-rfc_geo[0]/2 + 2.5, 0, +1)]); LE104.Name = exts+'LE041'
	coil1 = model.Unite([struct1, P1, LE101, LE102, LE103, LE104], True)
	coil1.Name = 'Circuit 1 '+exts

	struct2 = struct1.Clone(); struct2.Name = exts+'Loop2'
	P2 = model.CreatePolyLine([Vec3(-1, 0, rfc_geo[1]/2 - 1.25), Vec3(+1, 0, rfc_geo[1]/2 - 1.25)]); P2.Name = 'A'+exts+'2'
	LE201 = model.CreatePolyLine([Vec3(-1, 0, rfc_geo[1]/2 - 3.75), Vec3(+1, 0, rfc_geo[1]/2 - 3.75)]); LE201.Name = exts+'LE012'
	LE202 = model.CreatePolyLine([Vec3(+rfc_geo[0]/2 - 2.5, 0, +1), Vec3(+rfc_geo[0]/2 - 2.5, 0, -1)]); LE202.Name = exts+'LE022'
	LE203 = model.CreatePolyLine([Vec3(-1, 0, -rfc_geo[1]/2 + 2.5), Vec3(+1, 0, -rfc_geo[1]/2 + 2.5)]); LE203.Name = exts+'LE032'
	LE204 = model.CreatePolyLine([Vec3(-rfc_geo[0]/2 + 2.5, 0, -1), Vec3(-rfc_geo[0]/2 + 2.5, 0, +1)]); LE204.Name = exts+'LE042'
	coil2 = model.Unite([struct2, P2, LE201, LE202, LE203, LE204], True)
	coil2.Name = 'Circuit 2 '+exts

	struct3 = struct1.Clone(); struct3.Name = exts+'Loop3'
	P3 = model.CreatePolyLine([Vec3(-1, 0, rfc_geo[1]/2 - 1.25), Vec3(+1, 0, rfc_geo[1]/2 - 1.25)]); P3.Name = 'A'+exts+'3'
	LE301 = model.CreatePolyLine([Vec3(-1, 0, rfc_geo[1]/2 - 3.75), Vec3(+1, 0, rfc_geo[1]/2 - 3.75)]); LE301.Name = exts+'LE013'
	LE302 = model.CreatePolyLine([Vec3(+rfc_geo[0]/2 - 2.5, 0, +1), Vec3(+rfc_geo[0]/2 - 2.5, 0, -1)]); LE302.Name = exts+'LE023'
	LE303 = model.CreatePolyLine([Vec3(-1, 0, -rfc_geo[1]/2 + 2.5), Vec3(+1, 0, -rfc_geo[1]/2 + 2.5)]); LE303.Name = exts+'LE033'
	LE304 = model.CreatePolyLine([Vec3(-rfc_geo[0]/2 + 2.5, 0, -1), Vec3(-rfc_geo[0]/2 + 2.5, 0, +1)]); LE304.Name = exts+'LE043'
	coil3 = model.Unite([struct3, P3, LE301, LE302, LE303, LE304], True)
	coil3.Name = 'Circuit 3 '+exts

	struct4 = struct1.Clone(); struct4.Name = exts+'Loop4'
	P4 = P1.Clone(); P4.Name = 'A'+exts+'4'
	LE401 = LE101.Clone(); LE401.Name = exts+'LE014'
	LE402 = LE102.Clone(); LE402.Name = exts+'LE024'
	LE403 = LE103.Clone(); LE403.Name = exts+'LE034'
	LE404 = LE104.Clone(); LE404.Name = exts+'LE044'
	coil4 = model.Unite([struct4, P4, LE401, LE402, LE403, LE404], True)
	coil4.Name = 'Circuit 4 '+exts

	struct5 = struct1.Clone(); struct5.Name = exts+'Loop5'
	P5 = P1.Clone(); P5.Name = 'A'+exts+'5'
	LE501 = LE101.Clone(); LE501.Name = exts+'LE015'
	LE502 = LE102.Clone(); LE502.Name = exts+'LE025'
	LE503 = LE103.Clone(); LE503.Name = exts+'LE035'
	LE504 = LE104.Clone(); LE504.Name = exts+'LE045'
	coil5 = model.Unite([struct5, P5, LE501, LE502, LE503, LE504], True)
	coil5.Name = 'Circuit 5 '+exts

	struct6 = struct1.Clone(); struct6.Name = exts+'Loop6'
	P6 = P1.Clone(); P6.Name = 'A'+exts+'6'
	LE601 = LE201.Clone(); LE601.Name = exts+'LE016'
	LE602 = LE202.Clone(); LE602.Name = exts+'LE026'
	LE603 = LE203.Clone(); LE603.Name = exts+'LE036'
	LE604 = LE204.Clone(); LE604.Name = exts+'LE046'
	coil6 = model.Unite([struct6, P6, LE601, LE602, LE603, LE604], True)
	coil6.Name = 'Circuit 6 '+exts

	struct7 = struct1.Clone(); struct7.Name = exts+'Loop7'
	P7 = P1.Clone(); P7.Name = 'A'+exts+'7'
	LE701 = LE301.Clone(); LE701.Name = exts+'LE017'
	LE702 = LE302.Clone(); LE702.Name = exts+'LE027'
	LE703 = LE303.Clone(); LE703.Name = exts+'LE037'
	LE704 = LE304.Clone(); LE704.Name = exts+'LE047'
	coil7 = model.Unite([struct7, P7, LE701, LE702, LE703, LE704], True)
	coil7.Name = 'Circuit 7 '+exts

	struct8 = struct1.Clone(); struct8.Name = exts+'Loop8'
	P8 = P1.Clone(); P8.Name = 'A'+exts+'8'
	LE801 = LE401.Clone(); LE801.Name = exts+'LE018'
	LE802 = LE402.Clone(); LE802.Name = exts+'LE028'
	LE803 = LE403.Clone(); LE803.Name = exts+'LE038'
	LE804 = LE404.Clone(); LE804.Name = exts+'LE048'
	coil8 = model.Unite([struct8, P8, LE801, LE802, LE803, LE804], True)
	coil8.Name = 'Circuit 8 '+exts
	
	T = rfc_geo[3]
	coil1.ApplyTransform(T[0]); coil2.ApplyTransform(T[1]); coil3.ApplyTransform(T[2]); coil4.ApplyTransform(T[3])
	coil5.ApplyTransform(T[4]); coil6.ApplyTransform(T[5]); coil7.ApplyTransform(T[6]); coil8.ApplyTransform(T[7])
	
	coil_group = model.CreateGroup(exts)
	coil_group.Add(coil1); coil_group.Add(coil2); coil_group.Add(coil3); coil_group.Add(coil4)
	coil_group.Add(coil5); coil_group.Add(coil6); coil_group.Add(coil7); coil_group.Add(coil8)
	
	return [coil1, coil2, coil3, coil4, coil5, coil6, coil7, coil8]

def CreateRectangularPhantom(p_geo):
	phantom = model.CreateSolidBlock(Vec3(-p_geo[0]/2,-p_geo[1]/2,-p_geo[2]+140), Vec3(p_geo[0]/2,p_geo[1]/2,p_geo[2]-660))
	phantom.Name = 'Phantom'
	return phantom

def CreateSensorBoxes(p_logic):
	SBox = model.CreateWireBlock(Vec3(-260,-130,435), Vec3(285,170,-1500), True)
	SBox.Name = 'Main SensBox'
		
		# box = model.CreateWireBlock(Vec3(-130,-35,+150), Vec3(150,130,-1250), True)
		# box.Name = 'Box'
	if p_logic == 'No':
		HBox = model.CreateWireBlock(Vec3(-90,-40,+110), Vec3(110,125,-1230), True)
		HBox.Name = 'Huygens Box'
		
		SHBox = HBox.Clone()
		SHBox.Name = 'Huygens SensBox'
	
	if p_logic == 'Yes':
		box = model.CreateWireBlock(Vec3(-160,-100,200), Vec3(160,100,-1675), True)
		box.Name = 'Box'