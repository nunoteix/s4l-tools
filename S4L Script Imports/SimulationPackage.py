##############################################################
#
# IMPORTANT: READ BEFORE CHANGING ANYTHING
#
#
# ONLY CHANGE THIS SCRIPT IF OPENED FROM IMPORT IN SIM4LIFE
#
#
# DO NOT CHANGE ANYTHING IF OPENED FROM DROPBOX BACKUP
#
#
##############################################################

import s4l_v1.document as document
import s4l_v1.model as model
from s4l_v1 import Vec3
import s4l_v1.simulation.emfdtd as fdtd
import s4l_v1.units as units
import numpy

def CreateBigSim(sim_data):
	ElemNo = sim_data[0]
	p_logic = sim_data[1]
	n_periods = sim_data[2]
	rfc_descriptor = sim_data[3]
	sim_freq = sim_data[4]
	sim_bw = sim_data[5]
	sim_refl = sim_data[6]
	R = sim_data[7]
	C1 = sim_data[8]
	C2 = sim_data[9]
	C3 = sim_data[10]
	C4 = sim_data[11]
	
	sim = fdtd.MultiportSimulation()
	sim.Name = rfc_descriptor+' Channel '+str(ElemNo)
	sim.SetupSettings.SimulationTime = n_periods, units.Periods
	
	options = sim.SetupSettings.GlobalAutoTermination.enum
	sim.SetupSettings.GlobalAutoTermination = options.GlobalAutoTerminationUserDefined
	#sim.SetupSettings.ConvergenceLevel = -50
	
	# Import all model elements
	entities = model.AllEntities()
	
	#Sort Entities into type
	ShieldList = []; PhantomList = []; PortList = []; LoopList = []; PECList = [];
	LE1List = []; LE2List = []; LE3List = []; LE4List = []; LEList = []; WireList = []

	# WireList.append(entities['WireCore'])
	# WireList.append(entities['WireInsulation'])
	# PECList.append(entities['WireCore'])
	
	
	for i in range(0,len(entities)):
		tmp = entities[i]
		
		if ('Phantom' in tmp.Name):
			PhantomList.append(tmp)
			
		if ('RFS' in tmp.Name):
			ShieldList.append(tmp)
			
		if (rfc_descriptor+'Loop' in tmp.Name):
			LoopList.append(tmp)
			
		if (tmp.Name == rfc_descriptor+'LE01'+str(ElemNo)):
			LE1List.append(tmp)	
			# PortList.append(tmp)
		if (tmp.Name == rfc_descriptor+'LE02'+str(ElemNo)):
			LE2List.append(tmp)
			# PortList.append(tmp)
		if (tmp.Name == rfc_descriptor+'LE03'+str(ElemNo)):
			LE3List.append(tmp)
			# PortList.append(tmp)
		if (tmp.Name == rfc_descriptor+'LE04'+str(ElemNo)):
			LE4List.append(tmp)
			# PortList.append(tmp)
			
		if (tmp.Name == 'A'+rfc_descriptor+str(ElemNo)):
			PortList.append(tmp)
	
	LEList = LE1List+LE2List+LE3List+LE4List

	# Materials
	pec_settings = sim.AddMaterialSettings(LoopList+ShieldList)
	pec_settings.Name = 'PEC' 
	options = pec_settings.MaterialType.enum
	pec_settings.MaterialType = options.PEC
	
	# Phantom
	
	if p_logic == 'Yes':
		dielectric_settings = sim.AddMaterialSettings(PhantomList)
		dielectric_settings.Name = 'Phantom'
		dielectric_settings.MaterialType = options.Dielectric
		dielectric_settings.ElectricProps.Conductivity = 0.4 # S/m
		dielectric_settings.ElectricProps.RelativePermittivity = 30
		
		manual_grid_settings = sim.AddManualGridSettings(PhantomList)
		manual_grid_settings.MaxStep = (10, )*3 # model units
		manual_grid_settings.Resolution = (3.5, )*3
		
		sim.Add(default_voxel, PhantomList)

	# Ports
	edge_port_settings = sim.AddEdgePortSettings(PortList)
	edge_port_settings.Name = 'Sources'
	options = edge_port_settings.ExcitationType.enum
	edge_port_settings.ExcitationType = options.Gaussian
	edge_port_settings.CenterFrequency = sim_freq, units.Hz
	edge_port_settings.Bandwidth = sim_bw, units.Hz
	edge_port_settings.ReferenceLoad = sim_refl, units.Ohms
	
	LE_P_settings1 = sim.AddLumpedElementSettings(LE1List)
	LE_P_settings1.Type = LE_P_settings1.Type.enum.ResistorParallelCapacitor
	LE_P_settings1.Resistance = R
	LE_P_settings1.Capacitance = C1
	
	LE_P_settings2 = sim.AddLumpedElementSettings(LE2List)
	LE_P_settings2.Type = LE_P_settings2.Type.enum.ResistorParallelCapacitor
	LE_P_settings2.Resistance = R
	LE_P_settings2.Capacitance = C2

	LE_P_settings3 = sim.AddLumpedElementSettings(LE3List)
	LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
	LE_P_settings3.Resistance = R
	LE_P_settings3.Capacitance = C3
	
	LE_P_settings4 = sim.AddLumpedElementSettings(LE4List)
	LE_P_settings4.Type = LE_P_settings4.Type.enum.ResistorParallelCapacitor
	LE_P_settings4.Resistance = R
	LE_P_settings4.Capacitance = C4


	# Sensors
	edgesensor_settings = sim.AddEdgeSensorSettings(PortList)
	options = edgesensor_settings.AutoTermination.enum
	edgesensor_settings.AutoTermination = options.AutoTerminationUseGlobal

	edgesensor_settings2 = sim.AddEdgeSensorSettings(LEList)
	options = edgesensor_settings2.AutoTermination.enum
	edgesensor_settings2.AutoTermination = options.AutoTerminationUseGlobal

	settings2 = sim.AddFieldSensorSettings(entities['Main SensBox'])
	settings2.ExtractedFrequencies = sim_freq
	
	# settings2 = sim.AddFieldSensorSettings(entities['SensBox'])
	# settings2.ExtractedFrequencies = 64e6

	# overall_field_sensor = sim.AddOverallFieldSensorSettings()
	# overall_field_sensor.ExtractedFrequencies = 64e6

	# Boundary Conditions
	global_boundary_settings = fdtd.BoundarySettings()
	global_boundary_settings.BoundaryType = global_boundary_settings.BoundaryType.enum.ABC
	global_boundary_settings.PmlStrength = global_boundary_settings.PmlStrength.enum.Medium

	sim.GlobalGridSettings.PaddingMode = sim.GlobalGridSettings.PaddingMode.enum.Manual
	sim.GlobalGridSettings.BottomPadding = sim.GlobalGridSettings.TopPadding = (100,)*3
	
	# Grid Settings
	manual_grid_settings = sim.AddManualGridSettings(ShieldList)
	manual_grid_settings.MaxStep = (15, )*3 # model units
	manual_grid_settings.Resolution = (3.5, )*3
	
	manual_grid_settings = sim.AddManualGridSettings(LoopList)
	manual_grid_settings.MaxStep = (10, )*3 # model units
	manual_grid_settings.Resolution = (0, )*3

	manual_grid_settings2 = sim.AddManualGridSettings(PortList)
	manual_grid_settings2.MaxStep = (15, )*3 # model units
	manual_grid_settings2.Resolution = (3.5, )*3

	manual_grid_settings3 = sim.AddAutomaticGridSettings(LEList)
	manual_grid_settings3.MaxStep = (15, )*3 # model units
	manual_grid_settings3.Resolution = (3.5, )*3

	# manual_grid_settings5 = sim.AddManualGridSettings(entities['Box'])
	# manual_grid_settings5.MaxStep = (15, )*3 # model units
	# manual_grid_settings5.Resolution = (3.5, )*3
	
	manual_grid_settings5 = sim.AddManualGridSettings(entities['Main SensBox'])
	manual_grid_settings5.MaxStep = (15, )*3 # model units
	manual_grid_settings5.Resolution = (3.5, )*3
	
	# Voxels
	default_voxel = next(s for s in sim.AllSettings if isinstance(s, fdtd.AutomaticVoxelerSettings) )
	sim.Add(default_voxel, ShieldList)
	sim.Add(default_voxel, LoopList)

	sim.Add(default_voxel, PortList)
	sim.Add(default_voxel, LEList)
	sim.Add(default_voxel, entities['Main SensBox'])
	# sim.Add(default_voxel, entities['Box'])	

	# Solver settings
	sim.SolverSettings.Kernel = sim.SolverSettings.Kernel.enum.Cuda

	document.AllSimulations.Add(sim)
	sim.UpdateGrid()
	
	return sim
	
def CreateHuygensSim(hsim_data):
	ElemNo = hsim_data[0]
	p_logic = hsim_data[1]
	n_periods = hsim_data[2]
	rfc_descriptor = hsim_data[3]
	sim_freq = hsim_data[4]
	sim_bw = hsim_data[5]
	wire_descriptor = hsim_data[6]
	BigS = hsim_data[7]
	
	sim = fdtd.Simulation()
	sim.Name = rfc_descriptor+' Huygens Channel '+str(ElemNo)
	sim.SetupSettings.SimulationTime = n_periods, units.Periods
	options = sim.SetupSettings.GlobalAutoTermination.enum
	sim.SetupSettings.GlobalAutoTermination = options.GlobalAutoTerminationUserDefined
	#sim.SetupSettings.ConvergenceLevel = -50
	# Import all model elements
	entities = model.AllEntities()
	
	PECList = []
	InsulationList = []
	GridList = [];
	GridList.append(entities[wire_descriptor+'WireCore']);
	GridList.append(entities[wire_descriptor+'WireInsulation'])
	DielectricList = []
	PECList.append(entities[wire_descriptor+'WireCore'])
	InsulationList.append(entities[wire_descriptor+'WireInsulation'])
	
	# Phantom
	if p_logic == 'Yes':
		DielectricList.append(entities['Phantom'])
		
		dielectric_settings2 = sim.AddMaterialSettings(DielectricList)
		dielectric_settings2.Name = 'Dielectric'
		dielectric_settings2.MaterialType = options.Dielectric
		dielectric_settings2.ElectricProps.Conductivity = 0.4 # S/m
		dielectric_settings2.ElectricProps.RelativePermittivity = 30
		
		manual_grid_settings = sim.AddManualGridSettings(DielectricList)
		manual_grid_settings.MaxStep = (8, )*3 # model units
		manual_grid_settings.Resolution = (3.5, )*3
		
		sim.Add(default_voxel, entities['Phantom'])
	
	
	# Sensors
	local_field_sensor = sim.AddFieldSensorSettings(entities['Huygens SensBox'])
	local_field_sensor.ExtractedFrequencies = sim_freq
	
	# Materials
	pec_settings = sim.AddMaterialSettings(PECList)
	pec_settings.Name = 'PEC' 
	options = pec_settings.MaterialType.enum
	pec_settings.MaterialType = options.PEC

	# Insulation
	dielectric_settings = sim.AddMaterialSettings(InsulationList)
	dielectric_settings.Name = 'Dielectric'
	dielectric_settings.MaterialType = options.Dielectric
	dielectric_settings.ElectricProps.Conductivity = 5e-12 # S/m
	dielectric_settings.ElectricProps.RelativePermittivity = 2.3
	
	# Boundary Conditions
	global_boundary_settings = fdtd.BoundarySettings()
	global_boundary_settings.BoundaryType = global_boundary_settings.BoundaryType.enum.ABC
	global_boundary_settings.PmlStrength = global_boundary_settings.PmlStrength.enum.Medium

	sim.GlobalGridSettings.PaddingMode = sim.GlobalGridSettings.PaddingMode.enum.Manual
	sim.GlobalGridSettings.BottomPadding = sim.GlobalGridSettings.TopPadding = (200,)*3

	
	#Sources
	huyg_source_settings = sim.AddHuygensSourceSettings(entities['Huygens Box'], BigS[ElemNo-1].AllComponents['Main SensBox'])
	options = huyg_source_settings.ExcitationType.enum
	huyg_source_settings.ExcitationType = options.Gaussian
	huyg_source_settings.CenterFrequency = sim_freq, units.Hz
	huyg_source_settings.Bandwidth = sim_bw, units.Hz
	
	# Grid
	manual_grid_settings = sim.AddManualGridSettings(entities[wire_descriptor+'WireInsulation'])
	manual_grid_settings.MaxStep = (3, 3, 5) # model units
	manual_grid_settings.Resolution = (0.8, )*3
	
	manual_grid_settings = sim.AddManualGridSettings(entities[wire_descriptor+'WireCore'])
	manual_grid_settings.MaxStep = (3, 3, 5) # model units
	manual_grid_settings.Resolution = (1, )*3
	manual_grid_settings.Priority = 100
	
	manual_grid_settings = sim.AddManualGridSettings(entities['Huygens Box'])
	manual_grid_settings.MaxStep = (15, )*3 # model units
	manual_grid_settings.Resolution = (3.5, )*3
	
	manual_grid_settings = sim.AddManualGridSettings(entities['Huygens SensBox'])
	manual_grid_settings.MaxStep = (15,)*3 # model units
	manual_grid_settings.Resolution = (3.5, )*3
	

	
	# Voxels
	default_voxel = next(s for s in sim.AllSettings if isinstance(s, fdtd.AutomaticVoxelerSettings) )
	# sim.Add(default_voxel, entities[wire_descriptor+'WireInsulation'])

	sim.Add(default_voxel, entities['Huygens Box'])
	sim.Add(default_voxel, entities['Huygens SensBox'])
	
	wCore_voxel = sim.AddManualVoxelerSettings(entities[wire_descriptor+'WireCore'])
	wCore_voxel.Priority = 100
	wCore_voxel.UseConstraints = True
	wCore_voxel.EnclosedBackgroundProps.Policy = wCore_voxel.EnclosedBackgroundProps.Policy.enum.DetectOnly
	wCore_voxel.EnclosedBackgroundProps.WarningLevel = wCore_voxel.EnclosedBackgroundProps.WarningLevel.enum.Warning
	wCore_voxel.UnconnectedEdgesProps.Policy = wCore_voxel.UnconnectedEdgesProps.Policy.enum.Repair
	wCore_voxel.UnconnectedEdgesProps.WarningLevel = wCore_voxel.UnconnectedEdgesProps.WarningLevel.enum.Warning
	wCore_voxel.UnconnectedEdgesProps.SearchRadius = 5
	
	wIns_voxel = sim.AddManualVoxelerSettings(entities[wire_descriptor+'WireInsulation'])
	wIns_voxel.Priority = 50
	wIns_voxel.UseConstraints = True
	wIns_voxel.EnclosedBackgroundProps.Policy = wIns_voxel.EnclosedBackgroundProps.Policy.enum.DetectOnly
	wIns_voxel.EnclosedBackgroundProps.WarningLevel = wIns_voxel.EnclosedBackgroundProps.WarningLevel.enum.Warning
	wIns_voxel.UnconnectedEdgesProps.Policy = wIns_voxel.UnconnectedEdgesProps.Policy.enum.Repair
	wIns_voxel.UnconnectedEdgesProps.WarningLevel = wIns_voxel.UnconnectedEdgesProps.WarningLevel.enum.Warning
	wIns_voxel.UnconnectedEdgesProps.SearchRadius = 5
	
	document.AllSimulations.Add(sim)
	sim.UpdateGrid()
	
	sim.SolverSettings.Kernel = sim.SolverSettings.Kernel.enum.AXware
	
	return sim
	
def BaseSettings(sim_type, dielectric, ElemNo, rfc_descriptor, n_periods):
	sim = fdtd.Simulation()
	sim.Name = sim_type+' '+rfc_descriptor+' Channel '+str(ElemNo)+' '+dielectric
	sim.SetupSettings.SimulationTime = n_periods, units.Periods
	# options = sim.SetupSettings.GlobalAutoTermination.enum
	# sim.SetupSettings.GlobalAutoTermination = options.GlobalAutoTerminationUserDefined
	#sim.SetupSettings.ConvergenceLevel = -50
	# Import all model elements
	

	# entities = model.AllEntities()
	
	# PECList = []
	# InsulationList = []
	# GridList = [];
	# GridList.append(entities[wire_descriptor+'WireCore']);
	# GridList.append(entities[wire_descriptor+'WireInsulation'])
	# DielectricList = []
	# PECList.append(entities[wire_descriptor+'WireCore'])
	# InsulationList.append(entities[wire_descriptor+'WireInsulation'])
	return sim
	
def SetSensors(sim, SensorList, PortList, LE1List, LE2List, LE3List, LE4List, CurrentSensorList, sim_freq):	
	# Sensors
	local_field_sensor = sim.AddFieldSensorSettings(SensorList)
	local_field_sensor.ExtractedFrequencies = sim_freq
	
	edgesensor_settings = sim.AddEdgeSensorSettings(PortList)
	options = edgesensor_settings.AutoTermination.enum
	edgesensor_settings.AutoTermination = options.AutoTerminationUseGlobal

	edgesensor_settings2 = sim.AddEdgeSensorSettings(LE1List+LE2List+LE3List+LE4List)
	options = edgesensor_settings2.AutoTermination.enum
	edgesensor_settings2.AutoTermination = options.AutoTerminationUseGlobal
	
	current_sensor_settings = sim.AddCurrentSensorSettings(CurrentSensorList)
	current_sensor_settings.IsInterpolated = True
	
def SetMaterials(sim, PECList, DielectricList, InsulationList):
	# Materials
	pec_settings = sim.AddMaterialSettings(PECList)
	pec_settings.Name = 'PEC' 
	options = pec_settings.MaterialType.enum
	pec_settings.MaterialType = options.PEC
	
	dielectric_settings2 = sim.AddMaterialSettings(DielectricList)
	dielectric_settings2.Name = 'Dielectric'
	dielectric_settings2.MaterialType = options.Dielectric
	dielectric_settings2.ElectricProps.Conductivity = 0.4 # S/m
	dielectric_settings2.ElectricProps.RelativePermittivity = 60

	# Insulation
	dielectric_settings = sim.AddMaterialSettings(InsulationList)
	dielectric_settings.Name = 'Dielectric'
	dielectric_settings.MaterialType = options.Dielectric
	dielectric_settings.ElectricProps.Conductivity = 5e-12 # S/m
	dielectric_settings.ElectricProps.RelativePermittivity = 2.3
	
def SetBoundaries(sim, padding):
	# Boundary Conditions
	global_boundary_settings = fdtd.BoundarySettings()
	global_boundary_settings.BoundaryType = global_boundary_settings.BoundaryType.enum.ABC
	global_boundary_settings.PmlStrength = global_boundary_settings.PmlStrength.enum.Medium

	sim.GlobalGridSettings.PaddingMode = sim.GlobalGridSettings.PaddingMode.enum.Manual
	sim.GlobalGridSettings.BottomPadding = sim.GlobalGridSettings.TopPadding = (padding,)*3
	
def SetSources(sim, PortList, sim_freq, sim_bw, sim_refl):
	edge_port_settings = sim.AddEdgeSourceSettings(PortList)
	edge_port_settings.Name = 'Sources'
	options = edge_port_settings.ExcitationType.enum
	edge_port_settings.ExcitationType = options.Gaussian
	edge_port_settings.CenterFrequency = sim_freq, units.Hz
	edge_port_settings.Bandwidth = sim_bw, units.Hz
	edge_port_settings.ReferenceLoad = sim_refl, units.Ohms
	
def SetHuygensSources(sim, ElemNo, dielectric, sim_freq, h_box): # NEEDS TO BE SET UP STILL
	link__overall_field = document.AllSimulations[ElemNo-1].AllComponents["Main SensBox "+dielectric]

	# Adding a new HuygensSourceSettings
	huygens_source_settings = sim.AddHuygensSourceSettings([h_box[0], link__overall_field])
	huygens_source_settings.CenterFrequency = sim_freq, units.Hz
	
def SetLumpedElements(sim, LE1List, LE2List, LE3List, LE4List, R, C1, C2, C3, C4):
	LE_P_settings1 = sim.AddLumpedElementSettings(LE1List)
	LE_P_settings1.Type = LE_P_settings1.Type.enum.ResistorParallelCapacitor
	LE_P_settings1.Resistance = R
	LE_P_settings1.Capacitance = C1
	
	LE_P_settings2 = sim.AddLumpedElementSettings(LE2List)
	LE_P_settings2.Type = LE_P_settings2.Type.enum.ResistorParallelCapacitor
	LE_P_settings2.Resistance = R
	LE_P_settings2.Capacitance = C2

	LE_P_settings3 = sim.AddLumpedElementSettings(LE3List)
	LE_P_settings3.Type = LE_P_settings3.Type.enum.ResistorParallelCapacitor
	LE_P_settings3.Resistance = R
	LE_P_settings3.Capacitance = C3
	
	LE_P_settings4 = sim.AddLumpedElementSettings(LE4List)
	LE_P_settings4.Type = LE_P_settings4.Type.enum.ResistorParallelCapacitor
	LE_P_settings4.Resistance = R
	LE_P_settings4.Capacitance = C4
	
def SetGridVoxels(sim, CoreList, InsulationList, CoilList, AutoList, ShieldList, PortList, LEList, CurrentSensorList, dielectric):
	
	# Grid
	# auto_grid_settings = sim.AddAutomaticGridSettings(AutoList+PortList)
	if dielectric == 'Phantom':
		reso = 0.6
	if dielectric == 'Duke':
		reso = 3
		
	manual_grid_settings = sim.AddManualGridSettings(CoreList)
	manual_grid_settings.Name = "WireCore"
	manual_grid_settings.SubGridMode = manual_grid_settings.SubGridMode.enum.Adaptive
	manual_grid_settings.AdaptiveSubGridPadding = 1
	manual_grid_settings.MaxStep = numpy.array([3.0, 3.0, 5.0]), units.MilliMeters
	manual_grid_settings.Resolution = numpy.array([reso, reso, reso]), units.MilliMeters
	manual_grid_settings.Priority = 100.0

	manual_grid_settings = sim.AddManualGridSettings(InsulationList)
	manual_grid_settings.Name = "WireInsulation"
	manual_grid_settings.SubGridMode = manual_grid_settings.SubGridMode.enum.Adaptive
	manual_grid_settings.AdaptiveSubGridPadding = 1
	manual_grid_settings.MaxStep = numpy.array([3.0, 3.0, 5.0]), units.MilliMeters
	manual_grid_settings.Resolution = numpy.array([reso, reso, reso]), units.MilliMeters

	manual_grid_settings = sim.AddManualGridSettings(CoilList+LEList+PortList)
	manual_grid_settings.Name = "Coil"
	manual_grid_settings.MaxStep = numpy.array([10.0, 10.0, 10.0]), units.MilliMeters
	manual_grid_settings.Resolution = numpy.array([1.5, 1.5, 1.5]), units.MilliMeters
	
	manual_grid_settings = sim.AddManualGridSettings(ShieldList+AutoList)
	manual_grid_settings.Name = "Shield"
	manual_grid_settings.MaxStep = numpy.array([15.0, 15.0, 15.0]), units.MilliMeters
	manual_grid_settings.Resolution = numpy.array([3.5, 3.5, 3.5]), units.MilliMeters
	
	# manual_grid_settings = sim.AddManualGridSettings(DielectricList)
	# manual_grid_settings.MaxStep = (15, )*3 # model units
	# manual_grid_settings.Resolution = (3.5, )*3
	
	
	# Voxels
	automatic_voxeler_settings = [x for x in sim.AllSettings if isinstance(x, fdtd.AutomaticVoxelerSettings) and x.Name == "Automatic Voxeler Settings"][0]
	components = CoilList + AutoList + ShieldList + PortList + LEList + CurrentSensorList
	sim.Add(automatic_voxeler_settings, components)
	
	# auto_voxel_settings = sim.AddAutomaticVoxelerSettings(components)
	
	wCore_voxel = sim.AddManualVoxelerSettings(CoreList)
	wCore_voxel.Priority = 100
	wCore_voxel.UseConstraints = True
	wCore_voxel.EnclosedBackgroundProps.Policy = wCore_voxel.EnclosedBackgroundProps.Policy.enum.DetectOnly
	wCore_voxel.EnclosedBackgroundProps.WarningLevel = wCore_voxel.EnclosedBackgroundProps.WarningLevel.enum.Warning
	wCore_voxel.UnconnectedEdgesProps.Policy = wCore_voxel.UnconnectedEdgesProps.Policy.enum.Repair
	wCore_voxel.UnconnectedEdgesProps.WarningLevel = wCore_voxel.UnconnectedEdgesProps.WarningLevel.enum.Warning
	wCore_voxel.UnconnectedEdgesProps.SearchRadius = 5
	
	wIns_voxel = sim.AddManualVoxelerSettings(InsulationList)
	wIns_voxel.Priority = 50
	wIns_voxel.UseConstraints = True
	wIns_voxel.EnclosedBackgroundProps.Policy = wIns_voxel.EnclosedBackgroundProps.Policy.enum.DetectOnly
	wIns_voxel.EnclosedBackgroundProps.WarningLevel = wIns_voxel.EnclosedBackgroundProps.WarningLevel.enum.Warning
	wIns_voxel.UnconnectedEdgesProps.Policy = wIns_voxel.UnconnectedEdgesProps.Policy.enum.Repair
	wIns_voxel.UnconnectedEdgesProps.WarningLevel = wIns_voxel.UnconnectedEdgesProps.WarningLevel.enum.Warning
	wIns_voxel.UnconnectedEdgesProps.SearchRadius = 5	
	
	
# def SetPhantom():	
	# Phantom
		# DielectricList = []
		# DielectricList.append(entities['Phantom'])
		

		
		# sim.Add(default_voxel, entities['Phantom'])
		
# def CreateSubGridSim:
	# ElemNo = hsim_data[0]
	# p_logic = hsim_data[1]
	# n_periods = hsim_data[2]
	# rfc_descriptor = hsim_data[3]
	# sim_freq = hsim_data[4]
	# sim_bw = hsim_data[5]
	# wire_descriptor = hsim_data[6]
	# BigS = hsim_data[7]
	
def SetHuygensGridVoxels(sim, CoreList, InsulationList, DielectricList, CurrentSensorList, HuygensList, dielectric):
	
	# Grid
	# auto_grid_settings = sim.AddAutomaticGridSettings(AutoList+PortList)
	if dielectric == 'Phantom':
		reso = 0.1
		step = [0.2, 0.2, 5]
	if dielectric == 'Duke':
		reso = 0.9
		step = [2.5, 2.5, 2.5]
		
	manual_grid_settings = sim.AddManualGridSettings(CoreList)
	manual_grid_settings.Name = "WireCore"
	manual_grid_settings.MaxStep = numpy.array(step), units.MilliMeters
	manual_grid_settings.Resolution = numpy.array([reso, reso, reso]), units.MilliMeters
	manual_grid_settings.Priority = 100.0

	manual_grid_settings = sim.AddManualGridSettings(InsulationList)
	manual_grid_settings.Name = "WireInsulation"
	manual_grid_settings.MaxStep = numpy.array(step), units.MilliMeters
	manual_grid_settings.Resolution = numpy.array([reso, reso, reso]), units.MilliMeters
	
	manual_grid_settings = sim.AddManualGridSettings(DielectricList)
	manual_grid_settings.MaxStep = (15, )*3 # model units
	manual_grid_settings.Resolution = (3.5, )*3
	
	
	# Voxels
	automatic_voxeler_settings = [x for x in sim.AllSettings if isinstance(x, fdtd.AutomaticVoxelerSettings) and x.Name == "Automatic Voxeler Settings"][0]
	components = DielectricList + CurrentSensorList + HuygensList
	sim.Add(automatic_voxeler_settings, components)
	
	# auto_voxel_settings = sim.AddAutomaticVoxelerSettings(components)
	
	wCore_voxel = sim.AddManualVoxelerSettings(CoreList)
	wCore_voxel.Priority = 100
	wCore_voxel.UseConstraints = True
	wCore_voxel.EnclosedBackgroundProps.Policy = wCore_voxel.EnclosedBackgroundProps.Policy.enum.DetectOnly
	wCore_voxel.EnclosedBackgroundProps.WarningLevel = wCore_voxel.EnclosedBackgroundProps.WarningLevel.enum.Warning
	wCore_voxel.UnconnectedEdgesProps.Policy = wCore_voxel.UnconnectedEdgesProps.Policy.enum.Repair
	wCore_voxel.UnconnectedEdgesProps.WarningLevel = wCore_voxel.UnconnectedEdgesProps.WarningLevel.enum.Warning
	wCore_voxel.UnconnectedEdgesProps.SearchRadius = 5
	
	wIns_voxel = sim.AddManualVoxelerSettings(InsulationList)
	wIns_voxel.Priority = 50
	wIns_voxel.UseConstraints = True
	wIns_voxel.EnclosedBackgroundProps.Policy = wIns_voxel.EnclosedBackgroundProps.Policy.enum.DetectOnly
	wIns_voxel.EnclosedBackgroundProps.WarningLevel = wIns_voxel.EnclosedBackgroundProps.WarningLevel.enum.Warning
	wIns_voxel.UnconnectedEdgesProps.Policy = wIns_voxel.UnconnectedEdgesProps.Policy.enum.Repair
	wIns_voxel.UnconnectedEdgesProps.WarningLevel = wIns_voxel.UnconnectedEdgesProps.WarningLevel.enum.Warning
	wIns_voxel.UnconnectedEdgesProps.SearchRadius = 5